
function main() {

(function () {
   'use strict';
   
  	$('a.page-scroll').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
          var target = $(this.hash);
          target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
          if (target.length) {
            $('html,body').animate({
              scrollTop: target.offset().top - 40
            }, 900);
            return false;
          }
        }
      });

    // console.log('doc: '+$(document).height());
    // console.log('win: '+$(window).height());

    // Show Menu on Book
    $(window).bind('scroll', function() {
        // console.log($(window).scrollTop());
        var navHeight = $(window).height() - ($(window).height() - 50);
        var navHeight2 = $(window).height() - ($(window).height() * 0.80);
        var docbottom = $(window).scrollTop() + ($(window).height() + 325);
        // var bottom 

        if ($(window).scrollTop() > navHeight && $(window).scrollTop() < navHeight2) {
            $('.navbar-default').addClass('on');
                $('#demo').stop().fadeTo(500,0);
        } else if ($(window).scrollTop() >= navHeight2) {
            $('.navbar-default').addClass('on');
                $('#demo').stop().fadeTo(500,1);
                $('#demo').removeClass('hide');
        } else {
            $('.navbar-default').removeClass('on');
                $('#demo').stop().fadeTo(500,0);
        }

        if (docbottom >= $(document).height() || $(window).scrollTop() < navHeight2) {
                $('#demo').stop().fadeTo(500,0);
        }
    });

    $('body').scrollspy({ 
        target: '.navbar-default',
        offset: 80
    });

	// Hide nav on click
  $(".navbar-nav li a").click(function (event) {
    // check if window is small enough so dropdown is created
    var toggle = $(".navbar-toggle").is(":visible");
    if (toggle) {
      $(".navbar-collapse").collapse('hide');
    }
  });
	
  	// Portfolio isotope filter
    $(window).load(function() {
        var $container = $('.portfolio-items');
        $container.isotope({
            filter: '*',
            animationOptions: {
                duration: 750,
                easing: 'linear',
                queue: false
            }
        });
        $('.cat a').click(function() {
            $('.cat .active').removeClass('active');
            $(this).addClass('active');
            var selector = $(this).attr('data-filter');
            $container.isotope({
                filter: selector,
                animationOptions: {
                    duration: 750,
                    easing: 'linear',
                    queue: false
                }
            });
            return false;
        });

    });
	
    // Nivo Lightbox 
    $('.portfolio-item a').nivoLightbox({
            effect: 'slideDown',  
            keyboardNav: true,                            
        });

}());


}
main();