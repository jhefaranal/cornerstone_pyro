
function main() {

(function () {
	'use strict';
	
	$('#quote-carousel').carousel({
		pause: true, interval: 10000,
	});

	$('[data-toggle="tooltip"]').tooltip();

	var options = {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		direction: 'horizontal',
		effect: 'coverflow',
		coverflow: {
			rotate: 5,
			stretch: 0,
			depth:400,
			slideShadows : false
		},
		grabCursor: true,
		centeredSlides: true,
		speed: 800,
		autoplay: 3000,
		loop: true,
		slidesPerView: "auto",
		initialSlide: 0,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
	}

	$('.text-logo').addClass('invisible').viewportChecker({
		classToAdd: 'visible animate fadeInDown',
		offset: 100
	});

	$('.overflow-img.right').addClass('invisible').viewportChecker({
		classToAdd: 'visible animated slideInRight',
		offset: 250
	});

	$('.overflow-img.left').addClass('invisible').viewportChecker({
		classToAdd: 'visible animated slideInLeft',
		offset: 250
	});

	$('.overflow-img.left').hover(function(){
		// console.log('hello');
		$(this).find('.top').removeClass('top').addClass('bottom2');
		$(this).find('.bottom').removeClass('bottom').addClass('top').css({'z-index':'0', 'transition':'ease-in-out .50s'});
		$(this).find('.bottom2').removeClass('bottom2').addClass('bottom').css({'z-index':'99', 'transition':'ease-in-out .50s'});
	}, function(){
		// console.log('bye');
		$(this).find('.top').removeClass('top').addClass('bottom2');
		$(this).find('.bottom').removeClass('bottom').addClass('top').css({'z-index':'0', 'transition':'ease-in-out .50s'});
		$(this).find('.bottom2').removeClass('bottom2').addClass('bottom').css({'z-index':'99', 'transition':'ease-in-out .50s'});
	});

	//initialize swiper when document ready  
	var mySwiper = new Swiper('.swiper-container', options);

	$(document).on('click','a.page-scroll', function() {
		if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
		  var target = $(this.hash);
		  console.log(target);
		  target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
		  console.log(target);
		  if (target.length) {
			$('html,body').animate({
			  scrollTop: target.offset().top - 40
			}, 900);
			return false;
		  }
		}
	  });

	// Show Menu on Book
	$(window).bind('scroll', function() {
		// console.log($(window).scrollTop());
		var navHeight = $(window).height() - ($(window).height() - 50);
		var navHeight2 = $(window).height() - ($(window).height() - 625);
		var navHeight2_2 = $(window).height() - ($(window).height() - 700);
		var navHeight3 = navHeight2 + ($(window).height() * 0.95);
		var navHeight4 = navHeight3 + (($(window).height() * 0.65));
		// var navHeight5 = navHeight4 + (($(window).height() * 0.95));
		var navHeight5 = navHeight4 + (($(window).height() * 0.45));
		var navHeight6 = navHeight5 + (($(window).height() * 0.45));
		var navHeight7 = navHeight6 + (($(window).height() * 0.45));
		var docbottom = $(window).scrollTop() + ($(window).height() + 50);

		// var clients_url = base_url + 'clients';
		// var features_url = base_url + 'features';
		var clients_url = base_url + 'clients';
		var features_url = base_url + 'features';
		if (window.location.href == clients_url) {
			// for clients page
			if ($(window).scrollTop() > navHeight && $(window).scrollTop() < navHeight2_2) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
			} else {
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('#demo').stop().fadeTo(500,0);
			}
		} else if (window.location.href == features_url) {
			// for features page
			navHeight = $(window).height() - ($(window).height() - 50);
			navHeight2 = $(window).height() - ($(window).height() - 620);
			navHeight2_2 = $(window).height() - ($(window).height() - 700);
			navHeight3 = (navHeight2 - 20) + ($(window).height() * 0.95);
			navHeight4 = navHeight3 + (($(window).height() * 0.95));
			navHeight5 = navHeight4 + (($(window).height() * 0.95));
			navHeight6 = navHeight5 + (($(window).height() * 0.95));
			navHeight7 = navHeight6 + (($(window).height() * 0.95));
			var navHeight8 = navHeight7 + (($(window).height() * 0.95));
			var navHeight9 = navHeight8 + (($(window).height() * 0.95));
			var navHeight10 = navHeight9 + (($(window).height() * 0.95));
			var navHeight11 = navHeight10 + (($(window).height() * 0.95));
			if ($(window).scrollTop() > navHeight && $(window).scrollTop() < navHeight2) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
				$('.navbar-default').removeClass('on-3');
			} else if ($(window).scrollTop() >= navHeight2 && $(window).scrollTop() <= navHeight2_2) {
				$('.navbar-default').addClass('feature-header-1');
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			} else if ($(window).scrollTop() > navHeight2_2 && $(window).scrollTop() < navHeight3) {
				$('.navbar-default').addClass('feature-header-1_5');
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			} else if ($(window).scrollTop() > navHeight3 && $(window).scrollTop() < navHeight4) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			} else if ($(window).scrollTop() >= navHeight4 && $(window).scrollTop() < navHeight5) {
				$('.navbar-default').addClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
				$('.navbar-default').removeClass('on');
			} else if ($(window).scrollTop() > navHeight5 && $(window).scrollTop() < navHeight6) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			} else if ($(window).scrollTop() >= navHeight6 && $(window).scrollTop() < navHeight7) {
				$('.navbar-default').addClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
				$('.navbar-default').removeClass('on');
			} else if ($(window).scrollTop() > navHeight7 && $(window).scrollTop() < navHeight8) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			} else if ($(window).scrollTop() >= navHeight8 && $(window).scrollTop() < navHeight9) {
				$('.navbar-default').addClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-9');
				$('.navbar-default').removeClass('on');
			} else if ($(window).scrollTop() > navHeight9 && $(window).scrollTop() < navHeight10) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			} else if ($(window).scrollTop() > navHeight10 && $(window).scrollTop() < navHeight11) {
				$('.navbar-default').addClass('feature-header-9');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('on');
			} else {
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('feature-header-1');
				$('.navbar-default').removeClass('feature-header-1_5');
				$('.navbar-default').removeClass('feature-header-3');
				$('.navbar-default').removeClass('feature-header-5');
				$('.navbar-default').removeClass('feature-header-7');
				$('.navbar-default').removeClass('feature-header-9');
			}
		} else {
			if ($(window).scrollTop() > navHeight && $(window).scrollTop() < navHeight2) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('#demo').stop().fadeTo(500,1);
				$('#demo').removeClass('hide');
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() >= navHeight2 && $(window).scrollTop() <= navHeight2_2) {
				$('.navbar-default').addClass('on-2');
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('on-3');
				// $('#demo').stop().fadeTo(500,1);
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() > navHeight2_2 && $(window).scrollTop() < navHeight3) {
				$('.navbar-default').addClass('on-3');
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('on-2');
				// $('#demo').stop().fadeTo(500,1);
				// $('#demo').removeClass('hide');
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() >= navHeight3 && $(window).scrollTop() < navHeight4) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('#demo').stop().fadeTo(500,1);
				$('#demo').removeClass('hide');
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() >= navHeight4 && $(window).scrollTop() < navHeight5) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('#demo').stop().fadeTo(500,1);
				$('#demo').removeClass('hide');
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() >= navHeight5 && $(window).scrollTop() < navHeight6) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('#demo').stop().fadeTo(500,1);
				$('#demo').removeClass('hide');
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() >= navHeight6) {
				$('.navbar-default').addClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('#demo').stop().fadeTo(500,0);
				$('.scroll-up').removeClass('on-2');
			} else {
				$('.navbar-default').removeClass('on');
				$('.navbar-default').removeClass('on-2');
				$('.navbar-default').removeClass('on-3');
				$('.scroll-up').addClass('on-2');
			}
		}

		if (window.location.href == features_url) {
			// for features page
			navHeight = $(window).height() - ($(window).height() - 50);
			navHeight2 = $(window).height() - ($(window).height() - 700);
			navHeight3 = navHeight2 + ($(window).height() * 0.95);
			navHeight4 = navHeight3 + (($(window).height() * 0.95));
			navHeight5 = navHeight4 + (($(window).height() * 0.95));
			navHeight6 = navHeight5 + (($(window).height() * 0.95));
			navHeight7 = navHeight6 + (($(window).height() * 0.95));
			var navHeight8 = navHeight7 + (($(window).height() * 0.95));
			var navHeight9 = navHeight8 + (($(window).height() * 0.95));
			var navHeight10 = navHeight9 + (($(window).height() * 0.95));
			if ($(window).scrollTop() >= 0 && $(window).scrollTop() < navHeight2) {
				$('#demo').stop().fadeTo(500,1);
				$('#demo').removeClass('hide');
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() > navHeight2 && $(window).scrollTop() < navHeight3) {
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() > navHeight3 && $(window).scrollTop() < navHeight4) {
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() > navHeight4 && $(window).scrollTop() < navHeight5) {
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() > navHeight5 && $(window).scrollTop() < navHeight6) {
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() > navHeight6 && $(window).scrollTop() < navHeight7) {
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() > navHeight7 && $(window).scrollTop() < navHeight8) {
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() > navHeight8 && $(window).scrollTop() < navHeight9) {
				$('.scroll-up').removeClass('on-2');
			} else if ($(window).scrollTop() > navHeight9 && $(window).scrollTop() < navHeight10) {
				$('.scroll-up').addClass('on-2');
			} else if ($(window).scrollTop() > navHeight10 && $(window).scrollTop() < navHeight11) {
				$('.scroll-up').addClass('on-2');
			} else {
				$('.scroll-up').removeClass('on-2');
			}
		}

		if ($(window).scrollTop() < navHeight) {
			$('#demo').stop().fadeTo(500,0);
			$('#demo').css({'display':'none'});
			$('#demo').addClass('hide');
		}
	});

	$('body').scrollspy({ 
		target: '.navbar-default',
		offset: 80
	});

	// Hide nav on click
	$(".navbar-nav li a").click(function (event) {
		// check if window is small enough so dropdown is created
		var toggle = $(".navbar-toggle").is(":visible");
		if (toggle) {
			$(".navbar-collapse").collapse('hide');
		}
	});
	
	// Portfolio isotope filter
	$(window).load(function() {
		var $container = $('.portfolio-items');
		$container.isotope({
			filter: '*',
			animationOptions: {
				duration: 750,
				easing: 'linear',
				queue: false
			}
		});
		$('.cat a').click(function() {
			$('.cat .active').removeClass('active');
			$(this).addClass('active');
			var selector = $(this).attr('data-filter');
			$container.isotope({
				filter: selector,
				animationOptions: {
					duration: 750,
					easing: 'linear',
					queue: false
				}
			});
			return false;
		});

	});
	
	// Nivo Lightbox 
	$('.portfolio-item a').nivoLightbox({
		effect: 'slideDown',  
		keyboardNav: true,
	});

}());


}
main();