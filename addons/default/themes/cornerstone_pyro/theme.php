<?php defined('BASEPATH') OR exit('No direct script access allowed');
 
class Theme_Cornerstone_Pyro extends Theme
{
    public $name = 'Cornerstone Pyro';
    public $author = 'Forward Solutions';
    public $author_website = 'http://forward.ph';
    public $website = 'http://cornerstone.ph';
    public $description = '';
    public $version = '1.0';
}
 
 /* End of file theme.php */