$(function(event) {
	$("#login_form").find('form').validate({
		rules: {
			email: {
				required: true,
			},
			password: {
				required: true,
			}
		},
		submitHandler: function( form, event ) {
			event.preventDefault();
			window.location = base_url;
		}
	});
});