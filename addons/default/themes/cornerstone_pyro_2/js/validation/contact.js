$(function(event) {
	$("#contact_form").find('form').validate({
		rules: {
			full_name: {
				required: true,
			},
			company: {
				required: true,
			},
			email: {
				required: true,
				email: true
			},
			contact_number: {
				required: true,
			}
		},
		submitHandler: function( form, event ) {
			event.preventDefault();
			window.location = base_url+'contact-us-thank-you';
		}
	});
});