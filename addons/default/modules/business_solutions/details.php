<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Business Solutions module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Business Solutions
 */
class Module_Business_solutions extends Module
{
	public $version = '2.0.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Business Solutions',
				'ar' => 'المدوّنة',
				'br' => 'Business Solutions',
				'pt' => 'Business Solutions',
				'el' => 'Ιστολόγιο',
                            'fa' => 'بلاگ',
				'he' => 'בלוג',
				'id' => 'Business Solutions',
				'lt' => 'Business Solutionsas',
				'pl' => 'Business Solutions',
				'ru' => 'Блог',
				'tw' => '文章',
				'cn' => '文章',
				'hu' => 'Business Solutions',
				'fi' => 'Business Solutionsi',
				'th' => 'บล็อก',
				'se' => 'Business Solutionsg',
			),
			'description' => array(
				'en' => 'Post Business Solutions entries.',
				'ar' => 'أنشر المقالات على مدوّنتك.',
				'br' => 'Escrever publicações de business_solutions',
				'pt' => 'Escrever e editar publicações no business_solutions',
				'cs' => 'Publikujte nové články a příspěvky na business_solutions.', #update translation
				'da' => 'Skriv business_solutionsindlæg',
				'de' => 'Veröffentliche neue Artikel und Business Solutions-Einträge', #update translation
				'sl' => 'Objavite business_solutions prispevke',
				'fi' => 'Kirjoita business_solutionsi artikkeleita.',
				'el' => 'Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.',
				'es' => 'Escribe entradas para los artículos y business_solutions (web log).', #update translation
                                'fa' => 'مقالات منتشر شده در بلاگ',
				'fr' => 'Poster des articles d\'actualités.',
				'he' => 'ניהול בלוג',
				'id' => 'Post entri business_solutions',
				'it' => 'Pubblica notizie e post per il business_solutions.', #update translation
				'lt' => 'Rašykite naujienas bei business_solutions\'o įrašus.',
				'nl' => 'Post nieuwsartikelen en business_solutionss op uw site.',
				'pl' => 'Dodawaj nowe wpisy na business_solutionsu',
				'ru' => 'Управление записями блога.',
				'tw' => '發表新聞訊息、部落格等文章。',
				'cn' => '发表新闻讯息、部落格等文章。',
				'th' => 'โพสต์รายการบล็อก',
				'hu' => 'Business Solutions bejegyzések létrehozása.',
				'se' => 'Inlägg i business_solutionsgen.',
			),
			'frontend' => true,
			'backend' => true,
			'skip_xss' => true,
			'menu' => 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
				'posts' => array(
					'name' => 'business_solutions:posts_title',
					'uri' => 'admin/business_solutions',
					'shortcuts' => array(
						array(
							'name' => 'business_solutions:create_title',
							'uri' => 'admin/business_solutions/create',
							'class' => 'add',
						),
					),
				),
				'categories' => array(
					'name' => 'cat:list_title',
					'uri' => 'admin/business_solutions/categories',
					'shortcuts' => array(
						array(
							'name' => 'cat:create_title',
							'uri' => 'admin/business_solutions/categories/create',
							'class' => 'add',
						),
					),
				),
			),
		);

		/*if (function_exists('group_has_role'))
		{
			if(group_has_role('business_solutions', 'admin_business_solutions_fields'))
			{
				$info['sections']['fields'] = array(
							'name' 	=> 'global:custom_fields',
							'uri' 	=> 'admin/business_solutions/fields',
								'shortcuts' => array(
									'create' => array(
										'name' 	=> 'streams:add_field',
										'uri' 	=> 'admin/business_solutions/fields/create',
										'class' => 'add'
										)
									)
							);
			}
		}*/

		return $info;
	}

	public function install()
	{
		$this->dbforge->drop_table('business_solutions_categories');

		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('business_solutionss');

		// Just in case.
		$this->dbforge->drop_table('business_solutions');

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'business_solutionss')->delete('data_streams');
		}

		// Create the business_solutions categories table.
		$this->install_tables(array(
			'business_solutions_categories' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true, 'key' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true),
			),
		));

		$this->streams->streams->add_stream(
			'lang:business_solutions:business_solutions_title',
			'business_solutions',
			'business_solutionss',
			null,
			null
		);

		// Add the intro field.
		// This can be later removed by an admin.
		/*$intro_field = array(
			'name'		=> 'lang:business_solutions:intro_label',
			'slug'		=> 'intro',
			'namespace' => 'business_solutionss',
			'type'		=> 'wysiwyg',
			'assign'	=> 'business_solutions',
			'extra'		=> array('editor_type' => 'simple', 'allow_tags' => 'y'),
			'required'	=> true
		);
		$this->streams->fields->add_field($intro_field);*/

		// Ad the rest of the business_solutions fields the normal way.
		$business_solutions_fields = array(
				'title' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false, 'unique' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false),
				'category_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'body' => array('type' => 'TEXT'),
				'parsed' => array('type' => 'TEXT'),
				'keywords' => array('type' => 'VARCHAR', 'constraint' => 32, 'default' => ''),
				'author_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'created_on' => array('type' => 'INT', 'constraint' => 11),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'comments_enabled' => array('type' => 'ENUM', 'constraint' => array('no','1 day','1 week','2 weeks','1 month', '3 months', 'always'), 'default' => '3 months'),
				'status' => array('type' => 'ENUM', 'constraint' => array('draft', 'live'), 'default' => 'draft'),
				'type' => array('type' => 'SET', 'constraint' => array('html', 'markdown', 'wysiwyg-advanced', 'wysiwyg-simple')),
				'preview_hash' => array('type' => 'CHAR', 'constraint' => 32, 'default' => ''),
		);
		return $this->dbforge->add_column('business_solutions', $business_solutions_fields);
	}

	public function uninstall()
	{
		// Load the Streams Core
		/*$this->load->driver('Streams');

		// Remove streams and destruct everything
		$this->streams->utilities->remove_namespace('business_solutionss');
		$this->dbforge->drop_table('business_solutions');

		return true;*/

		return false;
	}

	public function upgrade($old_version)
	{
		return true;
	}
}
