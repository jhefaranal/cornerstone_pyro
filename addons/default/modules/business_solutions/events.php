<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Sample Events Class
*
* @package 		PyroCMS
* @subpackage 	Social Module
* @category 	business_solutionss
* @author 		PyroCMS Dev Team
*/
class Events_Business_solutions
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();

        // Load the search index model
        $this->ci->load->model('search/search_index_m');

		// Post a blog to twitter and whatnot
        Events::register('business_solutions_published', array($this, 'index_business_solutions'));
        Events::register('business_solutions_updated', array($this, 'index_business_solutions'));
        Events::register('business_solutions_deleted', array($this, 'drop_business_solutions'));

    }
    
    public function index_business_solutions($id)
    {
    	$this->ci->load->model('business_solutions/business_solutions_m');

    	$post = $this->ci->business_solutions_m->get($id);

    	// Only index live articles
    	if ($post->status === 'live')
    	{
    		$this->ci->search_index_m->index(
    			'business_solutions', 
    			'business_solutions:post', 
    			'business_solutions:posts', 
    			$id,
    			//'business_solutions/'.date('Y/m/', $business_solutions->created_on).$post->slug,
    			'business_solutionss',
    			$post->title,
    			$post->body, 
    			array(
    				'cp_edit_uri' 	=> 'admin/business_solutions/edit/'.$id,
    				'cp_delete_uri' => 'admin/business_solutions/delete/'.$id,
    				'keywords' 		=> $post->keywords,
    			)
    		);
    	}
    	// Remove draft articles
    	else
    	{
    		$this->ci->search_index_m->drop_index('business_solutions', 'business_solutions:post', $id);
    	}
	}

    public function drop_business_solutions($ids)
    {
    	foreach ($ids as $id)
    	{
			$this->ci->search_index_m->drop_index('business_solutions', 'business_solutions:post', $id);
		}
	}
  
}

/* End of file business_solutionss.php */