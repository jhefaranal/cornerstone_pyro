<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= '將文章上線';
$lang['business_solutions:role_edit_live']	= '編輯上線文章';
$lang['business_solutions:role_delete_live'] 	= '刪除上線文章';