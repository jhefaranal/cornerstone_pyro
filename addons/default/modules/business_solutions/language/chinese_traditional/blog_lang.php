<?php

$lang['business_solutions:post']                 		= '文章';
$lang['business_solutions:posts']                   	= '文章';

// labels
$lang['business_solutions:posted_label'] 				= '已發佈';
$lang['business_solutions:posted_label_alt']			= '發表於';
$lang['business_solutions:written_by_label']			= '撰文者';
$lang['business_solutions:author_unknown']			= '未知';
$lang['business_solutions:keywords_label']			= '關鍵字';
$lang['business_solutions:tagged_label']				= '標籤';
$lang['business_solutions:category_label'] 			= '分類';
$lang['business_solutions:post_label'] 				= '發表';
$lang['business_solutions:date_label'] 				= '日期';
$lang['business_solutions:date_at']					= '於';
$lang['business_solutions:time_label'] 				= '時間';
$lang['business_solutions:status_label'] 				= '狀態';
$lang['business_solutions:draft_label'] 				= '草稿';
$lang['business_solutions:live_label'] 				= '上線';
$lang['business_solutions:content_label'] 			= '內容';
$lang['business_solutions:options_label'] 			= '選項';
$lang['business_solutions:intro_label'] 				= '簡介';
$lang['business_solutions:no_category_select_label'] 	= '-- 無 --';
$lang['business_solutions:new_category_label'] 		= '新增分類';
$lang['business_solutions:subscripe_to_rss_label'] 	= '訂閱 RSS';
$lang['business_solutions:all_posts_label'] 			= '所有文章';
$lang['business_solutions:posts_of_category_suffix'] 	= ' 文章';
$lang['business_solutions:rss_name_suffix'] 			= ' 新聞';
$lang['business_solutions:rss_category_suffix'] 		= ' 新聞';
$lang['business_solutions:author_name_label'] 		= '作者名稱';
$lang['business_solutions:read_more_label'] 			= '閱讀更多&nbsp;&raquo;';
$lang['business_solutions:created_hour']              = '時間 (時)';
$lang['business_solutions:created_minute']            = '時間 (分)';
$lang['business_solutions:comments_enabled_label']    = '允許回應';

// titles
$lang['business_solutions:create_title'] 				= '新增文章';
$lang['business_solutions:edit_title'] 				= '編輯文章 "%s"';
$lang['business_solutions:archive_title'] 			= '檔案櫃';
$lang['business_solutions:posts_title'] 				= '文章';
$lang['business_solutions:rss_posts_title'] 			= '%s 的最新文章';
$lang['business_solutions:business_solutions_title'] 				= '新聞';
$lang['business_solutions:list_title'] 				= '文章列表';

// messages
$lang['business_solutions:disabled_after'] 			= '在 %s 後面 發表評論的功能 已經被關閉。';
$lang['business_solutions:no_posts'] 					= '沒有文章';
$lang['business_solutions:subscripe_to_rss_desc'] 	= '訂閱我們的 RSS 摘要可立即獲得最新的文章，您可以使用慣用的收件軟體，或試試看 <a href="http://reader.google.com.tw">Google 閱讀器</a>。';
$lang['business_solutions:currently_no_posts'] 		= '目前沒有文章';
$lang['business_solutions:post_add_success'] 			= '文章 "%s" 已經新增';
$lang['business_solutions:post_add_error'] 			= '發生了錯誤';
$lang['business_solutions:edit_success'] 				= '這文章 "%s" 更新了。';
$lang['business_solutions:edit_error'] 				= '發生了錯誤';
$lang['business_solutions:publish_success'] 			= '此文章 "%s" 已經被發佈。';
$lang['business_solutions:mass_publish_success'] 		= '這些文章 "%s" 已經被發佈。';
$lang['business_solutions:publish_error'] 			= '沒有文章被發佈。';
$lang['business_solutions:delete_success'] 			= '此文章 "%s" 已經被刪除。';
$lang['business_solutions:mass_delete_success'] 		= '這些文章 "%s" 已經被刪除。';
$lang['business_solutions:delete_error'] 				= '沒有文章被刪除。';
$lang['business_solutions:already_exist_error'] 		= '一則相同網址的文章已經存在。';

$lang['business_solutions:twitter_posted']			= '發佈 "%s" %s';
$lang['business_solutions:twitter_error'] 			= 'Twitter 錯誤';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation
