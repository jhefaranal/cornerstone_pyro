<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Læg artikler online';
$lang['business_solutions:role_edit_live']	= 'Redigér online artikler';
$lang['business_solutions:role_delete_live'] 	= 'Slet online artikler';