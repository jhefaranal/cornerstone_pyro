<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label']                   = 'Opslået';
$lang['business_solutions:posted_label_alt']               = 'Opslået på';
$lang['business_solutions:written_by_label']				= 'Skrevet af';
$lang['business_solutions:author_unknown']				= 'Ukendt';
$lang['business_solutions:keywords_label']				= 'Keywords'; #translate
$lang['business_solutions:tagged_label']					= 'Tagged'; #translate
$lang['business_solutions:category_label']                 = 'Kategori';
$lang['business_solutions:post_label']                     = 'Opslå';
$lang['business_solutions:date_label']                     = 'Dato';
$lang['business_solutions:date_at']                        = 'på';
$lang['business_solutions:time_label']                     = 'Tid';
$lang['business_solutions:status_label']                   = 'Status';
$lang['business_solutions:draft_label']                    = 'Udkast';
$lang['business_solutions:live_label']                     = 'Live';
$lang['business_solutions:content_label']                  = 'Indhold';
$lang['business_solutions:options_label']                  = 'Indstillinger';
$lang['business_solutions:intro_label']                    = 'Introduktion';
$lang['business_solutions:no_category_select_label']       = '-- Ingen --';
$lang['business_solutions:new_category_label']             = 'Tilføj en kategori';
$lang['business_solutions:subscripe_to_rss_label']         = 'Abonnér på RSS';
$lang['business_solutions:all_posts_label']             = 'Alle opslag';
$lang['business_solutions:posts_of_category_suffix']    = ' opslag';
$lang['business_solutions:rss_name_suffix']                = ' Business_solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business_solutions';
$lang['business_solutions:author_name_label']              = 'Forfatter';
$lang['business_solutions:read_more_label']                = 'Læs mere&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Oprettet on time';
$lang['business_solutions:created_minute']                 = 'Oprettet on minut';
$lang['business_solutions:comments_enabled_label']         = 'Kommentarer aktiveret';

// titles
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:create_title']                   = 'Tilføj opslag';
$lang['business_solutions:edit_title']                     = 'Redigér opslag "%s"';
$lang['business_solutions:archive_title']                  = 'Arkivér';
$lang['business_solutions:posts_title']                 = 'Opslag';
$lang['business_solutions:rss_posts_title']             = 'Business_solutions-opslag for %s';
$lang['business_solutions:business_solutions_title']                     = 'Business_solutions';
$lang['business_solutions:list_title']                     = 'Liste opslag';

// messages
$lang['business_solutions:no_posts']                    = 'Der er ingen opslag.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Få opslag med det same ved at abonnere på vores RSS feed. Dette kan du gøre dette via de mest populære e-mail klienter, eller du kan prøve <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']          = 'Der er i øjeblikket ingen opslag';
$lang['business_solutions:post_add_success']            = 'Opslaget "%s" er tilføjet.';
$lang['business_solutions:post_add_error']              = 'Der opstod en fejl.';
$lang['business_solutions:edit_success']                   = 'Opslaget "%s" er opdateret.';
$lang['business_solutions:edit_error']                     = 'Der opstod en fejl.';
$lang['business_solutions:publish_success']                = 'Opslaget "%s" er blevet publiceret.';
$lang['business_solutions:mass_publish_success']           = 'Opslagene "%s" er blevet publiceret.';
$lang['business_solutions:publish_error']                  = 'Ingen opslag er blevet publiceret.';
$lang['business_solutions:delete_success']                 = 'Opslaget "%s" er slettet.';
$lang['business_solutions:mass_delete_success']            = 'Opslagene "%s" er slettet.';
$lang['business_solutions:delete_error']                   = 'Ingen opslag er blevet slettet.';
$lang['business_solutions:already_exist_error']            = 'Et opslag med denne URL findes allerede.';

$lang['business_solutions:twitter_posted']                 = 'Publiceret "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter fejl';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
