<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                          = 'Entrada';
$lang['business_solutions:posts']                         = 'Entradas';

// labels
$lang['business_solutions:posted_label']					= 'Escrito';
$lang['business_solutions:keywords_label']				= 'Palabras clave';
$lang['business_solutions:tagged_label']					= 'Tagged'; #translate
$lang['business_solutions:category_label'] 				= 'Categoría';
$lang['business_solutions:written_by_label']				= 'Escrito por';
$lang['business_solutions:author_unknown']				= 'Desconocido';
$lang['business_solutions:post_label'] 					= 'Entrada';
$lang['business_solutions:date_label'] 					= 'Fecha';
$lang['business_solutions:time_label'] 					= 'Hora';
$lang['business_solutions:status_label'] 					= 'Estado';
$lang['business_solutions:content_label'] 				= 'Contenido';
$lang['business_solutions:options_label'] 				= 'Opciones';
$lang['business_solutions:intro_label'] 					= 'Introducción';
$lang['business_solutions:draft_label'] 					= 'Borrador';
$lang['business_solutions:live_label'] 					= 'En vivo';
$lang['business_solutions:no_category_select_label'] 		= '-- Ninguna --';
$lang['business_solutions:new_category_label'] 			= 'Agregar una categoría';
$lang['business_solutions:subscripe_to_rss_label'] 		= 'Suscribir al RSS';
$lang['business_solutions:all_posts_label'] 				= 'Todos los artículos';
$lang['business_solutions:posts_of_category_suffix']		= ' artículos';
$lang['business_solutions:rss_name_suffix'] 				= ' Noticias';
$lang['business_solutions:rss_category_suffix'] 			= ' Noticias';
$lang['business_solutions:posted_label'] 					= 'Escrito en';
$lang['business_solutions:author_name_label'] 			= 'Nombre del autor';
$lang['business_solutions:read_more_label'] 				= 'Leer más &raquo;';
$lang['business_solutions:created_hour']                 	= 'Hora (Hora)';
$lang['business_solutions:created_minute']				= 'Hora (Minutos)';
$lang['business_solutions:comments_enabled_label']         = 'Comentarios Habilitados';

// titles
$lang['business_solutions:create_title'] 					= 'Crear un artículo';
$lang['business_solutions:edit_title'] 					= 'Editar artículo "%s"';
$lang['business_solutions:archive_title'] 				= 'Archivo';
$lang['business_solutions:posts_title'] 					= 'Artículos';
$lang['business_solutions:rss_posts_title'] 				= 'Artículo Business_solutions de %s';
$lang['business_solutions:business_solutions_title'] 					= 'Noticias';
$lang['business_solutions:list_title'] 					= 'Lista de artículos';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts'] 						= 'No hay artículos.';
$lang['business_solutions:subscripe_to_rss_desc'] 		= 'Recibe nuestros artículos inmediatamente suscribiendote a nuestros feeds RSS. Puedes hacer esto utilizando los más clientes de correo electrónico mas populares o utilizando <a href="http://reader.google.com/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts'] 			= 'No hay artículos hasta el momento.';
$lang['business_solutions:post_add_success'] 				= 'El artículo "%s" fue agregado.';
$lang['business_solutions:post_add_error'] 				= 'Ha ocurrido un error.';
$lang['business_solutions:edit_success'] 					= 'El artículo "%s" fue actualizado.';
$lang['business_solutions:edit_error'] 					= 'Ha ocurrido un error.';
$lang['business_solutions:publish_success'] 				= 'El artículo "%s" ha sido publicado.';
$lang['business_solutions:mass_publish_success'] 			= 'Los artículos "%s" fueron publicados.';
$lang['business_solutions:publish_error'] 				= 'No se han publicado artículos.';
$lang['business_solutions:delete_success'] 				= 'El artículo "%s" ha sido eliminado.';
$lang['business_solutions:mass_delete_success'] 			= 'Los artículos "%s" han sido eliminados.';
$lang['business_solutions:delete_error'] 					= 'No se han eliminado artículos.';
$lang['business_solutions:already_exist_error'] 			= 'Ya existe un artículo con esta URL.';

$lang['business_solutions:twitter_posted']				= 'Escrito "%s" %s';
$lang['business_solutions:twitter_error'] 				= 'Error de Twitter';

// date
$lang['business_solutions:archive_date_format']			= "%B %Y";

/* End of file business_solutions:lang.php */