<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Colocar artículos disponibles';
$lang['business_solutions:role_edit_live']	= 'Editar artículos disponibles';
$lang['business_solutions:role_delete_live'] 	= 'Eliminar artículos disponibles';