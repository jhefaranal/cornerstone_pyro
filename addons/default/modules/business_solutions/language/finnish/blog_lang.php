<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Finnish translation.
 *
 * @author Mikael Kundert
 */

$lang['business_solutions:post']	= 'Artikkeli';
$lang['business_solutions:posts']	= 'Artikkelit';

// labels
$lang['business_solutions:posted_label']				= 'Lähetetty';
$lang['business_solutions:posted_label_alt']			= 'Lähetettiin';
$lang['business_solutions:written_by_label']			= 'Kirjoittanut';
$lang['business_solutions:author_unknown']			= 'Tuntematon';
$lang['business_solutions:keywords_label']			= 'Avainsanat';
$lang['business_solutions:tagged_label']				= 'Merkitty';
$lang['business_solutions:category_label']			= 'Kategoria';
$lang['business_solutions:post_label']				= 'Artikkeli';
$lang['business_solutions:date_label']				= 'Päivä';
$lang['business_solutions:date_at']					= 'at'; #translate (in finnish we use adessives by using suffixes!, see http://www.cs.tut.fi/~jkorpela/finnish-cases.html)
$lang['business_solutions:time_label']				= 'Aika';
$lang['business_solutions:status_label']				= 'Tila';
$lang['business_solutions:draft_label']				= 'Luonnos';
$lang['business_solutions:live_label']				= 'Julkinen';
$lang['business_solutions:content_label']				= 'Sisältö';
$lang['business_solutions:options_label']				= 'Valinnat';
$lang['business_solutions:intro_label']				= 'Alkuteksti';
$lang['business_solutions:no_category_select_label']	= '-- Ei mikään --';
$lang['business_solutions:new_category_label']		= 'Luo kategoria';
$lang['business_solutions:subscripe_to_rss_label']	= 'Tilaa RSS';
$lang['business_solutions:all_posts_label']			= 'Kaikki artikkelit';
$lang['business_solutions:posts_of_category_suffix']	= ' artikkelia';
$lang['business_solutions:rss_name_suffix']			= ' Uutiset';
$lang['business_solutions:rss_category_suffix']		= ' Uutiset';
$lang['business_solutions:author_name_label']			= 'Tekijän nimi';
$lang['business_solutions:read_more_label']			= 'Lue lisää&nbsp;&raquo;';
$lang['business_solutions:created_hour']				= 'Luotiin tunnissa';
$lang['business_solutions:created_minute']			= 'Luotiin minuutissa';
$lang['business_solutions:comments_enabled_label']	= 'Kommentit päällä';

// titles
$lang['business_solutions:disabled_after']	= 'Kommentointi on otettu pois käytöstä %s jälkeen.';
$lang['business_solutions:create_title']		= 'Luo artikkeli';
$lang['business_solutions:edit_title']		= 'Muokkaa artikkelia "%s"';
$lang['business_solutions:archive_title']		= 'Arkisto';
$lang['business_solutions:posts_title']		= 'Artikkelit';
$lang['business_solutions:rss_posts_title']	= 'Uutisartikkeleita %s';
$lang['business_solutions:business_solutions_title']		= 'Uutiset';
$lang['business_solutions:list_title']		= 'Listaa artikkelit';

// messages
$lang['business_solutions:no_posts']				= 'Artikkeleita ei ole.';
$lang['business_solutions:subscripe_to_rss_desc']	= 'Lue artikkelit tilaamalla RSS syöte. Suosituimmat sähköposti ohjelmat tukevat tätä. Voit myös vaihtoehtoisesti kokeilla <a href="http://reader.google.co.uk/">Google Readeria</a>.';
$lang['business_solutions:currently_no_posts']	= 'Ei artikkeleita tällä hetkellä.';
$lang['business_solutions:post_add_success']		= 'Artikkeli "%s" lisättiin.';
$lang['business_solutions:post_add_error']		= 'Tapahtui virhe.';
$lang['business_solutions:edit_success']			= 'Artikkeli "%s" päivitettiin.';
$lang['business_solutions:edit_error']			= 'Tapahtui virhe.';
$lang['business_solutions:publish_success']		= 'Artikkeli "%s" julkaistiin.';
$lang['business_solutions:mass_publish_success']	= 'Artikkelit "%s" julkaistiin.';
$lang['business_solutions:publish_error']			= 'Yhtään artikkelia ei julkaistu.';
$lang['business_solutions:delete_success']		= 'Artikkeli "%s" poistettiin.';
$lang['business_solutions:mass_delete_success']	= 'Artikkelit "%s" poistettiin.';
$lang['business_solutions:delete_error']			= 'Yhtään artikkelia ei poistettu.';
$lang['business_solutions:already_exist_error']	= 'Artikkelin URL osoite on jo käytössä.';

$lang['business_solutions:twitter_posted']	= 'Lähetetty "%s" %s';
$lang['business_solutions:twitter_error']		= 'Twitter Virhe';

// date
$lang['business_solutions:archive_date_format'] = "%B, %Y";

?>