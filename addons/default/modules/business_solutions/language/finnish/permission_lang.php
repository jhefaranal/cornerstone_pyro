<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Artikkelien julkaiseminen';
$lang['business_solutions:role_edit_live']	= 'Julkaistujen artikkeleiden muokkaaminen';
$lang['business_solutions:role_delete_live'] 	= 'Julkaistujen artikkeleiden poistaminen';
