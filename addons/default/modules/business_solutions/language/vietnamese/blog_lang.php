<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Author: Thanh Nguyen
* 		  nguyenhuuthanh@gmail.com
*
* Location: http://techmix.net
*
* Created:  10.26.2011
*
* Description:  Vietnamese language file
*
*/

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label']                   = 'Đã gửi';
$lang['business_solutions:posted_label_alt']               = 'Gửi lúc at';
$lang['business_solutions:written_by_label']				= 'Viết bởi';
$lang['business_solutions:author_unknown']				= 'Chưa rõ';
$lang['business_solutions:keywords_label']				= 'Keywords'; #translate
$lang['business_solutions:tagged_label']					= 'Tagged'; #translate
$lang['business_solutions:category_label']                 = 'Danh mục';
$lang['business_solutions:post_label']                     = 'Bài viết';
$lang['business_solutions:date_label']                     = 'Ngày';
$lang['business_solutions:date_at']                        = 'lúc';
$lang['business_solutions:time_label']                     = 'thời gian';
$lang['business_solutions:status_label']                   = 'Trạng thái';
$lang['business_solutions:draft_label']                    = 'Nháp';
$lang['business_solutions:live_label']                     = 'Xuất bản';
$lang['business_solutions:content_label']                  = 'Nội dung';
$lang['business_solutions:options_label']                  = 'Tùy biến';
$lang['business_solutions:intro_label']                    = 'Giới thiệu';
$lang['business_solutions:no_category_select_label']       = '-- Không --';
$lang['business_solutions:new_category_label']             = 'Thêm chuyên mục';
$lang['business_solutions:subscripe_to_rss_label']         = 'Nhận RSS';
$lang['business_solutions:all_posts_label']             = 'Tất cả bài viết';
$lang['business_solutions:posts_of_category_suffix']    = ' bài viết';
$lang['business_solutions:rss_name_suffix']                = ' Business_solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business_solutions';
$lang['business_solutions:author_name_label']              = 'Tên tác giả';
$lang['business_solutions:read_more_label']                = 'Đọc thêm&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Giờ tạo';
$lang['business_solutions:created_minute']                 = 'Phút tạo';
$lang['business_solutions:comments_enabled_label']         = 'Cho phép phản hồi';

// titles
$lang['business_solutions:create_title']                   = 'Thêm bài viết';
$lang['business_solutions:edit_title']                     = 'Sửa bài viết "%s"';
$lang['business_solutions:archive_title']                  = 'Lưu trữ';
$lang['business_solutions:posts_title']                 = 'Bài viết';
$lang['business_solutions:rss_posts_title']             = 'Các bài viết cho %s';
$lang['business_solutions:business_solutions_title']                     = 'Business_solutions';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts']                    = 'Không có bài viết nào.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Hãy sử dụng RSS Feed để nhận những bài viết mới nhất. Bạn có thể sử dụng nhiều chương trình khác nhau, hoặc sử dụng <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']          = 'Không có bài viết nào.';
$lang['business_solutions:post_add_success']            = 'Đã thêm bài viết "%s".';
$lang['business_solutions:post_add_error']              = 'Có lỗi xảy ra.';
$lang['business_solutions:edit_success']                   = 'Bài viết "%s" đã được cập nhật.';
$lang['business_solutions:edit_error']                     = 'Có lỗi xảy ra.';
$lang['business_solutions:publish_success']                = 'Bài viết "%s" đã được xuất bản.';
$lang['business_solutions:mass_publish_success']           = 'Bài viết "%s" đã được xuất bản.';
$lang['business_solutions:publish_error']                  = 'Không có bài viết nào được xuât bản.';
$lang['business_solutions:delete_success']                 = 'Đã xóa bài viết "%s".';
$lang['business_solutions:mass_delete_success']            = 'Đã xóa bài viết "%s".';
$lang['business_solutions:delete_error']                   = 'Không có bài viết nào được xóa.';
$lang['business_solutions:already_exist_error']            = 'URL của bài viết đã tồn tại.';

$lang['business_solutions:twitter_posted']                 = 'Đã gửi "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Lỗi Twitter';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
