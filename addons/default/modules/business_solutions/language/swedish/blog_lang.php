<?php defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * Swedish translation.
 *
 * @author		marcus@incore.se
 * @package		PyroCMS  
 * @link		http://pyrocms.com
 * @date		2012-10-23
 * @version		1.1.0
 */

$lang['business_solutions:post'] = 'Inlägg';
$lang['business_solutions:posts'] = 'Inlägg';
$lang['business_solutions:posted_label'] = 'Skriven';
$lang['business_solutions:posted_label_alt'] = 'Skriven den';
$lang['business_solutions:written_by_label'] = 'Skriven av';
$lang['business_solutions:author_unknown'] = 'Okänd';
$lang['business_solutions:keywords_label'] = 'Nyckelord';
$lang['business_solutions:tagged_label'] = 'Taggad';
$lang['business_solutions:category_label'] = 'Kategori';
$lang['business_solutions:post_label'] = 'Inlägg';
$lang['business_solutions:date_label'] = 'Datum';
$lang['business_solutions:date_at'] = 'den';
$lang['business_solutions:time_label'] = 'Tid';
$lang['business_solutions:status_label'] = 'Status';
$lang['business_solutions:draft_label'] = 'Utkast';
$lang['business_solutions:live_label'] = 'Publik';
$lang['business_solutions:content_label'] = 'Innehåll';
$lang['business_solutions:options_label'] = 'Val';
$lang['business_solutions:intro_label'] = 'Introduktion';
$lang['business_solutions:no_category_select_label'] = '-- Ingen --';
$lang['business_solutions:new_category_label'] = 'Lägg till en kategori';
$lang['business_solutions:subscripe_to_rss_label'] = 'Prenumerera på RSS';
$lang['business_solutions:all_posts_label'] = 'Alla inlägg';
$lang['business_solutions:posts_of_category_suffix'] = 'inlägg';
$lang['business_solutions:rss_name_suffix'] = 'Business_solutionsg';
$lang['business_solutions:rss_category_suffix'] = 'Business_solutionsg';
$lang['business_solutions:author_name_label'] = 'Skribent';
$lang['business_solutions:read_more_label'] = 'Läs mer »';
$lang['business_solutions:created_hour'] = 'Skapad, timme';
$lang['business_solutions:created_minute'] = 'Skapad, minut';
$lang['business_solutions:comments_enabled_label'] = 'Kommentarer aktiverad';
$lang['business_solutions:create_title'] = 'Lägg till post';
$lang['business_solutions:edit_title'] = 'Redigera post "%s"';
$lang['business_solutions:archive_title'] = 'Arkiv';
$lang['business_solutions:posts_title'] = 'Inlägg';
$lang['business_solutions:rss_posts_title'] = 'Business_solutionsginlägg för %s';
$lang['business_solutions:business_solutions_title'] = 'Business_solutionsg';
$lang['business_solutions:list_title'] = 'Lista inlägg';
$lang['business_solutions:disabled_after'] = 'Skickade kommentarer efter %s har blivit inaktiverade.';
$lang['business_solutions:no_posts'] = 'Det finns inga poster';
$lang['business_solutions:subscripe_to_rss_desc'] = 'Få inlägg direkt genom att prenumerera på vårt RSS-flöde. Du flesta populära e-postklienter har stöd för detta, eller prova <a href="http://reader.google.co.uk/">Google Reader</ a>.';
$lang['business_solutions:currently_no_posts'] = 'Det finns inga poster just nu';
$lang['business_solutions:post_add_success'] = 'Inlägget "%s" har lagts till.';
$lang['business_solutions:post_add_error'] = 'Ett fel inträffade.';
$lang['business_solutions:edit_success'] = 'Inlägget "%s" uppdaterades.';
$lang['business_solutions:edit_error'] = 'Ett fel inträffade.';
$lang['business_solutions:publish_success'] = 'Inlägget "%s" publicerades.';
$lang['business_solutions:mass_publish_success'] = 'Inläggen "%s" publicerades.';
$lang['business_solutions:publish_error'] = 'Inga inlägg publicerades';
$lang['business_solutions:delete_success'] = 'Inlägget "%s" har raderats.';
$lang['business_solutions:mass_delete_success'] = 'Inläggen "%s" har raderats.';
$lang['business_solutions:delete_error'] = 'Inga inlägg raderades';
$lang['business_solutions:already_exist_error'] = 'Ett inlägg med denna URL finns redan';
$lang['business_solutions:twitter_posted'] = 'Sparad "%s" %s';
$lang['business_solutions:twitter_error'] = 'Twitterfel';
$lang['business_solutions:archive_date_format'] = '%B\' %Y';


/* End of file business_solutions_lang.php */  
/* Location: system/cms/modules/business_solutions/language/swedish/business_solutions_lang.php */  
