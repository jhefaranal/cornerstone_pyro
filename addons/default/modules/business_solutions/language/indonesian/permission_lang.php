<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Publikasikan artikel';
$lang['business_solutions:role_edit_live']	= 'Edit artikel terpublikasi';
$lang['business_solutions:role_delete_live'] 	= 'Hapus artikel terpublikasi';
