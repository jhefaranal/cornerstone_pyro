<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label']                   = 'Dipostkan';
$lang['business_solutions:posted_label_alt']               = 'Dipostkan pada';
$lang['business_solutions:written_by_label']				= 'Ditulis oleh';
$lang['business_solutions:author_unknown']				= 'Tidak dikenal';
$lang['business_solutions:keywords_label']				= 'Kata Kunci';
$lang['business_solutions:tagged_label']					= 'Label';
$lang['business_solutions:category_label']                 = 'Kategori';
$lang['business_solutions:post_label']                     = 'Post';
$lang['business_solutions:date_label']                     = 'Tanggal';
$lang['business_solutions:date_at']                        = 'pada';
$lang['business_solutions:time_label']                     = 'Waktu';
$lang['business_solutions:status_label']                   = 'Status';
$lang['business_solutions:draft_label']                    = 'Draft';
$lang['business_solutions:live_label']                     = 'Publikasikan';
$lang['business_solutions:content_label']                  = 'Konten';
$lang['business_solutions:options_label']                  = 'Opsi';
$lang['business_solutions:intro_label']                    = 'Pendahuluan';
$lang['business_solutions:no_category_select_label']       = '-- tidak ada --';
$lang['business_solutions:new_category_label']             = 'Tambah kategori';
$lang['business_solutions:subscripe_to_rss_label']         = 'Berlangganan RSS';
$lang['business_solutions:all_posts_label']             = 'Semua post';
$lang['business_solutions:posts_of_category_suffix']    = ' post';
$lang['business_solutions:rss_name_suffix']                = ' Business_solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business_solutions';
$lang['business_solutions:author_name_label']              = 'Nama penulis';
$lang['business_solutions:read_more_label']                = 'Selengkapnya&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Dibuat pada Jam';
$lang['business_solutions:created_minute']                 = 'Dibuat pada Menit';
$lang['business_solutions:comments_enabled_label']         = 'Perbolehkan Komentar';

// titles
$lang['business_solutions:create_title']                   = 'Tambah Post';
$lang['business_solutions:edit_title']                     = 'Edit post "%s"';
$lang['business_solutions:archive_title']                 = 'Arsip';
$lang['business_solutions:posts_title']					= 'Post';
$lang['business_solutions:rss_posts_title']				= 'Business_solutions post untuk %s';
$lang['business_solutions:business_solutions_title']					= 'Business_solutions';
$lang['business_solutions:list_title']					= 'Daftar Post';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts']                    = 'Tidak ada post.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Ambil post langsung dengan berlangganan melalui RSS feed kami. Anda dapat melakukannya menggunakan hampir semua e-mail client, atau coba <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']          = 'Untuk sementara tidak ada post.';
$lang['business_solutions:post_add_success']            = 'Post "%s" telah ditambahkan.';
$lang['business_solutions:post_add_error']              = 'Terjadi kesalahan.';
$lang['business_solutions:edit_success']                   = 'Post "%s" telah diperbaharui.';
$lang['business_solutions:edit_error']                     = 'Terjadi kesalahan.';
$lang['business_solutions:publish_success']                = 'Post "%s" telah dipublikasikan.';
$lang['business_solutions:mass_publish_success']           = 'Post "%s" telah dipublikasikan.';
$lang['business_solutions:publish_error']                  = 'Tidak ada post yang terpublikasi.';
$lang['business_solutions:delete_success']                 = 'Post "%s" telah dihapus.';
$lang['business_solutions:mass_delete_success']            = 'Post "%s" telah dihapus.';
$lang['business_solutions:delete_error']                   = 'Tidak ada post yan terhapus.';
$lang['business_solutions:already_exist_error']            = 'Post dengan URL ini sudah ada.';

$lang['business_solutions:twitter_posted']                 = 'Dipostkan "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter Error';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
