<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                      = 'Post'; #translate
$lang['business_solutions:posts']                     = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label']				= 'Escrito';
$lang['business_solutions:posted_label_alt']			= 'Escrito em';
$lang['business_solutions:written_by_label']			= 'Por';
$lang['business_solutions:author_unknown']			= 'Desconhecido';
$lang['business_solutions:keywords_label']			= 'Palavras-chave';
$lang['business_solutions:tagged_label']				= 'Marcado';
$lang['business_solutions:category_label']			= 'Categoria';
$lang['business_solutions:post_label'] 				= 'Artigo';
$lang['business_solutions:date_label'] 				= 'Data';
$lang['business_solutions:date_at']					= 'às';
$lang['business_solutions:time_label'] 				= 'Hora';
$lang['business_solutions:status_label'] 				= 'Situação';
$lang['business_solutions:draft_label'] 				= 'Rascunho';
$lang['business_solutions:live_label'] 				= 'Publico';
$lang['business_solutions:content_label']				= 'Conteúdo';
$lang['business_solutions:options_label']				= 'Opções';
$lang['business_solutions:intro_label'] 				= 'Introdução';
$lang['business_solutions:no_category_select_label']	= '-- Nenhuma --';
$lang['business_solutions:new_category_label'] 		= 'Adicionar uma categoria';
$lang['business_solutions:subscripe_to_rss_label'] 	= 'Assinar o RSS';
$lang['business_solutions:all_posts_label'] 			= 'Todos os artigos';
$lang['business_solutions:posts_of_category_suffix'] 	= ' artigos';
$lang['business_solutions:rss_name_suffix']			= ' Business_solutions';
$lang['business_solutions:rss_category_suffix']		= ' Business_solutions';
$lang['business_solutions:author_name_label']			= 'Nome do autor';
$lang['business_solutions:read_more_label']			= 'Leia mais &raquo;';
$lang['business_solutions:created_hour']				= 'Horário (Hora)';
$lang['business_solutions:created_minute']			= 'Horário (Minuto)';
$lang['business_solutions:comments_enabled_label']	= 'Habilitar comentários';

// titles
$lang['business_solutions:create_title']				= 'Adicionar artigo';
$lang['business_solutions:edit_title']				= 'Editar artigo "%s"';
$lang['business_solutions:archive_title'] 			= 'Arquivo';
$lang['business_solutions:posts_title'] 				= 'Artigos';
$lang['business_solutions:rss_posts_title'] 			= 'Artigos novos para %s';
$lang['business_solutions:business_solutions_title']				= 'Business_solutions';
$lang['business_solutions:list_title']				= 'Listar artigos';

// messages
$lang['business_solutions:disabled_after'] 			= 'Postagem de comentários após %s foi desabilitado.';
$lang['business_solutions:no_posts']					= 'Nenhum artigo.';
$lang['business_solutions:subscripe_to_rss_desc']		= 'Fique por dentro das novidades do business_solutions assinando nosso feed RSS. Você pode fazer isto pelos mais populares leitores de e-mail ou pode experimentar o <a rel="nofollow" href="http://reader.google.com/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']		= 'Não existem artigos no momento.';
$lang['business_solutions:post_add_success']			= 'O artigo "%s" foi adicionado.';
$lang['business_solutions:post_add_error']			= 'Ocorreu um erro.';
$lang['business_solutions:edit_success']				= 'O artigo "%s" foi atualizado.';
$lang['business_solutions:edit_error']				= 'Ocorreu um erro.';
$lang['business_solutions:publish_success']			= 'O artigo "%s" foi publicado.';
$lang['business_solutions:mass_publish_success']		= 'Os artigos "%s" foram publicados.';
$lang['business_solutions:publish_error']				= 'Nenhum artigo foi publicado.';
$lang['business_solutions:delete_success']			= 'O artigo "%s" foi removido.';
$lang['business_solutions:mass_delete_success']		= 'Os artigos "%s" foram removidos.';
$lang['business_solutions:delete_error']				= 'Nenhuma publicação foi removida.';
$lang['business_solutions:already_exist_error']		= 'Um artigo com mesmo campo %s já existe.';

$lang['business_solutions:twitter_posted']			= 'Escrito "%s" %s';
$lang['business_solutions:twitter_error']				= 'Erro do Twitter';

// date
$lang['business_solutions:archive_date_format']		= "%B de %Y";

/* End of file business_solutions_lang.php */