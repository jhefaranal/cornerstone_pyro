<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label'] 					= 'Opublikowano w';
$lang['business_solutions:posted_label_alt']					= 'Opublikowano ';
$lang['business_solutions:written_by_label']					= 'Napisany przez';
$lang['business_solutions:author_unknown']					= 'Nieznany';
$lang['business_solutions:keywords_label']					= 'Słowa kluczowe';
$lang['business_solutions:tagged_label']					= 'Tagi';
$lang['business_solutions:category_label'] 					= 'Kategoria';
$lang['business_solutions:post_label'] 					= 'Post';
$lang['business_solutions:date_label'] 					= 'Data';
$lang['business_solutions:date_at']						= '';
$lang['business_solutions:time_label'] 					= 'Czas';
$lang['business_solutions:status_label'] 					= 'Status';
$lang['business_solutions:draft_label'] 					= 'Robocza';
$lang['business_solutions:live_label'] 					= 'Opublikowana';
$lang['business_solutions:content_label'] 					= 'Zawartość';
$lang['business_solutions:options_label'] 					= 'Opcje';
$lang['business_solutions:intro_label'] 					= 'Wprowadzenie';
$lang['business_solutions:no_category_select_label'] 				= '-- Brak --';
$lang['business_solutions:new_category_label'] 				= 'Dodaj kategorię';
$lang['business_solutions:subscripe_to_rss_label'] 				= 'Subskrybuj RSS';
$lang['business_solutions:all_posts_label'] 					= 'Wszystkie wpisy';
$lang['business_solutions:posts_of_category_suffix'] 				= ' - wpisy';
$lang['business_solutions:rss_name_suffix'] 					= '';
$lang['business_solutions:rss_category_suffix'] 				= '';
$lang['business_solutions:author_name_label'] 				= 'Imię autora';
$lang['business_solutions:read_more_label'] 					= 'Czytaj więcej&nbsp;&raquo;';
$lang['business_solutions:created_hour']               		    	= 'Czas (Godzina)';
$lang['business_solutions:created_minute']              		    	= 'Czas (Minuta)';
$lang['business_solutions:comments_enabled_label']         			= 'Komentarze aktywne';

// titles
$lang['business_solutions:create_title'] 					= 'Dodaj wpis';
$lang['business_solutions:edit_title'] 					= 'Edytuj wpis "%s"';
$lang['business_solutions:archive_title'] 					= 'Archiwum';
$lang['business_solutions:posts_title'] 					= 'Wpisy';
$lang['business_solutions:rss_posts_title'] 					= 'Najnowsze wpisy dla %s';
$lang['business_solutions:business_solutions_title'] 					= 'Business_solutions';
$lang['business_solutions:list_title'] 					= 'Lista wpisów';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts'] 						= 'Nie ma żadnych wpisów.';
$lang['business_solutions:subscripe_to_rss_desc'] 				= 'Bądź na bieżąco subskrybując nasz kanał RSS. Możesz to zrobić za pomocą popularnych klientów pocztowych, lub skorzystać z <a href="http://reader.google.com/">Google Reader\'a</a>.';
$lang['business_solutions:currently_no_posts'] 				= 'W tym momencie nie ma żadnych wpisów.';
$lang['business_solutions:post_add_success'] 					= 'Wpis "%s" został dodany.';
$lang['business_solutions:post_add_error'] 					= 'Wystąpił błąd.';
$lang['business_solutions:edit_success'] 					= 'Wpis "%s" został zaktualizowany.';
$lang['business_solutions:edit_error'] 					= 'Wystąpił błąd.';
$lang['business_solutions:publish_success'] 					= 'Wpis "%s" został opublikowany.';
$lang['business_solutions:mass_publish_success'] 				= 'Wpisy "%s" zostały opublikowane.';
$lang['business_solutions:publish_error'] 					= 'Żadne wpisy nie zostały opublikowane.';
$lang['business_solutions:delete_success'] 					= 'Wpis "%s" został usunięty.';
$lang['business_solutions:mass_delete_success'] 				= 'Wpisy "%s" zostały usunięte.';
$lang['business_solutions:delete_error'] 					= 'Żadne wpisy nie zostały usunięte.';
$lang['business_solutions:already_exist_error'] 				= 'Wpis z tym adresem URL już istnieje.';

$lang['business_solutions:twitter_posted'] 					= 'Opublikowano "%s" %s';
$lang['business_solutions:twitter_error'] 					= 'Błąd Twitter\'a';

// date
$lang['business_solutions:archive_date_format']				= "%B %Y";