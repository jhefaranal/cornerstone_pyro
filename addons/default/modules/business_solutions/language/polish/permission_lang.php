<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Publikuj wpisy';
$lang['business_solutions:role_edit_live']		= 'Edytuj opublikowane wpisy';
$lang['business_solutions:role_delete_live'] 		= 'Usuwaj opublikowane wpisy';