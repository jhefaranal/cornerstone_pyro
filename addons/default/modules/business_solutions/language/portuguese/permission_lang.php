<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Publicar artigos';
$lang['business_solutions:role_edit_live']	= 'Editar artigos publicados';
$lang['business_solutions:role_delete_live'] 	= 'Remover artigos publicados';