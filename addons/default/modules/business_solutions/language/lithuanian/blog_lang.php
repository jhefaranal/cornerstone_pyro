<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                          = 'Įrašas';
$lang['business_solutions:posts']                         = 'Įrašai';

// labels
$lang['business_solutions:posted_label']                  = 'Paskelbta';
$lang['business_solutions:posted_label_alt']              = 'Paskelbta...';
$lang['business_solutions:written_by_label']              = 'Autorius';
$lang['business_solutions:author_unknown']				= 'Nežinomas';
$lang['business_solutions:keywords_label']				= 'Raktažodžiai';
$lang['business_solutions:tagged_label']					= 'Žymėtas';
$lang['business_solutions:category_label']                = 'Kategorija';
$lang['business_solutions:post_label']                    = 'Įrašas';
$lang['business_solutions:date_label']                    = 'Data';
$lang['business_solutions:date_at']                       = '';
$lang['business_solutions:time_label']                    = 'Laikas';
$lang['business_solutions:status_label']                  = 'Statusas';
$lang['business_solutions:draft_label']                   = 'Projektas';
$lang['business_solutions:live_label']                    = 'Gyvai';
$lang['business_solutions:content_label']                 = 'Turinys';
$lang['business_solutions:options_label']                 = 'Funkcijos';
$lang['business_solutions:title_label']                   = 'Pavadinimas';
$lang['business_solutions:slug_label']                    = 'URL';
$lang['business_solutions:intro_label']                   = 'Įžanga';
$lang['business_solutions:no_category_select_label']      = '-- Nėra --';
$lang['business_solutions:new_category_label']            = 'Pridėti kategoriją';
$lang['business_solutions:subscripe_to_rss_label']        = 'Prenumeruoti RSS';
$lang['business_solutions:all_posts_label']               = 'Visi įrašai';
$lang['business_solutions:posts_of_category_suffix']      = ' įrašai';
$lang['business_solutions:rss_name_suffix']               = ' Naujienos';
$lang['business_solutions:rss_category_suffix']           = ' Naujienos';
$lang['business_solutions:author_name_label']             = 'Autoriaus vardas';
$lang['business_solutions:read_more_label']               = 'Plačiau&nbsp;&raquo;';
$lang['business_solutions:created_hour']                  = 'Sukurta valandą';
$lang['business_solutions:created_minute']                = 'Sukurta minutę';
$lang['business_solutions:comments_enabled_label']        = 'Įjungti komentarus?';

// titles
$lang['business_solutions:create_title']                  = 'Pridėti įrašą';
$lang['business_solutions:edit_title']                    = 'Redaguoti įrašą "%s"';
$lang['business_solutions:archive_title']                 = 'Archyvas';
$lang['business_solutions:posts_title']                   = 'Įrašai';
$lang['business_solutions:rss_posts_title']               = 'Naujienų įrašai %s';
$lang['business_solutions:business_solutions_title']                    = 'Naujiena';
$lang['business_solutions:list_title']                    = 'Įrašų sąrašas';

// messages
$lang['business_solutions:disabled_after'] 				= 'Paskelbti komentarai po %s buvo atjungti.';
$lang['business_solutions:no_posts']                      = 'Nėra įrašų.';
$lang['business_solutions:subscripe_to_rss_desc']         = 'Gaukite žinutes iš karto prenumeruodami į RSS kanalą. Jūs galite tai padaryti per populiariausias elektroninio pašto paskyras, arba bandykite <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']            = 'Šiuo metu nėra įrašų.';
$lang['business_solutions:post_add_success']              = 'Įrašas "%s" pridėtas.';
$lang['business_solutions:post_add_error']                = 'Įvyko klaida.';
$lang['business_solutions:edit_success']                  = 'Įrašas "%s" atnaujintas.';
$lang['business_solutions:edit_error']                    = 'Įvyko klaida.';
$lang['business_solutions:publish_success']               = 'Įrašas "%s" paskelbtas.';
$lang['business_solutions:mass_publish_success']          = 'Įrašai "%s" pasklbti.';
$lang['business_solutions:publish_error']                 = 'Nėra paskelbtų įrašų.';
$lang['business_solutions:delete_success']                = 'Įrašas "%s" ištrintas.';
$lang['business_solutions:mass_delete_success']           = 'Įrašai "%s" ištrinti.';
$lang['business_solutions:delete_error']                  = 'Nėra ištrintų įrašų.';
$lang['business_solutions:already_exist_error']           = 'Įrašas su šiuo URL jau egzistuoja.';

$lang['business_solutions:twitter_posted']                = 'Įrašyta "%s" %s';
$lang['business_solutions:twitter_error']                 = 'Twitter nepasiekiamas';

// date
$lang['business_solutions:archive_date_format']           = "%B %Y";
