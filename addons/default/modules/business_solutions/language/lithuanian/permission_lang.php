<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live'] = 'Įdėti straipsnį gyvai';
$lang['business_solutions:role_edit_live'] = 'Straipsnių gyvai redagavimas';
$lang['business_solutions:role_delete_live'] 	= 'Delete live articles'; #translate