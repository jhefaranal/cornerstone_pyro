<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label']                   = 'Objavljeno';
$lang['business_solutions:posted_label_alt']               = 'Objavljeno ob';
$lang['business_solutions:written_by_label']				= 'Objavil';
$lang['business_solutions:author_unknown']				= 'Neznan';
$lang['business_solutions:keywords_label']				= 'Klj. besede';
$lang['business_solutions:tagged_label']					= 'Označen';
$lang['business_solutions:category_label']                 = 'Kategorija';
$lang['business_solutions:post_label']                     = 'Objava';
$lang['business_solutions:date_label']                     = 'Datum';
$lang['business_solutions:date_at']                        = 'ob';
$lang['business_solutions:time_label']                     = 'Čas';
$lang['business_solutions:status_label']                   = 'Stanje';
$lang['business_solutions:draft_label']                    = 'Osnutek';
$lang['business_solutions:live_label']                     = 'Vidno';
$lang['business_solutions:content_label']                  = 'Vsebina';
$lang['business_solutions:options_label']                  = 'Možnosti';
$lang['business_solutions:intro_label']                    = 'Navodila';
$lang['business_solutions:no_category_select_label']       = '-- Brez --';
$lang['business_solutions:new_category_label']             = 'Dodaj kategorijo';
$lang['business_solutions:subscripe_to_rss_label']         = 'Prijava na RSS';
$lang['business_solutions:all_posts_label']             = 'Vsi prispevki';
$lang['business_solutions:posts_of_category_suffix']    = ' prispevki';
$lang['business_solutions:rss_name_suffix']                = ' Business_solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business_solutions';
$lang['business_solutions:author_name_label']              = 'Ime avtorja';
$lang['business_solutions:read_more_label']                = 'Preberi vse&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Ustvarjeno ura';
$lang['business_solutions:created_minute']                 = 'Ustvarjeno minuta';
$lang['business_solutions:comments_enabled_label']         = 'Komentaji omogočeni';

// titles
$lang['business_solutions:create_title']                   = 'Dodaj prispevek';
$lang['business_solutions:edit_title']                     = 'Uredi prispevek "%s"';
$lang['business_solutions:archive_title']                  = 'Arhiv';
$lang['business_solutions:posts_title']                 = 'Članki';
$lang['business_solutions:rss_posts_title']             = 'Business_solutions prispevki za %s';
$lang['business_solutions:business_solutions_title']                     = 'Business_solutions';
$lang['business_solutions:list_title']                     = 'Seznam prispevkov';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts']                    = 'Trenutno še ni prispevkov.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Pridite do prispevkov v najkrajšem možnem času tako da se prijavite na RSS podajalca. To lahko storite preko popularnega email programa ali poizkusite  <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']          = 'Ta trenutek ni prispevkov';
$lang['business_solutions:post_add_success']            = 'Vap prispevek je bil dodan "%s" ';
$lang['business_solutions:post_add_error']              = 'Prišlo je do napake';
$lang['business_solutions:edit_success']                   = 'Prispevek "%s" je bil oddan.';
$lang['business_solutions:edit_error']                     = 'Prišlo je do napake.';
$lang['business_solutions:publish_success']                = 'Prispevek "%s" je bil objavljen.';
$lang['business_solutions:mass_publish_success']           = 'Prispeveki "%s" so bili objavljeni.';
$lang['business_solutions:publish_error']                  = 'Noben prispevk ni bil objavljen.';
$lang['business_solutions:delete_success']                 = 'Prispevki "%s" so bili izbrisani.';
$lang['business_solutions:mass_delete_success']            = 'Prispevki "%s" so bili izbrisani.';
$lang['business_solutions:delete_error']                   = 'Noben od prispevkov ni bil izbrisan..';
$lang['business_solutions:already_exist_error']            = 'Prispevek s tem URL-jem že obstaja.';

$lang['business_solutions:twitter_posted']                 = 'Objavljeno "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter napaka';

// date
$lang['business_solutions:archive_date_format']		= "%d' %m' %Y";

/* End of file business_solutions_lang.php */