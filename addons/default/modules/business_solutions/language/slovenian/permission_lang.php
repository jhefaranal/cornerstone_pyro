<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Objavi članek';
$lang['business_solutions:role_edit_live']	= 'Uredi objavljen članek';
$lang['business_solutions:role_delete_live'] 	= 'Izbriši objavljen članek';

// slovenian premission_lang.php