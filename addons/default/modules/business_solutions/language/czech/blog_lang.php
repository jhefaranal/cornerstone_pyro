<?php

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label'] 			= 'Odesláno';
$lang['business_solutions:posted_label_alt']			= 'Odesláno';
$lang['business_solutions:written_by_label']				= 'Written by'; #translate
$lang['business_solutions:author_unknown']				= 'Unknown'; #translate
$lang['business_solutions:keywords_label']				= 'Keywords'; #translate
$lang['business_solutions:tagged_label']					= 'Tagged'; #translate
$lang['business_solutions:category_label'] 			= 'Kategorie';
$lang['business_solutions:post_label'] 			= 'Odeslat';
$lang['business_solutions:date_label'] 			= 'Datum';
$lang['business_solutions:date_at']				= 'v';
$lang['business_solutions:time_label'] 			= 'Čas';
$lang['business_solutions:status_label'] 			= 'Stav';
$lang['business_solutions:draft_label'] 			= 'Koncept';
$lang['business_solutions:live_label'] 			= 'Publikováno';
$lang['business_solutions:content_label'] 			= 'Obsah';
$lang['business_solutions:options_label'] 			= 'Možnosti';
$lang['business_solutions:intro_label'] 			= 'Úvod';
$lang['business_solutions:no_category_select_label'] 		= '-- Nic --';
$lang['business_solutions:new_category_label'] 		= 'Přidat kategorii';
$lang['business_solutions:subscripe_to_rss_label'] 		= 'Přihlásit se k odběru RSS';
$lang['business_solutions:all_posts_label'] 		= 'Všechny články';
$lang['business_solutions:posts_of_category_suffix'] 	= ' články';
$lang['business_solutions:rss_name_suffix'] 			= ' Novinky';
$lang['business_solutions:rss_category_suffix'] 		= ' Novinky';
$lang['business_solutions:author_name_label'] 		= 'Autor';
$lang['business_solutions:read_more_label'] 			= 'Čtěte dále';
$lang['business_solutions:created_hour']                      = 'Vytvořeno v hodině';
$lang['business_solutions:created_minute']                    = 'Vytvořeno v minutě';
$lang['business_solutions:comments_enabled_label']         = 'Komentáře povoleny';

// titles
$lang['business_solutions:create_title'] 			= 'Přidat článek';
$lang['business_solutions:edit_title'] 			= 'Upravit článek "%s"';
$lang['business_solutions:archive_title'] 			= 'Archiv';
$lang['business_solutions:posts_title'] 			= 'Články';
$lang['business_solutions:rss_posts_title'] 		= 'Novinky pro %s';
$lang['business_solutions:business_solutions_title'] 			= 'Novinky';
$lang['business_solutions:list_title'] 			= 'Seznam článků';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts'] 			= 'Nejsou zde žádné články.';
$lang['business_solutions:subscripe_to_rss_desc'] 		= 'Dostávejte články rovnou pomocí RSS. Můžete použít populární e-mailové klienty nebo zkuste <a href="http://reader.google.cz/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts'] 		= 'Nejsou zde žádné články.';
$lang['business_solutions:post_add_success'] 		= 'Článek "%s" byl přidán.';
$lang['business_solutions:post_add_error'] 		= 'Objevila se chyba.';
$lang['business_solutions:edit_success'] 			= 'Článek "%s" byl aktualizován.';
$lang['business_solutions:edit_error'] 			= 'Objevila se chyba.';
$lang['business_solutions:publish_success'] 			= 'Článek "%s" byl publikován.';
$lang['business_solutions:mass_publish_success'] 		= 'Články "%s" byly publikovány.';
$lang['business_solutions:publish_error'] 			= 'žádné články nebyly publikovány.';
$lang['business_solutions:delete_success'] 			= 'Článek "%s" byl smazán.';
$lang['business_solutions:mass_delete_success'] 		= 'Články "%s" byly smazány.';
$lang['business_solutions:delete_error'] 			= 'Žádné články nebyly smazány.';
$lang['business_solutions:already_exist_error'] 		= 'Článek s touto adresou URL již existuje.';

$lang['business_solutions:twitter_posted']			= 'Publikováno "%s" %s';
$lang['business_solutions:twitter_error'] 			= 'Chyba Twitteru';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";

?>