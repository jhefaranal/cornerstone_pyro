<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Publikování příspěvků';
$lang['business_solutions:role_edit_live']	= 'Úpravy publikovaných příspěvků';
$lang['business_solutions:role_delete_live'] 	= 'Mazání publikovaných příspěvků';