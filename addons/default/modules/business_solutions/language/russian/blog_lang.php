<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * PyroCMS
 * Русский перевод от Dark Preacher - dark[at]darklab.ru
 *
 * @package		PyroCMS
 * @author		Dark Preacher
 * @link			http://pyrocms.com
 */

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// подписи
$lang['business_solutions:posted_label']									= 'Дата';
$lang['business_solutions:posted_label_alt']							= 'Дата добавления';
$lang['business_solutions:written_by_label']							= 'Автор';
$lang['business_solutions:author_unknown']								= 'Неизвестно';
$lang['business_solutions:keywords_label']				= 'Keywords'; #translate
$lang['business_solutions:tagged_label']					= 'Tagged'; #translate
$lang['business_solutions:category_label']								= 'Категория';
$lang['business_solutions:post_label']										= 'Заголовок';
$lang['business_solutions:date_label']										= 'Дата';
$lang['business_solutions:date_at']												= 'в';
$lang['business_solutions:time_label']										= 'Время';
$lang['business_solutions:status_label']									= 'Статус';
$lang['business_solutions:draft_label']										= 'Черновик';
$lang['business_solutions:live_label']										= 'Опубликовано';
$lang['business_solutions:content_label']									= 'Содержание';
$lang['business_solutions:options_label']									= 'Опции';
$lang['business_solutions:intro_label']										= 'Анонс';
$lang['business_solutions:no_category_select_label']			= '-- нет --';
$lang['business_solutions:new_category_label']						= 'Создать категорию';
$lang['business_solutions:subscripe_to_rss_label']				= 'Подписаться на RSS';
$lang['business_solutions:all_posts_label']								= 'Все статьи';
$lang['business_solutions:posts_of_category_suffix']			= ' статьи';
$lang['business_solutions:rss_name_suffix']								= ' Блог';
$lang['business_solutions:rss_category_suffix']						= ' Блог';
$lang['business_solutions:author_name_label']							= 'Автор';
$lang['business_solutions:read_more_label']								= 'читать целиком&nbsp;&raquo;';
$lang['business_solutions:created_hour']									= 'Время (Час)';
$lang['business_solutions:created_minute']								= 'Время (Минута)';
$lang['business_solutions:comments_enabled_label']				= 'Комментарии разрешены';

// заголовки
$lang['business_solutions:create_title']									= 'Создать статью';
$lang['business_solutions:edit_title']										= 'Редактирование статьи "%s"';
$lang['business_solutions:archive_title']									= 'Архив';
$lang['business_solutions:posts_title']										= 'Статьи';
$lang['business_solutions:rss_posts_title']								= 'Статьи из %s';
$lang['business_solutions:business_solutions_title']										= 'Блог';
$lang['business_solutions:list_title']										= 'Список статей';

// сообщения
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts']											= 'Статьи отсутствуют.';
$lang['business_solutions:subscripe_to_rss_desc']					= 'Получайте статьи сразу после их публикации, подпишитесь на нашу ленту новостей. Вы можете сделать это с помощью самых популярных программ для чтения электронных писем, или попробуйте <a href="http://reader.google.ru/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']						= 'В данный момент новости отсутствуют.';
$lang['business_solutions:post_add_success']							= 'Статья "%s" добавлена.';
$lang['business_solutions:post_add_error']								= 'Во время добавления статьи произошла ошибка.';
$lang['business_solutions:edit_success']									= 'Статья "%s" сохранена.';
$lang['business_solutions:edit_error']										= 'Во время сохранения статьи произошла ошибка.';
$lang['business_solutions:publish_success']								= 'Статья "%s" опубликована.';
$lang['business_solutions:mass_publish_success']					= 'Статьи "%s" опубликованы.';
$lang['business_solutions:publish_error']									= 'Во время публикации статьи произошла ошибка.';
$lang['business_solutions:delete_success']								= 'Статья "%s" удалена.';
$lang['business_solutions:mass_delete_success']						= 'Статьи "%s" удалены.';
$lang['business_solutions:delete_error']									= 'Во время удаления статьи произошла ошибка.';
$lang['business_solutions:already_exist_error']						= 'Статья с данным адресом URL уже существует.';

$lang['business_solutions:twitter_posted']								= 'Добавлен "%s" %s';
$lang['business_solutions:twitter_error']									= 'Ошибка Twitter\'а';

// дата
$lang['business_solutions:archive_date_format']						= "%B %Y"; #see php strftime documentation

/* End of file business_solutions_lang.php */