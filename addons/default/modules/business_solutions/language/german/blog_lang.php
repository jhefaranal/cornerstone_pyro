<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Artikel';
$lang['business_solutions:posts']                   = 'Artikel';

// labels
$lang['business_solutions:posted_label']                   = 'Ver&ouml;ffentlicht';
$lang['business_solutions:postet_label_alt']               = 'Ver&ouml;ffentlicht in';
$lang['business_solutions:written_by_label']				 = 'Geschrieben von';
$lang['business_solutions:author_unknown']				 = 'Unbekannt';
$lang['business_solutions:keywords_label']				 = 'Stichw&ouml;rter';
$lang['business_solutions:tagged_label']					 = 'Gekennzeichnet';
$lang['business_solutions:category_label']                 = 'Kategorie';
$lang['business_solutions:post_label']                     = 'Artikel';
$lang['business_solutions:date_label']                     = 'Datum';
$lang['business_solutions:date_at']                        = 'um';
$lang['business_solutions:time_label']                     = 'Uhrzeit';
$lang['business_solutions:status_label']                   = 'Status';
$lang['business_solutions:draft_label']                    = 'Unver&ouml;ffentlicht';
$lang['business_solutions:live_label']                     = 'Ver&ouml;ffentlicht';
$lang['business_solutions:content_label']                  = 'Inhalt';
$lang['business_solutions:options_label']                  = 'Optionen';
$lang['business_solutions:intro_label']                    = 'Einf&uuml;hrung';
$lang['business_solutions:no_category_select_label']       = '-- Kein Label --';
$lang['business_solutions:new_category_label']             = 'Kategorie hinzuf&uuml;gen';
$lang['business_solutions:subscripe_to_rss_label']         = 'RSS abonnieren';
$lang['business_solutions:all_posts_label']                = 'Alle Artikel';
$lang['business_solutions:posts_of_category_suffix']       = ' Artikel';
$lang['business_solutions:rss_name_suffix']                = ' Business_solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business_solutions';
$lang['business_solutions:author_name_label']              = 'Autor';
$lang['business_solutions:read_more_label']                = 'Mehr lesen&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Erstellt zur Stunde';
$lang['business_solutions:created_minute']                 = 'Erstellt zur Minute';
$lang['business_solutions:comments_enabled_label']         = 'Kommentare aktiv';

// titles
$lang['business_solutions:create_title']                   = 'Artikel erstellen';
$lang['business_solutions:edit_title']                     = 'Artikel "%s" bearbeiten';
$lang['business_solutions:archive_title']                  = 'Archiv';
$lang['business_solutions:posts_title']                    = 'Artikel';
$lang['business_solutions:rss_posts_title']                = 'Business_solutions Artikel f&uuml;r %s';
$lang['business_solutions:business_solutions_title']                     = 'Business_solutions';
$lang['business_solutions:list_title']                     = 'Artikel auflisten';

// messages
$lang['business_solutions:disabled_after'] 				= 'Kommentare nach %s wurden deaktiviert.';
$lang['business_solutions:no_posts']                       = 'Es existieren keine Artikel.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Abonnieren Sie unseren RSS Feed und erhalten Sie alle Artikel frei Haus. Sie k&ouml;nnen dies mit den meisten Email-Clients tun, oder z.B. mit <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']             = 'Es existieren zur Zeit keine Artikel.';
$lang['business_solutions:post_add_success']               = 'Der Artikel "%s" wurde hinzugef&uuml;gt.';
$lang['business_solutions:post_add_error']                 = 'Ein Fehler ist aufgetreten.';
$lang['business_solutions:edit_success']                   = 'Der Artikel "%s" wurde aktualisiert.';
$lang['business_solutions:edit_error']                     = 'Ein Fehler ist aufgetreten.';
$lang['business_solutions:publish_success']                = 'Der Artikel "%s" wurde ver&ouml;ffentlicht.';
$lang['business_solutions:mass_publish_success']           = 'Die Artikel "%s" wurden ver&ouml;ffentlicht.';
$lang['business_solutions:publish_error']                  = 'Ein Fehler ist aufgetreten. Es wurden keine Artikel ver&ouml;ffentlicht.';
$lang['business_solutions:delete_success']                 = 'Der Artikel "%s" wurde gel&ouml;scht.';
$lang['business_solutions:mass_delete_success']            = 'Die Artikel "%s" wurden gel&ouml;scht.';
$lang['business_solutions:delete_error']                   = 'Ein Fehler ist aufgetreten. Keine Artikel wurden gel&ouml;scht.';
$lang['business_solutions:already_exist_error']            = 'Ein Artikel mit dieser URL existiert bereits.';

$lang['business_solutions:twitter_posted']                 = 'Gepostet "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter Fehler';

// date
$lang['business_solutions:archive_date_format']		     = "%B %Y";
