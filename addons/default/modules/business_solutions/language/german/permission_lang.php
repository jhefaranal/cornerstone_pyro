<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Artikel live setzen';
$lang['business_solutions:role_edit_live']	= 'Live-Artikel bearbeiten';
$lang['business_solutions:role_delete_live'] 	= 'Live-Artikel l&ouml;schen';

/* End of file permission_lang.php */