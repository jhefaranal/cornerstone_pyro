<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                           = 'Bejegyzés';
$lang['business_solutions:posts']                          = 'Bejegyzések';

// labels
$lang['business_solutions:posted_label']                   = 'Bejegyezve';
$lang['business_solutions:posted_label_alt']               = 'Bejegyezve';
$lang['business_solutions:written_by_label']               = 'Írta';
$lang['business_solutions:author_unknown']                 = 'Ismeretlen';
$lang['business_solutions:keywords_label']                 = 'Kulcsszavak';
$lang['business_solutions:tagged_label']                   = 'Tagged';
$lang['business_solutions:category_label']                 = 'Kategória';
$lang['business_solutions:post_label']                     = 'Bejegyzés';
$lang['business_solutions:date_label']                     = 'Dátum';
$lang['business_solutions:date_at']                        = '';
$lang['business_solutions:time_label']                     = 'Idő';
$lang['business_solutions:status_label']                   = 'Állapot';
$lang['business_solutions:draft_label']                    = 'Piszkozat';
$lang['business_solutions:live_label']                     = 'Élő';
$lang['business_solutions:content_label']                  = 'Tartalom';
$lang['business_solutions:options_label']                  = 'Beállítások';
$lang['business_solutions:intro_label']                    = 'Bevezető';
$lang['business_solutions:no_category_select_label']       = '-- Nincs --';
$lang['business_solutions:new_category_label']             = 'Kategória hozzáadása';
$lang['business_solutions:subscripe_to_rss_label']         = 'Feliratkozás RSS-re';
$lang['business_solutions:all_posts_label']                = 'Összes bejegyzés';
$lang['business_solutions:posts_of_category_suffix']       = ' bejegyzések';
$lang['business_solutions:rss_name_suffix']                = ' Business_solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business_solutions';
$lang['business_solutions:author_name_label']              = 'Szerző neve';
$lang['business_solutions:read_more_label']                = 'Tovább&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Létrehozva pár órája';
$lang['business_solutions:created_minute']                 = 'Létrehozva pár perce';
$lang['business_solutions:comments_enabled_label']         = 'Hozzászólás engedélyezése';

// titles
$lang['business_solutions:create_title']                   = 'Bejegyzés hozzáadása';
$lang['business_solutions:edit_title']                     = 'A(z) "%s" bejegyzés szerkesztése';
$lang['business_solutions:archive_title']                  = 'Archívum';
$lang['business_solutions:posts_title']                    = 'Bejegyzések';
$lang['business_solutions:rss_posts_title']                = 'Business_solutions bejegyzés %s-ért';
$lang['business_solutions:business_solutions_title']                     = 'Business_solutions';
$lang['business_solutions:list_title']                     = 'Bejegyzések listája';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts']                       = 'Nincs bejegyzés.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'A bejegyzést jelentesd meg saját RSS-en. Ezt meg lehet tenni az ismertebb levelezőkkel vagy a <a href="http://reader.google.co.uk/">Google Reader</a>rel.';
$lang['business_solutions:currently_no_posts']             = 'Ebben a pillanatban még nincs bejegyzés.';
$lang['business_solutions:post_add_success']               = 'A(z) "%s" bejegyzés sikeresen hozzáadva.';
$lang['business_solutions:post_add_error']                 = 'A bejegyzés hozzáadása sikertelen.';
$lang['business_solutions:edit_success']                   = 'A(z) "%s" bejegyzés sikeresen módosítva.';
$lang['business_solutions:edit_error']                     = 'A bejegyzés módosítása sikertelen.';
$lang['business_solutions:publish_success']                = 'A(z) "%s" bejegyzés közzétéve.';
$lang['business_solutions:mass_publish_success']           = 'A(z) "%s" bejegyzés már közzétéve.';
$lang['business_solutions:publish_error']                  = 'Nincs bejegyzés közzétéve.';
$lang['business_solutions:delete_success']                 = 'A(z) "%s" bejegyzés törölve.';
$lang['business_solutions:mass_delete_success']            = 'A(z) "%s" bejegyzés már törölve.';
$lang['business_solutions:delete_error']                   = 'Nincs törölhető bejegyzés.';
$lang['business_solutions:already_exist_error']            = 'Egy bejegyzés már létezik ezzel a hivatkozással.';

$lang['business_solutions:twitter_posted']                 = 'Bejegyezve "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter Hiba';

// date
$lang['business_solutions:archive_date_format']            = "%B %Y";
