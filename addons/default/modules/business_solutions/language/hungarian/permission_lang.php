<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Bejegyzés élővé tétele';
$lang['business_solutions:role_edit_live']            = 'Élő bejegyzés szerkesztése';
$lang['business_solutions:role_delete_live']          = 'Élő bejegyzés törlése';