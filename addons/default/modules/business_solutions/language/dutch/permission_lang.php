<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Zet artikelen online';
$lang['business_solutions:role_edit_live']	= 'Bewerk online artikelen';
$lang['business_solutions:role_delete_live'] 	= 'Verwijder online artikelen';