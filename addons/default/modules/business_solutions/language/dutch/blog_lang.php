<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label'] 				= 'Geplaatst';
$lang['business_solutions:posted_label_alt']			= 'Geplaatst op';
$lang['business_solutions:written_by_label']			= 'Geschreven door';
$lang['business_solutions:author_unknown']			= 'Onbekend';
$lang['business_solutions:keywords_label']			= 'Sleutelwoorden';
$lang['business_solutions:tagged_label']				= 'Etiket';
$lang['business_solutions:category_label'] 			= 'Categorie';
$lang['business_solutions:post_label'] 				= 'Post';
$lang['business_solutions:date_label'] 				= 'Datum';
$lang['business_solutions:date_at']					= 'op';
$lang['business_solutions:time_label'] 				= 'Tijd';
$lang['business_solutions:status_label'] 				= 'Status';
$lang['business_solutions:draft_label'] 				= 'Concept';
$lang['business_solutions:live_label'] 				= 'Live';
$lang['business_solutions:content_label'] 			= 'Content';
$lang['business_solutions:options_label'] 			= 'Opties';
$lang['business_solutions:slug_label'] 				= 'URL';
$lang['business_solutions:intro_label'] 				= 'Introductie';
$lang['business_solutions:no_category_select_label'] 	= '-- Geen --';
$lang['business_solutions:new_category_label'] 		= 'Voeg een categorie toe';
$lang['business_solutions:subscripe_to_rss_label'] 	= 'Abonneer op RSS';
$lang['business_solutions:all_posts_label'] 			= 'Alle artikelen';
$lang['business_solutions:posts_of_category_suffix'] 	= ' artikelen';
$lang['business_solutions:rss_name_suffix'] 			= ' Nieuws';
$lang['business_solutions:rss_category_suffix'] 		= ' Nieuws';
$lang['business_solutions:author_name_label'] 		= 'Auteur naam';
$lang['business_solutions:read_more_label'] 			= 'Lees Meer&nbsp;&raquo;';
$lang['business_solutions:created_hour']           	= 'Tijd (Uren)';
$lang['business_solutions:created_minute']       		= 'Tijd (Minuten)';
$lang['business_solutions:comments_enabled_label']	= 'Reacties ingeschakeld';

// titles
$lang['business_solutions:disabled_after'] 			= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:create_title'] 				= 'Voeg artikel toe';
$lang['business_solutions:edit_title'] 				= 'Wijzig artikel "%s"';
$lang['business_solutions:archive_title'] 			= 'Archief';
$lang['business_solutions:posts_title'] 				= 'Artikelen';
$lang['business_solutions:rss_posts_title'] 			= 'Nieuws artikelen voor %s';
$lang['business_solutions:business_solutions_title'] 				= 'Nieuws';
$lang['business_solutions:list_title'] 				= 'Overzicht artikelen';

// messages
$lang['business_solutions:no_posts'] 					= 'Er zijn geen artikelen.';
$lang['business_solutions:subscripe_to_rss_desc'] 	= 'Ontvang artikelen meteen door te abonneren op onze RSS feed. U kunt dit doen met de meeste populaire e-mail programma&acute;s, of probeer <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts'] 		= 'Er zijn momenteel geen artikelen.';
$lang['business_solutions:post_add_success'] 			= 'Het artikel "%s" is opgeslagen.';
$lang['business_solutions:post_add_error'] 			= 'Er is een fout opgetreden.';
$lang['business_solutions:edit_success'] 				= 'Het artikel "%s" is opgeslagen.';
$lang['business_solutions:edit_error'] 				= 'Er is een fout opgetreden.';
$lang['business_solutions:publish_success'] 			= 'Het artikel "%s" is gepubliceerd.';
$lang['business_solutions:mass_publish_success'] 		= 'De artikelen "%s" zijn gepubliceerd.';
$lang['business_solutions:publish_error'] 			= 'Geen artikelen zijn gepubliceerd.';
$lang['business_solutions:delete_success'] 			= 'Het artikel "%s" is verwijderd.';
$lang['business_solutions:mass_delete_success'] 		= 'De artikelen "%s" zijn verwijderd.';
$lang['business_solutions:delete_error'] 				= 'Geen artikelen zijn verwijderd.';
$lang['business_solutions:already_exist_error'] 		= 'Een artikel met deze URL bestaat al.';

$lang['business_solutions:twitter_posted']			= 'Geplaatst "%s" %s';
$lang['business_solutions:twitter_error'] 			= 'Twitter Fout';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
