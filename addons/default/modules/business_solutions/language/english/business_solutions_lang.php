<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'Post';
$lang['business_solutions:posts']                   = 'Posts';

// labels
$lang['business_solutions:posted_label']                   = 'Posted';
$lang['business_solutions:posted_label_alt']               = 'Posted at';
$lang['business_solutions:written_by_label']				= 'Written by';
$lang['business_solutions:author_unknown']				= 'Unknown';
$lang['business_solutions:keywords_label']				= 'Keywords';
$lang['business_solutions:tagged_label']					= 'Tagged';
$lang['business_solutions:category_label']                 = 'Category';
$lang['business_solutions:post_label']                     = 'Post';
$lang['business_solutions:date_label']                     = 'Date';
$lang['business_solutions:date_at']                        = 'at';
$lang['business_solutions:time_label']                     = 'Time';
$lang['business_solutions:status_label']                   = 'Status';
$lang['business_solutions:draft_label']                    = 'Draft';
$lang['business_solutions:live_label']                     = 'Live';
$lang['business_solutions:content_label']                  = 'Content';
$lang['business_solutions:options_label']                  = 'Options';
$lang['business_solutions:intro_label']                    = 'Introduction';
$lang['business_solutions:no_category_select_label']       = '-- None --';
$lang['business_solutions:new_category_label']             = 'Add a category';
$lang['business_solutions:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['business_solutions:all_posts_label']                = 'All posts';
$lang['business_solutions:posts_of_category_suffix']       = ' posts';
$lang['business_solutions:rss_name_suffix']                = ' Business Solutions';
$lang['business_solutions:rss_category_suffix']            = ' Business Solutions';
$lang['business_solutions:author_name_label']              = 'Author name';
$lang['business_solutions:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'Created on Hour';
$lang['business_solutions:created_minute']                 = 'Created on Minute';
$lang['business_solutions:comments_enabled_label']         = 'Comments Enabled';

// titles
$lang['business_solutions:create_title']                   = 'Add Business Solutions';
$lang['business_solutions:edit_title']                     = 'Edit Business Solutions "%s"';
$lang['business_solutions:archive_title']                 = 'Archive';
$lang['business_solutions:posts_title']					= 'Posts';
$lang['business_solutions:rss_posts_title']				= 'Business Solutions posts for %s';
$lang['business_solutions:business_solutions_title']					= 'Business Solutions';
$lang['business_solutions:list_title']					= 'List Posts';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.';
$lang['business_solutions:no_posts']                      = 'There are no posts.';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']          = 'There are no posts at the moment.';
$lang['business_solutions:post_add_success']            = 'The post "%s" was added.';
$lang['business_solutions:post_add_error']              = 'An error occured.';
$lang['business_solutions:edit_success']                   = 'The post "%s" was updated.';
$lang['business_solutions:edit_error']                     = 'An error occurred.';
$lang['business_solutions:publish_success']                = 'The post "%s" has been published.';
$lang['business_solutions:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['business_solutions:publish_error']                  = 'No posts were published.';
$lang['business_solutions:delete_success']                 = 'The post "%s" has been deleted.';
$lang['business_solutions:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['business_solutions:delete_error']                   = 'No posts were deleted.';
$lang['business_solutions:already_exist_error']            = 'A post with this URL already exists.';

$lang['business_solutions:twitter_posted']                 = 'Posted "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter Error';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
