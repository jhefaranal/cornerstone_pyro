<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Put articles live';
$lang['business_solutions:role_edit_live']	= 'Edit live articles';
$lang['business_solutions:role_delete_live'] 	= 'Delete live articles';