<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                 = 'تدوينة';
$lang['business_solutions:posts']                   = 'تدوينات';

// labels
$lang['business_solutions:posted_label'] 			= 'تاريخ النشر';
$lang['business_solutions:posted_label_alt']			= 'نشر في';
$lang['business_solutions:written_by_label']				= 'كتبها';
$lang['business_solutions:author_unknown']				= 'مجهول';
$lang['business_solutions:keywords_label']				= 'كلمات البحث';
$lang['business_solutions:tagged_label']					= 'موسومة';
$lang['business_solutions:category_label'] 			= 'التصنيف';
$lang['business_solutions:post_label'] 			= 'إرسال';
$lang['business_solutions:date_label'] 			= 'التاريخ';
$lang['business_solutions:date_at']				= 'عند';
$lang['business_solutions:time_label'] 			= 'الوقت';
$lang['business_solutions:status_label'] 			= 'الحالة';
$lang['business_solutions:draft_label'] 			= 'مسودّة';
$lang['business_solutions:live_label'] 			= 'منشور';
$lang['business_solutions:content_label'] 			= 'المُحتوى';
$lang['business_solutions:options_label'] 			= 'خيارات';
$lang['business_solutions:intro_label'] 			= 'المٌقدّمة';
$lang['business_solutions:no_category_select_label'] 		= '-- لاشيء --';
$lang['business_solutions:new_category_label'] 		= 'إضافة تصنيف';
$lang['business_solutions:subscripe_to_rss_label'] 		= 'اشترك في خدمة RSS';
$lang['business_solutions:all_posts_label'] 		= 'جميع التدوينات';
$lang['business_solutions:posts_of_category_suffix'] 	= ' &raquo; التدوينات';
$lang['business_solutions:rss_name_suffix'] 			= ' &raquo; المُدوّنة';
$lang['business_solutions:rss_category_suffix'] 		= ' &raquo; المُدوّنة';
$lang['business_solutions:author_name_label'] 		= 'إسم الكاتب';
$lang['business_solutions:read_more_label'] 			= 'إقرأ المزيد&nbsp;&raquo;';
$lang['business_solutions:created_hour']                  = 'الوقت (الساعة)';
$lang['business_solutions:created_minute']                = 'الوقت (الدقيقة)';
$lang['business_solutions:comments_enabled_label']         = 'إتاحة التعليقات';

// titles
$lang['business_solutions:create_title'] 			= 'إضافة مقال';
$lang['business_solutions:edit_title'] 			= 'تعديل التدوينة "%s"';
$lang['business_solutions:archive_title'] 			= 'الأرشيف';
$lang['business_solutions:posts_title'] 			= 'التدوينات';
$lang['business_solutions:rss_posts_title'] 		= 'تدوينات %s';
$lang['business_solutions:business_solutions_title'] 			= 'المُدوّنة';
$lang['business_solutions:list_title'] 			= 'سرد التدوينات';

// messages
$lang['business_solutions:disabled_after'] 				= 'تم تعطيل التعليقات بعد %s.';
$lang['business_solutions:no_posts'] 			= 'لا يوجد تدوينات.';
$lang['business_solutions:subscripe_to_rss_desc'] 		= 'اطلع على آخر التدوينات مباشرة بالاشتراك بخدمة RSS. يمكنك القيام بذلك من خلال معظم برامج البريد الإلكتروني الشائعة، أو تجربة <a href="http://reader.google.com/">قارئ جوجل</a>.';
$lang['business_solutions:currently_no_posts'] 		= 'لا يوجد تدوينات حالياً.';
$lang['business_solutions:post_add_success'] 		= 'تمت إضافة التدوينة "%s".';
$lang['business_solutions:post_add_error'] 		= 'حدث خطأ.';
$lang['business_solutions:edit_success'] 			= 'تم تحديث التدوينة "%s".';
$lang['business_solutions:edit_error'] 			= 'حدث خطأ.';
$lang['business_solutions:publish_success'] 			= 'تم نشر التدوينة "%s".';
$lang['business_solutions:mass_publish_success'] 		= 'تم نشر التدوينات "%s".';
$lang['business_solutions:publish_error'] 			= 'لم يتم نشر أي تدوينات.';
$lang['business_solutions:delete_success'] 			= 'تم حذف التدوينة "%s".';
$lang['business_solutions:mass_delete_success'] 		= 'تم حذف التدوينات "%s".';
$lang['business_solutions:delete_error'] 			= 'لم يتم حذف أي تدوينات.';
$lang['business_solutions:already_exist_error'] 		= 'يوجد مقال له عنوان URL مطابق.';

$lang['business_solutions:twitter_posted']			= 'منشور في "%s" %s';
$lang['business_solutions:twitter_error'] 			= 'خطأ في تويتر';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";

?>
