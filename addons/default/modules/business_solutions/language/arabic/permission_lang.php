<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live'] = 'نشر التدوينات';
$lang['business_solutions:role_edit_live'] = 'تعديل التدوينات المنشورة';
$lang['business_solutions:role_delete_live'] 	= 'حذف التدوينات المنشورة';
