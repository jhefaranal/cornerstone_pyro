<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                          = 'پست';
$lang['business_solutions:posts']                         = 'پست ها';

// labels
$lang['business_solutions:posted_label']                  = 'ارسال شده';
$lang['business_solutions:posted_label_alt']               = 'ارسال شده';
$lang['business_solutions:written_by_label']		 = 'نوشته شده توسط';
$lang['business_solutions:author_unknown']	  	 = 'نامشخص';
$lang['business_solutions:keywords_label']		 = 'کلمات کلیدی';
$lang['business_solutions:tagged_label']			 = 'تگ ها';
$lang['business_solutions:category_label']                = 'مجموعه';
$lang['business_solutions:post_label']                    = 'پست';
$lang['business_solutions:date_label']                    = 'تاریخ';
$lang['business_solutions:date_at']                       = 'در';
$lang['business_solutions:time_label']                    = 'زمان';
$lang['business_solutions:status_label']                  = 'وضعیت';
$lang['business_solutions:draft_label']                    = 'پیشنویس';
$lang['business_solutions:live_label']                     = 'ارسال شده';
$lang['business_solutions:content_label']                  = 'محتوا';
$lang['business_solutions:options_label']                  = 'تنظیمات';
$lang['business_solutions:intro_label']                    = 'چکیده';
$lang['business_solutions:no_category_select_label']       = '-- هیچیک --';
$lang['business_solutions:new_category_label']             = 'مجموعه جدید';
$lang['business_solutions:subscripe_to_rss_label']         = 'عضویت در RSS';
$lang['business_solutions:all_posts_label']                = 'همه ی  پست ها';
$lang['business_solutions:posts_of_category_suffix']       = ' پست ها';
$lang['business_solutions:rss_name_suffix']                = ' بلاگ';
$lang['business_solutions:rss_category_suffix']            = ' بلاگ';
$lang['business_solutions:author_name_label']              = 'نام نویسنده';
$lang['business_solutions:read_more_label']                = 'مشاهده جزئیات';
$lang['business_solutions:created_hour']                   = 'ایجاد شده در ساعت';
$lang['business_solutions:created_minute']                 = 'ایجاد شده در دقیقه ی ';
$lang['business_solutions:comments_enabled_label']         = 'فعال کردن کامنت ها';

// titles
$lang['business_solutions:create_title']                   = 'پست جدید';
$lang['business_solutions:edit_title']                     = 'ویرایش پست "%s"';
$lang['business_solutions:archive_title']                 = 'آرشیو';
$lang['business_solutions:posts_title']					= 'پست ها';
$lang['business_solutions:rss_posts_title']				= 'ارسال های مربوط به %s';
$lang['business_solutions:business_solutions_title']					= 'بلاگ';
$lang['business_solutions:list_title']					= 'لیست پست ها';

// messages
$lang['business_solutions:disabled_after'] 		= 'ارسال نظرات پس از %s غیر فعال شده است.';
$lang['business_solutions:no_posts']                      = 'هیچ پستی وجود ندارد';
$lang['business_solutions:subscripe_to_rss_desc']          = 'مشترک خبرخوانRSS ما بشوید.';
$lang['business_solutions:currently_no_posts']          = 'در حال حاضر هیچ پستی وجود ندارد.';
$lang['business_solutions:post_add_success']            = '"%s" اضافه شد.';
$lang['business_solutions:post_add_error']              = 'خطایی رخ داده است.';
$lang['business_solutions:edit_success']                   = '"%s" آپدیت شد.';
$lang['business_solutions:edit_error']                     = 'خطایی رخ داده است.';
$lang['business_solutions:publish_success']                = 'پست "%s" منتشر شد.';
$lang['business_solutions:mass_publish_success']           = 'پست های "%s" منتشر شدند.';
$lang['business_solutions:publish_error']                  = 'هیچ پستی منتشر نشد';
$lang['business_solutions:delete_success']                 = 'پست "%s" دلیت شد.';
$lang['business_solutions:mass_delete_success']            = 'پست های "%s" دلیت شدند.';
$lang['business_solutions:delete_error']                   = 'هیچ پستی دلیت نشد.';
$lang['business_solutions:already_exist_error']            = 'همینک  یک پست با این URL موجود است.';

$lang['business_solutions:twitter_posted']                 = 'پست شد "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter خطای';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
