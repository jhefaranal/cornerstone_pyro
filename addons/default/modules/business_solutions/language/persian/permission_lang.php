<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'تغییر یک پست به حالت منتشر شده';
$lang['business_solutions:role_edit_live']        = 'ویرایش پست های منتظر شده';
$lang['business_solutions:role_delete_live'] 	= 'حذف پست های منتشر شده';