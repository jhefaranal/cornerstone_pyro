<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Pubblicare nuovi articoli';
$lang['business_solutions:role_edit_live']	= 'Modificare articoli pubblicati'; 
$lang['business_solutions:role_delete_live'] 	= 'Cancellare articoli pubblicati'; 