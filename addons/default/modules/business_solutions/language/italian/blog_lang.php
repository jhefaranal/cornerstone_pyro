<?php
/* Translation made Nicola Tudino */
/* Date 07/11/2010 */

$lang['business_solutions:post']                 = 'Articolo';
$lang['business_solutions:posts']                   = 'Articoli';

// labels
$lang['business_solutions:posted_label'] 			= 'Pubblicato';
$lang['business_solutions:posted_label_alt']			= 'Pubblicato alle';
$lang['business_solutions:written_by_label']				= 'Scritto da';
$lang['business_solutions:author_unknown']				= 'Sconosciuto';
$lang['business_solutions:keywords_label']				= 'Parole chiave'; 
$lang['business_solutions:tagged_label']					= 'Taggato';
$lang['business_solutions:category_label'] 			= 'Categoria';
$lang['business_solutions:post_label'] 			= 'Articolo';
$lang['business_solutions:date_label'] 			= 'Data';
$lang['business_solutions:date_at']				= 'alle';
$lang['business_solutions:time_label'] 			= 'Ora';
$lang['business_solutions:status_label'] 			= 'Stato';
$lang['business_solutions:draft_label'] 			= 'Bozza';
$lang['business_solutions:live_label'] 			= 'Pubblicato';
$lang['business_solutions:content_label'] 			= 'Contenuto';
$lang['business_solutions:options_label'] 			= 'Opzioni';
$lang['business_solutions:intro_label'] 			= 'Introduzione';
$lang['business_solutions:no_category_select_label'] 		= '-- Nessuna --';
$lang['business_solutions:new_category_label'] 		= 'Aggiungi una categoria';
$lang['business_solutions:subscripe_to_rss_label'] 		= 'Abbonati all\'RSS';
$lang['business_solutions:all_posts_label'] 		= 'Tutti gli articoli';
$lang['business_solutions:posts_of_category_suffix'] 	= ' articoli';
$lang['business_solutions:rss_name_suffix'] 			= ' Notizie';
$lang['business_solutions:rss_category_suffix'] 		= ' Notizie';
$lang['business_solutions:author_name_label'] 		= 'Nome autore';
$lang['business_solutions:read_more_label'] 			= 'Leggi tutto&nbsp;&raquo;';
$lang['business_solutions:created_hour']                  = 'Time (Ora)'; 
$lang['business_solutions:created_minute']                = 'Time (Minuto)';
$lang['business_solutions:comments_enabled_label']         = 'Commenti abilitati';

// titles
$lang['business_solutions:create_title'] 			= 'Aggiungi un articolo';
$lang['business_solutions:edit_title'] 			= 'Modifica l\'articolo "%s"';
$lang['business_solutions:archive_title'] 			= 'Archivio';
$lang['business_solutions:posts_title'] 			= 'Articoli';
$lang['business_solutions:rss_posts_title'] 		= 'Notizie per %s';
$lang['business_solutions:business_solutions_title'] 			= 'Notizie';
$lang['business_solutions:list_title'] 			= 'Elenco articoli';

// messages
$lang['business_solutions:disabled_after'] 				= 'Non sarà più possibile inserire commenti dopo %s.';
$lang['business_solutions:no_posts'] 			= 'Non ci sono articoli.';
$lang['business_solutions:subscripe_to_rss_desc'] 		= 'Ricevi gli articoli subito abbonandoti al nostro feed RSS. Lo puoi fare con i comuni programmi di posta elettronica, altrimenti prova <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts'] 		= 'Non ci sono articoli al momento.';
$lang['business_solutions:post_add_success'] 		= 'L\'articolo "%s" è stato aggiunto.';
$lang['business_solutions:post_add_error'] 		= 'C\'è stato un errore.';
$lang['business_solutions:edit_success'] 			= 'L\'articolo "%s" è stato modificato.';
$lang['business_solutions:edit_error'] 			= 'C\'è stato un errore.';
$lang['business_solutions:publish_success'] 			= 'L\'articolo "%s" è stato pubblicato.';
$lang['business_solutions:mass_publish_success'] 		= 'Gli articoli "%s" sono stati pubblicati.';
$lang['business_solutions:publish_error'] 			= 'Gli articoli non saranno pubblicati.';
$lang['business_solutions:delete_success'] 			= 'L\'articolo "%s" è stato eliminato.';
$lang['business_solutions:mass_delete_success'] 		= 'Gli articoli "%s" sono stati eliminati.';
$lang['business_solutions:delete_error'] 			= 'Nessun articolo è stato eliminato.';
$lang['business_solutions:already_exist_error'] 		= 'Un articolo con questo URL esiste già.';

$lang['business_solutions:twitter_posted']			= 'Inviato "%s" %s';
$lang['business_solutions:twitter_error'] 			= 'Errore per Twitter';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation

?>