<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']  = 'Actualité';
$lang['business_solutions:posts'] = 'Actualités';

// labels
$lang['business_solutions:posted_label']             = 'Publié le';
$lang['business_solutions:posted_label_alt']         = 'Envoyé à';
$lang['business_solutions:written_by_label']         = 'Écrit par';
$lang['business_solutions:author_unknown']           = 'Inconnu';
$lang['business_solutions:keywords_label']           = 'Mots-clés';
$lang['business_solutions:tagged_label']             = 'Taggé';
$lang['business_solutions:category_label']           = 'Catégorie';
$lang['business_solutions:post_label']               = 'Article';
$lang['business_solutions:date_label']               = 'Date';
$lang['business_solutions:date_at']                  = 'le';
$lang['business_solutions:time_label']               = 'Heure';
$lang['business_solutions:status_label']             = 'Statut';
$lang['business_solutions:draft_label']              = 'Brouillon';
$lang['business_solutions:live_label']               = 'Live';
$lang['business_solutions:content_label']            = 'Contenu';
$lang['business_solutions:options_label']            = 'Options';
$lang['business_solutions:intro_label']              = 'Introduction';
$lang['business_solutions:no_category_select_label'] = '-- Aucune --';
$lang['business_solutions:new_category_label']       = 'Ajouter une catégorie';
$lang['business_solutions:subscripe_to_rss_label']   = 'Souscrire au RSS';
$lang['business_solutions:all_posts_label']          = 'Tous les Articles';
$lang['business_solutions:posts_of_category_suffix'] = ' articles';
$lang['business_solutions:rss_name_suffix']          = ' Articles';
$lang['business_solutions:rss_category_suffix']      = ' Articles';
$lang['business_solutions:author_name_label']        = 'Auteur';
$lang['business_solutions:read_more_label']          = 'Lire la suite&nbsp;&raquo;';
$lang['business_solutions:created_hour']             = 'Heure de création';
$lang['business_solutions:created_minute']           = 'Minute de création';
$lang['business_solutions:comments_enabled_label']   = 'Commentaires activés';

// titles
$lang['business_solutions:create_title']    			= 'Créer un article';
$lang['business_solutions:edit_title']      			= 'Modifier l\'article "%s"';
$lang['business_solutions:archive_title']   			= 'Archives';
$lang['business_solutions:posts_title']     			= 'Articles';
$lang['business_solutions:rss_posts_title'] 			= ' pour %s';
$lang['business_solutions:business_solutions_title']      			= 'Articles';
$lang['business_solutions:list_title']      			= 'Lister les posts';

// messages
$lang['business_solutions:disabled_after'] 			= 'Poster des commentaires après  %s a été désactivé.';
$lang['business_solutions:no_posts']              = 'Il n\'y a pas d\'articles.';
$lang['business_solutions:subscripe_to_rss_desc'] = 'Restez informé des derniers articles en temps réel en vous abonnant au flux RSS. Vous pouvez faire cela avec la plupart des logiciels de messagerie, ou essayez <a href="http://www.google.fr/reader">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']    = 'Il n\'y a aucun article actuellement.';
$lang['business_solutions:post_add_success']      = 'L\'article "%s" a été ajouté.';
$lang['business_solutions:post_add_error']        = 'Une erreur est survenue.';
$lang['business_solutions:edit_success']          = 'Le post "%s" a été mis à jour.';
$lang['business_solutions:edit_error']            = 'Une erreur est survenue.';
$lang['business_solutions:publish_success']       = 'Les articles "%s" ont été publiés.';
$lang['business_solutions:mass_publish_success']  = 'Les articles "%s" ont été publiés.';
$lang['business_solutions:publish_error']         = 'Aucun article n\'a été publié.';
$lang['business_solutions:delete_success']        = 'Les articles "%s" ont été supprimés.';
$lang['business_solutions:mass_delete_success']   = 'Les articles "%s" ont été supprimés.';
$lang['business_solutions:delete_error']          = 'Aucun article n\'a été supprimé.';
$lang['business_solutions:already_exist_error']   = 'Un article avec cet URL existe déjà.';

$lang['business_solutions:twitter_posted'] = 'Envoyé "%s" %s';
$lang['business_solutions:twitter_error']  = 'Erreur Twitter';

// date
$lang['business_solutions:archive_date_format'] = "%B %Y";
