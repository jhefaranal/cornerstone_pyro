<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live']		= 'Mettre l\'article en direct';
$lang['business_solutions:role_edit_live']	= 'Modifier les articles en direct';
$lang['business_solutions:role_delete_live'] 	= 'Supprimer les articles en direct';