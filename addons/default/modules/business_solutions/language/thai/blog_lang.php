<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Thai translation.
*
* @author	Nateetorn Lertkhonsan <nateetorn.l@gmail.com>
* @package	PyroCMS  
* @link		http://pyrocms.com
* @date		2012-04-19
* @version	1.0.0
**/

$lang['business_solutions:post']                 = 'Post'; #translate
$lang['business_solutions:posts']                   = 'Posts'; #translate

// labels
$lang['business_solutions:posted_label']                   = 'โพสต์แล้ว';
$lang['business_solutions:posted_label_alt']               = 'โพสต์แล้วที่';
$lang['business_solutions:written_by_label']				= 'เขียนโดย';
$lang['business_solutions:author_unknown']				= 'ไม่ทราบ';
$lang['business_solutions:keywords_label']				= 'คำหลัก';
$lang['business_solutions:tagged_label']					= 'แท็ก';
$lang['business_solutions:category_label']                 = 'หมวดหมู่';
$lang['business_solutions:post_label']                     = 'โพสต์';
$lang['business_solutions:date_label']                     = 'วันที่';
$lang['business_solutions:date_at']                        = 'ที่';
$lang['business_solutions:time_label']                     = 'เวลา';
$lang['business_solutions:status_label']                   = 'สถานะ';
$lang['business_solutions:draft_label']                    = 'ร่างบล็อก';
$lang['business_solutions:live_label']                     = 'ไลฟ์บล็อก';
$lang['business_solutions:content_label']                  = 'เนื้อหา';
$lang['business_solutions:options_label']                  = 'ตัวเลือก';
$lang['business_solutions:intro_label']                    = 'บทนำ';
$lang['business_solutions:no_category_select_label']       = '-- ไม่มี --';
$lang['business_solutions:new_category_label']             = 'เพิ่มหมวดหมู่';
$lang['business_solutions:subscripe_to_rss_label']         = 'สมัครสมาชิก RSS';
$lang['business_solutions:all_posts_label']             = 'โพสต์ทั้งหมด';
$lang['business_solutions:posts_of_category_suffix']    = ' โพสต์';
$lang['business_solutions:rss_name_suffix']                = ' บล็อก';
$lang['business_solutions:rss_category_suffix']            = ' บล็อก';
$lang['business_solutions:author_name_label']              = 'ชื่อผู้เขียน';
$lang['business_solutions:read_more_label']                = 'อ่านเพิ่มเติม&nbsp;&raquo;';
$lang['business_solutions:created_hour']                   = 'สร้างเมื่อชั่วโมงที่แล้ว';
$lang['business_solutions:created_minute']                 = 'สร้างเมื่อนาทีที่แล้ว';
$lang['business_solutions:comments_enabled_label']         = 'ความคิดเห็นถูกเปิดใช้งาน';

// titles
$lang['business_solutions:create_title']                   = 'เพิ่มโพสต์';
$lang['business_solutions:edit_title']                     = 'แก้ไขโพสต์ "%s"';
$lang['business_solutions:archive_title']                 = 'คลัง';
$lang['business_solutions:posts_title']					= 'โพสต์';
$lang['business_solutions:rss_posts_title']				= 'โพสต์บล็อกสำหรับ %s';
$lang['business_solutions:business_solutions_title']					= 'บล็อก';
$lang['business_solutions:list_title']					= 'รายการโพสต์';

// messages
$lang['business_solutions:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['business_solutions:no_posts']                    = 'ไม่มีโพสต์';
$lang['business_solutions:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts']          = 'ไม่มีโพสต์ในขณะนี้';
$lang['business_solutions:post_add_success']            = 'โพสต์ "%s" ถูกเพิ่มแล้ว';
$lang['business_solutions:post_add_error']              = 'เกิดข้อผิดพลาด';
$lang['business_solutions:edit_success']                   = 'ปรับปรุงโพสต์ "%s" แล้ว';
$lang['business_solutions:edit_error']                     = 'เกิดข้อผิดพลาด';
$lang['business_solutions:publish_success']                = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['business_solutions:mass_publish_success']           = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['business_solutions:publish_error']                  = 'ไม่มีโพสต์ถูกเผยแพร่.';
$lang['business_solutions:delete_success']                 = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['business_solutions:mass_delete_success']            = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['business_solutions:delete_error']                   = 'ไม่มีโพสต์ถูกลบ';
$lang['business_solutions:already_exist_error']            = 'โพสต์ลิงค์นี้มีอยู่แล้ว';

$lang['business_solutions:twitter_posted']                 = 'โพสต์ "%s" %s';
$lang['business_solutions:twitter_error']                  = 'Twitter มีข้อผิดพลาด';

// date
$lang['business_solutions:archive_date_format']		= "%B %Y";
