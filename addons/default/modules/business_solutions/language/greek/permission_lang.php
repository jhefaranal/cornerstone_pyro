<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Business_solutions Permissions
$lang['business_solutions:role_put_live'] = 'Να δημοσιεύει άρθρα';
$lang['business_solutions:role_edit_live'] = 'Επεξεργασία δημοσιευμένων άρθρων';
$lang['business_solutions:role_delete_live'] 	= 'Διαγραφή δημοσιευμένων άρθρων';