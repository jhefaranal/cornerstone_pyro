<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['business_solutions:post']                      = 'Ανάρτηση';
$lang['business_solutions:posts']                     = 'Αναρτήσεις';

// labels
$lang['business_solutions:posted_label'] 		= 'Δημοσιεύτηκε';
$lang['business_solutions:posted_label_alt'] 		= 'Δημοσιεύτηκε στις';
$lang['business_solutions:written_by_label'] 		= 'Γράφτηκε από';
$lang['business_solutions:author_unknown'] 		= 'Άγνωστο';
$lang['business_solutions:keywords_label'] 		= 'Λέξεις κλειδιά';
$lang['business_solutions:tagged_label'] 		= 'Ετικέτες';
$lang['business_solutions:category_label'] 		= 'Κατηγορία';
$lang['business_solutions:post_label'] 		= 'Δημοσίευση';
$lang['business_solutions:date_label'] 		= 'Ημερομηνία';
$lang['business_solutions:date_at'] 			= 'στις';
$lang['business_solutions:time_label'] 		= 'Χρόνος';
$lang['business_solutions:status_label'] 		= 'Κατάσταση';
$lang['business_solutions:draft_label'] 		= 'Πρόχειρο';
$lang['business_solutions:live_label'] 		= 'Δημοσιευμένο';
$lang['business_solutions:content_label'] 		= 'Περιεχόμενο';
$lang['business_solutions:options_label'] 		= 'Επιλογές';
$lang['business_solutions:intro_label'] 		= 'Εισαγωγή';
$lang['business_solutions:no_category_select_label'] 	= '-- Καμμία --';
$lang['business_solutions:new_category_label'] 	= 'Προσθήκη κατηγορίας';
$lang['business_solutions:subscripe_to_rss_label'] 	= 'Εγγραφή στο RSS';
$lang['business_solutions:all_posts_label'] 		= 'Όλες οι δημοσιεύσεις';
$lang['business_solutions:posts_of_category_suffix'] 	= ' δημοσιεύσεις';
$lang['business_solutions:rss_name_suffix'] 		= ' Ιστολόγιο';
$lang['business_solutions:rss_category_suffix'] 	= ' Ιστολόγιο';
$lang['business_solutions:author_name_label'] 	= 'Όνομα συγγραφέα';
$lang['business_solutions:read_more_label'] 		= 'Διαβάστε περισσότερα &raquo;';
$lang['business_solutions:created_hour'] 		= 'Ώρα δημιουργίας';
$lang['business_solutions:created_minute'] 		= 'Λεπτό δημιουργίας';
$lang['business_solutions:comments_enabled_label'] 	= 'Σχόλια Ενεργά';

// titles
$lang['business_solutions:create_title'] 		= 'Προσθήκη δημοσίευσης';
$lang['business_solutions:edit_title'] 		= 'Επεξεργασία δημοσίευσης "%s"';
$lang['business_solutions:archive_title'] 		= 'Αρχειοθήκη';
$lang['business_solutions:posts_title'] 		= 'Δημοσιεύσεις';
$lang['business_solutions:rss_posts_title'] 		= 'Δημοσιεύσεις ιστολογίου για %s';
$lang['business_solutions:business_solutions_title'] 		= 'Ιστολόγιο';
$lang['business_solutions:list_title'] 		= 'Λίστα δημοσιεύσεων';

// messages
$lang['business_solutions:disabled_after'] 				= 'Η ανάρτηση σχολίων μετά τις %s έχει απενεργοποιηθεί.';
$lang['business_solutions:no_posts'] 			= 'Δεν υπάρχουν δημοσιεύσεις.';
$lang['business_solutions:subscripe_to_rss_desc'] 	= 'Μπορείτε να λαμβάνετε νεότερες δημοσιεύσεις με το να εγγραφείτε στην τροφοδοσία RSS μας. Μπορείτε να το κάνετε χρησιμοποιώντας τα δημοφιλή προγράμματα e-mail, ή με το <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['business_solutions:currently_no_posts'] 	= 'Προς το παρόν δεν υπάρχουν δημοσιεύσεις.';
$lang['business_solutions:post_add_success'] 		= 'Η δημοσίευση "%s" προστέθηκε.';
$lang['business_solutions:post_add_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['business_solutions:edit_success'] 		= 'Η δημοσίευση "%s" ενημερώθηκε.';
$lang['business_solutions:edit_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['business_solutions:publish_success'] 		= 'Η δημοσίευση "%s" αναρτήθηκε.';
$lang['business_solutions:mass_publish_success'] 	= 'Οι δημοσιεύσεις "%s" αναρτήθηκαν.';
$lang['business_solutions:publish_error'] 		= 'Δεν αναρτήθηκε καμία δημοσίευση.';
$lang['business_solutions:delete_success'] 		= 'Η δημοσίευση "%s" διαγράφηκε.';
$lang['business_solutions:mass_delete_success'] 	= 'Οι δημοσιεύσεις "%s" διαγράφηκαν.';
$lang['business_solutions:delete_error'] 		= 'Δεν διαγράφηκε καμία δημοσίευση.';
$lang['business_solutions:already_exist_error'] 	= 'Υπάρχει ήδη μια δημοσίευση με αυτό το URL.';

$lang['business_solutions:twitter_posted'] 		= 'Δημοσιεύτηκε "%s" %s';
$lang['business_solutions:twitter_error'] 		= 'Σφάλμα Twitter';

// date
$lang['business_solutions:archive_date_format'] 	= "%B %Y";
