<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Show a list of business_solutions categories.
 *
 * @author        Stephen Cozart
 * @author        PyroCMS Dev Team
 * @package       PyroCMS\Core\Modules\Business_solutions\Widgets
 */
class Widget_Business_solutions_categories extends Widgets
{
	public $author = 'Stephen Cozart';

	public $website = 'http://github.com/clip/';

	public $version = '1.0.0';

	public $title = array(
		'en' => 'Business_solutions Categories',
		'br' => 'Categorias do Business_solutions',
		'pt' => 'Categorias do Business_solutions',
		'el' => 'Κατηγορίες Ιστολογίου',
		'fr' => 'Catégories du Business_solutions',
		'ru' => 'Категории Блога',
		'id' => 'Kateori Business_solutions',
            'fa' => 'مجموعه های بلاگ',
	);

	public $description = array(
		'en' => 'Show a list of business_solutions categories',
		'br' => 'Mostra uma lista de navegação com as categorias do Business_solutions',
		'pt' => 'Mostra uma lista de navegação com as categorias do Business_solutions',
		'el' => 'Προβάλει την λίστα των κατηγοριών του ιστολογίου σας',
		'fr' => 'Permet d\'afficher la liste de Catégories du Business_solutions',
		'ru' => 'Выводит список категорий блога',
		'id' => 'Menampilkan daftar kategori tulisan',
            'fa' => 'نمایش لیستی از مجموعه های بلاگ',
	);

	public function run()
	{
		$this->load->model('business_solutions/business_solutions_categories_m');

		$categories = $this->business_solutions_categories_m->order_by('title')->get_all();

		return array('categories' => $categories);
	}

}
