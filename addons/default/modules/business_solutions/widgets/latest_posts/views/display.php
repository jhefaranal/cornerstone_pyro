<ul class="navigation">
	<?php foreach($business_solutions_widget as $post_widget): ?>
		<li><?php echo anchor('business_solutions/'.date('Y/m', $post_widget->created_on) .'/'.$post_widget->slug, $post_widget->title) ?></li>
	<?php endforeach ?>
</ul>