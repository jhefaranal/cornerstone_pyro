<section class="title">
	<h4><?php echo lang('business_solutions:business_solutionss_title') ?></h4>
</section>

<section class="item">
	<div class="content">

	<?php if ($business_solutionss): ?>
	
		<table border="0" class="table-list" cellspacing="0">
			<thead>
			<tr>
				<th><?php echo lang('business_solutions:business_solutions_title') ?></th>
				<th><?php echo lang('global:slug') ?></th>
				<th width="120"></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($business_solutionss as $business_solutions): ?>
			<tr>
				<td><?php echo lang_label($business_solutions->stream_name); ?></td>
				<td><?php echo $business_solutions->business_solutions_uri; ?></td>
				<td>
					<a href="" class="btn">Edit</a>
					<a href="" class="btn">Delete</a>
					<a href="" class="btn">New Post</a>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php echo form_close() ?>

<?php $this->load->view('admin/partials/pagination') ?>

	<?php else: ?>
		<div class="no_data">No business_solutionss</div>
	<?php endif ?>
	</div>
</section>