<?php //echo "<pre>"; print_r($info); echo "</pre>"; ?>

<div class="container">
	<div class="row module-row">
		<div class="col-md-4">
			<img src="<?php echo base_url() ?>files/large/<?php echo $info->logo ?>" height="80px">

			<br/>
			<br/>
			<h2><?php echo $info->title ?></h2>
			<p><?php echo $info->intro ?></p>

			<a href="<?php echo base_url() ?>features/<?php echo $info->slug ?>" class="hide">Read More&nbsp;»</a>
		</div>
		<div class="col-md-8">
			<img src="<?php echo base_url() ?>files/large/<?php echo $info->modal_image ?>" width="100%">
		</div>
	</div>
</div>