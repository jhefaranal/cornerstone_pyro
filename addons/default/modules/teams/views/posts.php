
<div id="teamss" class="container">
	<div class="row" style="padding:20x 0px;">
		<div class="col-md-6">
			<h3 style="margin-top:0px"> <b>DOWNLOADS</b> </h3>
		</div>
		<div class="col-md-3">
			&nbsp;
		</div>
		<div class="col-md-3">
			<select class="form-control pull-right" id="filter_project">
				<option value="0">CHOOSE A PROJECT</option>
				<?php foreach ($categories as $key => $category) { ?>
					<option value="<?php echo $category->slug;?>">
						<?php echo $category->title; ?>
					</option>
				<?php } ?>
			</select>
		</div>
	</div>

	<div id="dload-list">
		<?php if ($posts): ?>
			<?php foreach ($posts as $key => $post) { ?>
				<div class="row dload-post">
					<div class="col-md-3" style="margin-top:5px;">
						<?php echo $post['title']; ?>
					</div>
					<div class="col-md-3 file-info" style="margin-top:5px;">
						<center><?php echo @$post['category']['title']; ?></center>
					</div>
					<div class="col-md-3 file-info" style="margin-top:5px;">
						<center><?php echo $post['file_type']; ?></center>
					</div>
					
					<div class="col-md-3 file-info">
						<?php echo $post['file']['ext']; ?>
						<?php echo $post['file']['filesize']; ?> KB
						<a target="_blank" href="<?php 
						if (strtoupper($post['file_type']) == "IMAGE" || strtoupper($post['file_type']) == "DOCUMENT")
							echo str_replace('teams','large',$post['file']['file']); 
						else
							echo $post['file']['file']; 
						?>">
							<img src="addons/default/themes/mandaluyong/img/dload.png">
						</a>
					</div>
				</div>
			<?php } ?>
		<?php else: ?>
			<div class="alert-info alert" style="margin-top: 10px;">Sorry, no results found.</div>
		<?php endif; ?>
	</div>

</div>
<style type="text/css">
	
</style>
<script type="text/javascript">
	$(document).ready(function(){
		$('.dload-post').click(function(){
			$('.dload-post').removeClass('on');
			$(this).addClass('on');
		});
	});
</script>
<style type="text/css">
	.dload-post {
		border: 2px solid #f2f2f7;
		padding: 5px;
		font-size: 13px;
		margin-bottom: 10px;
		cursor: pointer;
		margin-top: 10px;
	}
	.file-info{
		color:#a1a0a0;
		text-align: right;
		text-transform: uppercase;
	}
	.dload-post.on{
		background-color: #f2f2f7
	}
</style>