<style type="text/css">
	.losange {
	    transform: rotate(30deg) translateY(10px);
	    border-radius: 20px;
	}

	.losange, .losange div {
	    margin: 0 auto;
	    transform-origin: 50% 50%;
	    overflow: hidden;
	    width: 200px;
    	height: 200px;
	}

	.losange .los1 {
        width: 300px;
	    height: 300px;
	    transform: rotate(-30deg)translateY(-55px)translateX(-36px);
	}

	.losange .los1 img {
	    width: 65%;
	    height: auto;
	}
</style>

<div class="row text-center" style="padding-top:25px;">
	<?php foreach ($teams as $key => $team): ?>
		<div class="col-md-4 wow fadeInUp" data-wow-duration="<?php echo $key+1; ?>" style="height:620px !important;">
			<?php if (empty($team['profile_picture'])): ?>
				<div id="imgTeamMember"></div>
			<?php else: ?>
				<div class="losange">
					<div class="los1">
						<img src="{{ url:site }}files/large/<?php echo $team['profile_picture'] ?>">
					</div>
				</div>
			<?php endif ?>
			
			<p style="margin-top:70px;">
				<span style="font-style:italic; height:200px;">"<?php echo $team['intro'] ?>"</span>
				<br/>
				<br/><small style="font-weight:600; color:#444;"><?php echo $team['title'] ?></small>
				<br/><small><?php echo $team['position'] ?></small>
			</p>
		</div>
	<?php endforeach ?>
</div>