<fieldset id="filters">
	<legend><?php echo lang('global:filters') ?></legend>

	<?php echo form_open('', '', array('f_module' => $module_details['slug'])) ?>
		<ul>
			<li class="">
        		<label for="f_status"><?php echo lang('teams:status_label') ?></label>
        		<?php echo form_dropdown('f_status', array(0 => lang('global:select-all'), 'draft'=>lang('teams:draft_label'), 'live'=>lang('teams:live_label'))) ?>
    		</li>

			<li class="">
        		<label for="f_category"><?php echo lang('teams:category_label') ?></label>
       			<?php echo form_dropdown('f_category', array(0 => lang('global:select-all')) + $categories) ?>
    		</li>

			<li class="">
				<label for="f_category"><?php echo lang('global:keywords') ?></label>
				<?php echo form_input('f_keywords', '', 'style="width: 55%;"') ?>
			</li>

			<li>
				<label for="no_pagination">
				<?php echo form_checkbox('no_pagination',1,false,'id="no_pagination" style="vertical-align: middle;margin-top: 2px"'); ?>
				&nbsp;Disable Pagination</label>
				<small>(Useful when reordering posts)</small>
			</li>

			<li class="">
				<?php echo anchor(current_url() . '#', lang('buttons:cancel'), 'class="button red"') ?>
			</li>
		</ul>
	<?php echo form_close() ?>
</fieldset>

<script>
	$(function() {
		$(document).on('change', '[name="no_pagination"]',function() {
			// trigger other filter to run form submit logic
			$('[name="f_category"]').trigger('change');
		});
	});
</script>
