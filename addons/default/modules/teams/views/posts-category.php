<?php if ($posts): ?>
	<?php foreach ($posts as $key => $post) { ?>
		<div class="row dload-post">
			<div class="col-md-3" style="margin-top:5px;">
				<?php echo $post['title']; ?>
			</div>
			<div class="col-md-3 file-info" style="margin-top:5px;">
				<center><?php echo @$post['category']['title']; ?></center>
			</div>
			<div class="col-md-3 file-info" style="margin-top:5px;">
				<center><?php echo $post['file_type']; ?></center>
			</div>
			
			<div class="col-md-3 file-info">
				<?php echo $post['file']['ext']; ?>
				<?php echo $post['file']['filesize']; ?> KB
				<a target="_blank" href="<?php echo $post['file']['file']; ?>">
					<img src="addons/default/themes/mandaluyong/img/dload.png">
				</a>
			</div>
		</div>
	<?php } ?>
<?php else: ?>
	<div class="alert-info alert" style="margin-top: 10px;">Sorry, no results found.</div>
<?php endif; ?>