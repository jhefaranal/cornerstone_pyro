<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Sample Events Class
*
* @package 		PyroCMS
* @subpackage 	Social Module
* @category 	teamss
* @author 		PyroCMS Dev Team
*/
class Events_Teams
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();

        // Load the search index model
        $this->ci->load->model('search/search_index_m');

		// Post a blog to twitter and whatnot
        Events::register('teams_published', array($this, 'index_teams'));
        Events::register('teams_updated', array($this, 'index_teams'));
        Events::register('teams_deleted', array($this, 'drop_teams'));

    }
    
    public function index_teams($id)
    {
    	$this->ci->load->model('teams/teams_m');

    	$post = $this->ci->teams_m->get($id);

    	// Only index live articles
    	if ($post->status === 'live')
    	{
    		$this->ci->search_index_m->index(
    			'teams', 
    			'teams:post', 
    			'teams:posts', 
    			$id,
    			'food-and-merchandise#ads-food-and-merch',
    			$post->title,
    			$post->body, 
    			array(
    				'cp_edit_uri' 	=> 'admin/teams/edit/'.$id,
    				'cp_delete_uri' => 'admin/teams/delete/'.$id,
    				'keywords' 		=> $post->keywords,
    			)
    		);
    	}
    	// Remove draft articles
    	else
    	{
    		$this->ci->search_index_m->drop_index('teams', 'teams:post', $id);
    	}
	}

    public function drop_teams($ids)
    {
    	foreach ($ids as $id)
    	{
			$this->ci->search_index_m->drop_index('teams', 'teams:post', $id);
		}
	}

}

/* End of file teamss.php */