<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Show a list of teams categories.
 *
 * @author        Stephen Cozart
 * @author        PyroCMS Dev Team
 * @package       PyroCMS\Core\Modules\Teams\Widgets
 */
class Widget_Download_categories extends Widgets
{
	public $author = 'Stephen Cozart';

	public $website = 'http://github.com/clip/';

	public $version = '1.0.0';

	public $title = array(
		'en' => 'Teams Categories',
		'br' => 'Categorias do Teams',
		'pt' => 'Categorias do Teams',
		'el' => 'Κατηγορίες Ιστολογίου',
		'fr' => 'Catégories du Teams',
		'ru' => 'Категории Блога',
		'id' => 'Kateori Teams',
            'fa' => 'مجموعه های بلاگ',
	);

	public $description = array(
		'en' => 'Show a list of teams categories',
		'br' => 'Mostra uma lista de navegação com as categorias do Teams',
		'pt' => 'Mostra uma lista de navegação com as categorias do Teams',
		'el' => 'Προβάλει την λίστα των κατηγοριών του ιστολογίου σας',
		'fr' => 'Permet d\'afficher la liste de Catégories du Teams',
		'ru' => 'Выводит список категорий блога',
		'id' => 'Menampilkan daftar kategori tulisan',
            'fa' => 'نمایش لیستی از مجموعه های بلاگ',
	);

	public function run()
	{
		$this->load->model('teams/teams_categories_m');

		$categories = $this->teams_categories_m->order_by('title')->get_all();

		return array('categories' => $categories);
	}

}
