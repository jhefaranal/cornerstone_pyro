<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * PyroCMS
 * Русский перевод от Dark Preacher - dark[at]darklab.ru
 *
 * @package		PyroCMS
 * @author		Dark Preacher
 * @link			http://pyrocms.com
 */

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// подписи
$lang['teams:posted_label']									= 'Дата';
$lang['teams:posted_label_alt']							= 'Дата добавления';
$lang['teams:written_by_label']							= 'Автор';
$lang['teams:author_unknown']								= 'Неизвестно';
$lang['teams:keywords_label']				= 'Keywords'; #translate
$lang['teams:tagged_label']					= 'Tagged'; #translate
$lang['teams:category_label']								= 'Категория';
$lang['teams:post_label']										= 'Заголовок';
$lang['teams:date_label']										= 'Дата';
$lang['teams:date_at']												= 'в';
$lang['teams:time_label']										= 'Время';
$lang['teams:status_label']									= 'Статус';
$lang['teams:draft_label']										= 'Черновик';
$lang['teams:live_label']										= 'Опубликовано';
$lang['teams:content_label']									= 'Содержание';
$lang['teams:options_label']									= 'Опции';
$lang['teams:intro_label']										= 'Анонс';
$lang['teams:no_category_select_label']			= '-- нет --';
$lang['teams:new_category_label']						= 'Создать категорию';
$lang['teams:subscripe_to_rss_label']				= 'Подписаться на RSS';
$lang['teams:all_posts_label']								= 'Все статьи';
$lang['teams:posts_of_category_suffix']			= ' статьи';
$lang['teams:rss_name_suffix']								= ' Блог';
$lang['teams:rss_category_suffix']						= ' Блог';
$lang['teams:author_name_label']							= 'Автор';
$lang['teams:read_more_label']								= 'читать целиком&nbsp;&raquo;';
$lang['teams:created_hour']									= 'Время (Час)';
$lang['teams:created_minute']								= 'Время (Минута)';
$lang['teams:comments_enabled_label']				= 'Комментарии разрешены';

// заголовки
$lang['teams:create_title']									= 'Создать статью';
$lang['teams:edit_title']										= 'Редактирование статьи "%s"';
$lang['teams:archive_title']									= 'Архив';
$lang['teams:posts_title']										= 'Статьи';
$lang['teams:rss_posts_title']								= 'Статьи из %s';
$lang['teams:teams_title']										= 'Блог';
$lang['teams:list_title']										= 'Список статей';

// сообщения
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']											= 'Статьи отсутствуют.';
$lang['teams:subscripe_to_rss_desc']					= 'Получайте статьи сразу после их публикации, подпишитесь на нашу ленту новостей. Вы можете сделать это с помощью самых популярных программ для чтения электронных писем, или попробуйте <a href="http://reader.google.ru/">Google Reader</a>.';
$lang['teams:currently_no_posts']						= 'В данный момент новости отсутствуют.';
$lang['teams:post_add_success']							= 'Статья "%s" добавлена.';
$lang['teams:post_add_error']								= 'Во время добавления статьи произошла ошибка.';
$lang['teams:edit_success']									= 'Статья "%s" сохранена.';
$lang['teams:edit_error']										= 'Во время сохранения статьи произошла ошибка.';
$lang['teams:publish_success']								= 'Статья "%s" опубликована.';
$lang['teams:mass_publish_success']					= 'Статьи "%s" опубликованы.';
$lang['teams:publish_error']									= 'Во время публикации статьи произошла ошибка.';
$lang['teams:delete_success']								= 'Статья "%s" удалена.';
$lang['teams:mass_delete_success']						= 'Статьи "%s" удалены.';
$lang['teams:delete_error']									= 'Во время удаления статьи произошла ошибка.';
$lang['teams:already_exist_error']						= 'Статья с данным адресом URL уже существует.';

$lang['teams:twitter_posted']								= 'Добавлен "%s" %s';
$lang['teams:twitter_error']									= 'Ошибка Twitter\'а';

// дата
$lang['teams:archive_date_format']						= "%B %Y"; #see php strftime documentation

/* End of file teams_lang.php */