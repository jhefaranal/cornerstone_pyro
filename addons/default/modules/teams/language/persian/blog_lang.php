<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                          = 'پست';
$lang['teams:posts']                         = 'پست ها';

// labels
$lang['teams:posted_label']                  = 'ارسال شده';
$lang['teams:posted_label_alt']               = 'ارسال شده';
$lang['teams:written_by_label']		 = 'نوشته شده توسط';
$lang['teams:author_unknown']	  	 = 'نامشخص';
$lang['teams:keywords_label']		 = 'کلمات کلیدی';
$lang['teams:tagged_label']			 = 'تگ ها';
$lang['teams:category_label']                = 'مجموعه';
$lang['teams:post_label']                    = 'پست';
$lang['teams:date_label']                    = 'تاریخ';
$lang['teams:date_at']                       = 'در';
$lang['teams:time_label']                    = 'زمان';
$lang['teams:status_label']                  = 'وضعیت';
$lang['teams:draft_label']                    = 'پیشنویس';
$lang['teams:live_label']                     = 'ارسال شده';
$lang['teams:content_label']                  = 'محتوا';
$lang['teams:options_label']                  = 'تنظیمات';
$lang['teams:intro_label']                    = 'چکیده';
$lang['teams:no_category_select_label']       = '-- هیچیک --';
$lang['teams:new_category_label']             = 'مجموعه جدید';
$lang['teams:subscripe_to_rss_label']         = 'عضویت در RSS';
$lang['teams:all_posts_label']                = 'همه ی  پست ها';
$lang['teams:posts_of_category_suffix']       = ' پست ها';
$lang['teams:rss_name_suffix']                = ' بلاگ';
$lang['teams:rss_category_suffix']            = ' بلاگ';
$lang['teams:author_name_label']              = 'نام نویسنده';
$lang['teams:read_more_label']                = 'مشاهده جزئیات';
$lang['teams:created_hour']                   = 'ایجاد شده در ساعت';
$lang['teams:created_minute']                 = 'ایجاد شده در دقیقه ی ';
$lang['teams:comments_enabled_label']         = 'فعال کردن کامنت ها';

// titles
$lang['teams:create_title']                   = 'پست جدید';
$lang['teams:edit_title']                     = 'ویرایش پست "%s"';
$lang['teams:archive_title']                 = 'آرشیو';
$lang['teams:posts_title']					= 'پست ها';
$lang['teams:rss_posts_title']				= 'ارسال های مربوط به %s';
$lang['teams:teams_title']					= 'بلاگ';
$lang['teams:list_title']					= 'لیست پست ها';

// messages
$lang['teams:disabled_after'] 		= 'ارسال نظرات پس از %s غیر فعال شده است.';
$lang['teams:no_posts']                      = 'هیچ پستی وجود ندارد';
$lang['teams:subscripe_to_rss_desc']          = 'مشترک خبرخوانRSS ما بشوید.';
$lang['teams:currently_no_posts']          = 'در حال حاضر هیچ پستی وجود ندارد.';
$lang['teams:post_add_success']            = '"%s" اضافه شد.';
$lang['teams:post_add_error']              = 'خطایی رخ داده است.';
$lang['teams:edit_success']                   = '"%s" آپدیت شد.';
$lang['teams:edit_error']                     = 'خطایی رخ داده است.';
$lang['teams:publish_success']                = 'پست "%s" منتشر شد.';
$lang['teams:mass_publish_success']           = 'پست های "%s" منتشر شدند.';
$lang['teams:publish_error']                  = 'هیچ پستی منتشر نشد';
$lang['teams:delete_success']                 = 'پست "%s" دلیت شد.';
$lang['teams:mass_delete_success']            = 'پست های "%s" دلیت شدند.';
$lang['teams:delete_error']                   = 'هیچ پستی دلیت نشد.';
$lang['teams:already_exist_error']            = 'همینک  یک پست با این URL موجود است.';

$lang['teams:twitter_posted']                 = 'پست شد "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter خطای';

// date
$lang['teams:archive_date_format']		= "%B %Y";
