<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'تغییر یک پست به حالت منتشر شده';
$lang['teams:role_edit_live']        = 'ویرایش پست های منتظر شده';
$lang['teams:role_delete_live'] 	= 'حذف پست های منتشر شده';