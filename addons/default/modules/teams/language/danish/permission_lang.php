<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Læg artikler online';
$lang['teams:role_edit_live']	= 'Redigér online artikler';
$lang['teams:role_delete_live'] 	= 'Slet online artikler';