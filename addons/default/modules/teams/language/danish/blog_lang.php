<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']                   = 'Opslået';
$lang['teams:posted_label_alt']               = 'Opslået på';
$lang['teams:written_by_label']				= 'Skrevet af';
$lang['teams:author_unknown']				= 'Ukendt';
$lang['teams:keywords_label']				= 'Keywords'; #translate
$lang['teams:tagged_label']					= 'Tagged'; #translate
$lang['teams:category_label']                 = 'Kategori';
$lang['teams:post_label']                     = 'Opslå';
$lang['teams:date_label']                     = 'Dato';
$lang['teams:date_at']                        = 'på';
$lang['teams:time_label']                     = 'Tid';
$lang['teams:status_label']                   = 'Status';
$lang['teams:draft_label']                    = 'Udkast';
$lang['teams:live_label']                     = 'Live';
$lang['teams:content_label']                  = 'Indhold';
$lang['teams:options_label']                  = 'Indstillinger';
$lang['teams:intro_label']                    = 'Introduktion';
$lang['teams:no_category_select_label']       = '-- Ingen --';
$lang['teams:new_category_label']             = 'Tilføj en kategori';
$lang['teams:subscripe_to_rss_label']         = 'Abonnér på RSS';
$lang['teams:all_posts_label']             = 'Alle opslag';
$lang['teams:posts_of_category_suffix']    = ' opslag';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Forfatter';
$lang['teams:read_more_label']                = 'Læs mere&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Oprettet on time';
$lang['teams:created_minute']                 = 'Oprettet on minut';
$lang['teams:comments_enabled_label']         = 'Kommentarer aktiveret';

// titles
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:create_title']                   = 'Tilføj opslag';
$lang['teams:edit_title']                     = 'Redigér opslag "%s"';
$lang['teams:archive_title']                  = 'Arkivér';
$lang['teams:posts_title']                 = 'Opslag';
$lang['teams:rss_posts_title']             = 'Teams-opslag for %s';
$lang['teams:teams_title']                     = 'Teams';
$lang['teams:list_title']                     = 'Liste opslag';

// messages
$lang['teams:no_posts']                    = 'Der er ingen opslag.';
$lang['teams:subscripe_to_rss_desc']          = 'Få opslag med det same ved at abonnere på vores RSS feed. Dette kan du gøre dette via de mest populære e-mail klienter, eller du kan prøve <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'Der er i øjeblikket ingen opslag';
$lang['teams:post_add_success']            = 'Opslaget "%s" er tilføjet.';
$lang['teams:post_add_error']              = 'Der opstod en fejl.';
$lang['teams:edit_success']                   = 'Opslaget "%s" er opdateret.';
$lang['teams:edit_error']                     = 'Der opstod en fejl.';
$lang['teams:publish_success']                = 'Opslaget "%s" er blevet publiceret.';
$lang['teams:mass_publish_success']           = 'Opslagene "%s" er blevet publiceret.';
$lang['teams:publish_error']                  = 'Ingen opslag er blevet publiceret.';
$lang['teams:delete_success']                 = 'Opslaget "%s" er slettet.';
$lang['teams:mass_delete_success']            = 'Opslagene "%s" er slettet.';
$lang['teams:delete_error']                   = 'Ingen opslag er blevet slettet.';
$lang['teams:already_exist_error']            = 'Et opslag med denne URL findes allerede.';

$lang['teams:twitter_posted']                 = 'Publiceret "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter fejl';

// date
$lang['teams:archive_date_format']		= "%B %Y";
