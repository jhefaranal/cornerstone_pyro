<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                           = 'Bejegyzés';
$lang['teams:posts']                          = 'Bejegyzések';

// labels
$lang['teams:posted_label']                   = 'Bejegyezve';
$lang['teams:posted_label_alt']               = 'Bejegyezve';
$lang['teams:written_by_label']               = 'Írta';
$lang['teams:author_unknown']                 = 'Ismeretlen';
$lang['teams:keywords_label']                 = 'Kulcsszavak';
$lang['teams:tagged_label']                   = 'Tagged';
$lang['teams:category_label']                 = 'Kategória';
$lang['teams:post_label']                     = 'Bejegyzés';
$lang['teams:date_label']                     = 'Dátum';
$lang['teams:date_at']                        = '';
$lang['teams:time_label']                     = 'Idő';
$lang['teams:status_label']                   = 'Állapot';
$lang['teams:draft_label']                    = 'Piszkozat';
$lang['teams:live_label']                     = 'Élő';
$lang['teams:content_label']                  = 'Tartalom';
$lang['teams:options_label']                  = 'Beállítások';
$lang['teams:intro_label']                    = 'Bevezető';
$lang['teams:no_category_select_label']       = '-- Nincs --';
$lang['teams:new_category_label']             = 'Kategória hozzáadása';
$lang['teams:subscripe_to_rss_label']         = 'Feliratkozás RSS-re';
$lang['teams:all_posts_label']                = 'Összes bejegyzés';
$lang['teams:posts_of_category_suffix']       = ' bejegyzések';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Szerző neve';
$lang['teams:read_more_label']                = 'Tovább&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Létrehozva pár órája';
$lang['teams:created_minute']                 = 'Létrehozva pár perce';
$lang['teams:comments_enabled_label']         = 'Hozzászólás engedélyezése';

// titles
$lang['teams:create_title']                   = 'Bejegyzés hozzáadása';
$lang['teams:edit_title']                     = 'A(z) "%s" bejegyzés szerkesztése';
$lang['teams:archive_title']                  = 'Archívum';
$lang['teams:posts_title']                    = 'Bejegyzések';
$lang['teams:rss_posts_title']                = 'Teams bejegyzés %s-ért';
$lang['teams:teams_title']                     = 'Teams';
$lang['teams:list_title']                     = 'Bejegyzések listája';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']                       = 'Nincs bejegyzés.';
$lang['teams:subscripe_to_rss_desc']          = 'A bejegyzést jelentesd meg saját RSS-en. Ezt meg lehet tenni az ismertebb levelezőkkel vagy a <a href="http://reader.google.co.uk/">Google Reader</a>rel.';
$lang['teams:currently_no_posts']             = 'Ebben a pillanatban még nincs bejegyzés.';
$lang['teams:post_add_success']               = 'A(z) "%s" bejegyzés sikeresen hozzáadva.';
$lang['teams:post_add_error']                 = 'A bejegyzés hozzáadása sikertelen.';
$lang['teams:edit_success']                   = 'A(z) "%s" bejegyzés sikeresen módosítva.';
$lang['teams:edit_error']                     = 'A bejegyzés módosítása sikertelen.';
$lang['teams:publish_success']                = 'A(z) "%s" bejegyzés közzétéve.';
$lang['teams:mass_publish_success']           = 'A(z) "%s" bejegyzés már közzétéve.';
$lang['teams:publish_error']                  = 'Nincs bejegyzés közzétéve.';
$lang['teams:delete_success']                 = 'A(z) "%s" bejegyzés törölve.';
$lang['teams:mass_delete_success']            = 'A(z) "%s" bejegyzés már törölve.';
$lang['teams:delete_error']                   = 'Nincs törölhető bejegyzés.';
$lang['teams:already_exist_error']            = 'Egy bejegyzés már létezik ezzel a hivatkozással.';

$lang['teams:twitter_posted']                 = 'Bejegyezve "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter Hiba';

// date
$lang['teams:archive_date_format']            = "%B %Y";
