<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Bejegyzés élővé tétele';
$lang['teams:role_edit_live']            = 'Élő bejegyzés szerkesztése';
$lang['teams:role_delete_live']          = 'Élő bejegyzés törlése';