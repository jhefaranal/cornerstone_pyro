<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                          = 'Įrašas';
$lang['teams:posts']                         = 'Įrašai';

// labels
$lang['teams:posted_label']                  = 'Paskelbta';
$lang['teams:posted_label_alt']              = 'Paskelbta...';
$lang['teams:written_by_label']              = 'Autorius';
$lang['teams:author_unknown']				= 'Nežinomas';
$lang['teams:keywords_label']				= 'Raktažodžiai';
$lang['teams:tagged_label']					= 'Žymėtas';
$lang['teams:category_label']                = 'Kategorija';
$lang['teams:post_label']                    = 'Įrašas';
$lang['teams:date_label']                    = 'Data';
$lang['teams:date_at']                       = '';
$lang['teams:time_label']                    = 'Laikas';
$lang['teams:status_label']                  = 'Statusas';
$lang['teams:draft_label']                   = 'Projektas';
$lang['teams:live_label']                    = 'Gyvai';
$lang['teams:content_label']                 = 'Turinys';
$lang['teams:options_label']                 = 'Funkcijos';
$lang['teams:title_label']                   = 'Pavadinimas';
$lang['teams:slug_label']                    = 'URL';
$lang['teams:intro_label']                   = 'Įžanga';
$lang['teams:no_category_select_label']      = '-- Nėra --';
$lang['teams:new_category_label']            = 'Pridėti kategoriją';
$lang['teams:subscripe_to_rss_label']        = 'Prenumeruoti RSS';
$lang['teams:all_posts_label']               = 'Visi įrašai';
$lang['teams:posts_of_category_suffix']      = ' įrašai';
$lang['teams:rss_name_suffix']               = ' Naujienos';
$lang['teams:rss_category_suffix']           = ' Naujienos';
$lang['teams:author_name_label']             = 'Autoriaus vardas';
$lang['teams:read_more_label']               = 'Plačiau&nbsp;&raquo;';
$lang['teams:created_hour']                  = 'Sukurta valandą';
$lang['teams:created_minute']                = 'Sukurta minutę';
$lang['teams:comments_enabled_label']        = 'Įjungti komentarus?';

// titles
$lang['teams:create_title']                  = 'Pridėti įrašą';
$lang['teams:edit_title']                    = 'Redaguoti įrašą "%s"';
$lang['teams:archive_title']                 = 'Archyvas';
$lang['teams:posts_title']                   = 'Įrašai';
$lang['teams:rss_posts_title']               = 'Naujienų įrašai %s';
$lang['teams:teams_title']                    = 'Naujiena';
$lang['teams:list_title']                    = 'Įrašų sąrašas';

// messages
$lang['teams:disabled_after'] 				= 'Paskelbti komentarai po %s buvo atjungti.';
$lang['teams:no_posts']                      = 'Nėra įrašų.';
$lang['teams:subscripe_to_rss_desc']         = 'Gaukite žinutes iš karto prenumeruodami į RSS kanalą. Jūs galite tai padaryti per populiariausias elektroninio pašto paskyras, arba bandykite <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']            = 'Šiuo metu nėra įrašų.';
$lang['teams:post_add_success']              = 'Įrašas "%s" pridėtas.';
$lang['teams:post_add_error']                = 'Įvyko klaida.';
$lang['teams:edit_success']                  = 'Įrašas "%s" atnaujintas.';
$lang['teams:edit_error']                    = 'Įvyko klaida.';
$lang['teams:publish_success']               = 'Įrašas "%s" paskelbtas.';
$lang['teams:mass_publish_success']          = 'Įrašai "%s" pasklbti.';
$lang['teams:publish_error']                 = 'Nėra paskelbtų įrašų.';
$lang['teams:delete_success']                = 'Įrašas "%s" ištrintas.';
$lang['teams:mass_delete_success']           = 'Įrašai "%s" ištrinti.';
$lang['teams:delete_error']                  = 'Nėra ištrintų įrašų.';
$lang['teams:already_exist_error']           = 'Įrašas su šiuo URL jau egzistuoja.';

$lang['teams:twitter_posted']                = 'Įrašyta "%s" %s';
$lang['teams:twitter_error']                 = 'Twitter nepasiekiamas';

// date
$lang['teams:archive_date_format']           = "%B %Y";
