<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']                   = 'Posted';
$lang['teams:posted_label_alt']               = 'Posted at';
$lang['teams:written_by_label']				= 'Written by'; #translate
$lang['teams:author_unknown']				= 'Unknown'; #translate
$lang['teams:keywords_label']				= 'Keywords'; #translate
$lang['teams:tagged_label']					= 'Tagged'; #translate
$lang['teams:category_label']                 = 'Category';
$lang['teams:post_label']                     = 'Post';
$lang['teams:date_label']                     = 'Date';
$lang['teams:date_at']                        = 'at';
$lang['teams:time_label']                     = 'Time';
$lang['teams:status_label']                   = 'Status';
$lang['teams:draft_label']                    = 'Draft';
$lang['teams:live_label']                     = 'Live';
$lang['teams:content_label']                  = 'Content';
$lang['teams:options_label']                  = 'Options';
$lang['teams:intro_label']                    = 'Introduction';
$lang['teams:no_category_select_label']       = '-- None --';
$lang['teams:new_category_label']             = 'Add a category';
$lang['teams:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['teams:all_posts_label']             = 'All posts';
$lang['teams:posts_of_category_suffix']    = ' posts';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Author name';
$lang['teams:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Created on Hour';
$lang['teams:created_minute']                 = 'Created on Minute';

// titles
$lang['teams:create_title']                   = 'הוספ פוסט';
$lang['teams:edit_title']                     = 'ערוך פוסט "%s"';
$lang['teams:archive_title']                  = 'ארכיון';
$lang['teams:posts_title']                 = 'פוסטים';
$lang['teams:rss_posts_title']             = 'Teams posts for %s';
$lang['teams:teams_title']                     = 'בלוג';
$lang['teams:list_title']                     = 'רשימת הפוסטים';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']                    = 'אין פוסטים.';
$lang['teams:subscripe_to_rss_desc']          = 'קבל פוסטי ישירות ע"י הרשמה לRSS שלנו. ניתןלעשות זאת בעזרת <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'כרגע אין פוסטים.';
$lang['teams:post_add_success']            = 'הפוסט "%s" הוסף בהצלחה.';
$lang['teams:post_add_error']              = 'התרחשה שגיעה.';
$lang['teams:edit_success']                   = 'The post "%s" was updated.';
$lang['teams:edit_error']                     = 'התרחשה שגיעה.';
$lang['teams:publish_success']                = 'The post "%s" has been published.';
$lang['teams:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['teams:publish_error']                  = 'No posts were published.';
$lang['teams:delete_success']                 = 'The post "%s" has been deleted.';
$lang['teams:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['teams:delete_error']                   = 'No posts were deleted.';
$lang['teams:already_exist_error']            = 'An post with this URL already exists.';

$lang['teams:twitter_posted']                 = 'Posted "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter Error';

// date
$lang['teams:archive_date_format']		= "%B %Y";
