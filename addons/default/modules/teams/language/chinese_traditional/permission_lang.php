<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= '將文章上線';
$lang['teams:role_edit_live']	= '編輯上線文章';
$lang['teams:role_delete_live'] 	= '刪除上線文章';