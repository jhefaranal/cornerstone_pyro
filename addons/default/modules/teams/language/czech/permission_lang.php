<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Publikování příspěvků';
$lang['teams:role_edit_live']	= 'Úpravy publikovaných příspěvků';
$lang['teams:role_delete_live'] 	= 'Mazání publikovaných příspěvků';