<?php

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label'] 			= 'Odesláno';
$lang['teams:posted_label_alt']			= 'Odesláno';
$lang['teams:written_by_label']				= 'Written by'; #translate
$lang['teams:author_unknown']				= 'Unknown'; #translate
$lang['teams:keywords_label']				= 'Keywords'; #translate
$lang['teams:tagged_label']					= 'Tagged'; #translate
$lang['teams:category_label'] 			= 'Kategorie';
$lang['teams:post_label'] 			= 'Odeslat';
$lang['teams:date_label'] 			= 'Datum';
$lang['teams:date_at']				= 'v';
$lang['teams:time_label'] 			= 'Čas';
$lang['teams:status_label'] 			= 'Stav';
$lang['teams:draft_label'] 			= 'Koncept';
$lang['teams:live_label'] 			= 'Publikováno';
$lang['teams:content_label'] 			= 'Obsah';
$lang['teams:options_label'] 			= 'Možnosti';
$lang['teams:intro_label'] 			= 'Úvod';
$lang['teams:no_category_select_label'] 		= '-- Nic --';
$lang['teams:new_category_label'] 		= 'Přidat kategorii';
$lang['teams:subscripe_to_rss_label'] 		= 'Přihlásit se k odběru RSS';
$lang['teams:all_posts_label'] 		= 'Všechny články';
$lang['teams:posts_of_category_suffix'] 	= ' články';
$lang['teams:rss_name_suffix'] 			= ' Novinky';
$lang['teams:rss_category_suffix'] 		= ' Novinky';
$lang['teams:author_name_label'] 		= 'Autor';
$lang['teams:read_more_label'] 			= 'Čtěte dále';
$lang['teams:created_hour']                      = 'Vytvořeno v hodině';
$lang['teams:created_minute']                    = 'Vytvořeno v minutě';
$lang['teams:comments_enabled_label']         = 'Komentáře povoleny';

// titles
$lang['teams:create_title'] 			= 'Přidat článek';
$lang['teams:edit_title'] 			= 'Upravit článek "%s"';
$lang['teams:archive_title'] 			= 'Archiv';
$lang['teams:posts_title'] 			= 'Články';
$lang['teams:rss_posts_title'] 		= 'Novinky pro %s';
$lang['teams:teams_title'] 			= 'Novinky';
$lang['teams:list_title'] 			= 'Seznam článků';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts'] 			= 'Nejsou zde žádné články.';
$lang['teams:subscripe_to_rss_desc'] 		= 'Dostávejte články rovnou pomocí RSS. Můžete použít populární e-mailové klienty nebo zkuste <a href="http://reader.google.cz/">Google Reader</a>.';
$lang['teams:currently_no_posts'] 		= 'Nejsou zde žádné články.';
$lang['teams:post_add_success'] 		= 'Článek "%s" byl přidán.';
$lang['teams:post_add_error'] 		= 'Objevila se chyba.';
$lang['teams:edit_success'] 			= 'Článek "%s" byl aktualizován.';
$lang['teams:edit_error'] 			= 'Objevila se chyba.';
$lang['teams:publish_success'] 			= 'Článek "%s" byl publikován.';
$lang['teams:mass_publish_success'] 		= 'Články "%s" byly publikovány.';
$lang['teams:publish_error'] 			= 'žádné články nebyly publikovány.';
$lang['teams:delete_success'] 			= 'Článek "%s" byl smazán.';
$lang['teams:mass_delete_success'] 		= 'Články "%s" byly smazány.';
$lang['teams:delete_error'] 			= 'Žádné články nebyly smazány.';
$lang['teams:already_exist_error'] 		= 'Článek s touto adresou URL již existuje.';

$lang['teams:twitter_posted']			= 'Publikováno "%s" %s';
$lang['teams:twitter_error'] 			= 'Chyba Twitteru';

// date
$lang['teams:archive_date_format']		= "%B %Y";

?>