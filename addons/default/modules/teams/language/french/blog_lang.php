<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']  = 'Actualité';
$lang['teams:posts'] = 'Actualités';

// labels
$lang['teams:posted_label']             = 'Publié le';
$lang['teams:posted_label_alt']         = 'Envoyé à';
$lang['teams:written_by_label']         = 'Écrit par';
$lang['teams:author_unknown']           = 'Inconnu';
$lang['teams:keywords_label']           = 'Mots-clés';
$lang['teams:tagged_label']             = 'Taggé';
$lang['teams:category_label']           = 'Catégorie';
$lang['teams:post_label']               = 'Article';
$lang['teams:date_label']               = 'Date';
$lang['teams:date_at']                  = 'le';
$lang['teams:time_label']               = 'Heure';
$lang['teams:status_label']             = 'Statut';
$lang['teams:draft_label']              = 'Brouillon';
$lang['teams:live_label']               = 'Live';
$lang['teams:content_label']            = 'Contenu';
$lang['teams:options_label']            = 'Options';
$lang['teams:intro_label']              = 'Introduction';
$lang['teams:no_category_select_label'] = '-- Aucune --';
$lang['teams:new_category_label']       = 'Ajouter une catégorie';
$lang['teams:subscripe_to_rss_label']   = 'Souscrire au RSS';
$lang['teams:all_posts_label']          = 'Tous les Articles';
$lang['teams:posts_of_category_suffix'] = ' articles';
$lang['teams:rss_name_suffix']          = ' Articles';
$lang['teams:rss_category_suffix']      = ' Articles';
$lang['teams:author_name_label']        = 'Auteur';
$lang['teams:read_more_label']          = 'Lire la suite&nbsp;&raquo;';
$lang['teams:created_hour']             = 'Heure de création';
$lang['teams:created_minute']           = 'Minute de création';
$lang['teams:comments_enabled_label']   = 'Commentaires activés';

// titles
$lang['teams:create_title']    			= 'Créer un article';
$lang['teams:edit_title']      			= 'Modifier l\'article "%s"';
$lang['teams:archive_title']   			= 'Archives';
$lang['teams:posts_title']     			= 'Articles';
$lang['teams:rss_posts_title'] 			= ' pour %s';
$lang['teams:teams_title']      			= 'Articles';
$lang['teams:list_title']      			= 'Lister les posts';

// messages
$lang['teams:disabled_after'] 			= 'Poster des commentaires après  %s a été désactivé.';
$lang['teams:no_posts']              = 'Il n\'y a pas d\'articles.';
$lang['teams:subscripe_to_rss_desc'] = 'Restez informé des derniers articles en temps réel en vous abonnant au flux RSS. Vous pouvez faire cela avec la plupart des logiciels de messagerie, ou essayez <a href="http://www.google.fr/reader">Google Reader</a>.';
$lang['teams:currently_no_posts']    = 'Il n\'y a aucun article actuellement.';
$lang['teams:post_add_success']      = 'L\'article "%s" a été ajouté.';
$lang['teams:post_add_error']        = 'Une erreur est survenue.';
$lang['teams:edit_success']          = 'Le post "%s" a été mis à jour.';
$lang['teams:edit_error']            = 'Une erreur est survenue.';
$lang['teams:publish_success']       = 'Les articles "%s" ont été publiés.';
$lang['teams:mass_publish_success']  = 'Les articles "%s" ont été publiés.';
$lang['teams:publish_error']         = 'Aucun article n\'a été publié.';
$lang['teams:delete_success']        = 'Les articles "%s" ont été supprimés.';
$lang['teams:mass_delete_success']   = 'Les articles "%s" ont été supprimés.';
$lang['teams:delete_error']          = 'Aucun article n\'a été supprimé.';
$lang['teams:already_exist_error']   = 'Un article avec cet URL existe déjà.';

$lang['teams:twitter_posted'] = 'Envoyé "%s" %s';
$lang['teams:twitter_error']  = 'Erreur Twitter';

// date
$lang['teams:archive_date_format'] = "%B %Y";
