<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Mettre l\'article en direct';
$lang['teams:role_edit_live']	= 'Modifier les articles en direct';
$lang['teams:role_delete_live'] 	= 'Supprimer les articles en direct';