<?php
/* Translation made Nicola Tudino */
/* Date 07/11/2010 */

$lang['teams:post']                 = 'Articolo';
$lang['teams:posts']                   = 'Articoli';

// labels
$lang['teams:posted_label'] 			= 'Pubblicato';
$lang['teams:posted_label_alt']			= 'Pubblicato alle';
$lang['teams:written_by_label']				= 'Scritto da';
$lang['teams:author_unknown']				= 'Sconosciuto';
$lang['teams:keywords_label']				= 'Parole chiave'; 
$lang['teams:tagged_label']					= 'Taggato';
$lang['teams:category_label'] 			= 'Categoria';
$lang['teams:post_label'] 			= 'Articolo';
$lang['teams:date_label'] 			= 'Data';
$lang['teams:date_at']				= 'alle';
$lang['teams:time_label'] 			= 'Ora';
$lang['teams:status_label'] 			= 'Stato';
$lang['teams:draft_label'] 			= 'Bozza';
$lang['teams:live_label'] 			= 'Pubblicato';
$lang['teams:content_label'] 			= 'Contenuto';
$lang['teams:options_label'] 			= 'Opzioni';
$lang['teams:intro_label'] 			= 'Introduzione';
$lang['teams:no_category_select_label'] 		= '-- Nessuna --';
$lang['teams:new_category_label'] 		= 'Aggiungi una categoria';
$lang['teams:subscripe_to_rss_label'] 		= 'Abbonati all\'RSS';
$lang['teams:all_posts_label'] 		= 'Tutti gli articoli';
$lang['teams:posts_of_category_suffix'] 	= ' articoli';
$lang['teams:rss_name_suffix'] 			= ' Notizie';
$lang['teams:rss_category_suffix'] 		= ' Notizie';
$lang['teams:author_name_label'] 		= 'Nome autore';
$lang['teams:read_more_label'] 			= 'Leggi tutto&nbsp;&raquo;';
$lang['teams:created_hour']                  = 'Time (Ora)'; 
$lang['teams:created_minute']                = 'Time (Minuto)';
$lang['teams:comments_enabled_label']         = 'Commenti abilitati';

// titles
$lang['teams:create_title'] 			= 'Aggiungi un articolo';
$lang['teams:edit_title'] 			= 'Modifica l\'articolo "%s"';
$lang['teams:archive_title'] 			= 'Archivio';
$lang['teams:posts_title'] 			= 'Articoli';
$lang['teams:rss_posts_title'] 		= 'Notizie per %s';
$lang['teams:teams_title'] 			= 'Notizie';
$lang['teams:list_title'] 			= 'Elenco articoli';

// messages
$lang['teams:disabled_after'] 				= 'Non sarà più possibile inserire commenti dopo %s.';
$lang['teams:no_posts'] 			= 'Non ci sono articoli.';
$lang['teams:subscripe_to_rss_desc'] 		= 'Ricevi gli articoli subito abbonandoti al nostro feed RSS. Lo puoi fare con i comuni programmi di posta elettronica, altrimenti prova <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts'] 		= 'Non ci sono articoli al momento.';
$lang['teams:post_add_success'] 		= 'L\'articolo "%s" è stato aggiunto.';
$lang['teams:post_add_error'] 		= 'C\'è stato un errore.';
$lang['teams:edit_success'] 			= 'L\'articolo "%s" è stato modificato.';
$lang['teams:edit_error'] 			= 'C\'è stato un errore.';
$lang['teams:publish_success'] 			= 'L\'articolo "%s" è stato pubblicato.';
$lang['teams:mass_publish_success'] 		= 'Gli articoli "%s" sono stati pubblicati.';
$lang['teams:publish_error'] 			= 'Gli articoli non saranno pubblicati.';
$lang['teams:delete_success'] 			= 'L\'articolo "%s" è stato eliminato.';
$lang['teams:mass_delete_success'] 		= 'Gli articoli "%s" sono stati eliminati.';
$lang['teams:delete_error'] 			= 'Nessun articolo è stato eliminato.';
$lang['teams:already_exist_error'] 		= 'Un articolo con questo URL esiste già.';

$lang['teams:twitter_posted']			= 'Inviato "%s" %s';
$lang['teams:twitter_error'] 			= 'Errore per Twitter';

// date
$lang['teams:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation

?>