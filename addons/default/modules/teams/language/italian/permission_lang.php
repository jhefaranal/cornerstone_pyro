<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Pubblicare nuovi articoli';
$lang['teams:role_edit_live']	= 'Modificare articoli pubblicati'; 
$lang['teams:role_delete_live'] 	= 'Cancellare articoli pubblicati'; 