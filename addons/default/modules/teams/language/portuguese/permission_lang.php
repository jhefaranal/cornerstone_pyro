<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Publicar artigos';
$lang['teams:role_edit_live']	= 'Editar artigos publicados';
$lang['teams:role_delete_live'] 	= 'Remover artigos publicados';