<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']				= 'Escrito';
$lang['teams:posted_label_alt']			= 'Escrito em';
$lang['teams:written_by_label']			= 'Por';
$lang['teams:author_unknown']			= 'Desconhecido';
$lang['teams:keywords_label']			= 'Palavras-chave';
$lang['teams:tagged_label']				= 'Tagged'; #translate
$lang['teams:category_label']			= 'Categoria';
$lang['teams:post_label'] 				= 'Artigo';
$lang['teams:date_label'] 				= 'Data';
$lang['teams:date_at']					= 'às';
$lang['teams:time_label'] 				= 'Hora';
$lang['teams:status_label'] 				= 'Situação';
$lang['teams:draft_label'] 				= 'Rascunho';
$lang['teams:live_label'] 				= 'Publico';
$lang['teams:content_label']				= 'Conteúdo';
$lang['teams:options_label']				= 'Opções';
$lang['teams:intro_label'] 				= 'Introdução';
$lang['teams:no_category_select_label']	= '-- Nenhuma --';
$lang['teams:new_category_label'] 		= 'Adicionar uma categoria';
$lang['teams:subscripe_to_rss_label'] 	= 'Assinar o RSS';
$lang['teams:all_posts_label'] 			= 'Todos os artigos';
$lang['teams:posts_of_category_suffix'] 	= ' artigos';
$lang['teams:rss_name_suffix']			= ' Teams';
$lang['teams:rss_category_suffix']		= ' Teams';
$lang['teams:author_name_label']			= 'Nome do autor';
$lang['teams:read_more_label']			= 'Leia mais &raquo;';
$lang['teams:created_hour']				= 'Horário (Hora)';
$lang['teams:created_minute']			= 'Horário (Minuto)';
$lang['teams:comments_enabled_label']	= 'Habilitar comentários';

// titles
$lang['teams:create_title']				= 'Adicionar artigo';
$lang['teams:edit_title']				= 'Editar artigo "%s"';
$lang['teams:archive_title'] 			= 'Arquivo';
$lang['teams:posts_title'] 				= 'Artigos';
$lang['teams:rss_posts_title'] 			= 'Artigos novos para %s';
$lang['teams:teams_title']				= 'Teams';
$lang['teams:list_title']				= 'Listar artigos';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']					= 'Nenhum artigo.';
$lang['teams:subscripe_to_rss_desc']		= 'Fique por dentro das novidades do teams assinando o nosso feed RSS. Pode fazer isto pelos mais populares leitores de e-mail ou pode experimentar o <a rel="nofollow" href="http://reader.google.com/">Google Reader</a>.';
$lang['teams:currently_no_posts']		= 'Não existem artigos no momento.';
$lang['teams:post_add_success']			= 'O artigo "%s" foi adicionado.';
$lang['teams:post_add_error']			= 'Ocorreu um erro.';
$lang['teams:edit_success']				= 'O artigo "%s" foi actualizado.';
$lang['teams:edit_error']				= 'Ocorreu um erro.';
$lang['teams:publish_success']			= 'O artigo "%s" foi publicado.';
$lang['teams:mass_publish_success']		= 'Os artigos "%s" foram publicados.';
$lang['teams:publish_error']				= 'Nenhum artigo foi publicado.';
$lang['teams:delete_success']			= 'O artigo "%s" foi removido.';
$lang['teams:mass_delete_success']		= 'Os artigos "%s" foram removidos.';
$lang['teams:delete_error']				= 'Nenhuma publicação foi removida.';
$lang['teams:already_exist_error']		= 'Um artigo com mesmo campo %s já existe.';

$lang['teams:twitter_posted']			= 'Escrito "%s" %s';
$lang['teams:twitter_error']				= 'Erro do Twitter';

// date
$lang['teams:archive_date_format']		= "%B de %Y";