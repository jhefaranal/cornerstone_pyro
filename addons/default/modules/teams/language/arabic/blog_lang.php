<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'تدوينة';
$lang['teams:posts']                   = 'تدوينات';

// labels
$lang['teams:posted_label'] 			= 'تاريخ النشر';
$lang['teams:posted_label_alt']			= 'نشر في';
$lang['teams:written_by_label']				= 'كتبها';
$lang['teams:author_unknown']				= 'مجهول';
$lang['teams:keywords_label']				= 'كلمات البحث';
$lang['teams:tagged_label']					= 'موسومة';
$lang['teams:category_label'] 			= 'التصنيف';
$lang['teams:post_label'] 			= 'إرسال';
$lang['teams:date_label'] 			= 'التاريخ';
$lang['teams:date_at']				= 'عند';
$lang['teams:time_label'] 			= 'الوقت';
$lang['teams:status_label'] 			= 'الحالة';
$lang['teams:draft_label'] 			= 'مسودّة';
$lang['teams:live_label'] 			= 'منشور';
$lang['teams:content_label'] 			= 'المُحتوى';
$lang['teams:options_label'] 			= 'خيارات';
$lang['teams:intro_label'] 			= 'المٌقدّمة';
$lang['teams:no_category_select_label'] 		= '-- لاشيء --';
$lang['teams:new_category_label'] 		= 'إضافة تصنيف';
$lang['teams:subscripe_to_rss_label'] 		= 'اشترك في خدمة RSS';
$lang['teams:all_posts_label'] 		= 'جميع التدوينات';
$lang['teams:posts_of_category_suffix'] 	= ' &raquo; التدوينات';
$lang['teams:rss_name_suffix'] 			= ' &raquo; المُدوّنة';
$lang['teams:rss_category_suffix'] 		= ' &raquo; المُدوّنة';
$lang['teams:author_name_label'] 		= 'إسم الكاتب';
$lang['teams:read_more_label'] 			= 'إقرأ المزيد&nbsp;&raquo;';
$lang['teams:created_hour']                  = 'الوقت (الساعة)';
$lang['teams:created_minute']                = 'الوقت (الدقيقة)';
$lang['teams:comments_enabled_label']         = 'إتاحة التعليقات';

// titles
$lang['teams:create_title'] 			= 'إضافة مقال';
$lang['teams:edit_title'] 			= 'تعديل التدوينة "%s"';
$lang['teams:archive_title'] 			= 'الأرشيف';
$lang['teams:posts_title'] 			= 'التدوينات';
$lang['teams:rss_posts_title'] 		= 'تدوينات %s';
$lang['teams:teams_title'] 			= 'المُدوّنة';
$lang['teams:list_title'] 			= 'سرد التدوينات';

// messages
$lang['teams:disabled_after'] 				= 'تم تعطيل التعليقات بعد %s.';
$lang['teams:no_posts'] 			= 'لا يوجد تدوينات.';
$lang['teams:subscripe_to_rss_desc'] 		= 'اطلع على آخر التدوينات مباشرة بالاشتراك بخدمة RSS. يمكنك القيام بذلك من خلال معظم برامج البريد الإلكتروني الشائعة، أو تجربة <a href="http://reader.google.com/">قارئ جوجل</a>.';
$lang['teams:currently_no_posts'] 		= 'لا يوجد تدوينات حالياً.';
$lang['teams:post_add_success'] 		= 'تمت إضافة التدوينة "%s".';
$lang['teams:post_add_error'] 		= 'حدث خطأ.';
$lang['teams:edit_success'] 			= 'تم تحديث التدوينة "%s".';
$lang['teams:edit_error'] 			= 'حدث خطأ.';
$lang['teams:publish_success'] 			= 'تم نشر التدوينة "%s".';
$lang['teams:mass_publish_success'] 		= 'تم نشر التدوينات "%s".';
$lang['teams:publish_error'] 			= 'لم يتم نشر أي تدوينات.';
$lang['teams:delete_success'] 			= 'تم حذف التدوينة "%s".';
$lang['teams:mass_delete_success'] 		= 'تم حذف التدوينات "%s".';
$lang['teams:delete_error'] 			= 'لم يتم حذف أي تدوينات.';
$lang['teams:already_exist_error'] 		= 'يوجد مقال له عنوان URL مطابق.';

$lang['teams:twitter_posted']			= 'منشور في "%s" %s';
$lang['teams:twitter_error'] 			= 'خطأ في تويتر';

// date
$lang['teams:archive_date_format']		= "%B %Y";

?>
