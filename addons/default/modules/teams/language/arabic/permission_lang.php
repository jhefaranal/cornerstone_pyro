<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live'] = 'نشر التدوينات';
$lang['teams:role_edit_live'] = 'تعديل التدوينات المنشورة';
$lang['teams:role_delete_live'] 	= 'حذف التدوينات المنشورة';
