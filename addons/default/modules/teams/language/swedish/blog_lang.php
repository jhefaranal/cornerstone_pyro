<?php defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * Swedish translation.
 *
 * @author		marcus@incore.se
 * @package		PyroCMS  
 * @link		http://pyrocms.com
 * @date		2012-10-23
 * @version		1.1.0
 */

$lang['teams:post'] = 'Inlägg';
$lang['teams:posts'] = 'Inlägg';
$lang['teams:posted_label'] = 'Skriven';
$lang['teams:posted_label_alt'] = 'Skriven den';
$lang['teams:written_by_label'] = 'Skriven av';
$lang['teams:author_unknown'] = 'Okänd';
$lang['teams:keywords_label'] = 'Nyckelord';
$lang['teams:tagged_label'] = 'Taggad';
$lang['teams:category_label'] = 'Kategori';
$lang['teams:post_label'] = 'Inlägg';
$lang['teams:date_label'] = 'Datum';
$lang['teams:date_at'] = 'den';
$lang['teams:time_label'] = 'Tid';
$lang['teams:status_label'] = 'Status';
$lang['teams:draft_label'] = 'Utkast';
$lang['teams:live_label'] = 'Publik';
$lang['teams:content_label'] = 'Innehåll';
$lang['teams:options_label'] = 'Val';
$lang['teams:intro_label'] = 'Introduktion';
$lang['teams:no_category_select_label'] = '-- Ingen --';
$lang['teams:new_category_label'] = 'Lägg till en kategori';
$lang['teams:subscripe_to_rss_label'] = 'Prenumerera på RSS';
$lang['teams:all_posts_label'] = 'Alla inlägg';
$lang['teams:posts_of_category_suffix'] = 'inlägg';
$lang['teams:rss_name_suffix'] = 'Teamsg';
$lang['teams:rss_category_suffix'] = 'Teamsg';
$lang['teams:author_name_label'] = 'Skribent';
$lang['teams:read_more_label'] = 'Läs mer »';
$lang['teams:created_hour'] = 'Skapad, timme';
$lang['teams:created_minute'] = 'Skapad, minut';
$lang['teams:comments_enabled_label'] = 'Kommentarer aktiverad';
$lang['teams:create_title'] = 'Lägg till post';
$lang['teams:edit_title'] = 'Redigera post "%s"';
$lang['teams:archive_title'] = 'Arkiv';
$lang['teams:posts_title'] = 'Inlägg';
$lang['teams:rss_posts_title'] = 'Teamsginlägg för %s';
$lang['teams:teams_title'] = 'Teamsg';
$lang['teams:list_title'] = 'Lista inlägg';
$lang['teams:disabled_after'] = 'Skickade kommentarer efter %s har blivit inaktiverade.';
$lang['teams:no_posts'] = 'Det finns inga poster';
$lang['teams:subscripe_to_rss_desc'] = 'Få inlägg direkt genom att prenumerera på vårt RSS-flöde. Du flesta populära e-postklienter har stöd för detta, eller prova <a href="http://reader.google.co.uk/">Google Reader</ a>.';
$lang['teams:currently_no_posts'] = 'Det finns inga poster just nu';
$lang['teams:post_add_success'] = 'Inlägget "%s" har lagts till.';
$lang['teams:post_add_error'] = 'Ett fel inträffade.';
$lang['teams:edit_success'] = 'Inlägget "%s" uppdaterades.';
$lang['teams:edit_error'] = 'Ett fel inträffade.';
$lang['teams:publish_success'] = 'Inlägget "%s" publicerades.';
$lang['teams:mass_publish_success'] = 'Inläggen "%s" publicerades.';
$lang['teams:publish_error'] = 'Inga inlägg publicerades';
$lang['teams:delete_success'] = 'Inlägget "%s" har raderats.';
$lang['teams:mass_delete_success'] = 'Inläggen "%s" har raderats.';
$lang['teams:delete_error'] = 'Inga inlägg raderades';
$lang['teams:already_exist_error'] = 'Ett inlägg med denna URL finns redan';
$lang['teams:twitter_posted'] = 'Sparad "%s" %s';
$lang['teams:twitter_error'] = 'Twitterfel';
$lang['teams:archive_date_format'] = '%B\' %Y';


/* End of file teams_lang.php */  
/* Location: system/cms/modules/teams/language/swedish/teams_lang.php */  
