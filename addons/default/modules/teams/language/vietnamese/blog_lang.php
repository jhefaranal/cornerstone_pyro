<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Author: Thanh Nguyen
* 		  nguyenhuuthanh@gmail.com
*
* Location: http://techmix.net
*
* Created:  10.26.2011
*
* Description:  Vietnamese language file
*
*/

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']                   = 'Đã gửi';
$lang['teams:posted_label_alt']               = 'Gửi lúc at';
$lang['teams:written_by_label']				= 'Viết bởi';
$lang['teams:author_unknown']				= 'Chưa rõ';
$lang['teams:keywords_label']				= 'Keywords'; #translate
$lang['teams:tagged_label']					= 'Tagged'; #translate
$lang['teams:category_label']                 = 'Danh mục';
$lang['teams:post_label']                     = 'Bài viết';
$lang['teams:date_label']                     = 'Ngày';
$lang['teams:date_at']                        = 'lúc';
$lang['teams:time_label']                     = 'thời gian';
$lang['teams:status_label']                   = 'Trạng thái';
$lang['teams:draft_label']                    = 'Nháp';
$lang['teams:live_label']                     = 'Xuất bản';
$lang['teams:content_label']                  = 'Nội dung';
$lang['teams:options_label']                  = 'Tùy biến';
$lang['teams:intro_label']                    = 'Giới thiệu';
$lang['teams:no_category_select_label']       = '-- Không --';
$lang['teams:new_category_label']             = 'Thêm chuyên mục';
$lang['teams:subscripe_to_rss_label']         = 'Nhận RSS';
$lang['teams:all_posts_label']             = 'Tất cả bài viết';
$lang['teams:posts_of_category_suffix']    = ' bài viết';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Tên tác giả';
$lang['teams:read_more_label']                = 'Đọc thêm&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Giờ tạo';
$lang['teams:created_minute']                 = 'Phút tạo';
$lang['teams:comments_enabled_label']         = 'Cho phép phản hồi';

// titles
$lang['teams:create_title']                   = 'Thêm bài viết';
$lang['teams:edit_title']                     = 'Sửa bài viết "%s"';
$lang['teams:archive_title']                  = 'Lưu trữ';
$lang['teams:posts_title']                 = 'Bài viết';
$lang['teams:rss_posts_title']             = 'Các bài viết cho %s';
$lang['teams:teams_title']                     = 'Teams';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']                    = 'Không có bài viết nào.';
$lang['teams:subscripe_to_rss_desc']          = 'Hãy sử dụng RSS Feed để nhận những bài viết mới nhất. Bạn có thể sử dụng nhiều chương trình khác nhau, hoặc sử dụng <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'Không có bài viết nào.';
$lang['teams:post_add_success']            = 'Đã thêm bài viết "%s".';
$lang['teams:post_add_error']              = 'Có lỗi xảy ra.';
$lang['teams:edit_success']                   = 'Bài viết "%s" đã được cập nhật.';
$lang['teams:edit_error']                     = 'Có lỗi xảy ra.';
$lang['teams:publish_success']                = 'Bài viết "%s" đã được xuất bản.';
$lang['teams:mass_publish_success']           = 'Bài viết "%s" đã được xuất bản.';
$lang['teams:publish_error']                  = 'Không có bài viết nào được xuât bản.';
$lang['teams:delete_success']                 = 'Đã xóa bài viết "%s".';
$lang['teams:mass_delete_success']            = 'Đã xóa bài viết "%s".';
$lang['teams:delete_error']                   = 'Không có bài viết nào được xóa.';
$lang['teams:already_exist_error']            = 'URL của bài viết đã tồn tại.';

$lang['teams:twitter_posted']                 = 'Đã gửi "%s" %s';
$lang['teams:twitter_error']                  = 'Lỗi Twitter';

// date
$lang['teams:archive_date_format']		= "%B %Y";
