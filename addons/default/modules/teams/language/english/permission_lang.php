<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Put articles live';
$lang['teams:role_edit_live']	= 'Edit live articles';
$lang['teams:role_delete_live'] 	= 'Delete live articles';