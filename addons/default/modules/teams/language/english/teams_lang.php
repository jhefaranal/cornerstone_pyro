<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post';
$lang['teams:posts']                   = 'Posts';

// labels
$lang['teams:posted_label']                   = 'Posted';
$lang['teams:posted_label_alt']               = 'Posted at';
$lang['teams:written_by_label']				= 'Written by';
$lang['teams:author_unknown']				= 'Unknown';
$lang['teams:keywords_label']				= 'Keywords';
$lang['teams:tagged_label']					= 'Tagged';
$lang['teams:category_label']                 = 'Category';
$lang['teams:post_label']                     = 'Post';
$lang['teams:date_label']                     = 'Date';
$lang['teams:date_at']                        = 'at';
$lang['teams:time_label']                     = 'Time';
$lang['teams:status_label']                   = 'Status';
$lang['teams:draft_label']                    = 'Draft';
$lang['teams:live_label']                     = 'Live';
$lang['teams:content_label']                  = 'Content';
$lang['teams:options_label']                  = 'Options';
$lang['teams:intro_label']                    = 'Introduction';
$lang['teams:no_category_select_label']       = '-- None --';
$lang['teams:new_category_label']             = 'Add a category';
$lang['teams:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['teams:all_posts_label']                = 'All posts';
$lang['teams:posts_of_category_suffix']       = ' posts';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Author name';
$lang['teams:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Created on Hour';
$lang['teams:created_minute']                 = 'Created on Minute';
$lang['teams:comments_enabled_label']         = 'Comments Enabled';

// titles
$lang['teams:create_title']                   = 'Add Teams';
$lang['teams:edit_title']                     = 'Edit Teams "%s"';
$lang['teams:archive_title']                 = 'Archive';
$lang['teams:posts_title']					= 'Posts';
$lang['teams:rss_posts_title']				= 'Teams posts for %s';
$lang['teams:teams_title']					= 'Teams';
$lang['teams:list_title']					= 'List Posts';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.';
$lang['teams:no_posts']                      = 'There are no posts.';
$lang['teams:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'There are no posts at the moment.';
$lang['teams:post_add_success']            = 'The post "%s" was added.';
$lang['teams:post_add_error']              = 'An error occured.';
$lang['teams:edit_success']                   = 'The post "%s" was updated.';
$lang['teams:edit_error']                     = 'An error occurred.';
$lang['teams:publish_success']                = 'The post "%s" has been published.';
$lang['teams:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['teams:publish_error']                  = 'No posts were published.';
$lang['teams:delete_success']                 = 'The post "%s" has been deleted.';
$lang['teams:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['teams:delete_error']                   = 'No posts were deleted.';
$lang['teams:already_exist_error']            = 'A post with this URL already exists.';

$lang['teams:twitter_posted']                 = 'Posted "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter Error';

// date
$lang['teams:archive_date_format']		= "%B %Y";
