<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Thai translation.
*
* @author	Nateetorn Lertkhonsan <nateetorn.l@gmail.com>
* @package	PyroCMS  
* @link		http://pyrocms.com
* @date		2012-04-19
* @version	1.0.0
**/

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']                   = 'โพสต์แล้ว';
$lang['teams:posted_label_alt']               = 'โพสต์แล้วที่';
$lang['teams:written_by_label']				= 'เขียนโดย';
$lang['teams:author_unknown']				= 'ไม่ทราบ';
$lang['teams:keywords_label']				= 'คำหลัก';
$lang['teams:tagged_label']					= 'แท็ก';
$lang['teams:category_label']                 = 'หมวดหมู่';
$lang['teams:post_label']                     = 'โพสต์';
$lang['teams:date_label']                     = 'วันที่';
$lang['teams:date_at']                        = 'ที่';
$lang['teams:time_label']                     = 'เวลา';
$lang['teams:status_label']                   = 'สถานะ';
$lang['teams:draft_label']                    = 'ร่างบล็อก';
$lang['teams:live_label']                     = 'ไลฟ์บล็อก';
$lang['teams:content_label']                  = 'เนื้อหา';
$lang['teams:options_label']                  = 'ตัวเลือก';
$lang['teams:intro_label']                    = 'บทนำ';
$lang['teams:no_category_select_label']       = '-- ไม่มี --';
$lang['teams:new_category_label']             = 'เพิ่มหมวดหมู่';
$lang['teams:subscripe_to_rss_label']         = 'สมัครสมาชิก RSS';
$lang['teams:all_posts_label']             = 'โพสต์ทั้งหมด';
$lang['teams:posts_of_category_suffix']    = ' โพสต์';
$lang['teams:rss_name_suffix']                = ' บล็อก';
$lang['teams:rss_category_suffix']            = ' บล็อก';
$lang['teams:author_name_label']              = 'ชื่อผู้เขียน';
$lang['teams:read_more_label']                = 'อ่านเพิ่มเติม&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'สร้างเมื่อชั่วโมงที่แล้ว';
$lang['teams:created_minute']                 = 'สร้างเมื่อนาทีที่แล้ว';
$lang['teams:comments_enabled_label']         = 'ความคิดเห็นถูกเปิดใช้งาน';

// titles
$lang['teams:create_title']                   = 'เพิ่มโพสต์';
$lang['teams:edit_title']                     = 'แก้ไขโพสต์ "%s"';
$lang['teams:archive_title']                 = 'คลัง';
$lang['teams:posts_title']					= 'โพสต์';
$lang['teams:rss_posts_title']				= 'โพสต์บล็อกสำหรับ %s';
$lang['teams:teams_title']					= 'บล็อก';
$lang['teams:list_title']					= 'รายการโพสต์';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']                    = 'ไม่มีโพสต์';
$lang['teams:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'ไม่มีโพสต์ในขณะนี้';
$lang['teams:post_add_success']            = 'โพสต์ "%s" ถูกเพิ่มแล้ว';
$lang['teams:post_add_error']              = 'เกิดข้อผิดพลาด';
$lang['teams:edit_success']                   = 'ปรับปรุงโพสต์ "%s" แล้ว';
$lang['teams:edit_error']                     = 'เกิดข้อผิดพลาด';
$lang['teams:publish_success']                = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['teams:mass_publish_success']           = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['teams:publish_error']                  = 'ไม่มีโพสต์ถูกเผยแพร่.';
$lang['teams:delete_success']                 = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['teams:mass_delete_success']            = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['teams:delete_error']                   = 'ไม่มีโพสต์ถูกลบ';
$lang['teams:already_exist_error']            = 'โพสต์ลิงค์นี้มีอยู่แล้ว';

$lang['teams:twitter_posted']                 = 'โพสต์ "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter มีข้อผิดพลาด';

// date
$lang['teams:archive_date_format']		= "%B %Y";
