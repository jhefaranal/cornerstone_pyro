<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                      = 'Ανάρτηση';
$lang['teams:posts']                     = 'Αναρτήσεις';

// labels
$lang['teams:posted_label'] 		= 'Δημοσιεύτηκε';
$lang['teams:posted_label_alt'] 		= 'Δημοσιεύτηκε στις';
$lang['teams:written_by_label'] 		= 'Γράφτηκε από';
$lang['teams:author_unknown'] 		= 'Άγνωστο';
$lang['teams:keywords_label'] 		= 'Λέξεις κλειδιά';
$lang['teams:tagged_label'] 		= 'Ετικέτες';
$lang['teams:category_label'] 		= 'Κατηγορία';
$lang['teams:post_label'] 		= 'Δημοσίευση';
$lang['teams:date_label'] 		= 'Ημερομηνία';
$lang['teams:date_at'] 			= 'στις';
$lang['teams:time_label'] 		= 'Χρόνος';
$lang['teams:status_label'] 		= 'Κατάσταση';
$lang['teams:draft_label'] 		= 'Πρόχειρο';
$lang['teams:live_label'] 		= 'Δημοσιευμένο';
$lang['teams:content_label'] 		= 'Περιεχόμενο';
$lang['teams:options_label'] 		= 'Επιλογές';
$lang['teams:intro_label'] 		= 'Εισαγωγή';
$lang['teams:no_category_select_label'] 	= '-- Καμμία --';
$lang['teams:new_category_label'] 	= 'Προσθήκη κατηγορίας';
$lang['teams:subscripe_to_rss_label'] 	= 'Εγγραφή στο RSS';
$lang['teams:all_posts_label'] 		= 'Όλες οι δημοσιεύσεις';
$lang['teams:posts_of_category_suffix'] 	= ' δημοσιεύσεις';
$lang['teams:rss_name_suffix'] 		= ' Ιστολόγιο';
$lang['teams:rss_category_suffix'] 	= ' Ιστολόγιο';
$lang['teams:author_name_label'] 	= 'Όνομα συγγραφέα';
$lang['teams:read_more_label'] 		= 'Διαβάστε περισσότερα &raquo;';
$lang['teams:created_hour'] 		= 'Ώρα δημιουργίας';
$lang['teams:created_minute'] 		= 'Λεπτό δημιουργίας';
$lang['teams:comments_enabled_label'] 	= 'Σχόλια Ενεργά';

// titles
$lang['teams:create_title'] 		= 'Προσθήκη δημοσίευσης';
$lang['teams:edit_title'] 		= 'Επεξεργασία δημοσίευσης "%s"';
$lang['teams:archive_title'] 		= 'Αρχειοθήκη';
$lang['teams:posts_title'] 		= 'Δημοσιεύσεις';
$lang['teams:rss_posts_title'] 		= 'Δημοσιεύσεις ιστολογίου για %s';
$lang['teams:teams_title'] 		= 'Ιστολόγιο';
$lang['teams:list_title'] 		= 'Λίστα δημοσιεύσεων';

// messages
$lang['teams:disabled_after'] 				= 'Η ανάρτηση σχολίων μετά τις %s έχει απενεργοποιηθεί.';
$lang['teams:no_posts'] 			= 'Δεν υπάρχουν δημοσιεύσεις.';
$lang['teams:subscripe_to_rss_desc'] 	= 'Μπορείτε να λαμβάνετε νεότερες δημοσιεύσεις με το να εγγραφείτε στην τροφοδοσία RSS μας. Μπορείτε να το κάνετε χρησιμοποιώντας τα δημοφιλή προγράμματα e-mail, ή με το <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts'] 	= 'Προς το παρόν δεν υπάρχουν δημοσιεύσεις.';
$lang['teams:post_add_success'] 		= 'Η δημοσίευση "%s" προστέθηκε.';
$lang['teams:post_add_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['teams:edit_success'] 		= 'Η δημοσίευση "%s" ενημερώθηκε.';
$lang['teams:edit_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['teams:publish_success'] 		= 'Η δημοσίευση "%s" αναρτήθηκε.';
$lang['teams:mass_publish_success'] 	= 'Οι δημοσιεύσεις "%s" αναρτήθηκαν.';
$lang['teams:publish_error'] 		= 'Δεν αναρτήθηκε καμία δημοσίευση.';
$lang['teams:delete_success'] 		= 'Η δημοσίευση "%s" διαγράφηκε.';
$lang['teams:mass_delete_success'] 	= 'Οι δημοσιεύσεις "%s" διαγράφηκαν.';
$lang['teams:delete_error'] 		= 'Δεν διαγράφηκε καμία δημοσίευση.';
$lang['teams:already_exist_error'] 	= 'Υπάρχει ήδη μια δημοσίευση με αυτό το URL.';

$lang['teams:twitter_posted'] 		= 'Δημοσιεύτηκε "%s" %s';
$lang['teams:twitter_error'] 		= 'Σφάλμα Twitter';

// date
$lang['teams:archive_date_format'] 	= "%B %Y";
