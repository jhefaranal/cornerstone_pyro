<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live'] = 'Να δημοσιεύει άρθρα';
$lang['teams:role_edit_live'] = 'Επεξεργασία δημοσιευμένων άρθρων';
$lang['teams:role_delete_live'] 	= 'Διαγραφή δημοσιευμένων άρθρων';