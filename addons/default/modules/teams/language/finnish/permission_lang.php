<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Artikkelien julkaiseminen';
$lang['teams:role_edit_live']	= 'Julkaistujen artikkeleiden muokkaaminen';
$lang['teams:role_delete_live'] 	= 'Julkaistujen artikkeleiden poistaminen';
