<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Finnish translation.
 *
 * @author Mikael Kundert
 */

$lang['teams:post']	= 'Artikkeli';
$lang['teams:posts']	= 'Artikkelit';

// labels
$lang['teams:posted_label']				= 'Lähetetty';
$lang['teams:posted_label_alt']			= 'Lähetettiin';
$lang['teams:written_by_label']			= 'Kirjoittanut';
$lang['teams:author_unknown']			= 'Tuntematon';
$lang['teams:keywords_label']			= 'Avainsanat';
$lang['teams:tagged_label']				= 'Merkitty';
$lang['teams:category_label']			= 'Kategoria';
$lang['teams:post_label']				= 'Artikkeli';
$lang['teams:date_label']				= 'Päivä';
$lang['teams:date_at']					= 'at'; #translate (in finnish we use adessives by using suffixes!, see http://www.cs.tut.fi/~jkorpela/finnish-cases.html)
$lang['teams:time_label']				= 'Aika';
$lang['teams:status_label']				= 'Tila';
$lang['teams:draft_label']				= 'Luonnos';
$lang['teams:live_label']				= 'Julkinen';
$lang['teams:content_label']				= 'Sisältö';
$lang['teams:options_label']				= 'Valinnat';
$lang['teams:intro_label']				= 'Alkuteksti';
$lang['teams:no_category_select_label']	= '-- Ei mikään --';
$lang['teams:new_category_label']		= 'Luo kategoria';
$lang['teams:subscripe_to_rss_label']	= 'Tilaa RSS';
$lang['teams:all_posts_label']			= 'Kaikki artikkelit';
$lang['teams:posts_of_category_suffix']	= ' artikkelia';
$lang['teams:rss_name_suffix']			= ' Uutiset';
$lang['teams:rss_category_suffix']		= ' Uutiset';
$lang['teams:author_name_label']			= 'Tekijän nimi';
$lang['teams:read_more_label']			= 'Lue lisää&nbsp;&raquo;';
$lang['teams:created_hour']				= 'Luotiin tunnissa';
$lang['teams:created_minute']			= 'Luotiin minuutissa';
$lang['teams:comments_enabled_label']	= 'Kommentit päällä';

// titles
$lang['teams:disabled_after']	= 'Kommentointi on otettu pois käytöstä %s jälkeen.';
$lang['teams:create_title']		= 'Luo artikkeli';
$lang['teams:edit_title']		= 'Muokkaa artikkelia "%s"';
$lang['teams:archive_title']		= 'Arkisto';
$lang['teams:posts_title']		= 'Artikkelit';
$lang['teams:rss_posts_title']	= 'Uutisartikkeleita %s';
$lang['teams:teams_title']		= 'Uutiset';
$lang['teams:list_title']		= 'Listaa artikkelit';

// messages
$lang['teams:no_posts']				= 'Artikkeleita ei ole.';
$lang['teams:subscripe_to_rss_desc']	= 'Lue artikkelit tilaamalla RSS syöte. Suosituimmat sähköposti ohjelmat tukevat tätä. Voit myös vaihtoehtoisesti kokeilla <a href="http://reader.google.co.uk/">Google Readeria</a>.';
$lang['teams:currently_no_posts']	= 'Ei artikkeleita tällä hetkellä.';
$lang['teams:post_add_success']		= 'Artikkeli "%s" lisättiin.';
$lang['teams:post_add_error']		= 'Tapahtui virhe.';
$lang['teams:edit_success']			= 'Artikkeli "%s" päivitettiin.';
$lang['teams:edit_error']			= 'Tapahtui virhe.';
$lang['teams:publish_success']		= 'Artikkeli "%s" julkaistiin.';
$lang['teams:mass_publish_success']	= 'Artikkelit "%s" julkaistiin.';
$lang['teams:publish_error']			= 'Yhtään artikkelia ei julkaistu.';
$lang['teams:delete_success']		= 'Artikkeli "%s" poistettiin.';
$lang['teams:mass_delete_success']	= 'Artikkelit "%s" poistettiin.';
$lang['teams:delete_error']			= 'Yhtään artikkelia ei poistettu.';
$lang['teams:already_exist_error']	= 'Artikkelin URL osoite on jo käytössä.';

$lang['teams:twitter_posted']	= 'Lähetetty "%s" %s';
$lang['teams:twitter_error']		= 'Twitter Virhe';

// date
$lang['teams:archive_date_format'] = "%B, %Y";

?>