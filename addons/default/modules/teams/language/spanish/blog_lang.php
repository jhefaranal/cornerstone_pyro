<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                          = 'Entrada';
$lang['teams:posts']                         = 'Entradas';

// labels
$lang['teams:posted_label']					= 'Escrito';
$lang['teams:keywords_label']				= 'Palabras clave';
$lang['teams:tagged_label']					= 'Tagged'; #translate
$lang['teams:category_label'] 				= 'Categoría';
$lang['teams:written_by_label']				= 'Escrito por';
$lang['teams:author_unknown']				= 'Desconocido';
$lang['teams:post_label'] 					= 'Entrada';
$lang['teams:date_label'] 					= 'Fecha';
$lang['teams:time_label'] 					= 'Hora';
$lang['teams:status_label'] 					= 'Estado';
$lang['teams:content_label'] 				= 'Contenido';
$lang['teams:options_label'] 				= 'Opciones';
$lang['teams:intro_label'] 					= 'Introducción';
$lang['teams:draft_label'] 					= 'Borrador';
$lang['teams:live_label'] 					= 'En vivo';
$lang['teams:no_category_select_label'] 		= '-- Ninguna --';
$lang['teams:new_category_label'] 			= 'Agregar una categoría';
$lang['teams:subscripe_to_rss_label'] 		= 'Suscribir al RSS';
$lang['teams:all_posts_label'] 				= 'Todos los artículos';
$lang['teams:posts_of_category_suffix']		= ' artículos';
$lang['teams:rss_name_suffix'] 				= ' Noticias';
$lang['teams:rss_category_suffix'] 			= ' Noticias';
$lang['teams:posted_label'] 					= 'Escrito en';
$lang['teams:author_name_label'] 			= 'Nombre del autor';
$lang['teams:read_more_label'] 				= 'Leer más &raquo;';
$lang['teams:created_hour']                 	= 'Hora (Hora)';
$lang['teams:created_minute']				= 'Hora (Minutos)';
$lang['teams:comments_enabled_label']         = 'Comentarios Habilitados';

// titles
$lang['teams:create_title'] 					= 'Crear un artículo';
$lang['teams:edit_title'] 					= 'Editar artículo "%s"';
$lang['teams:archive_title'] 				= 'Archivo';
$lang['teams:posts_title'] 					= 'Artículos';
$lang['teams:rss_posts_title'] 				= 'Artículo Teams de %s';
$lang['teams:teams_title'] 					= 'Noticias';
$lang['teams:list_title'] 					= 'Lista de artículos';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts'] 						= 'No hay artículos.';
$lang['teams:subscripe_to_rss_desc'] 		= 'Recibe nuestros artículos inmediatamente suscribiendote a nuestros feeds RSS. Puedes hacer esto utilizando los más clientes de correo electrónico mas populares o utilizando <a href="http://reader.google.com/">Google Reader</a>.';
$lang['teams:currently_no_posts'] 			= 'No hay artículos hasta el momento.';
$lang['teams:post_add_success'] 				= 'El artículo "%s" fue agregado.';
$lang['teams:post_add_error'] 				= 'Ha ocurrido un error.';
$lang['teams:edit_success'] 					= 'El artículo "%s" fue actualizado.';
$lang['teams:edit_error'] 					= 'Ha ocurrido un error.';
$lang['teams:publish_success'] 				= 'El artículo "%s" ha sido publicado.';
$lang['teams:mass_publish_success'] 			= 'Los artículos "%s" fueron publicados.';
$lang['teams:publish_error'] 				= 'No se han publicado artículos.';
$lang['teams:delete_success'] 				= 'El artículo "%s" ha sido eliminado.';
$lang['teams:mass_delete_success'] 			= 'Los artículos "%s" han sido eliminados.';
$lang['teams:delete_error'] 					= 'No se han eliminado artículos.';
$lang['teams:already_exist_error'] 			= 'Ya existe un artículo con esta URL.';

$lang['teams:twitter_posted']				= 'Escrito "%s" %s';
$lang['teams:twitter_error'] 				= 'Error de Twitter';

// date
$lang['teams:archive_date_format']			= "%B %Y";

/* End of file teams:lang.php */