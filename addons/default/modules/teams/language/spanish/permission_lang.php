<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Colocar artículos disponibles';
$lang['teams:role_edit_live']	= 'Editar artículos disponibles';
$lang['teams:role_delete_live'] 	= 'Eliminar artículos disponibles';