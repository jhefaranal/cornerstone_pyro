<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Publikuj wpisy';
$lang['teams:role_edit_live']		= 'Edytuj opublikowane wpisy';
$lang['teams:role_delete_live'] 		= 'Usuwaj opublikowane wpisy';