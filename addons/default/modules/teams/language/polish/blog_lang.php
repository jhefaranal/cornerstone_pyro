<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label'] 					= 'Opublikowano w';
$lang['teams:posted_label_alt']					= 'Opublikowano ';
$lang['teams:written_by_label']					= 'Napisany przez';
$lang['teams:author_unknown']					= 'Nieznany';
$lang['teams:keywords_label']					= 'Słowa kluczowe';
$lang['teams:tagged_label']					= 'Tagi';
$lang['teams:category_label'] 					= 'Kategoria';
$lang['teams:post_label'] 					= 'Post';
$lang['teams:date_label'] 					= 'Data';
$lang['teams:date_at']						= '';
$lang['teams:time_label'] 					= 'Czas';
$lang['teams:status_label'] 					= 'Status';
$lang['teams:draft_label'] 					= 'Robocza';
$lang['teams:live_label'] 					= 'Opublikowana';
$lang['teams:content_label'] 					= 'Zawartość';
$lang['teams:options_label'] 					= 'Opcje';
$lang['teams:intro_label'] 					= 'Wprowadzenie';
$lang['teams:no_category_select_label'] 				= '-- Brak --';
$lang['teams:new_category_label'] 				= 'Dodaj kategorię';
$lang['teams:subscripe_to_rss_label'] 				= 'Subskrybuj RSS';
$lang['teams:all_posts_label'] 					= 'Wszystkie wpisy';
$lang['teams:posts_of_category_suffix'] 				= ' - wpisy';
$lang['teams:rss_name_suffix'] 					= '';
$lang['teams:rss_category_suffix'] 				= '';
$lang['teams:author_name_label'] 				= 'Imię autora';
$lang['teams:read_more_label'] 					= 'Czytaj więcej&nbsp;&raquo;';
$lang['teams:created_hour']               		    	= 'Czas (Godzina)';
$lang['teams:created_minute']              		    	= 'Czas (Minuta)';
$lang['teams:comments_enabled_label']         			= 'Komentarze aktywne';

// titles
$lang['teams:create_title'] 					= 'Dodaj wpis';
$lang['teams:edit_title'] 					= 'Edytuj wpis "%s"';
$lang['teams:archive_title'] 					= 'Archiwum';
$lang['teams:posts_title'] 					= 'Wpisy';
$lang['teams:rss_posts_title'] 					= 'Najnowsze wpisy dla %s';
$lang['teams:teams_title'] 					= 'Teams';
$lang['teams:list_title'] 					= 'Lista wpisów';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts'] 						= 'Nie ma żadnych wpisów.';
$lang['teams:subscripe_to_rss_desc'] 				= 'Bądź na bieżąco subskrybując nasz kanał RSS. Możesz to zrobić za pomocą popularnych klientów pocztowych, lub skorzystać z <a href="http://reader.google.com/">Google Reader\'a</a>.';
$lang['teams:currently_no_posts'] 				= 'W tym momencie nie ma żadnych wpisów.';
$lang['teams:post_add_success'] 					= 'Wpis "%s" został dodany.';
$lang['teams:post_add_error'] 					= 'Wystąpił błąd.';
$lang['teams:edit_success'] 					= 'Wpis "%s" został zaktualizowany.';
$lang['teams:edit_error'] 					= 'Wystąpił błąd.';
$lang['teams:publish_success'] 					= 'Wpis "%s" został opublikowany.';
$lang['teams:mass_publish_success'] 				= 'Wpisy "%s" zostały opublikowane.';
$lang['teams:publish_error'] 					= 'Żadne wpisy nie zostały opublikowane.';
$lang['teams:delete_success'] 					= 'Wpis "%s" został usunięty.';
$lang['teams:mass_delete_success'] 				= 'Wpisy "%s" zostały usunięte.';
$lang['teams:delete_error'] 					= 'Żadne wpisy nie zostały usunięte.';
$lang['teams:already_exist_error'] 				= 'Wpis z tym adresem URL już istnieje.';

$lang['teams:twitter_posted'] 					= 'Opublikowano "%s" %s';
$lang['teams:twitter_error'] 					= 'Błąd Twitter\'a';

// date
$lang['teams:archive_date_format']				= "%B %Y";