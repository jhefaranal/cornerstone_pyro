<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label'] 				= 'Geplaatst';
$lang['teams:posted_label_alt']			= 'Geplaatst op';
$lang['teams:written_by_label']			= 'Geschreven door';
$lang['teams:author_unknown']			= 'Onbekend';
$lang['teams:keywords_label']			= 'Sleutelwoorden';
$lang['teams:tagged_label']				= 'Etiket';
$lang['teams:category_label'] 			= 'Categorie';
$lang['teams:post_label'] 				= 'Post';
$lang['teams:date_label'] 				= 'Datum';
$lang['teams:date_at']					= 'op';
$lang['teams:time_label'] 				= 'Tijd';
$lang['teams:status_label'] 				= 'Status';
$lang['teams:draft_label'] 				= 'Concept';
$lang['teams:live_label'] 				= 'Live';
$lang['teams:content_label'] 			= 'Content';
$lang['teams:options_label'] 			= 'Opties';
$lang['teams:slug_label'] 				= 'URL';
$lang['teams:intro_label'] 				= 'Introductie';
$lang['teams:no_category_select_label'] 	= '-- Geen --';
$lang['teams:new_category_label'] 		= 'Voeg een categorie toe';
$lang['teams:subscripe_to_rss_label'] 	= 'Abonneer op RSS';
$lang['teams:all_posts_label'] 			= 'Alle artikelen';
$lang['teams:posts_of_category_suffix'] 	= ' artikelen';
$lang['teams:rss_name_suffix'] 			= ' Nieuws';
$lang['teams:rss_category_suffix'] 		= ' Nieuws';
$lang['teams:author_name_label'] 		= 'Auteur naam';
$lang['teams:read_more_label'] 			= 'Lees Meer&nbsp;&raquo;';
$lang['teams:created_hour']           	= 'Tijd (Uren)';
$lang['teams:created_minute']       		= 'Tijd (Minuten)';
$lang['teams:comments_enabled_label']	= 'Reacties ingeschakeld';

// titles
$lang['teams:disabled_after'] 			= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:create_title'] 				= 'Voeg artikel toe';
$lang['teams:edit_title'] 				= 'Wijzig artikel "%s"';
$lang['teams:archive_title'] 			= 'Archief';
$lang['teams:posts_title'] 				= 'Artikelen';
$lang['teams:rss_posts_title'] 			= 'Nieuws artikelen voor %s';
$lang['teams:teams_title'] 				= 'Nieuws';
$lang['teams:list_title'] 				= 'Overzicht artikelen';

// messages
$lang['teams:no_posts'] 					= 'Er zijn geen artikelen.';
$lang['teams:subscripe_to_rss_desc'] 	= 'Ontvang artikelen meteen door te abonneren op onze RSS feed. U kunt dit doen met de meeste populaire e-mail programma&acute;s, of probeer <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts'] 		= 'Er zijn momenteel geen artikelen.';
$lang['teams:post_add_success'] 			= 'Het artikel "%s" is opgeslagen.';
$lang['teams:post_add_error'] 			= 'Er is een fout opgetreden.';
$lang['teams:edit_success'] 				= 'Het artikel "%s" is opgeslagen.';
$lang['teams:edit_error'] 				= 'Er is een fout opgetreden.';
$lang['teams:publish_success'] 			= 'Het artikel "%s" is gepubliceerd.';
$lang['teams:mass_publish_success'] 		= 'De artikelen "%s" zijn gepubliceerd.';
$lang['teams:publish_error'] 			= 'Geen artikelen zijn gepubliceerd.';
$lang['teams:delete_success'] 			= 'Het artikel "%s" is verwijderd.';
$lang['teams:mass_delete_success'] 		= 'De artikelen "%s" zijn verwijderd.';
$lang['teams:delete_error'] 				= 'Geen artikelen zijn verwijderd.';
$lang['teams:already_exist_error'] 		= 'Een artikel met deze URL bestaat al.';

$lang['teams:twitter_posted']			= 'Geplaatst "%s" %s';
$lang['teams:twitter_error'] 			= 'Twitter Fout';

// date
$lang['teams:archive_date_format']		= "%B %Y";
