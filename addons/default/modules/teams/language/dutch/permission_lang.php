<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Zet artikelen online';
$lang['teams:role_edit_live']	= 'Bewerk online artikelen';
$lang['teams:role_delete_live'] 	= 'Verwijder online artikelen';