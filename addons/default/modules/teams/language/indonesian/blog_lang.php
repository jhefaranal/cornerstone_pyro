<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']                   = 'Dipostkan';
$lang['teams:posted_label_alt']               = 'Dipostkan pada';
$lang['teams:written_by_label']				= 'Ditulis oleh';
$lang['teams:author_unknown']				= 'Tidak dikenal';
$lang['teams:keywords_label']				= 'Kata Kunci';
$lang['teams:tagged_label']					= 'Label';
$lang['teams:category_label']                 = 'Kategori';
$lang['teams:post_label']                     = 'Post';
$lang['teams:date_label']                     = 'Tanggal';
$lang['teams:date_at']                        = 'pada';
$lang['teams:time_label']                     = 'Waktu';
$lang['teams:status_label']                   = 'Status';
$lang['teams:draft_label']                    = 'Draft';
$lang['teams:live_label']                     = 'Publikasikan';
$lang['teams:content_label']                  = 'Konten';
$lang['teams:options_label']                  = 'Opsi';
$lang['teams:intro_label']                    = 'Pendahuluan';
$lang['teams:no_category_select_label']       = '-- tidak ada --';
$lang['teams:new_category_label']             = 'Tambah kategori';
$lang['teams:subscripe_to_rss_label']         = 'Berlangganan RSS';
$lang['teams:all_posts_label']             = 'Semua post';
$lang['teams:posts_of_category_suffix']    = ' post';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Nama penulis';
$lang['teams:read_more_label']                = 'Selengkapnya&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Dibuat pada Jam';
$lang['teams:created_minute']                 = 'Dibuat pada Menit';
$lang['teams:comments_enabled_label']         = 'Perbolehkan Komentar';

// titles
$lang['teams:create_title']                   = 'Tambah Post';
$lang['teams:edit_title']                     = 'Edit post "%s"';
$lang['teams:archive_title']                 = 'Arsip';
$lang['teams:posts_title']					= 'Post';
$lang['teams:rss_posts_title']				= 'Teams post untuk %s';
$lang['teams:teams_title']					= 'Teams';
$lang['teams:list_title']					= 'Daftar Post';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']                    = 'Tidak ada post.';
$lang['teams:subscripe_to_rss_desc']          = 'Ambil post langsung dengan berlangganan melalui RSS feed kami. Anda dapat melakukannya menggunakan hampir semua e-mail client, atau coba <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'Untuk sementara tidak ada post.';
$lang['teams:post_add_success']            = 'Post "%s" telah ditambahkan.';
$lang['teams:post_add_error']              = 'Terjadi kesalahan.';
$lang['teams:edit_success']                   = 'Post "%s" telah diperbaharui.';
$lang['teams:edit_error']                     = 'Terjadi kesalahan.';
$lang['teams:publish_success']                = 'Post "%s" telah dipublikasikan.';
$lang['teams:mass_publish_success']           = 'Post "%s" telah dipublikasikan.';
$lang['teams:publish_error']                  = 'Tidak ada post yang terpublikasi.';
$lang['teams:delete_success']                 = 'Post "%s" telah dihapus.';
$lang['teams:mass_delete_success']            = 'Post "%s" telah dihapus.';
$lang['teams:delete_error']                   = 'Tidak ada post yan terhapus.';
$lang['teams:already_exist_error']            = 'Post dengan URL ini sudah ada.';

$lang['teams:twitter_posted']                 = 'Dipostkan "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter Error';

// date
$lang['teams:archive_date_format']		= "%B %Y";
