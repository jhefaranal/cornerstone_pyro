<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Publikasikan artikel';
$lang['teams:role_edit_live']	= 'Edit artikel terpublikasi';
$lang['teams:role_delete_live'] 	= 'Hapus artikel terpublikasi';
