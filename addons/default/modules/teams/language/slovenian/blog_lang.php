<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Post'; #translate
$lang['teams:posts']                   = 'Posts'; #translate

// labels
$lang['teams:posted_label']                   = 'Objavljeno';
$lang['teams:posted_label_alt']               = 'Objavljeno ob';
$lang['teams:written_by_label']				= 'Objavil';
$lang['teams:author_unknown']				= 'Neznan';
$lang['teams:keywords_label']				= 'Klj. besede';
$lang['teams:tagged_label']					= 'Označen';
$lang['teams:category_label']                 = 'Kategorija';
$lang['teams:post_label']                     = 'Objava';
$lang['teams:date_label']                     = 'Datum';
$lang['teams:date_at']                        = 'ob';
$lang['teams:time_label']                     = 'Čas';
$lang['teams:status_label']                   = 'Stanje';
$lang['teams:draft_label']                    = 'Osnutek';
$lang['teams:live_label']                     = 'Vidno';
$lang['teams:content_label']                  = 'Vsebina';
$lang['teams:options_label']                  = 'Možnosti';
$lang['teams:intro_label']                    = 'Navodila';
$lang['teams:no_category_select_label']       = '-- Brez --';
$lang['teams:new_category_label']             = 'Dodaj kategorijo';
$lang['teams:subscripe_to_rss_label']         = 'Prijava na RSS';
$lang['teams:all_posts_label']             = 'Vsi prispevki';
$lang['teams:posts_of_category_suffix']    = ' prispevki';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Ime avtorja';
$lang['teams:read_more_label']                = 'Preberi vse&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Ustvarjeno ura';
$lang['teams:created_minute']                 = 'Ustvarjeno minuta';
$lang['teams:comments_enabled_label']         = 'Komentaji omogočeni';

// titles
$lang['teams:create_title']                   = 'Dodaj prispevek';
$lang['teams:edit_title']                     = 'Uredi prispevek "%s"';
$lang['teams:archive_title']                  = 'Arhiv';
$lang['teams:posts_title']                 = 'Članki';
$lang['teams:rss_posts_title']             = 'Teams prispevki za %s';
$lang['teams:teams_title']                     = 'Teams';
$lang['teams:list_title']                     = 'Seznam prispevkov';

// messages
$lang['teams:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['teams:no_posts']                    = 'Trenutno še ni prispevkov.';
$lang['teams:subscripe_to_rss_desc']          = 'Pridite do prispevkov v najkrajšem možnem času tako da se prijavite na RSS podajalca. To lahko storite preko popularnega email programa ali poizkusite  <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']          = 'Ta trenutek ni prispevkov';
$lang['teams:post_add_success']            = 'Vap prispevek je bil dodan "%s" ';
$lang['teams:post_add_error']              = 'Prišlo je do napake';
$lang['teams:edit_success']                   = 'Prispevek "%s" je bil oddan.';
$lang['teams:edit_error']                     = 'Prišlo je do napake.';
$lang['teams:publish_success']                = 'Prispevek "%s" je bil objavljen.';
$lang['teams:mass_publish_success']           = 'Prispeveki "%s" so bili objavljeni.';
$lang['teams:publish_error']                  = 'Noben prispevk ni bil objavljen.';
$lang['teams:delete_success']                 = 'Prispevki "%s" so bili izbrisani.';
$lang['teams:mass_delete_success']            = 'Prispevki "%s" so bili izbrisani.';
$lang['teams:delete_error']                   = 'Noben od prispevkov ni bil izbrisan..';
$lang['teams:already_exist_error']            = 'Prispevek s tem URL-jem že obstaja.';

$lang['teams:twitter_posted']                 = 'Objavljeno "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter napaka';

// date
$lang['teams:archive_date_format']		= "%d' %m' %Y";

/* End of file teams_lang.php */