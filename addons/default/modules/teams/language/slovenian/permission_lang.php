<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Objavi članek';
$lang['teams:role_edit_live']	= 'Uredi objavljen članek';
$lang['teams:role_delete_live'] 	= 'Izbriši objavljen članek';

// slovenian premission_lang.php