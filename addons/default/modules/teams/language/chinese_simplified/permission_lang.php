<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Chinese Simpplified translation.
 *
 * @author		Kefeng DENG
 * @package		PyroCMS
 * @subpackage 	Teams Module
 * @category	Modules
 * @link		http://pyrocms.com
 * @date		2012-06-22
 * @version		1.0
 */
// Teams Permissions
$lang['teams:role_put_live']		= '將文章上线';
$lang['teams:role_edit_live']	= '编辑上线文章';
$lang['teams:role_delete_live'] 	= '刪除上线文章';