<?php
/**
 * Chinese Simpplified translation.
 *
 * @author		Kefeng DENG
 * @package		PyroCMS
 * @subpackage 	Teams Module
 * @category	Modules
 * @link		http://pyrocms.com
 * @date		2012-06-22
 * @version		1.0
 */
$lang['teams:post']                 = '帖子'; #translate
$lang['teams:posts']                   = '帖子'; #translate

// labels
$lang['teams:posted_label'] 				= '已發佈';
$lang['teams:posted_label_alt']			= '發表於';
$lang['teams:written_by_label']				= '撰文者';
$lang['teams:author_unknown']				= '未知';
$lang['teams:keywords_label']				= '關鍵字';
$lang['teams:tagged_label']					= '標籤';
$lang['teams:category_label'] 			= '分類';
$lang['teams:post_label'] 				= '發表';
$lang['teams:date_label'] 				= '日期';
$lang['teams:date_at']					= '於';
$lang['teams:time_label'] 				= '時間';
$lang['teams:status_label'] 				= '狀態';
$lang['teams:draft_label'] 				= '草稿';
$lang['teams:live_label'] 				= '上線';
$lang['teams:content_label'] 			= '內容';
$lang['teams:options_label'] 			= '選項';
$lang['teams:intro_label'] 				= '簡介';
$lang['teams:no_category_select_label'] 	= '-- 無 --';
$lang['teams:new_category_label'] 		= '新增分類';
$lang['teams:subscripe_to_rss_label'] 	= '訂閱 RSS';
$lang['teams:all_posts_label'] 		= '所有文章';
$lang['teams:posts_of_category_suffix'] 		= ' 文章';
$lang['teams:rss_name_suffix'] 					= ' 新聞';
$lang['teams:rss_category_suffix'] 				= ' 新聞';
$lang['teams:author_name_label'] 				= '作者名稱';
$lang['teams:read_more_label'] 					= '閱讀更多&nbsp;&raquo;';
$lang['teams:created_hour']                  = '時間 (時)';
$lang['teams:created_minute']                = '時間 (分)';
$lang['teams:comments_enabled_label']         = '允許回應';

// titles
$lang['teams:create_title'] 				= '新增文章';
$lang['teams:edit_title'] 				= '編輯文章 "%s"';
$lang['teams:archive_title'] 			= '檔案櫃';
$lang['teams:posts_title'] 			= '文章';
$lang['teams:rss_posts_title'] 		= '%s 的最新文章';
$lang['teams:teams_title'] 				= '新聞';
$lang['teams:list_title'] 				= '文章列表';

// messages
$lang['teams:disabled_after'] 				= '在%s之后，回复功能将会被关闭。'; #translate
$lang['teams:no_posts'] 				= '沒有文章';
$lang['teams:subscripe_to_rss_desc'] 	= '訂閱我們的 RSS 摘要可立即獲得最新的文章，您可以使用慣用的收件軟體，或試試看 <a href="http://reader.google.com.tw">Google 閱讀器</a>。';
$lang['teams:currently_no_posts'] 	= '目前沒有文章';
$lang['teams:post_add_success'] 		= '文章 "%s" 已經新增';
$lang['teams:post_add_error'] 		= '發生了錯誤';
$lang['teams:edit_success'] 				= '這文章 "%s" 更新了。';
$lang['teams:edit_error'] 				= '發生了錯誤';
$lang['teams:publish_success'] 			= '此文章 "%s" 已經被發佈。';
$lang['teams:mass_publish_success'] 		= '這些文章 "%s" 已經被發佈。';
$lang['teams:publish_error'] 			= '沒有文章被發佈。';
$lang['teams:delete_success'] 			= '此文章 "%s" 已經被刪除。';
$lang['teams:mass_delete_success'] 		= '這些文章 "%s" 已經被刪除。';
$lang['teams:delete_error'] 				= '沒有文章被刪除。';
$lang['teams:already_exist_error'] 		= '一則相同網址的文章已經存在。';

$lang['teams:twitter_posted']			= '發佈 "%s" %s';
$lang['teams:twitter_error'] 			= 'Twitter 錯誤';

// date
$lang['teams:archive_date_format']		= "%B' %Y"; #translate format - see php strftime documentation