<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['teams:post']                 = 'Artikel';
$lang['teams:posts']                   = 'Artikel';

// labels
$lang['teams:posted_label']                   = 'Ver&ouml;ffentlicht';
$lang['teams:postet_label_alt']               = 'Ver&ouml;ffentlicht in';
$lang['teams:written_by_label']				 = 'Geschrieben von';
$lang['teams:author_unknown']				 = 'Unbekannt';
$lang['teams:keywords_label']				 = 'Stichw&ouml;rter';
$lang['teams:tagged_label']					 = 'Gekennzeichnet';
$lang['teams:category_label']                 = 'Kategorie';
$lang['teams:post_label']                     = 'Artikel';
$lang['teams:date_label']                     = 'Datum';
$lang['teams:date_at']                        = 'um';
$lang['teams:time_label']                     = 'Uhrzeit';
$lang['teams:status_label']                   = 'Status';
$lang['teams:draft_label']                    = 'Unver&ouml;ffentlicht';
$lang['teams:live_label']                     = 'Ver&ouml;ffentlicht';
$lang['teams:content_label']                  = 'Inhalt';
$lang['teams:options_label']                  = 'Optionen';
$lang['teams:intro_label']                    = 'Einf&uuml;hrung';
$lang['teams:no_category_select_label']       = '-- Kein Label --';
$lang['teams:new_category_label']             = 'Kategorie hinzuf&uuml;gen';
$lang['teams:subscripe_to_rss_label']         = 'RSS abonnieren';
$lang['teams:all_posts_label']                = 'Alle Artikel';
$lang['teams:posts_of_category_suffix']       = ' Artikel';
$lang['teams:rss_name_suffix']                = ' Teams';
$lang['teams:rss_category_suffix']            = ' Teams';
$lang['teams:author_name_label']              = 'Autor';
$lang['teams:read_more_label']                = 'Mehr lesen&nbsp;&raquo;';
$lang['teams:created_hour']                   = 'Erstellt zur Stunde';
$lang['teams:created_minute']                 = 'Erstellt zur Minute';
$lang['teams:comments_enabled_label']         = 'Kommentare aktiv';

// titles
$lang['teams:create_title']                   = 'Artikel erstellen';
$lang['teams:edit_title']                     = 'Artikel "%s" bearbeiten';
$lang['teams:archive_title']                  = 'Archiv';
$lang['teams:posts_title']                    = 'Artikel';
$lang['teams:rss_posts_title']                = 'Teams Artikel f&uuml;r %s';
$lang['teams:teams_title']                     = 'Teams';
$lang['teams:list_title']                     = 'Artikel auflisten';

// messages
$lang['teams:disabled_after'] 				= 'Kommentare nach %s wurden deaktiviert.';
$lang['teams:no_posts']                       = 'Es existieren keine Artikel.';
$lang['teams:subscripe_to_rss_desc']          = 'Abonnieren Sie unseren RSS Feed und erhalten Sie alle Artikel frei Haus. Sie k&ouml;nnen dies mit den meisten Email-Clients tun, oder z.B. mit <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['teams:currently_no_posts']             = 'Es existieren zur Zeit keine Artikel.';
$lang['teams:post_add_success']               = 'Der Artikel "%s" wurde hinzugef&uuml;gt.';
$lang['teams:post_add_error']                 = 'Ein Fehler ist aufgetreten.';
$lang['teams:edit_success']                   = 'Der Artikel "%s" wurde aktualisiert.';
$lang['teams:edit_error']                     = 'Ein Fehler ist aufgetreten.';
$lang['teams:publish_success']                = 'Der Artikel "%s" wurde ver&ouml;ffentlicht.';
$lang['teams:mass_publish_success']           = 'Die Artikel "%s" wurden ver&ouml;ffentlicht.';
$lang['teams:publish_error']                  = 'Ein Fehler ist aufgetreten. Es wurden keine Artikel ver&ouml;ffentlicht.';
$lang['teams:delete_success']                 = 'Der Artikel "%s" wurde gel&ouml;scht.';
$lang['teams:mass_delete_success']            = 'Die Artikel "%s" wurden gel&ouml;scht.';
$lang['teams:delete_error']                   = 'Ein Fehler ist aufgetreten. Keine Artikel wurden gel&ouml;scht.';
$lang['teams:already_exist_error']            = 'Ein Artikel mit dieser URL existiert bereits.';

$lang['teams:twitter_posted']                 = 'Gepostet "%s" %s';
$lang['teams:twitter_error']                  = 'Twitter Fehler';

// date
$lang['teams:archive_date_format']		     = "%B %Y";
