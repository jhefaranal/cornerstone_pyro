<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Teams Permissions
$lang['teams:role_put_live']		= 'Artikel live setzen';
$lang['teams:role_edit_live']	= 'Live-Artikel bearbeiten';
$lang['teams:role_delete_live'] 	= 'Live-Artikel l&ouml;schen';

/* End of file permission_lang.php */