<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Teams module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Teams
 */
class Module_Teams extends Module
{
	public $version = '2.0.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Teams',
				'ar' => 'المدوّنة',
				'br' => 'Teams',
				'pt' => 'Teams',
				'el' => 'Ιστολόγιο',
                            'fa' => 'بلاگ',
				'he' => 'בלוג',
				'id' => 'Teams',
				'lt' => 'Teamsas',
				'pl' => 'Teams',
				'ru' => 'Блог',
				'tw' => '文章',
				'cn' => '文章',
				'hu' => 'Teams',
				'fi' => 'Teamsi',
				'th' => 'บล็อก',
				'se' => 'Teamsg',
			),
			'description' => array(
				'en' => 'Post Teams entries.',
				'ar' => 'أنشر المقالات على مدوّنتك.',
				'br' => 'Escrever publicações de teams',
				'pt' => 'Escrever e editar publicações no teams',
				'cs' => 'Publikujte nové články a příspěvky na teams.', #update translation
				'da' => 'Skriv teamsindlæg',
				'de' => 'Veröffentliche neue Artikel und Teams-Einträge', #update translation
				'sl' => 'Objavite teams prispevke',
				'fi' => 'Kirjoita teamsi artikkeleita.',
				'el' => 'Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.',
				'es' => 'Escribe entradas para los artículos y teams (web log).', #update translation
                                'fa' => 'مقالات منتشر شده در بلاگ',
				'fr' => 'Poster des articles d\'actualités.',
				'he' => 'ניהול בלוג',
				'id' => 'Post entri teams',
				'it' => 'Pubblica notizie e post per il teams.', #update translation
				'lt' => 'Rašykite naujienas bei teams\'o įrašus.',
				'nl' => 'Post nieuwsartikelen en teamss op uw site.',
				'pl' => 'Dodawaj nowe wpisy na teamsu',
				'ru' => 'Управление записями блога.',
				'tw' => '發表新聞訊息、部落格等文章。',
				'cn' => '发表新闻讯息、部落格等文章。',
				'th' => 'โพสต์รายการบล็อก',
				'hu' => 'Teams bejegyzések létrehozása.',
				'se' => 'Inlägg i teamsgen.',
			),
			'frontend' => true,
			'backend' => true,
			'skip_xss' => true,
			'menu' => 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
				'posts' => array(
					'name' => 'teams:posts_title',
					'uri' => 'admin/teams',
					'shortcuts' => array(
						array(
							'name' => 'teams:create_title',
							'uri' => 'admin/teams/create',
							'class' => 'add',
						),
					),
				),
				'categories' => array(
					'name' => 'cat:list_title',
					'uri' => 'admin/teams/categories',
					'shortcuts' => array(
						array(
							'name' => 'cat:create_title',
							'uri' => 'admin/teams/categories/create',
							'class' => 'add',
						),
					),
				),
			),
		);

		/*if (function_exists('group_has_role'))
		{
			if(group_has_role('teams', 'admin_teams_fields'))
			{
				$info['sections']['fields'] = array(
							'name' 	=> 'global:custom_fields',
							'uri' 	=> 'admin/teams/fields',
								'shortcuts' => array(
									'create' => array(
										'name' 	=> 'streams:add_field',
										'uri' 	=> 'admin/teams/fields/create',
										'class' => 'add'
										)
									)
							);
			}
		}*/

		return $info;
	}

	public function install()
	{
		$this->dbforge->drop_table('teams_categories');

		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('teamss');

		// Just in case.
		$this->dbforge->drop_table('teams');

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'teamss')->delete('data_streams');
		}

		// Create the teams categories table.
		$this->install_tables(array(
			'teams_categories' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true, 'key' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true),
			),
		));

		$this->streams->streams->add_stream(
			'lang:teams:teams_title',
			'teams',
			'teamss',
			null,
			null
		);

		// Add the intro field.
		// This can be later removed by an admin.
		$intro_field = array(
			'name'		=> 'lang:teams:intro_label',
			'slug'		=> 'intro',
			'namespace' => 'teamss',
			'type'		=> 'wysiwyg',
			'assign'	=> 'teams',
			'extra'		=> array('editor_type' => 'simple', 'allow_tags' => 'y'),
			'required'	=> true
		);
		$this->streams->fields->add_field($intro_field);

		// Ad the rest of the teams fields the normal way.
		$teams_fields = array(
				'title' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false, 'unique' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false),
				'category_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'body' => array('type' => 'TEXT'),
				'parsed' => array('type' => 'TEXT'),
				'keywords' => array('type' => 'VARCHAR', 'constraint' => 32, 'default' => ''),
				'author_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'created_on' => array('type' => 'INT', 'constraint' => 11),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'comments_enabled' => array('type' => 'ENUM', 'constraint' => array('no','1 day','1 week','2 weeks','1 month', '3 months', 'always'), 'default' => '3 months'),
				'status' => array('type' => 'ENUM', 'constraint' => array('draft', 'live'), 'default' => 'draft'),
				'type' => array('type' => 'SET', 'constraint' => array('html', 'markdown', 'wysiwyg-advanced', 'wysiwyg-simple')),
				'preview_hash' => array('type' => 'CHAR', 'constraint' => 32, 'default' => ''),
		);
		return $this->dbforge->add_column('teams', $teams_fields);
	}

	public function uninstall()
	{
		// This is a core module, lets keep it around.
		return false;
	}

	public function upgrade($old_version)
	{
		return true;
	}
}
