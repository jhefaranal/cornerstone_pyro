<?php

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label'] 			= 'Odesláno';
$lang['faq:posted_label_alt']			= 'Odesláno';
$lang['faq:written_by_label']				= 'Written by'; #translate
$lang['faq:author_unknown']				= 'Unknown'; #translate
$lang['faq:keywords_label']				= 'Keywords'; #translate
$lang['faq:tagged_label']					= 'Tagged'; #translate
$lang['faq:category_label'] 			= 'Kategorie';
$lang['faq:post_label'] 			= 'Odeslat';
$lang['faq:date_label'] 			= 'Datum';
$lang['faq:date_at']				= 'v';
$lang['faq:time_label'] 			= 'Čas';
$lang['faq:status_label'] 			= 'Stav';
$lang['faq:draft_label'] 			= 'Koncept';
$lang['faq:live_label'] 			= 'Publikováno';
$lang['faq:content_label'] 			= 'Obsah';
$lang['faq:options_label'] 			= 'Možnosti';
$lang['faq:intro_label'] 			= 'Úvod';
$lang['faq:no_category_select_label'] 		= '-- Nic --';
$lang['faq:new_category_label'] 		= 'Přidat kategorii';
$lang['faq:subscripe_to_rss_label'] 		= 'Přihlásit se k odběru RSS';
$lang['faq:all_posts_label'] 		= 'Všechny články';
$lang['faq:posts_of_category_suffix'] 	= ' články';
$lang['faq:rss_name_suffix'] 			= ' Novinky';
$lang['faq:rss_category_suffix'] 		= ' Novinky';
$lang['faq:author_name_label'] 		= 'Autor';
$lang['faq:read_more_label'] 			= 'Čtěte dále';
$lang['faq:created_hour']                      = 'Vytvořeno v hodině';
$lang['faq:created_minute']                    = 'Vytvořeno v minutě';
$lang['faq:comments_enabled_label']         = 'Komentáře povoleny';

// titles
$lang['faq:create_title'] 			= 'Přidat článek';
$lang['faq:edit_title'] 			= 'Upravit článek "%s"';
$lang['faq:archive_title'] 			= 'Archiv';
$lang['faq:posts_title'] 			= 'Články';
$lang['faq:rss_posts_title'] 		= 'Novinky pro %s';
$lang['faq:faq_title'] 			= 'Novinky';
$lang['faq:list_title'] 			= 'Seznam článků';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts'] 			= 'Nejsou zde žádné články.';
$lang['faq:subscripe_to_rss_desc'] 		= 'Dostávejte články rovnou pomocí RSS. Můžete použít populární e-mailové klienty nebo zkuste <a href="http://reader.google.cz/">Google Reader</a>.';
$lang['faq:currently_no_posts'] 		= 'Nejsou zde žádné články.';
$lang['faq:post_add_success'] 		= 'Článek "%s" byl přidán.';
$lang['faq:post_add_error'] 		= 'Objevila se chyba.';
$lang['faq:edit_success'] 			= 'Článek "%s" byl aktualizován.';
$lang['faq:edit_error'] 			= 'Objevila se chyba.';
$lang['faq:publish_success'] 			= 'Článek "%s" byl publikován.';
$lang['faq:mass_publish_success'] 		= 'Články "%s" byly publikovány.';
$lang['faq:publish_error'] 			= 'žádné články nebyly publikovány.';
$lang['faq:delete_success'] 			= 'Článek "%s" byl smazán.';
$lang['faq:mass_delete_success'] 		= 'Články "%s" byly smazány.';
$lang['faq:delete_error'] 			= 'Žádné články nebyly smazány.';
$lang['faq:already_exist_error'] 		= 'Článek s touto adresou URL již existuje.';

$lang['faq:twitter_posted']			= 'Publikováno "%s" %s';
$lang['faq:twitter_error'] 			= 'Chyba Twitteru';

// date
$lang['faq:archive_date_format']		= "%B %Y";

?>