<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Publikování příspěvků';
$lang['faq:role_edit_live']	= 'Úpravy publikovaných příspěvků';
$lang['faq:role_delete_live'] 	= 'Mazání publikovaných příspěvků';