<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                           = 'Bejegyzés';
$lang['faq:posts']                          = 'Bejegyzések';

// labels
$lang['faq:posted_label']                   = 'Bejegyezve';
$lang['faq:posted_label_alt']               = 'Bejegyezve';
$lang['faq:written_by_label']               = 'Írta';
$lang['faq:author_unknown']                 = 'Ismeretlen';
$lang['faq:keywords_label']                 = 'Kulcsszavak';
$lang['faq:tagged_label']                   = 'Tagged';
$lang['faq:category_label']                 = 'Kategória';
$lang['faq:post_label']                     = 'Bejegyzés';
$lang['faq:date_label']                     = 'Dátum';
$lang['faq:date_at']                        = '';
$lang['faq:time_label']                     = 'Idő';
$lang['faq:status_label']                   = 'Állapot';
$lang['faq:draft_label']                    = 'Piszkozat';
$lang['faq:live_label']                     = 'Élő';
$lang['faq:content_label']                  = 'Tartalom';
$lang['faq:options_label']                  = 'Beállítások';
$lang['faq:intro_label']                    = 'Bevezető';
$lang['faq:no_category_select_label']       = '-- Nincs --';
$lang['faq:new_category_label']             = 'Kategória hozzáadása';
$lang['faq:subscripe_to_rss_label']         = 'Feliratkozás RSS-re';
$lang['faq:all_posts_label']                = 'Összes bejegyzés';
$lang['faq:posts_of_category_suffix']       = ' bejegyzések';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Szerző neve';
$lang['faq:read_more_label']                = 'Tovább&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Létrehozva pár órája';
$lang['faq:created_minute']                 = 'Létrehozva pár perce';
$lang['faq:comments_enabled_label']         = 'Hozzászólás engedélyezése';

// titles
$lang['faq:create_title']                   = 'Bejegyzés hozzáadása';
$lang['faq:edit_title']                     = 'A(z) "%s" bejegyzés szerkesztése';
$lang['faq:archive_title']                  = 'Archívum';
$lang['faq:posts_title']                    = 'Bejegyzések';
$lang['faq:rss_posts_title']                = 'Faq bejegyzés %s-ért';
$lang['faq:faq_title']                     = 'Faq';
$lang['faq:list_title']                     = 'Bejegyzések listája';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']                       = 'Nincs bejegyzés.';
$lang['faq:subscripe_to_rss_desc']          = 'A bejegyzést jelentesd meg saját RSS-en. Ezt meg lehet tenni az ismertebb levelezőkkel vagy a <a href="http://reader.google.co.uk/">Google Reader</a>rel.';
$lang['faq:currently_no_posts']             = 'Ebben a pillanatban még nincs bejegyzés.';
$lang['faq:post_add_success']               = 'A(z) "%s" bejegyzés sikeresen hozzáadva.';
$lang['faq:post_add_error']                 = 'A bejegyzés hozzáadása sikertelen.';
$lang['faq:edit_success']                   = 'A(z) "%s" bejegyzés sikeresen módosítva.';
$lang['faq:edit_error']                     = 'A bejegyzés módosítása sikertelen.';
$lang['faq:publish_success']                = 'A(z) "%s" bejegyzés közzétéve.';
$lang['faq:mass_publish_success']           = 'A(z) "%s" bejegyzés már közzétéve.';
$lang['faq:publish_error']                  = 'Nincs bejegyzés közzétéve.';
$lang['faq:delete_success']                 = 'A(z) "%s" bejegyzés törölve.';
$lang['faq:mass_delete_success']            = 'A(z) "%s" bejegyzés már törölve.';
$lang['faq:delete_error']                   = 'Nincs törölhető bejegyzés.';
$lang['faq:already_exist_error']            = 'Egy bejegyzés már létezik ezzel a hivatkozással.';

$lang['faq:twitter_posted']                 = 'Bejegyezve "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter Hiba';

// date
$lang['faq:archive_date_format']            = "%B %Y";
