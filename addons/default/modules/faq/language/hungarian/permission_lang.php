<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Bejegyzés élővé tétele';
$lang['faq:role_edit_live']            = 'Élő bejegyzés szerkesztése';
$lang['faq:role_delete_live']          = 'Élő bejegyzés törlése';