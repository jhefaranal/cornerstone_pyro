<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Artikel';
$lang['faq:posts']                   = 'Artikel';

// labels
$lang['faq:posted_label']                   = 'Ver&ouml;ffentlicht';
$lang['faq:postet_label_alt']               = 'Ver&ouml;ffentlicht in';
$lang['faq:written_by_label']				 = 'Geschrieben von';
$lang['faq:author_unknown']				 = 'Unbekannt';
$lang['faq:keywords_label']				 = 'Stichw&ouml;rter';
$lang['faq:tagged_label']					 = 'Gekennzeichnet';
$lang['faq:category_label']                 = 'Kategorie';
$lang['faq:post_label']                     = 'Artikel';
$lang['faq:date_label']                     = 'Datum';
$lang['faq:date_at']                        = 'um';
$lang['faq:time_label']                     = 'Uhrzeit';
$lang['faq:status_label']                   = 'Status';
$lang['faq:draft_label']                    = 'Unver&ouml;ffentlicht';
$lang['faq:live_label']                     = 'Ver&ouml;ffentlicht';
$lang['faq:content_label']                  = 'Inhalt';
$lang['faq:options_label']                  = 'Optionen';
$lang['faq:intro_label']                    = 'Einf&uuml;hrung';
$lang['faq:no_category_select_label']       = '-- Kein Label --';
$lang['faq:new_category_label']             = 'Kategorie hinzuf&uuml;gen';
$lang['faq:subscripe_to_rss_label']         = 'RSS abonnieren';
$lang['faq:all_posts_label']                = 'Alle Artikel';
$lang['faq:posts_of_category_suffix']       = ' Artikel';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Autor';
$lang['faq:read_more_label']                = 'Mehr lesen&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Erstellt zur Stunde';
$lang['faq:created_minute']                 = 'Erstellt zur Minute';
$lang['faq:comments_enabled_label']         = 'Kommentare aktiv';

// titles
$lang['faq:create_title']                   = 'Artikel erstellen';
$lang['faq:edit_title']                     = 'Artikel "%s" bearbeiten';
$lang['faq:archive_title']                  = 'Archiv';
$lang['faq:posts_title']                    = 'Artikel';
$lang['faq:rss_posts_title']                = 'Faq Artikel f&uuml;r %s';
$lang['faq:faq_title']                     = 'Faq';
$lang['faq:list_title']                     = 'Artikel auflisten';

// messages
$lang['faq:disabled_after'] 				= 'Kommentare nach %s wurden deaktiviert.';
$lang['faq:no_posts']                       = 'Es existieren keine Artikel.';
$lang['faq:subscripe_to_rss_desc']          = 'Abonnieren Sie unseren RSS Feed und erhalten Sie alle Artikel frei Haus. Sie k&ouml;nnen dies mit den meisten Email-Clients tun, oder z.B. mit <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']             = 'Es existieren zur Zeit keine Artikel.';
$lang['faq:post_add_success']               = 'Der Artikel "%s" wurde hinzugef&uuml;gt.';
$lang['faq:post_add_error']                 = 'Ein Fehler ist aufgetreten.';
$lang['faq:edit_success']                   = 'Der Artikel "%s" wurde aktualisiert.';
$lang['faq:edit_error']                     = 'Ein Fehler ist aufgetreten.';
$lang['faq:publish_success']                = 'Der Artikel "%s" wurde ver&ouml;ffentlicht.';
$lang['faq:mass_publish_success']           = 'Die Artikel "%s" wurden ver&ouml;ffentlicht.';
$lang['faq:publish_error']                  = 'Ein Fehler ist aufgetreten. Es wurden keine Artikel ver&ouml;ffentlicht.';
$lang['faq:delete_success']                 = 'Der Artikel "%s" wurde gel&ouml;scht.';
$lang['faq:mass_delete_success']            = 'Die Artikel "%s" wurden gel&ouml;scht.';
$lang['faq:delete_error']                   = 'Ein Fehler ist aufgetreten. Keine Artikel wurden gel&ouml;scht.';
$lang['faq:already_exist_error']            = 'Ein Artikel mit dieser URL existiert bereits.';

$lang['faq:twitter_posted']                 = 'Gepostet "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter Fehler';

// date
$lang['faq:archive_date_format']		     = "%B %Y";
