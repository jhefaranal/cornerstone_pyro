<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Artikel live setzen';
$lang['faq:role_edit_live']	= 'Live-Artikel bearbeiten';
$lang['faq:role_delete_live'] 	= 'Live-Artikel l&ouml;schen';

/* End of file permission_lang.php */