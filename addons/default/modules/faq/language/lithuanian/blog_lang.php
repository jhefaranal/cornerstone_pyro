<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                          = 'Įrašas';
$lang['faq:posts']                         = 'Įrašai';

// labels
$lang['faq:posted_label']                  = 'Paskelbta';
$lang['faq:posted_label_alt']              = 'Paskelbta...';
$lang['faq:written_by_label']              = 'Autorius';
$lang['faq:author_unknown']				= 'Nežinomas';
$lang['faq:keywords_label']				= 'Raktažodžiai';
$lang['faq:tagged_label']					= 'Žymėtas';
$lang['faq:category_label']                = 'Kategorija';
$lang['faq:post_label']                    = 'Įrašas';
$lang['faq:date_label']                    = 'Data';
$lang['faq:date_at']                       = '';
$lang['faq:time_label']                    = 'Laikas';
$lang['faq:status_label']                  = 'Statusas';
$lang['faq:draft_label']                   = 'Projektas';
$lang['faq:live_label']                    = 'Gyvai';
$lang['faq:content_label']                 = 'Turinys';
$lang['faq:options_label']                 = 'Funkcijos';
$lang['faq:title_label']                   = 'Pavadinimas';
$lang['faq:slug_label']                    = 'URL';
$lang['faq:intro_label']                   = 'Įžanga';
$lang['faq:no_category_select_label']      = '-- Nėra --';
$lang['faq:new_category_label']            = 'Pridėti kategoriją';
$lang['faq:subscripe_to_rss_label']        = 'Prenumeruoti RSS';
$lang['faq:all_posts_label']               = 'Visi įrašai';
$lang['faq:posts_of_category_suffix']      = ' įrašai';
$lang['faq:rss_name_suffix']               = ' Naujienos';
$lang['faq:rss_category_suffix']           = ' Naujienos';
$lang['faq:author_name_label']             = 'Autoriaus vardas';
$lang['faq:read_more_label']               = 'Plačiau&nbsp;&raquo;';
$lang['faq:created_hour']                  = 'Sukurta valandą';
$lang['faq:created_minute']                = 'Sukurta minutę';
$lang['faq:comments_enabled_label']        = 'Įjungti komentarus?';

// titles
$lang['faq:create_title']                  = 'Pridėti įrašą';
$lang['faq:edit_title']                    = 'Redaguoti įrašą "%s"';
$lang['faq:archive_title']                 = 'Archyvas';
$lang['faq:posts_title']                   = 'Įrašai';
$lang['faq:rss_posts_title']               = 'Naujienų įrašai %s';
$lang['faq:faq_title']                    = 'Naujiena';
$lang['faq:list_title']                    = 'Įrašų sąrašas';

// messages
$lang['faq:disabled_after'] 				= 'Paskelbti komentarai po %s buvo atjungti.';
$lang['faq:no_posts']                      = 'Nėra įrašų.';
$lang['faq:subscripe_to_rss_desc']         = 'Gaukite žinutes iš karto prenumeruodami į RSS kanalą. Jūs galite tai padaryti per populiariausias elektroninio pašto paskyras, arba bandykite <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']            = 'Šiuo metu nėra įrašų.';
$lang['faq:post_add_success']              = 'Įrašas "%s" pridėtas.';
$lang['faq:post_add_error']                = 'Įvyko klaida.';
$lang['faq:edit_success']                  = 'Įrašas "%s" atnaujintas.';
$lang['faq:edit_error']                    = 'Įvyko klaida.';
$lang['faq:publish_success']               = 'Įrašas "%s" paskelbtas.';
$lang['faq:mass_publish_success']          = 'Įrašai "%s" pasklbti.';
$lang['faq:publish_error']                 = 'Nėra paskelbtų įrašų.';
$lang['faq:delete_success']                = 'Įrašas "%s" ištrintas.';
$lang['faq:mass_delete_success']           = 'Įrašai "%s" ištrinti.';
$lang['faq:delete_error']                  = 'Nėra ištrintų įrašų.';
$lang['faq:already_exist_error']           = 'Įrašas su šiuo URL jau egzistuoja.';

$lang['faq:twitter_posted']                = 'Įrašyta "%s" %s';
$lang['faq:twitter_error']                 = 'Twitter nepasiekiamas';

// date
$lang['faq:archive_date_format']           = "%B %Y";
