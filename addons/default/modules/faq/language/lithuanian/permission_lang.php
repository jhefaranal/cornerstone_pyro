<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live'] = 'Įdėti straipsnį gyvai';
$lang['faq:role_edit_live'] = 'Straipsnių gyvai redagavimas';
$lang['faq:role_delete_live'] 	= 'Delete live articles'; #translate