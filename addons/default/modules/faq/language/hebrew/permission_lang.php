<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live'] = 'הכנס מאמרים חיים';
$lang['faq:role_edit_live'] = 'ערוך מאמרים חיים';
$lang['faq:role_delete_live'] 	= 'Delete live articles'; #translate