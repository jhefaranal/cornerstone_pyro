<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']                   = 'Posted';
$lang['faq:posted_label_alt']               = 'Posted at';
$lang['faq:written_by_label']				= 'Written by'; #translate
$lang['faq:author_unknown']				= 'Unknown'; #translate
$lang['faq:keywords_label']				= 'Keywords'; #translate
$lang['faq:tagged_label']					= 'Tagged'; #translate
$lang['faq:category_label']                 = 'Category';
$lang['faq:post_label']                     = 'Post';
$lang['faq:date_label']                     = 'Date';
$lang['faq:date_at']                        = 'at';
$lang['faq:time_label']                     = 'Time';
$lang['faq:status_label']                   = 'Status';
$lang['faq:draft_label']                    = 'Draft';
$lang['faq:live_label']                     = 'Live';
$lang['faq:content_label']                  = 'Content';
$lang['faq:options_label']                  = 'Options';
$lang['faq:intro_label']                    = 'Introduction';
$lang['faq:no_category_select_label']       = '-- None --';
$lang['faq:new_category_label']             = 'Add a category';
$lang['faq:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['faq:all_posts_label']             = 'All posts';
$lang['faq:posts_of_category_suffix']    = ' posts';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Author name';
$lang['faq:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Created on Hour';
$lang['faq:created_minute']                 = 'Created on Minute';

// titles
$lang['faq:create_title']                   = 'הוספ פוסט';
$lang['faq:edit_title']                     = 'ערוך פוסט "%s"';
$lang['faq:archive_title']                  = 'ארכיון';
$lang['faq:posts_title']                 = 'פוסטים';
$lang['faq:rss_posts_title']             = 'Faq posts for %s';
$lang['faq:faq_title']                     = 'בלוג';
$lang['faq:list_title']                     = 'רשימת הפוסטים';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']                    = 'אין פוסטים.';
$lang['faq:subscripe_to_rss_desc']          = 'קבל פוסטי ישירות ע"י הרשמה לRSS שלנו. ניתןלעשות זאת בעזרת <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'כרגע אין פוסטים.';
$lang['faq:post_add_success']            = 'הפוסט "%s" הוסף בהצלחה.';
$lang['faq:post_add_error']              = 'התרחשה שגיעה.';
$lang['faq:edit_success']                   = 'The post "%s" was updated.';
$lang['faq:edit_error']                     = 'התרחשה שגיעה.';
$lang['faq:publish_success']                = 'The post "%s" has been published.';
$lang['faq:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['faq:publish_error']                  = 'No posts were published.';
$lang['faq:delete_success']                 = 'The post "%s" has been deleted.';
$lang['faq:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['faq:delete_error']                   = 'No posts were deleted.';
$lang['faq:already_exist_error']            = 'An post with this URL already exists.';

$lang['faq:twitter_posted']                 = 'Posted "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter Error';

// date
$lang['faq:archive_date_format']		= "%B %Y";
