<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']				= 'Escrito';
$lang['faq:posted_label_alt']			= 'Escrito em';
$lang['faq:written_by_label']			= 'Por';
$lang['faq:author_unknown']			= 'Desconhecido';
$lang['faq:keywords_label']			= 'Palavras-chave';
$lang['faq:tagged_label']				= 'Tagged'; #translate
$lang['faq:category_label']			= 'Categoria';
$lang['faq:post_label'] 				= 'Artigo';
$lang['faq:date_label'] 				= 'Data';
$lang['faq:date_at']					= 'às';
$lang['faq:time_label'] 				= 'Hora';
$lang['faq:status_label'] 				= 'Situação';
$lang['faq:draft_label'] 				= 'Rascunho';
$lang['faq:live_label'] 				= 'Publico';
$lang['faq:content_label']				= 'Conteúdo';
$lang['faq:options_label']				= 'Opções';
$lang['faq:intro_label'] 				= 'Introdução';
$lang['faq:no_category_select_label']	= '-- Nenhuma --';
$lang['faq:new_category_label'] 		= 'Adicionar uma categoria';
$lang['faq:subscripe_to_rss_label'] 	= 'Assinar o RSS';
$lang['faq:all_posts_label'] 			= 'Todos os artigos';
$lang['faq:posts_of_category_suffix'] 	= ' artigos';
$lang['faq:rss_name_suffix']			= ' Faq';
$lang['faq:rss_category_suffix']		= ' Faq';
$lang['faq:author_name_label']			= 'Nome do autor';
$lang['faq:read_more_label']			= 'Leia mais &raquo;';
$lang['faq:created_hour']				= 'Horário (Hora)';
$lang['faq:created_minute']			= 'Horário (Minuto)';
$lang['faq:comments_enabled_label']	= 'Habilitar comentários';

// titles
$lang['faq:create_title']				= 'Adicionar artigo';
$lang['faq:edit_title']				= 'Editar artigo "%s"';
$lang['faq:archive_title'] 			= 'Arquivo';
$lang['faq:posts_title'] 				= 'Artigos';
$lang['faq:rss_posts_title'] 			= 'Artigos novos para %s';
$lang['faq:faq_title']				= 'Faq';
$lang['faq:list_title']				= 'Listar artigos';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']					= 'Nenhum artigo.';
$lang['faq:subscripe_to_rss_desc']		= 'Fique por dentro das novidades do faq assinando o nosso feed RSS. Pode fazer isto pelos mais populares leitores de e-mail ou pode experimentar o <a rel="nofollow" href="http://reader.google.com/">Google Reader</a>.';
$lang['faq:currently_no_posts']		= 'Não existem artigos no momento.';
$lang['faq:post_add_success']			= 'O artigo "%s" foi adicionado.';
$lang['faq:post_add_error']			= 'Ocorreu um erro.';
$lang['faq:edit_success']				= 'O artigo "%s" foi actualizado.';
$lang['faq:edit_error']				= 'Ocorreu um erro.';
$lang['faq:publish_success']			= 'O artigo "%s" foi publicado.';
$lang['faq:mass_publish_success']		= 'Os artigos "%s" foram publicados.';
$lang['faq:publish_error']				= 'Nenhum artigo foi publicado.';
$lang['faq:delete_success']			= 'O artigo "%s" foi removido.';
$lang['faq:mass_delete_success']		= 'Os artigos "%s" foram removidos.';
$lang['faq:delete_error']				= 'Nenhuma publicação foi removida.';
$lang['faq:already_exist_error']		= 'Um artigo com mesmo campo %s já existe.';

$lang['faq:twitter_posted']			= 'Escrito "%s" %s';
$lang['faq:twitter_error']				= 'Erro do Twitter';

// date
$lang['faq:archive_date_format']		= "%B de %Y";