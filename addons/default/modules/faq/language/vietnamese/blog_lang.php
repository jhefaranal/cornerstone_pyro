<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Author: Thanh Nguyen
* 		  nguyenhuuthanh@gmail.com
*
* Location: http://techmix.net
*
* Created:  10.26.2011
*
* Description:  Vietnamese language file
*
*/

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']                   = 'Đã gửi';
$lang['faq:posted_label_alt']               = 'Gửi lúc at';
$lang['faq:written_by_label']				= 'Viết bởi';
$lang['faq:author_unknown']				= 'Chưa rõ';
$lang['faq:keywords_label']				= 'Keywords'; #translate
$lang['faq:tagged_label']					= 'Tagged'; #translate
$lang['faq:category_label']                 = 'Danh mục';
$lang['faq:post_label']                     = 'Bài viết';
$lang['faq:date_label']                     = 'Ngày';
$lang['faq:date_at']                        = 'lúc';
$lang['faq:time_label']                     = 'thời gian';
$lang['faq:status_label']                   = 'Trạng thái';
$lang['faq:draft_label']                    = 'Nháp';
$lang['faq:live_label']                     = 'Xuất bản';
$lang['faq:content_label']                  = 'Nội dung';
$lang['faq:options_label']                  = 'Tùy biến';
$lang['faq:intro_label']                    = 'Giới thiệu';
$lang['faq:no_category_select_label']       = '-- Không --';
$lang['faq:new_category_label']             = 'Thêm chuyên mục';
$lang['faq:subscripe_to_rss_label']         = 'Nhận RSS';
$lang['faq:all_posts_label']             = 'Tất cả bài viết';
$lang['faq:posts_of_category_suffix']    = ' bài viết';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Tên tác giả';
$lang['faq:read_more_label']                = 'Đọc thêm&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Giờ tạo';
$lang['faq:created_minute']                 = 'Phút tạo';
$lang['faq:comments_enabled_label']         = 'Cho phép phản hồi';

// titles
$lang['faq:create_title']                   = 'Thêm bài viết';
$lang['faq:edit_title']                     = 'Sửa bài viết "%s"';
$lang['faq:archive_title']                  = 'Lưu trữ';
$lang['faq:posts_title']                 = 'Bài viết';
$lang['faq:rss_posts_title']             = 'Các bài viết cho %s';
$lang['faq:faq_title']                     = 'Faq';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']                    = 'Không có bài viết nào.';
$lang['faq:subscripe_to_rss_desc']          = 'Hãy sử dụng RSS Feed để nhận những bài viết mới nhất. Bạn có thể sử dụng nhiều chương trình khác nhau, hoặc sử dụng <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'Không có bài viết nào.';
$lang['faq:post_add_success']            = 'Đã thêm bài viết "%s".';
$lang['faq:post_add_error']              = 'Có lỗi xảy ra.';
$lang['faq:edit_success']                   = 'Bài viết "%s" đã được cập nhật.';
$lang['faq:edit_error']                     = 'Có lỗi xảy ra.';
$lang['faq:publish_success']                = 'Bài viết "%s" đã được xuất bản.';
$lang['faq:mass_publish_success']           = 'Bài viết "%s" đã được xuất bản.';
$lang['faq:publish_error']                  = 'Không có bài viết nào được xuât bản.';
$lang['faq:delete_success']                 = 'Đã xóa bài viết "%s".';
$lang['faq:mass_delete_success']            = 'Đã xóa bài viết "%s".';
$lang['faq:delete_error']                   = 'Không có bài viết nào được xóa.';
$lang['faq:already_exist_error']            = 'URL của bài viết đã tồn tại.';

$lang['faq:twitter_posted']                 = 'Đã gửi "%s" %s';
$lang['faq:twitter_error']                  = 'Lỗi Twitter';

// date
$lang['faq:archive_date_format']		= "%B %Y";
