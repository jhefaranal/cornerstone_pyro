<?php
/**
 * Chinese Simpplified translation.
 *
 * @author		Kefeng DENG
 * @package		PyroCMS
 * @subpackage 	Faq Module
 * @category	Modules
 * @link		http://pyrocms.com
 * @date		2012-06-22
 * @version		1.0
 */
$lang['faq:post']                 = '帖子'; #translate
$lang['faq:posts']                   = '帖子'; #translate

// labels
$lang['faq:posted_label'] 				= '已發佈';
$lang['faq:posted_label_alt']			= '發表於';
$lang['faq:written_by_label']				= '撰文者';
$lang['faq:author_unknown']				= '未知';
$lang['faq:keywords_label']				= '關鍵字';
$lang['faq:tagged_label']					= '標籤';
$lang['faq:category_label'] 			= '分類';
$lang['faq:post_label'] 				= '發表';
$lang['faq:date_label'] 				= '日期';
$lang['faq:date_at']					= '於';
$lang['faq:time_label'] 				= '時間';
$lang['faq:status_label'] 				= '狀態';
$lang['faq:draft_label'] 				= '草稿';
$lang['faq:live_label'] 				= '上線';
$lang['faq:content_label'] 			= '內容';
$lang['faq:options_label'] 			= '選項';
$lang['faq:intro_label'] 				= '簡介';
$lang['faq:no_category_select_label'] 	= '-- 無 --';
$lang['faq:new_category_label'] 		= '新增分類';
$lang['faq:subscripe_to_rss_label'] 	= '訂閱 RSS';
$lang['faq:all_posts_label'] 		= '所有文章';
$lang['faq:posts_of_category_suffix'] 		= ' 文章';
$lang['faq:rss_name_suffix'] 					= ' 新聞';
$lang['faq:rss_category_suffix'] 				= ' 新聞';
$lang['faq:author_name_label'] 				= '作者名稱';
$lang['faq:read_more_label'] 					= '閱讀更多&nbsp;&raquo;';
$lang['faq:created_hour']                  = '時間 (時)';
$lang['faq:created_minute']                = '時間 (分)';
$lang['faq:comments_enabled_label']         = '允許回應';

// titles
$lang['faq:create_title'] 				= '新增文章';
$lang['faq:edit_title'] 				= '編輯文章 "%s"';
$lang['faq:archive_title'] 			= '檔案櫃';
$lang['faq:posts_title'] 			= '文章';
$lang['faq:rss_posts_title'] 		= '%s 的最新文章';
$lang['faq:faq_title'] 				= '新聞';
$lang['faq:list_title'] 				= '文章列表';

// messages
$lang['faq:disabled_after'] 				= '在%s之后，回复功能将会被关闭。'; #translate
$lang['faq:no_posts'] 				= '沒有文章';
$lang['faq:subscripe_to_rss_desc'] 	= '訂閱我們的 RSS 摘要可立即獲得最新的文章，您可以使用慣用的收件軟體，或試試看 <a href="http://reader.google.com.tw">Google 閱讀器</a>。';
$lang['faq:currently_no_posts'] 	= '目前沒有文章';
$lang['faq:post_add_success'] 		= '文章 "%s" 已經新增';
$lang['faq:post_add_error'] 		= '發生了錯誤';
$lang['faq:edit_success'] 				= '這文章 "%s" 更新了。';
$lang['faq:edit_error'] 				= '發生了錯誤';
$lang['faq:publish_success'] 			= '此文章 "%s" 已經被發佈。';
$lang['faq:mass_publish_success'] 		= '這些文章 "%s" 已經被發佈。';
$lang['faq:publish_error'] 			= '沒有文章被發佈。';
$lang['faq:delete_success'] 			= '此文章 "%s" 已經被刪除。';
$lang['faq:mass_delete_success'] 		= '這些文章 "%s" 已經被刪除。';
$lang['faq:delete_error'] 				= '沒有文章被刪除。';
$lang['faq:already_exist_error'] 		= '一則相同網址的文章已經存在。';

$lang['faq:twitter_posted']			= '發佈 "%s" %s';
$lang['faq:twitter_error'] 			= 'Twitter 錯誤';

// date
$lang['faq:archive_date_format']		= "%B' %Y"; #translate format - see php strftime documentation