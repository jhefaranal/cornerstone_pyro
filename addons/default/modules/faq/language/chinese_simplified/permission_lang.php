<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Chinese Simpplified translation.
 *
 * @author		Kefeng DENG
 * @package		PyroCMS
 * @subpackage 	Faq Module
 * @category	Modules
 * @link		http://pyrocms.com
 * @date		2012-06-22
 * @version		1.0
 */
// Faq Permissions
$lang['faq:role_put_live']		= '將文章上线';
$lang['faq:role_edit_live']	= '编辑上线文章';
$lang['faq:role_delete_live'] 	= '刪除上线文章';