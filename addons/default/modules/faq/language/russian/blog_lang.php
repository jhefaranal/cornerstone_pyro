<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * PyroCMS
 * Русский перевод от Dark Preacher - dark[at]darklab.ru
 *
 * @package		PyroCMS
 * @author		Dark Preacher
 * @link			http://pyrocms.com
 */

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// подписи
$lang['faq:posted_label']									= 'Дата';
$lang['faq:posted_label_alt']							= 'Дата добавления';
$lang['faq:written_by_label']							= 'Автор';
$lang['faq:author_unknown']								= 'Неизвестно';
$lang['faq:keywords_label']				= 'Keywords'; #translate
$lang['faq:tagged_label']					= 'Tagged'; #translate
$lang['faq:category_label']								= 'Категория';
$lang['faq:post_label']										= 'Заголовок';
$lang['faq:date_label']										= 'Дата';
$lang['faq:date_at']												= 'в';
$lang['faq:time_label']										= 'Время';
$lang['faq:status_label']									= 'Статус';
$lang['faq:draft_label']										= 'Черновик';
$lang['faq:live_label']										= 'Опубликовано';
$lang['faq:content_label']									= 'Содержание';
$lang['faq:options_label']									= 'Опции';
$lang['faq:intro_label']										= 'Анонс';
$lang['faq:no_category_select_label']			= '-- нет --';
$lang['faq:new_category_label']						= 'Создать категорию';
$lang['faq:subscripe_to_rss_label']				= 'Подписаться на RSS';
$lang['faq:all_posts_label']								= 'Все статьи';
$lang['faq:posts_of_category_suffix']			= ' статьи';
$lang['faq:rss_name_suffix']								= ' Блог';
$lang['faq:rss_category_suffix']						= ' Блог';
$lang['faq:author_name_label']							= 'Автор';
$lang['faq:read_more_label']								= 'читать целиком&nbsp;&raquo;';
$lang['faq:created_hour']									= 'Время (Час)';
$lang['faq:created_minute']								= 'Время (Минута)';
$lang['faq:comments_enabled_label']				= 'Комментарии разрешены';

// заголовки
$lang['faq:create_title']									= 'Создать статью';
$lang['faq:edit_title']										= 'Редактирование статьи "%s"';
$lang['faq:archive_title']									= 'Архив';
$lang['faq:posts_title']										= 'Статьи';
$lang['faq:rss_posts_title']								= 'Статьи из %s';
$lang['faq:faq_title']										= 'Блог';
$lang['faq:list_title']										= 'Список статей';

// сообщения
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']											= 'Статьи отсутствуют.';
$lang['faq:subscripe_to_rss_desc']					= 'Получайте статьи сразу после их публикации, подпишитесь на нашу ленту новостей. Вы можете сделать это с помощью самых популярных программ для чтения электронных писем, или попробуйте <a href="http://reader.google.ru/">Google Reader</a>.';
$lang['faq:currently_no_posts']						= 'В данный момент новости отсутствуют.';
$lang['faq:post_add_success']							= 'Статья "%s" добавлена.';
$lang['faq:post_add_error']								= 'Во время добавления статьи произошла ошибка.';
$lang['faq:edit_success']									= 'Статья "%s" сохранена.';
$lang['faq:edit_error']										= 'Во время сохранения статьи произошла ошибка.';
$lang['faq:publish_success']								= 'Статья "%s" опубликована.';
$lang['faq:mass_publish_success']					= 'Статьи "%s" опубликованы.';
$lang['faq:publish_error']									= 'Во время публикации статьи произошла ошибка.';
$lang['faq:delete_success']								= 'Статья "%s" удалена.';
$lang['faq:mass_delete_success']						= 'Статьи "%s" удалены.';
$lang['faq:delete_error']									= 'Во время удаления статьи произошла ошибка.';
$lang['faq:already_exist_error']						= 'Статья с данным адресом URL уже существует.';

$lang['faq:twitter_posted']								= 'Добавлен "%s" %s';
$lang['faq:twitter_error']									= 'Ошибка Twitter\'а';

// дата
$lang['faq:archive_date_format']						= "%B %Y"; #see php strftime documentation

/* End of file faq_lang.php */