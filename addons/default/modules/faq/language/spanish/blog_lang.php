<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                          = 'Entrada';
$lang['faq:posts']                         = 'Entradas';

// labels
$lang['faq:posted_label']					= 'Escrito';
$lang['faq:keywords_label']				= 'Palabras clave';
$lang['faq:tagged_label']					= 'Tagged'; #translate
$lang['faq:category_label'] 				= 'Categoría';
$lang['faq:written_by_label']				= 'Escrito por';
$lang['faq:author_unknown']				= 'Desconocido';
$lang['faq:post_label'] 					= 'Entrada';
$lang['faq:date_label'] 					= 'Fecha';
$lang['faq:time_label'] 					= 'Hora';
$lang['faq:status_label'] 					= 'Estado';
$lang['faq:content_label'] 				= 'Contenido';
$lang['faq:options_label'] 				= 'Opciones';
$lang['faq:intro_label'] 					= 'Introducción';
$lang['faq:draft_label'] 					= 'Borrador';
$lang['faq:live_label'] 					= 'En vivo';
$lang['faq:no_category_select_label'] 		= '-- Ninguna --';
$lang['faq:new_category_label'] 			= 'Agregar una categoría';
$lang['faq:subscripe_to_rss_label'] 		= 'Suscribir al RSS';
$lang['faq:all_posts_label'] 				= 'Todos los artículos';
$lang['faq:posts_of_category_suffix']		= ' artículos';
$lang['faq:rss_name_suffix'] 				= ' Noticias';
$lang['faq:rss_category_suffix'] 			= ' Noticias';
$lang['faq:posted_label'] 					= 'Escrito en';
$lang['faq:author_name_label'] 			= 'Nombre del autor';
$lang['faq:read_more_label'] 				= 'Leer más &raquo;';
$lang['faq:created_hour']                 	= 'Hora (Hora)';
$lang['faq:created_minute']				= 'Hora (Minutos)';
$lang['faq:comments_enabled_label']         = 'Comentarios Habilitados';

// titles
$lang['faq:create_title'] 					= 'Crear un artículo';
$lang['faq:edit_title'] 					= 'Editar artículo "%s"';
$lang['faq:archive_title'] 				= 'Archivo';
$lang['faq:posts_title'] 					= 'Artículos';
$lang['faq:rss_posts_title'] 				= 'Artículo Faq de %s';
$lang['faq:faq_title'] 					= 'Noticias';
$lang['faq:list_title'] 					= 'Lista de artículos';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts'] 						= 'No hay artículos.';
$lang['faq:subscripe_to_rss_desc'] 		= 'Recibe nuestros artículos inmediatamente suscribiendote a nuestros feeds RSS. Puedes hacer esto utilizando los más clientes de correo electrónico mas populares o utilizando <a href="http://reader.google.com/">Google Reader</a>.';
$lang['faq:currently_no_posts'] 			= 'No hay artículos hasta el momento.';
$lang['faq:post_add_success'] 				= 'El artículo "%s" fue agregado.';
$lang['faq:post_add_error'] 				= 'Ha ocurrido un error.';
$lang['faq:edit_success'] 					= 'El artículo "%s" fue actualizado.';
$lang['faq:edit_error'] 					= 'Ha ocurrido un error.';
$lang['faq:publish_success'] 				= 'El artículo "%s" ha sido publicado.';
$lang['faq:mass_publish_success'] 			= 'Los artículos "%s" fueron publicados.';
$lang['faq:publish_error'] 				= 'No se han publicado artículos.';
$lang['faq:delete_success'] 				= 'El artículo "%s" ha sido eliminado.';
$lang['faq:mass_delete_success'] 			= 'Los artículos "%s" han sido eliminados.';
$lang['faq:delete_error'] 					= 'No se han eliminado artículos.';
$lang['faq:already_exist_error'] 			= 'Ya existe un artículo con esta URL.';

$lang['faq:twitter_posted']				= 'Escrito "%s" %s';
$lang['faq:twitter_error'] 				= 'Error de Twitter';

// date
$lang['faq:archive_date_format']			= "%B %Y";

/* End of file faq:lang.php */