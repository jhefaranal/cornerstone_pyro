<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Colocar artículos disponibles';
$lang['faq:role_edit_live']	= 'Editar artículos disponibles';
$lang['faq:role_delete_live'] 	= 'Eliminar artículos disponibles';