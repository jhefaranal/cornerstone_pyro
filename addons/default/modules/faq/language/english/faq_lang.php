<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post';
$lang['faq:posts']                   = 'Posts';

// labels
$lang['faq:posted_label']                   = 'Posted';
$lang['faq:posted_label_alt']               = 'Posted at';
$lang['faq:written_by_label']				= 'Written by';
$lang['faq:author_unknown']				= 'Unknown';
$lang['faq:keywords_label']				= 'Keywords';
$lang['faq:tagged_label']					= 'Tagged';
$lang['faq:category_label']                 = 'Category';
$lang['faq:post_label']                     = 'Post';
$lang['faq:date_label']                     = 'Date';
$lang['faq:date_at']                        = 'at';
$lang['faq:time_label']                     = 'Time';
$lang['faq:status_label']                   = 'Status';
$lang['faq:draft_label']                    = 'Draft';
$lang['faq:live_label']                     = 'Live';
$lang['faq:content_label']                  = 'Content';
$lang['faq:options_label']                  = 'Options';
$lang['faq:intro_label']                    = 'Introduction';
$lang['faq:no_category_select_label']       = '-- None --';
$lang['faq:new_category_label']             = 'Add a category';
$lang['faq:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['faq:all_posts_label']                = 'All posts';
$lang['faq:posts_of_category_suffix']       = ' posts';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Author name';
$lang['faq:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Created on Hour';
$lang['faq:created_minute']                 = 'Created on Minute';
$lang['faq:comments_enabled_label']         = 'Comments Enabled';

// titles
$lang['faq:create_title']                   = 'Add Faq';
$lang['faq:edit_title']                     = 'Edit Faq "%s"';
$lang['faq:archive_title']                 = 'Archive';
$lang['faq:posts_title']					= 'Posts';
$lang['faq:rss_posts_title']				= 'Faq posts for %s';
$lang['faq:faq_title']					= 'Faqs';
$lang['faq:list_title']					= 'List Posts';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.';
$lang['faq:no_posts']                      = 'There are no posts.';
$lang['faq:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'There are no posts at the moment.';
$lang['faq:post_add_success']            = 'The post "%s" was added.';
$lang['faq:post_add_error']              = 'An error occured.';
$lang['faq:edit_success']                   = 'The post "%s" was updated.';
$lang['faq:edit_error']                     = 'An error occurred.';
$lang['faq:publish_success']                = 'The post "%s" has been published.';
$lang['faq:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['faq:publish_error']                  = 'No posts were published.';
$lang['faq:delete_success']                 = 'The post "%s" has been deleted.';
$lang['faq:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['faq:delete_error']                   = 'No posts were deleted.';
$lang['faq:already_exist_error']            = 'A post with this URL already exists.';

$lang['faq:twitter_posted']                 = 'Posted "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter Error';

// date
$lang['faq:archive_date_format']		= "%B %Y";
