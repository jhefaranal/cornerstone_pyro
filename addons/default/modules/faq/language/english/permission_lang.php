<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Put articles live';
$lang['faq:role_edit_live']	= 'Edit live articles';
$lang['faq:role_delete_live'] 	= 'Delete live articles';