<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Publikasikan artikel';
$lang['faq:role_edit_live']	= 'Edit artikel terpublikasi';
$lang['faq:role_delete_live'] 	= 'Hapus artikel terpublikasi';
