<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']                   = 'Dipostkan';
$lang['faq:posted_label_alt']               = 'Dipostkan pada';
$lang['faq:written_by_label']				= 'Ditulis oleh';
$lang['faq:author_unknown']				= 'Tidak dikenal';
$lang['faq:keywords_label']				= 'Kata Kunci';
$lang['faq:tagged_label']					= 'Label';
$lang['faq:category_label']                 = 'Kategori';
$lang['faq:post_label']                     = 'Post';
$lang['faq:date_label']                     = 'Tanggal';
$lang['faq:date_at']                        = 'pada';
$lang['faq:time_label']                     = 'Waktu';
$lang['faq:status_label']                   = 'Status';
$lang['faq:draft_label']                    = 'Draft';
$lang['faq:live_label']                     = 'Publikasikan';
$lang['faq:content_label']                  = 'Konten';
$lang['faq:options_label']                  = 'Opsi';
$lang['faq:intro_label']                    = 'Pendahuluan';
$lang['faq:no_category_select_label']       = '-- tidak ada --';
$lang['faq:new_category_label']             = 'Tambah kategori';
$lang['faq:subscripe_to_rss_label']         = 'Berlangganan RSS';
$lang['faq:all_posts_label']             = 'Semua post';
$lang['faq:posts_of_category_suffix']    = ' post';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Nama penulis';
$lang['faq:read_more_label']                = 'Selengkapnya&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Dibuat pada Jam';
$lang['faq:created_minute']                 = 'Dibuat pada Menit';
$lang['faq:comments_enabled_label']         = 'Perbolehkan Komentar';

// titles
$lang['faq:create_title']                   = 'Tambah Post';
$lang['faq:edit_title']                     = 'Edit post "%s"';
$lang['faq:archive_title']                 = 'Arsip';
$lang['faq:posts_title']					= 'Post';
$lang['faq:rss_posts_title']				= 'Faq post untuk %s';
$lang['faq:faq_title']					= 'Faq';
$lang['faq:list_title']					= 'Daftar Post';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']                    = 'Tidak ada post.';
$lang['faq:subscripe_to_rss_desc']          = 'Ambil post langsung dengan berlangganan melalui RSS feed kami. Anda dapat melakukannya menggunakan hampir semua e-mail client, atau coba <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'Untuk sementara tidak ada post.';
$lang['faq:post_add_success']            = 'Post "%s" telah ditambahkan.';
$lang['faq:post_add_error']              = 'Terjadi kesalahan.';
$lang['faq:edit_success']                   = 'Post "%s" telah diperbaharui.';
$lang['faq:edit_error']                     = 'Terjadi kesalahan.';
$lang['faq:publish_success']                = 'Post "%s" telah dipublikasikan.';
$lang['faq:mass_publish_success']           = 'Post "%s" telah dipublikasikan.';
$lang['faq:publish_error']                  = 'Tidak ada post yang terpublikasi.';
$lang['faq:delete_success']                 = 'Post "%s" telah dihapus.';
$lang['faq:mass_delete_success']            = 'Post "%s" telah dihapus.';
$lang['faq:delete_error']                   = 'Tidak ada post yan terhapus.';
$lang['faq:already_exist_error']            = 'Post dengan URL ini sudah ada.';

$lang['faq:twitter_posted']                 = 'Dipostkan "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter Error';

// date
$lang['faq:archive_date_format']		= "%B %Y";
