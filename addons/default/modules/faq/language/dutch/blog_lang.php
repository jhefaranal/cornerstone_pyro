<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label'] 				= 'Geplaatst';
$lang['faq:posted_label_alt']			= 'Geplaatst op';
$lang['faq:written_by_label']			= 'Geschreven door';
$lang['faq:author_unknown']			= 'Onbekend';
$lang['faq:keywords_label']			= 'Sleutelwoorden';
$lang['faq:tagged_label']				= 'Etiket';
$lang['faq:category_label'] 			= 'Categorie';
$lang['faq:post_label'] 				= 'Post';
$lang['faq:date_label'] 				= 'Datum';
$lang['faq:date_at']					= 'op';
$lang['faq:time_label'] 				= 'Tijd';
$lang['faq:status_label'] 				= 'Status';
$lang['faq:draft_label'] 				= 'Concept';
$lang['faq:live_label'] 				= 'Live';
$lang['faq:content_label'] 			= 'Content';
$lang['faq:options_label'] 			= 'Opties';
$lang['faq:slug_label'] 				= 'URL';
$lang['faq:intro_label'] 				= 'Introductie';
$lang['faq:no_category_select_label'] 	= '-- Geen --';
$lang['faq:new_category_label'] 		= 'Voeg een categorie toe';
$lang['faq:subscripe_to_rss_label'] 	= 'Abonneer op RSS';
$lang['faq:all_posts_label'] 			= 'Alle artikelen';
$lang['faq:posts_of_category_suffix'] 	= ' artikelen';
$lang['faq:rss_name_suffix'] 			= ' Nieuws';
$lang['faq:rss_category_suffix'] 		= ' Nieuws';
$lang['faq:author_name_label'] 		= 'Auteur naam';
$lang['faq:read_more_label'] 			= 'Lees Meer&nbsp;&raquo;';
$lang['faq:created_hour']           	= 'Tijd (Uren)';
$lang['faq:created_minute']       		= 'Tijd (Minuten)';
$lang['faq:comments_enabled_label']	= 'Reacties ingeschakeld';

// titles
$lang['faq:disabled_after'] 			= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:create_title'] 				= 'Voeg artikel toe';
$lang['faq:edit_title'] 				= 'Wijzig artikel "%s"';
$lang['faq:archive_title'] 			= 'Archief';
$lang['faq:posts_title'] 				= 'Artikelen';
$lang['faq:rss_posts_title'] 			= 'Nieuws artikelen voor %s';
$lang['faq:faq_title'] 				= 'Nieuws';
$lang['faq:list_title'] 				= 'Overzicht artikelen';

// messages
$lang['faq:no_posts'] 					= 'Er zijn geen artikelen.';
$lang['faq:subscripe_to_rss_desc'] 	= 'Ontvang artikelen meteen door te abonneren op onze RSS feed. U kunt dit doen met de meeste populaire e-mail programma&acute;s, of probeer <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts'] 		= 'Er zijn momenteel geen artikelen.';
$lang['faq:post_add_success'] 			= 'Het artikel "%s" is opgeslagen.';
$lang['faq:post_add_error'] 			= 'Er is een fout opgetreden.';
$lang['faq:edit_success'] 				= 'Het artikel "%s" is opgeslagen.';
$lang['faq:edit_error'] 				= 'Er is een fout opgetreden.';
$lang['faq:publish_success'] 			= 'Het artikel "%s" is gepubliceerd.';
$lang['faq:mass_publish_success'] 		= 'De artikelen "%s" zijn gepubliceerd.';
$lang['faq:publish_error'] 			= 'Geen artikelen zijn gepubliceerd.';
$lang['faq:delete_success'] 			= 'Het artikel "%s" is verwijderd.';
$lang['faq:mass_delete_success'] 		= 'De artikelen "%s" zijn verwijderd.';
$lang['faq:delete_error'] 				= 'Geen artikelen zijn verwijderd.';
$lang['faq:already_exist_error'] 		= 'Een artikel met deze URL bestaat al.';

$lang['faq:twitter_posted']			= 'Geplaatst "%s" %s';
$lang['faq:twitter_error'] 			= 'Twitter Fout';

// date
$lang['faq:archive_date_format']		= "%B %Y";
