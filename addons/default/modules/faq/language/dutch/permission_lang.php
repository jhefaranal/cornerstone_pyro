<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Zet artikelen online';
$lang['faq:role_edit_live']	= 'Bewerk online artikelen';
$lang['faq:role_delete_live'] 	= 'Verwijder online artikelen';