<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Thai translation.
*
* @author	Nateetorn Lertkhonsan <nateetorn.l@gmail.com>
* @package	PyroCMS  
* @link		http://pyrocms.com
* @date		2012-04-19
* @version	1.0.0
**/

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']                   = 'โพสต์แล้ว';
$lang['faq:posted_label_alt']               = 'โพสต์แล้วที่';
$lang['faq:written_by_label']				= 'เขียนโดย';
$lang['faq:author_unknown']				= 'ไม่ทราบ';
$lang['faq:keywords_label']				= 'คำหลัก';
$lang['faq:tagged_label']					= 'แท็ก';
$lang['faq:category_label']                 = 'หมวดหมู่';
$lang['faq:post_label']                     = 'โพสต์';
$lang['faq:date_label']                     = 'วันที่';
$lang['faq:date_at']                        = 'ที่';
$lang['faq:time_label']                     = 'เวลา';
$lang['faq:status_label']                   = 'สถานะ';
$lang['faq:draft_label']                    = 'ร่างบล็อก';
$lang['faq:live_label']                     = 'ไลฟ์บล็อก';
$lang['faq:content_label']                  = 'เนื้อหา';
$lang['faq:options_label']                  = 'ตัวเลือก';
$lang['faq:intro_label']                    = 'บทนำ';
$lang['faq:no_category_select_label']       = '-- ไม่มี --';
$lang['faq:new_category_label']             = 'เพิ่มหมวดหมู่';
$lang['faq:subscripe_to_rss_label']         = 'สมัครสมาชิก RSS';
$lang['faq:all_posts_label']             = 'โพสต์ทั้งหมด';
$lang['faq:posts_of_category_suffix']    = ' โพสต์';
$lang['faq:rss_name_suffix']                = ' บล็อก';
$lang['faq:rss_category_suffix']            = ' บล็อก';
$lang['faq:author_name_label']              = 'ชื่อผู้เขียน';
$lang['faq:read_more_label']                = 'อ่านเพิ่มเติม&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'สร้างเมื่อชั่วโมงที่แล้ว';
$lang['faq:created_minute']                 = 'สร้างเมื่อนาทีที่แล้ว';
$lang['faq:comments_enabled_label']         = 'ความคิดเห็นถูกเปิดใช้งาน';

// titles
$lang['faq:create_title']                   = 'เพิ่มโพสต์';
$lang['faq:edit_title']                     = 'แก้ไขโพสต์ "%s"';
$lang['faq:archive_title']                 = 'คลัง';
$lang['faq:posts_title']					= 'โพสต์';
$lang['faq:rss_posts_title']				= 'โพสต์บล็อกสำหรับ %s';
$lang['faq:faq_title']					= 'บล็อก';
$lang['faq:list_title']					= 'รายการโพสต์';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']                    = 'ไม่มีโพสต์';
$lang['faq:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'ไม่มีโพสต์ในขณะนี้';
$lang['faq:post_add_success']            = 'โพสต์ "%s" ถูกเพิ่มแล้ว';
$lang['faq:post_add_error']              = 'เกิดข้อผิดพลาด';
$lang['faq:edit_success']                   = 'ปรับปรุงโพสต์ "%s" แล้ว';
$lang['faq:edit_error']                     = 'เกิดข้อผิดพลาด';
$lang['faq:publish_success']                = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['faq:mass_publish_success']           = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['faq:publish_error']                  = 'ไม่มีโพสต์ถูกเผยแพร่.';
$lang['faq:delete_success']                 = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['faq:mass_delete_success']            = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['faq:delete_error']                   = 'ไม่มีโพสต์ถูกลบ';
$lang['faq:already_exist_error']            = 'โพสต์ลิงค์นี้มีอยู่แล้ว';

$lang['faq:twitter_posted']                 = 'โพสต์ "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter มีข้อผิดพลาด';

// date
$lang['faq:archive_date_format']		= "%B %Y";
