<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Thai translation.
*
* @author	Nateetorn Lertkhonsan <nateetorn.l@gmail.com>
* @package	PyroCMS  
* @link		http://pyrocms.com
* @date		2012-04-19
* @version	1.0.0
**/

// Faq Permissions
$lang['faq:role_put_live']		= 'วางบทบทความไลฟ์';
$lang['faq:role_edit_live']	= 'แก้ไขบทความไลฟ์';
$lang['faq:role_delete_live'] 	= 'ลบบทความไลฟ์';