<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Artikkelien julkaiseminen';
$lang['faq:role_edit_live']	= 'Julkaistujen artikkeleiden muokkaaminen';
$lang['faq:role_delete_live'] 	= 'Julkaistujen artikkeleiden poistaminen';
