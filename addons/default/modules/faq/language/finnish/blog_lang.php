<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Finnish translation.
 *
 * @author Mikael Kundert
 */

$lang['faq:post']	= 'Artikkeli';
$lang['faq:posts']	= 'Artikkelit';

// labels
$lang['faq:posted_label']				= 'Lähetetty';
$lang['faq:posted_label_alt']			= 'Lähetettiin';
$lang['faq:written_by_label']			= 'Kirjoittanut';
$lang['faq:author_unknown']			= 'Tuntematon';
$lang['faq:keywords_label']			= 'Avainsanat';
$lang['faq:tagged_label']				= 'Merkitty';
$lang['faq:category_label']			= 'Kategoria';
$lang['faq:post_label']				= 'Artikkeli';
$lang['faq:date_label']				= 'Päivä';
$lang['faq:date_at']					= 'at'; #translate (in finnish we use adessives by using suffixes!, see http://www.cs.tut.fi/~jkorpela/finnish-cases.html)
$lang['faq:time_label']				= 'Aika';
$lang['faq:status_label']				= 'Tila';
$lang['faq:draft_label']				= 'Luonnos';
$lang['faq:live_label']				= 'Julkinen';
$lang['faq:content_label']				= 'Sisältö';
$lang['faq:options_label']				= 'Valinnat';
$lang['faq:intro_label']				= 'Alkuteksti';
$lang['faq:no_category_select_label']	= '-- Ei mikään --';
$lang['faq:new_category_label']		= 'Luo kategoria';
$lang['faq:subscripe_to_rss_label']	= 'Tilaa RSS';
$lang['faq:all_posts_label']			= 'Kaikki artikkelit';
$lang['faq:posts_of_category_suffix']	= ' artikkelia';
$lang['faq:rss_name_suffix']			= ' Uutiset';
$lang['faq:rss_category_suffix']		= ' Uutiset';
$lang['faq:author_name_label']			= 'Tekijän nimi';
$lang['faq:read_more_label']			= 'Lue lisää&nbsp;&raquo;';
$lang['faq:created_hour']				= 'Luotiin tunnissa';
$lang['faq:created_minute']			= 'Luotiin minuutissa';
$lang['faq:comments_enabled_label']	= 'Kommentit päällä';

// titles
$lang['faq:disabled_after']	= 'Kommentointi on otettu pois käytöstä %s jälkeen.';
$lang['faq:create_title']		= 'Luo artikkeli';
$lang['faq:edit_title']		= 'Muokkaa artikkelia "%s"';
$lang['faq:archive_title']		= 'Arkisto';
$lang['faq:posts_title']		= 'Artikkelit';
$lang['faq:rss_posts_title']	= 'Uutisartikkeleita %s';
$lang['faq:faq_title']		= 'Uutiset';
$lang['faq:list_title']		= 'Listaa artikkelit';

// messages
$lang['faq:no_posts']				= 'Artikkeleita ei ole.';
$lang['faq:subscripe_to_rss_desc']	= 'Lue artikkelit tilaamalla RSS syöte. Suosituimmat sähköposti ohjelmat tukevat tätä. Voit myös vaihtoehtoisesti kokeilla <a href="http://reader.google.co.uk/">Google Readeria</a>.';
$lang['faq:currently_no_posts']	= 'Ei artikkeleita tällä hetkellä.';
$lang['faq:post_add_success']		= 'Artikkeli "%s" lisättiin.';
$lang['faq:post_add_error']		= 'Tapahtui virhe.';
$lang['faq:edit_success']			= 'Artikkeli "%s" päivitettiin.';
$lang['faq:edit_error']			= 'Tapahtui virhe.';
$lang['faq:publish_success']		= 'Artikkeli "%s" julkaistiin.';
$lang['faq:mass_publish_success']	= 'Artikkelit "%s" julkaistiin.';
$lang['faq:publish_error']			= 'Yhtään artikkelia ei julkaistu.';
$lang['faq:delete_success']		= 'Artikkeli "%s" poistettiin.';
$lang['faq:mass_delete_success']	= 'Artikkelit "%s" poistettiin.';
$lang['faq:delete_error']			= 'Yhtään artikkelia ei poistettu.';
$lang['faq:already_exist_error']	= 'Artikkelin URL osoite on jo käytössä.';

$lang['faq:twitter_posted']	= 'Lähetetty "%s" %s';
$lang['faq:twitter_error']		= 'Twitter Virhe';

// date
$lang['faq:archive_date_format'] = "%B, %Y";

?>