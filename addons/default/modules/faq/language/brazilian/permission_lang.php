<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Publicar artigos';
$lang['faq:role_edit_live']	= 'Editar artigos publicados';
$lang['faq:role_delete_live'] 	= 'Remover artigos publicados';