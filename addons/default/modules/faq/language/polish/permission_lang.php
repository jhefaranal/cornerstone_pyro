<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Publikuj wpisy';
$lang['faq:role_edit_live']		= 'Edytuj opublikowane wpisy';
$lang['faq:role_delete_live'] 		= 'Usuwaj opublikowane wpisy';