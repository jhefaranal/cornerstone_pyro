<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label'] 					= 'Opublikowano w';
$lang['faq:posted_label_alt']					= 'Opublikowano ';
$lang['faq:written_by_label']					= 'Napisany przez';
$lang['faq:author_unknown']					= 'Nieznany';
$lang['faq:keywords_label']					= 'Słowa kluczowe';
$lang['faq:tagged_label']					= 'Tagi';
$lang['faq:category_label'] 					= 'Kategoria';
$lang['faq:post_label'] 					= 'Post';
$lang['faq:date_label'] 					= 'Data';
$lang['faq:date_at']						= '';
$lang['faq:time_label'] 					= 'Czas';
$lang['faq:status_label'] 					= 'Status';
$lang['faq:draft_label'] 					= 'Robocza';
$lang['faq:live_label'] 					= 'Opublikowana';
$lang['faq:content_label'] 					= 'Zawartość';
$lang['faq:options_label'] 					= 'Opcje';
$lang['faq:intro_label'] 					= 'Wprowadzenie';
$lang['faq:no_category_select_label'] 				= '-- Brak --';
$lang['faq:new_category_label'] 				= 'Dodaj kategorię';
$lang['faq:subscripe_to_rss_label'] 				= 'Subskrybuj RSS';
$lang['faq:all_posts_label'] 					= 'Wszystkie wpisy';
$lang['faq:posts_of_category_suffix'] 				= ' - wpisy';
$lang['faq:rss_name_suffix'] 					= '';
$lang['faq:rss_category_suffix'] 				= '';
$lang['faq:author_name_label'] 				= 'Imię autora';
$lang['faq:read_more_label'] 					= 'Czytaj więcej&nbsp;&raquo;';
$lang['faq:created_hour']               		    	= 'Czas (Godzina)';
$lang['faq:created_minute']              		    	= 'Czas (Minuta)';
$lang['faq:comments_enabled_label']         			= 'Komentarze aktywne';

// titles
$lang['faq:create_title'] 					= 'Dodaj wpis';
$lang['faq:edit_title'] 					= 'Edytuj wpis "%s"';
$lang['faq:archive_title'] 					= 'Archiwum';
$lang['faq:posts_title'] 					= 'Wpisy';
$lang['faq:rss_posts_title'] 					= 'Najnowsze wpisy dla %s';
$lang['faq:faq_title'] 					= 'Faq';
$lang['faq:list_title'] 					= 'Lista wpisów';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts'] 						= 'Nie ma żadnych wpisów.';
$lang['faq:subscripe_to_rss_desc'] 				= 'Bądź na bieżąco subskrybując nasz kanał RSS. Możesz to zrobić za pomocą popularnych klientów pocztowych, lub skorzystać z <a href="http://reader.google.com/">Google Reader\'a</a>.';
$lang['faq:currently_no_posts'] 				= 'W tym momencie nie ma żadnych wpisów.';
$lang['faq:post_add_success'] 					= 'Wpis "%s" został dodany.';
$lang['faq:post_add_error'] 					= 'Wystąpił błąd.';
$lang['faq:edit_success'] 					= 'Wpis "%s" został zaktualizowany.';
$lang['faq:edit_error'] 					= 'Wystąpił błąd.';
$lang['faq:publish_success'] 					= 'Wpis "%s" został opublikowany.';
$lang['faq:mass_publish_success'] 				= 'Wpisy "%s" zostały opublikowane.';
$lang['faq:publish_error'] 					= 'Żadne wpisy nie zostały opublikowane.';
$lang['faq:delete_success'] 					= 'Wpis "%s" został usunięty.';
$lang['faq:mass_delete_success'] 				= 'Wpisy "%s" zostały usunięte.';
$lang['faq:delete_error'] 					= 'Żadne wpisy nie zostały usunięte.';
$lang['faq:already_exist_error'] 				= 'Wpis z tym adresem URL już istnieje.';

$lang['faq:twitter_posted'] 					= 'Opublikowano "%s" %s';
$lang['faq:twitter_error'] 					= 'Błąd Twitter\'a';

// date
$lang['faq:archive_date_format']				= "%B %Y";