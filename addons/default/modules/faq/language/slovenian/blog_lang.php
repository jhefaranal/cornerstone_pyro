<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']                   = 'Objavljeno';
$lang['faq:posted_label_alt']               = 'Objavljeno ob';
$lang['faq:written_by_label']				= 'Objavil';
$lang['faq:author_unknown']				= 'Neznan';
$lang['faq:keywords_label']				= 'Klj. besede';
$lang['faq:tagged_label']					= 'Označen';
$lang['faq:category_label']                 = 'Kategorija';
$lang['faq:post_label']                     = 'Objava';
$lang['faq:date_label']                     = 'Datum';
$lang['faq:date_at']                        = 'ob';
$lang['faq:time_label']                     = 'Čas';
$lang['faq:status_label']                   = 'Stanje';
$lang['faq:draft_label']                    = 'Osnutek';
$lang['faq:live_label']                     = 'Vidno';
$lang['faq:content_label']                  = 'Vsebina';
$lang['faq:options_label']                  = 'Možnosti';
$lang['faq:intro_label']                    = 'Navodila';
$lang['faq:no_category_select_label']       = '-- Brez --';
$lang['faq:new_category_label']             = 'Dodaj kategorijo';
$lang['faq:subscripe_to_rss_label']         = 'Prijava na RSS';
$lang['faq:all_posts_label']             = 'Vsi prispevki';
$lang['faq:posts_of_category_suffix']    = ' prispevki';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Ime avtorja';
$lang['faq:read_more_label']                = 'Preberi vse&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Ustvarjeno ura';
$lang['faq:created_minute']                 = 'Ustvarjeno minuta';
$lang['faq:comments_enabled_label']         = 'Komentaji omogočeni';

// titles
$lang['faq:create_title']                   = 'Dodaj prispevek';
$lang['faq:edit_title']                     = 'Uredi prispevek "%s"';
$lang['faq:archive_title']                  = 'Arhiv';
$lang['faq:posts_title']                 = 'Članki';
$lang['faq:rss_posts_title']             = 'Faq prispevki za %s';
$lang['faq:faq_title']                     = 'Faq';
$lang['faq:list_title']                     = 'Seznam prispevkov';

// messages
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:no_posts']                    = 'Trenutno še ni prispevkov.';
$lang['faq:subscripe_to_rss_desc']          = 'Pridite do prispevkov v najkrajšem možnem času tako da se prijavite na RSS podajalca. To lahko storite preko popularnega email programa ali poizkusite  <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'Ta trenutek ni prispevkov';
$lang['faq:post_add_success']            = 'Vap prispevek je bil dodan "%s" ';
$lang['faq:post_add_error']              = 'Prišlo je do napake';
$lang['faq:edit_success']                   = 'Prispevek "%s" je bil oddan.';
$lang['faq:edit_error']                     = 'Prišlo je do napake.';
$lang['faq:publish_success']                = 'Prispevek "%s" je bil objavljen.';
$lang['faq:mass_publish_success']           = 'Prispeveki "%s" so bili objavljeni.';
$lang['faq:publish_error']                  = 'Noben prispevk ni bil objavljen.';
$lang['faq:delete_success']                 = 'Prispevki "%s" so bili izbrisani.';
$lang['faq:mass_delete_success']            = 'Prispevki "%s" so bili izbrisani.';
$lang['faq:delete_error']                   = 'Noben od prispevkov ni bil izbrisan..';
$lang['faq:already_exist_error']            = 'Prispevek s tem URL-jem že obstaja.';

$lang['faq:twitter_posted']                 = 'Objavljeno "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter napaka';

// date
$lang['faq:archive_date_format']		= "%d' %m' %Y";

/* End of file faq_lang.php */