<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Objavi članek';
$lang['faq:role_edit_live']	= 'Uredi objavljen članek';
$lang['faq:role_delete_live'] 	= 'Izbriši objavljen članek';

// slovenian premission_lang.php