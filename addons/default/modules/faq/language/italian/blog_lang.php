<?php
/* Translation made Nicola Tudino */
/* Date 07/11/2010 */

$lang['faq:post']                 = 'Articolo';
$lang['faq:posts']                   = 'Articoli';

// labels
$lang['faq:posted_label'] 			= 'Pubblicato';
$lang['faq:posted_label_alt']			= 'Pubblicato alle';
$lang['faq:written_by_label']				= 'Scritto da';
$lang['faq:author_unknown']				= 'Sconosciuto';
$lang['faq:keywords_label']				= 'Parole chiave'; 
$lang['faq:tagged_label']					= 'Taggato';
$lang['faq:category_label'] 			= 'Categoria';
$lang['faq:post_label'] 			= 'Articolo';
$lang['faq:date_label'] 			= 'Data';
$lang['faq:date_at']				= 'alle';
$lang['faq:time_label'] 			= 'Ora';
$lang['faq:status_label'] 			= 'Stato';
$lang['faq:draft_label'] 			= 'Bozza';
$lang['faq:live_label'] 			= 'Pubblicato';
$lang['faq:content_label'] 			= 'Contenuto';
$lang['faq:options_label'] 			= 'Opzioni';
$lang['faq:intro_label'] 			= 'Introduzione';
$lang['faq:no_category_select_label'] 		= '-- Nessuna --';
$lang['faq:new_category_label'] 		= 'Aggiungi una categoria';
$lang['faq:subscripe_to_rss_label'] 		= 'Abbonati all\'RSS';
$lang['faq:all_posts_label'] 		= 'Tutti gli articoli';
$lang['faq:posts_of_category_suffix'] 	= ' articoli';
$lang['faq:rss_name_suffix'] 			= ' Notizie';
$lang['faq:rss_category_suffix'] 		= ' Notizie';
$lang['faq:author_name_label'] 		= 'Nome autore';
$lang['faq:read_more_label'] 			= 'Leggi tutto&nbsp;&raquo;';
$lang['faq:created_hour']                  = 'Time (Ora)'; 
$lang['faq:created_minute']                = 'Time (Minuto)';
$lang['faq:comments_enabled_label']         = 'Commenti abilitati';

// titles
$lang['faq:create_title'] 			= 'Aggiungi un articolo';
$lang['faq:edit_title'] 			= 'Modifica l\'articolo "%s"';
$lang['faq:archive_title'] 			= 'Archivio';
$lang['faq:posts_title'] 			= 'Articoli';
$lang['faq:rss_posts_title'] 		= 'Notizie per %s';
$lang['faq:faq_title'] 			= 'Notizie';
$lang['faq:list_title'] 			= 'Elenco articoli';

// messages
$lang['faq:disabled_after'] 				= 'Non sarà più possibile inserire commenti dopo %s.';
$lang['faq:no_posts'] 			= 'Non ci sono articoli.';
$lang['faq:subscripe_to_rss_desc'] 		= 'Ricevi gli articoli subito abbonandoti al nostro feed RSS. Lo puoi fare con i comuni programmi di posta elettronica, altrimenti prova <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts'] 		= 'Non ci sono articoli al momento.';
$lang['faq:post_add_success'] 		= 'L\'articolo "%s" è stato aggiunto.';
$lang['faq:post_add_error'] 		= 'C\'è stato un errore.';
$lang['faq:edit_success'] 			= 'L\'articolo "%s" è stato modificato.';
$lang['faq:edit_error'] 			= 'C\'è stato un errore.';
$lang['faq:publish_success'] 			= 'L\'articolo "%s" è stato pubblicato.';
$lang['faq:mass_publish_success'] 		= 'Gli articoli "%s" sono stati pubblicati.';
$lang['faq:publish_error'] 			= 'Gli articoli non saranno pubblicati.';
$lang['faq:delete_success'] 			= 'L\'articolo "%s" è stato eliminato.';
$lang['faq:mass_delete_success'] 		= 'Gli articoli "%s" sono stati eliminati.';
$lang['faq:delete_error'] 			= 'Nessun articolo è stato eliminato.';
$lang['faq:already_exist_error'] 		= 'Un articolo con questo URL esiste già.';

$lang['faq:twitter_posted']			= 'Inviato "%s" %s';
$lang['faq:twitter_error'] 			= 'Errore per Twitter';

// date
$lang['faq:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation

?>