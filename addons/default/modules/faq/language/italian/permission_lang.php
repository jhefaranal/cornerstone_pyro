<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Pubblicare nuovi articoli';
$lang['faq:role_edit_live']	= 'Modificare articoli pubblicati'; 
$lang['faq:role_delete_live'] 	= 'Cancellare articoli pubblicati'; 