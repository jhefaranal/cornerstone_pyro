<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'Post'; #translate
$lang['faq:posts']                   = 'Posts'; #translate

// labels
$lang['faq:posted_label']                   = 'Opslået';
$lang['faq:posted_label_alt']               = 'Opslået på';
$lang['faq:written_by_label']				= 'Skrevet af';
$lang['faq:author_unknown']				= 'Ukendt';
$lang['faq:keywords_label']				= 'Keywords'; #translate
$lang['faq:tagged_label']					= 'Tagged'; #translate
$lang['faq:category_label']                 = 'Kategori';
$lang['faq:post_label']                     = 'Opslå';
$lang['faq:date_label']                     = 'Dato';
$lang['faq:date_at']                        = 'på';
$lang['faq:time_label']                     = 'Tid';
$lang['faq:status_label']                   = 'Status';
$lang['faq:draft_label']                    = 'Udkast';
$lang['faq:live_label']                     = 'Live';
$lang['faq:content_label']                  = 'Indhold';
$lang['faq:options_label']                  = 'Indstillinger';
$lang['faq:intro_label']                    = 'Introduktion';
$lang['faq:no_category_select_label']       = '-- Ingen --';
$lang['faq:new_category_label']             = 'Tilføj en kategori';
$lang['faq:subscripe_to_rss_label']         = 'Abonnér på RSS';
$lang['faq:all_posts_label']             = 'Alle opslag';
$lang['faq:posts_of_category_suffix']    = ' opslag';
$lang['faq:rss_name_suffix']                = ' Faq';
$lang['faq:rss_category_suffix']            = ' Faq';
$lang['faq:author_name_label']              = 'Forfatter';
$lang['faq:read_more_label']                = 'Læs mere&nbsp;&raquo;';
$lang['faq:created_hour']                   = 'Oprettet on time';
$lang['faq:created_minute']                 = 'Oprettet on minut';
$lang['faq:comments_enabled_label']         = 'Kommentarer aktiveret';

// titles
$lang['faq:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['faq:create_title']                   = 'Tilføj opslag';
$lang['faq:edit_title']                     = 'Redigér opslag "%s"';
$lang['faq:archive_title']                  = 'Arkivér';
$lang['faq:posts_title']                 = 'Opslag';
$lang['faq:rss_posts_title']             = 'Faq-opslag for %s';
$lang['faq:faq_title']                     = 'Faq';
$lang['faq:list_title']                     = 'Liste opslag';

// messages
$lang['faq:no_posts']                    = 'Der er ingen opslag.';
$lang['faq:subscripe_to_rss_desc']          = 'Få opslag med det same ved at abonnere på vores RSS feed. Dette kan du gøre dette via de mest populære e-mail klienter, eller du kan prøve <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts']          = 'Der er i øjeblikket ingen opslag';
$lang['faq:post_add_success']            = 'Opslaget "%s" er tilføjet.';
$lang['faq:post_add_error']              = 'Der opstod en fejl.';
$lang['faq:edit_success']                   = 'Opslaget "%s" er opdateret.';
$lang['faq:edit_error']                     = 'Der opstod en fejl.';
$lang['faq:publish_success']                = 'Opslaget "%s" er blevet publiceret.';
$lang['faq:mass_publish_success']           = 'Opslagene "%s" er blevet publiceret.';
$lang['faq:publish_error']                  = 'Ingen opslag er blevet publiceret.';
$lang['faq:delete_success']                 = 'Opslaget "%s" er slettet.';
$lang['faq:mass_delete_success']            = 'Opslagene "%s" er slettet.';
$lang['faq:delete_error']                   = 'Ingen opslag er blevet slettet.';
$lang['faq:already_exist_error']            = 'Et opslag med denne URL findes allerede.';

$lang['faq:twitter_posted']                 = 'Publiceret "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter fejl';

// date
$lang['faq:archive_date_format']		= "%B %Y";
