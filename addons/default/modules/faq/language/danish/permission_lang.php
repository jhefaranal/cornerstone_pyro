<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Læg artikler online';
$lang['faq:role_edit_live']	= 'Redigér online artikler';
$lang['faq:role_delete_live'] 	= 'Slet online artikler';