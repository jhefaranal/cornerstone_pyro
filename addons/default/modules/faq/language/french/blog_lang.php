<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']  = 'Actualité';
$lang['faq:posts'] = 'Actualités';

// labels
$lang['faq:posted_label']             = 'Publié le';
$lang['faq:posted_label_alt']         = 'Envoyé à';
$lang['faq:written_by_label']         = 'Écrit par';
$lang['faq:author_unknown']           = 'Inconnu';
$lang['faq:keywords_label']           = 'Mots-clés';
$lang['faq:tagged_label']             = 'Taggé';
$lang['faq:category_label']           = 'Catégorie';
$lang['faq:post_label']               = 'Article';
$lang['faq:date_label']               = 'Date';
$lang['faq:date_at']                  = 'le';
$lang['faq:time_label']               = 'Heure';
$lang['faq:status_label']             = 'Statut';
$lang['faq:draft_label']              = 'Brouillon';
$lang['faq:live_label']               = 'Live';
$lang['faq:content_label']            = 'Contenu';
$lang['faq:options_label']            = 'Options';
$lang['faq:intro_label']              = 'Introduction';
$lang['faq:no_category_select_label'] = '-- Aucune --';
$lang['faq:new_category_label']       = 'Ajouter une catégorie';
$lang['faq:subscripe_to_rss_label']   = 'Souscrire au RSS';
$lang['faq:all_posts_label']          = 'Tous les Articles';
$lang['faq:posts_of_category_suffix'] = ' articles';
$lang['faq:rss_name_suffix']          = ' Articles';
$lang['faq:rss_category_suffix']      = ' Articles';
$lang['faq:author_name_label']        = 'Auteur';
$lang['faq:read_more_label']          = 'Lire la suite&nbsp;&raquo;';
$lang['faq:created_hour']             = 'Heure de création';
$lang['faq:created_minute']           = 'Minute de création';
$lang['faq:comments_enabled_label']   = 'Commentaires activés';

// titles
$lang['faq:create_title']    			= 'Créer un article';
$lang['faq:edit_title']      			= 'Modifier l\'article "%s"';
$lang['faq:archive_title']   			= 'Archives';
$lang['faq:posts_title']     			= 'Articles';
$lang['faq:rss_posts_title'] 			= ' pour %s';
$lang['faq:faq_title']      			= 'Articles';
$lang['faq:list_title']      			= 'Lister les posts';

// messages
$lang['faq:disabled_after'] 			= 'Poster des commentaires après  %s a été désactivé.';
$lang['faq:no_posts']              = 'Il n\'y a pas d\'articles.';
$lang['faq:subscripe_to_rss_desc'] = 'Restez informé des derniers articles en temps réel en vous abonnant au flux RSS. Vous pouvez faire cela avec la plupart des logiciels de messagerie, ou essayez <a href="http://www.google.fr/reader">Google Reader</a>.';
$lang['faq:currently_no_posts']    = 'Il n\'y a aucun article actuellement.';
$lang['faq:post_add_success']      = 'L\'article "%s" a été ajouté.';
$lang['faq:post_add_error']        = 'Une erreur est survenue.';
$lang['faq:edit_success']          = 'Le post "%s" a été mis à jour.';
$lang['faq:edit_error']            = 'Une erreur est survenue.';
$lang['faq:publish_success']       = 'Les articles "%s" ont été publiés.';
$lang['faq:mass_publish_success']  = 'Les articles "%s" ont été publiés.';
$lang['faq:publish_error']         = 'Aucun article n\'a été publié.';
$lang['faq:delete_success']        = 'Les articles "%s" ont été supprimés.';
$lang['faq:mass_delete_success']   = 'Les articles "%s" ont été supprimés.';
$lang['faq:delete_error']          = 'Aucun article n\'a été supprimé.';
$lang['faq:already_exist_error']   = 'Un article avec cet URL existe déjà.';

$lang['faq:twitter_posted'] = 'Envoyé "%s" %s';
$lang['faq:twitter_error']  = 'Erreur Twitter';

// date
$lang['faq:archive_date_format'] = "%B %Y";
