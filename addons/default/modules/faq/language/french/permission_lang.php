<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'Mettre l\'article en direct';
$lang['faq:role_edit_live']	= 'Modifier les articles en direct';
$lang['faq:role_delete_live'] 	= 'Supprimer les articles en direct';