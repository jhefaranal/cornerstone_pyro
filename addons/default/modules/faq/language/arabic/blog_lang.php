<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                 = 'تدوينة';
$lang['faq:posts']                   = 'تدوينات';

// labels
$lang['faq:posted_label'] 			= 'تاريخ النشر';
$lang['faq:posted_label_alt']			= 'نشر في';
$lang['faq:written_by_label']				= 'كتبها';
$lang['faq:author_unknown']				= 'مجهول';
$lang['faq:keywords_label']				= 'كلمات البحث';
$lang['faq:tagged_label']					= 'موسومة';
$lang['faq:category_label'] 			= 'التصنيف';
$lang['faq:post_label'] 			= 'إرسال';
$lang['faq:date_label'] 			= 'التاريخ';
$lang['faq:date_at']				= 'عند';
$lang['faq:time_label'] 			= 'الوقت';
$lang['faq:status_label'] 			= 'الحالة';
$lang['faq:draft_label'] 			= 'مسودّة';
$lang['faq:live_label'] 			= 'منشور';
$lang['faq:content_label'] 			= 'المُحتوى';
$lang['faq:options_label'] 			= 'خيارات';
$lang['faq:intro_label'] 			= 'المٌقدّمة';
$lang['faq:no_category_select_label'] 		= '-- لاشيء --';
$lang['faq:new_category_label'] 		= 'إضافة تصنيف';
$lang['faq:subscripe_to_rss_label'] 		= 'اشترك في خدمة RSS';
$lang['faq:all_posts_label'] 		= 'جميع التدوينات';
$lang['faq:posts_of_category_suffix'] 	= ' &raquo; التدوينات';
$lang['faq:rss_name_suffix'] 			= ' &raquo; المُدوّنة';
$lang['faq:rss_category_suffix'] 		= ' &raquo; المُدوّنة';
$lang['faq:author_name_label'] 		= 'إسم الكاتب';
$lang['faq:read_more_label'] 			= 'إقرأ المزيد&nbsp;&raquo;';
$lang['faq:created_hour']                  = 'الوقت (الساعة)';
$lang['faq:created_minute']                = 'الوقت (الدقيقة)';
$lang['faq:comments_enabled_label']         = 'إتاحة التعليقات';

// titles
$lang['faq:create_title'] 			= 'إضافة مقال';
$lang['faq:edit_title'] 			= 'تعديل التدوينة "%s"';
$lang['faq:archive_title'] 			= 'الأرشيف';
$lang['faq:posts_title'] 			= 'التدوينات';
$lang['faq:rss_posts_title'] 		= 'تدوينات %s';
$lang['faq:faq_title'] 			= 'المُدوّنة';
$lang['faq:list_title'] 			= 'سرد التدوينات';

// messages
$lang['faq:disabled_after'] 				= 'تم تعطيل التعليقات بعد %s.';
$lang['faq:no_posts'] 			= 'لا يوجد تدوينات.';
$lang['faq:subscripe_to_rss_desc'] 		= 'اطلع على آخر التدوينات مباشرة بالاشتراك بخدمة RSS. يمكنك القيام بذلك من خلال معظم برامج البريد الإلكتروني الشائعة، أو تجربة <a href="http://reader.google.com/">قارئ جوجل</a>.';
$lang['faq:currently_no_posts'] 		= 'لا يوجد تدوينات حالياً.';
$lang['faq:post_add_success'] 		= 'تمت إضافة التدوينة "%s".';
$lang['faq:post_add_error'] 		= 'حدث خطأ.';
$lang['faq:edit_success'] 			= 'تم تحديث التدوينة "%s".';
$lang['faq:edit_error'] 			= 'حدث خطأ.';
$lang['faq:publish_success'] 			= 'تم نشر التدوينة "%s".';
$lang['faq:mass_publish_success'] 		= 'تم نشر التدوينات "%s".';
$lang['faq:publish_error'] 			= 'لم يتم نشر أي تدوينات.';
$lang['faq:delete_success'] 			= 'تم حذف التدوينة "%s".';
$lang['faq:mass_delete_success'] 		= 'تم حذف التدوينات "%s".';
$lang['faq:delete_error'] 			= 'لم يتم حذف أي تدوينات.';
$lang['faq:already_exist_error'] 		= 'يوجد مقال له عنوان URL مطابق.';

$lang['faq:twitter_posted']			= 'منشور في "%s" %s';
$lang['faq:twitter_error'] 			= 'خطأ في تويتر';

// date
$lang['faq:archive_date_format']		= "%B %Y";

?>
