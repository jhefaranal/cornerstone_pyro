<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live'] = 'نشر التدوينات';
$lang['faq:role_edit_live'] = 'تعديل التدوينات المنشورة';
$lang['faq:role_delete_live'] 	= 'حذف التدوينات المنشورة';
