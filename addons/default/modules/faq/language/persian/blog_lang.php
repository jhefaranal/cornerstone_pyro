<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                          = 'پست';
$lang['faq:posts']                         = 'پست ها';

// labels
$lang['faq:posted_label']                  = 'ارسال شده';
$lang['faq:posted_label_alt']               = 'ارسال شده';
$lang['faq:written_by_label']		 = 'نوشته شده توسط';
$lang['faq:author_unknown']	  	 = 'نامشخص';
$lang['faq:keywords_label']		 = 'کلمات کلیدی';
$lang['faq:tagged_label']			 = 'تگ ها';
$lang['faq:category_label']                = 'مجموعه';
$lang['faq:post_label']                    = 'پست';
$lang['faq:date_label']                    = 'تاریخ';
$lang['faq:date_at']                       = 'در';
$lang['faq:time_label']                    = 'زمان';
$lang['faq:status_label']                  = 'وضعیت';
$lang['faq:draft_label']                    = 'پیشنویس';
$lang['faq:live_label']                     = 'ارسال شده';
$lang['faq:content_label']                  = 'محتوا';
$lang['faq:options_label']                  = 'تنظیمات';
$lang['faq:intro_label']                    = 'چکیده';
$lang['faq:no_category_select_label']       = '-- هیچیک --';
$lang['faq:new_category_label']             = 'مجموعه جدید';
$lang['faq:subscripe_to_rss_label']         = 'عضویت در RSS';
$lang['faq:all_posts_label']                = 'همه ی  پست ها';
$lang['faq:posts_of_category_suffix']       = ' پست ها';
$lang['faq:rss_name_suffix']                = ' بلاگ';
$lang['faq:rss_category_suffix']            = ' بلاگ';
$lang['faq:author_name_label']              = 'نام نویسنده';
$lang['faq:read_more_label']                = 'مشاهده جزئیات';
$lang['faq:created_hour']                   = 'ایجاد شده در ساعت';
$lang['faq:created_minute']                 = 'ایجاد شده در دقیقه ی ';
$lang['faq:comments_enabled_label']         = 'فعال کردن کامنت ها';

// titles
$lang['faq:create_title']                   = 'پست جدید';
$lang['faq:edit_title']                     = 'ویرایش پست "%s"';
$lang['faq:archive_title']                 = 'آرشیو';
$lang['faq:posts_title']					= 'پست ها';
$lang['faq:rss_posts_title']				= 'ارسال های مربوط به %s';
$lang['faq:faq_title']					= 'بلاگ';
$lang['faq:list_title']					= 'لیست پست ها';

// messages
$lang['faq:disabled_after'] 		= 'ارسال نظرات پس از %s غیر فعال شده است.';
$lang['faq:no_posts']                      = 'هیچ پستی وجود ندارد';
$lang['faq:subscripe_to_rss_desc']          = 'مشترک خبرخوانRSS ما بشوید.';
$lang['faq:currently_no_posts']          = 'در حال حاضر هیچ پستی وجود ندارد.';
$lang['faq:post_add_success']            = '"%s" اضافه شد.';
$lang['faq:post_add_error']              = 'خطایی رخ داده است.';
$lang['faq:edit_success']                   = '"%s" آپدیت شد.';
$lang['faq:edit_error']                     = 'خطایی رخ داده است.';
$lang['faq:publish_success']                = 'پست "%s" منتشر شد.';
$lang['faq:mass_publish_success']           = 'پست های "%s" منتشر شدند.';
$lang['faq:publish_error']                  = 'هیچ پستی منتشر نشد';
$lang['faq:delete_success']                 = 'پست "%s" دلیت شد.';
$lang['faq:mass_delete_success']            = 'پست های "%s" دلیت شدند.';
$lang['faq:delete_error']                   = 'هیچ پستی دلیت نشد.';
$lang['faq:already_exist_error']            = 'همینک  یک پست با این URL موجود است.';

$lang['faq:twitter_posted']                 = 'پست شد "%s" %s';
$lang['faq:twitter_error']                  = 'Twitter خطای';

// date
$lang['faq:archive_date_format']		= "%B %Y";
