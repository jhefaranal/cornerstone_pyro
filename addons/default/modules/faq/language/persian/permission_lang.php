<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= 'تغییر یک پست به حالت منتشر شده';
$lang['faq:role_edit_live']        = 'ویرایش پست های منتظر شده';
$lang['faq:role_delete_live'] 	= 'حذف پست های منتشر شده';