<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live'] = 'Να δημοσιεύει άρθρα';
$lang['faq:role_edit_live'] = 'Επεξεργασία δημοσιευμένων άρθρων';
$lang['faq:role_delete_live'] 	= 'Διαγραφή δημοσιευμένων άρθρων';