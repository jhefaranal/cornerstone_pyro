<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['faq:post']                      = 'Ανάρτηση';
$lang['faq:posts']                     = 'Αναρτήσεις';

// labels
$lang['faq:posted_label'] 		= 'Δημοσιεύτηκε';
$lang['faq:posted_label_alt'] 		= 'Δημοσιεύτηκε στις';
$lang['faq:written_by_label'] 		= 'Γράφτηκε από';
$lang['faq:author_unknown'] 		= 'Άγνωστο';
$lang['faq:keywords_label'] 		= 'Λέξεις κλειδιά';
$lang['faq:tagged_label'] 		= 'Ετικέτες';
$lang['faq:category_label'] 		= 'Κατηγορία';
$lang['faq:post_label'] 		= 'Δημοσίευση';
$lang['faq:date_label'] 		= 'Ημερομηνία';
$lang['faq:date_at'] 			= 'στις';
$lang['faq:time_label'] 		= 'Χρόνος';
$lang['faq:status_label'] 		= 'Κατάσταση';
$lang['faq:draft_label'] 		= 'Πρόχειρο';
$lang['faq:live_label'] 		= 'Δημοσιευμένο';
$lang['faq:content_label'] 		= 'Περιεχόμενο';
$lang['faq:options_label'] 		= 'Επιλογές';
$lang['faq:intro_label'] 		= 'Εισαγωγή';
$lang['faq:no_category_select_label'] 	= '-- Καμμία --';
$lang['faq:new_category_label'] 	= 'Προσθήκη κατηγορίας';
$lang['faq:subscripe_to_rss_label'] 	= 'Εγγραφή στο RSS';
$lang['faq:all_posts_label'] 		= 'Όλες οι δημοσιεύσεις';
$lang['faq:posts_of_category_suffix'] 	= ' δημοσιεύσεις';
$lang['faq:rss_name_suffix'] 		= ' Ιστολόγιο';
$lang['faq:rss_category_suffix'] 	= ' Ιστολόγιο';
$lang['faq:author_name_label'] 	= 'Όνομα συγγραφέα';
$lang['faq:read_more_label'] 		= 'Διαβάστε περισσότερα &raquo;';
$lang['faq:created_hour'] 		= 'Ώρα δημιουργίας';
$lang['faq:created_minute'] 		= 'Λεπτό δημιουργίας';
$lang['faq:comments_enabled_label'] 	= 'Σχόλια Ενεργά';

// titles
$lang['faq:create_title'] 		= 'Προσθήκη δημοσίευσης';
$lang['faq:edit_title'] 		= 'Επεξεργασία δημοσίευσης "%s"';
$lang['faq:archive_title'] 		= 'Αρχειοθήκη';
$lang['faq:posts_title'] 		= 'Δημοσιεύσεις';
$lang['faq:rss_posts_title'] 		= 'Δημοσιεύσεις ιστολογίου για %s';
$lang['faq:faq_title'] 		= 'Ιστολόγιο';
$lang['faq:list_title'] 		= 'Λίστα δημοσιεύσεων';

// messages
$lang['faq:disabled_after'] 				= 'Η ανάρτηση σχολίων μετά τις %s έχει απενεργοποιηθεί.';
$lang['faq:no_posts'] 			= 'Δεν υπάρχουν δημοσιεύσεις.';
$lang['faq:subscripe_to_rss_desc'] 	= 'Μπορείτε να λαμβάνετε νεότερες δημοσιεύσεις με το να εγγραφείτε στην τροφοδοσία RSS μας. Μπορείτε να το κάνετε χρησιμοποιώντας τα δημοφιλή προγράμματα e-mail, ή με το <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['faq:currently_no_posts'] 	= 'Προς το παρόν δεν υπάρχουν δημοσιεύσεις.';
$lang['faq:post_add_success'] 		= 'Η δημοσίευση "%s" προστέθηκε.';
$lang['faq:post_add_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['faq:edit_success'] 		= 'Η δημοσίευση "%s" ενημερώθηκε.';
$lang['faq:edit_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['faq:publish_success'] 		= 'Η δημοσίευση "%s" αναρτήθηκε.';
$lang['faq:mass_publish_success'] 	= 'Οι δημοσιεύσεις "%s" αναρτήθηκαν.';
$lang['faq:publish_error'] 		= 'Δεν αναρτήθηκε καμία δημοσίευση.';
$lang['faq:delete_success'] 		= 'Η δημοσίευση "%s" διαγράφηκε.';
$lang['faq:mass_delete_success'] 	= 'Οι δημοσιεύσεις "%s" διαγράφηκαν.';
$lang['faq:delete_error'] 		= 'Δεν διαγράφηκε καμία δημοσίευση.';
$lang['faq:already_exist_error'] 	= 'Υπάρχει ήδη μια δημοσίευση με αυτό το URL.';

$lang['faq:twitter_posted'] 		= 'Δημοσιεύτηκε "%s" %s';
$lang['faq:twitter_error'] 		= 'Σφάλμα Twitter';

// date
$lang['faq:archive_date_format'] 	= "%B %Y";
