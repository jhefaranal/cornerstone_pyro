<?php defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * Swedish translation.
 *
 * @author		marcus@incore.se
 * @package		PyroCMS  
 * @link		http://pyrocms.com
 * @date		2012-10-23
 * @version		1.1.0
 */

$lang['faq:post'] = 'Inlägg';
$lang['faq:posts'] = 'Inlägg';
$lang['faq:posted_label'] = 'Skriven';
$lang['faq:posted_label_alt'] = 'Skriven den';
$lang['faq:written_by_label'] = 'Skriven av';
$lang['faq:author_unknown'] = 'Okänd';
$lang['faq:keywords_label'] = 'Nyckelord';
$lang['faq:tagged_label'] = 'Taggad';
$lang['faq:category_label'] = 'Kategori';
$lang['faq:post_label'] = 'Inlägg';
$lang['faq:date_label'] = 'Datum';
$lang['faq:date_at'] = 'den';
$lang['faq:time_label'] = 'Tid';
$lang['faq:status_label'] = 'Status';
$lang['faq:draft_label'] = 'Utkast';
$lang['faq:live_label'] = 'Publik';
$lang['faq:content_label'] = 'Innehåll';
$lang['faq:options_label'] = 'Val';
$lang['faq:intro_label'] = 'Introduktion';
$lang['faq:no_category_select_label'] = '-- Ingen --';
$lang['faq:new_category_label'] = 'Lägg till en kategori';
$lang['faq:subscripe_to_rss_label'] = 'Prenumerera på RSS';
$lang['faq:all_posts_label'] = 'Alla inlägg';
$lang['faq:posts_of_category_suffix'] = 'inlägg';
$lang['faq:rss_name_suffix'] = 'Faqg';
$lang['faq:rss_category_suffix'] = 'Faqg';
$lang['faq:author_name_label'] = 'Skribent';
$lang['faq:read_more_label'] = 'Läs mer »';
$lang['faq:created_hour'] = 'Skapad, timme';
$lang['faq:created_minute'] = 'Skapad, minut';
$lang['faq:comments_enabled_label'] = 'Kommentarer aktiverad';
$lang['faq:create_title'] = 'Lägg till post';
$lang['faq:edit_title'] = 'Redigera post "%s"';
$lang['faq:archive_title'] = 'Arkiv';
$lang['faq:posts_title'] = 'Inlägg';
$lang['faq:rss_posts_title'] = 'Faqginlägg för %s';
$lang['faq:faq_title'] = 'Faqg';
$lang['faq:list_title'] = 'Lista inlägg';
$lang['faq:disabled_after'] = 'Skickade kommentarer efter %s har blivit inaktiverade.';
$lang['faq:no_posts'] = 'Det finns inga poster';
$lang['faq:subscripe_to_rss_desc'] = 'Få inlägg direkt genom att prenumerera på vårt RSS-flöde. Du flesta populära e-postklienter har stöd för detta, eller prova <a href="http://reader.google.co.uk/">Google Reader</ a>.';
$lang['faq:currently_no_posts'] = 'Det finns inga poster just nu';
$lang['faq:post_add_success'] = 'Inlägget "%s" har lagts till.';
$lang['faq:post_add_error'] = 'Ett fel inträffade.';
$lang['faq:edit_success'] = 'Inlägget "%s" uppdaterades.';
$lang['faq:edit_error'] = 'Ett fel inträffade.';
$lang['faq:publish_success'] = 'Inlägget "%s" publicerades.';
$lang['faq:mass_publish_success'] = 'Inläggen "%s" publicerades.';
$lang['faq:publish_error'] = 'Inga inlägg publicerades';
$lang['faq:delete_success'] = 'Inlägget "%s" har raderats.';
$lang['faq:mass_delete_success'] = 'Inläggen "%s" har raderats.';
$lang['faq:delete_error'] = 'Inga inlägg raderades';
$lang['faq:already_exist_error'] = 'Ett inlägg med denna URL finns redan';
$lang['faq:twitter_posted'] = 'Sparad "%s" %s';
$lang['faq:twitter_error'] = 'Twitterfel';
$lang['faq:archive_date_format'] = '%B\' %Y';


/* End of file faq_lang.php */  
/* Location: system/cms/modules/faq/language/swedish/faq_lang.php */  
