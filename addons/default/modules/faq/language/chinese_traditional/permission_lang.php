<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Faq Permissions
$lang['faq:role_put_live']		= '將文章上線';
$lang['faq:role_edit_live']	= '編輯上線文章';
$lang['faq:role_delete_live'] 	= '刪除上線文章';