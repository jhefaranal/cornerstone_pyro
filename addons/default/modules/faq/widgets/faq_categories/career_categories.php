<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Show a list of faq categories.
 *
 * @author        Stephen Cozart
 * @author        PyroCMS Dev Team
 * @package       PyroCMS\Core\Modules\Faq\Widgets
 */
class Widget_Faq_categories extends Widgets
{
	public $author = 'Stephen Cozart';

	public $website = 'http://github.com/clip/';

	public $version = '1.0.0';

	public $title = array(
		'en' => 'Faq Categories',
		'br' => 'Categorias do Faq',
		'pt' => 'Categorias do Faq',
		'el' => 'Κατηγορίες Ιστολογίου',
		'fr' => 'Catégories du Faq',
		'ru' => 'Категории Блога',
		'id' => 'Kateori Faq',
            'fa' => 'مجموعه های بلاگ',
	);

	public $description = array(
		'en' => 'Show a list of faq categories',
		'br' => 'Mostra uma lista de navegação com as categorias do Faq',
		'pt' => 'Mostra uma lista de navegação com as categorias do Faq',
		'el' => 'Προβάλει την λίστα των κατηγοριών του ιστολογίου σας',
		'fr' => 'Permet d\'afficher la liste de Catégories du Faq',
		'ru' => 'Выводит список категорий блога',
		'id' => 'Menampilkan daftar kategori tulisan',
            'fa' => 'نمایش لیستی از مجموعه های بلاگ',
	);

	public function run()
	{
		$this->load->model('faq/faq_categories_m');

		$categories = $this->faq_categories_m->order_by('title')->get_all();

		return array('categories' => $categories);
	}

}
