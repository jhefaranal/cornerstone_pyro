<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Sample Events Class
*
* @package 		PyroCMS
* @subpackage 	Social Module
* @category 	faqs
* @author 		PyroCMS Dev Team
*/
class Events_Faq
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();

        // Load the search index model
        $this->ci->load->model('search/search_index_m');

		// Post a blog to twitter and whatnot
        Events::register('faq_published', array($this, 'index_faq'));
        Events::register('faq_updated', array($this, 'index_faq'));
        Events::register('faq_deleted', array($this, 'drop_faq'));

    }
    
    public function index_faq($id)
    {
    	$this->ci->load->model('faq/faq_m');

    	$post = $this->ci->faq_m->get($id);

    	// Only index live articles
    	if ($post->status === 'live')
    	{
    		$this->ci->search_index_m->index(
    			'faq', 
    			'faq:post', 
    			'faq:posts', 
    			$id,
    			//'faq/'.date('Y/m/', $faq->created_on).$post->slug,
    			'faqs',
    			$post->title,
    			$post->body, 
    			array(
    				'cp_edit_uri' 	=> 'admin/faq/edit/'.$id,
    				'cp_delete_uri' => 'admin/faq/delete/'.$id,
    				'keywords' 		=> $post->keywords,
    			)
    		);
    	}
    	// Remove draft articles
    	else
    	{
    		$this->ci->search_index_m->drop_index('faq', 'faq:post', $id);
    	}
	}

    public function drop_faq($ids)
    {
    	foreach ($ids as $id)
    	{
			$this->ci->search_index_m->drop_index('faq', 'faq:post', $id);
		}
	}
  
}

/* End of file faqs.php */