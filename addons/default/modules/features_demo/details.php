<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Features Demo module
 *
 * @author  PyroCMS Dev Team
 * @package PyroCMS\Core\Modules\Features Demo
 */
class Module_Features_demo extends Module
{
	public $version = '2.0.0';

	public function info()
	{
		$info = array(
			'name' => array(
				'en' => 'Features Demo',
				'ar' => 'المدوّنة',
				'br' => 'Features Demo',
				'pt' => 'Features Demo',
				'el' => 'Ιστολόγιο',
                            'fa' => 'بلاگ',
				'he' => 'בלוג',
				'id' => 'Features Demo',
				'lt' => 'Features Demoas',
				'pl' => 'Features Demo',
				'ru' => 'Блог',
				'tw' => '文章',
				'cn' => '文章',
				'hu' => 'Features Demo',
				'fi' => 'Features Demoi',
				'th' => 'บล็อก',
				'se' => 'Features Demog',
			),
			'description' => array(
				'en' => 'Post Features Demo entries.',
				'ar' => 'أنشر المقالات على مدوّنتك.',
				'br' => 'Escrever publicações de features_demo',
				'pt' => 'Escrever e editar publicações no features_demo',
				'cs' => 'Publikujte nové články a příspěvky na features_demo.', #update translation
				'da' => 'Skriv features_demoindlæg',
				'de' => 'Veröffentliche neue Artikel und Features Demo-Einträge', #update translation
				'sl' => 'Objavite features_demo prispevke',
				'fi' => 'Kirjoita features_demoi artikkeleita.',
				'el' => 'Δημιουργήστε άρθρα και εγγραφές στο ιστολόγιο σας.',
				'es' => 'Escribe entradas para los artículos y features_demo (web log).', #update translation
                                'fa' => 'مقالات منتشر شده در بلاگ',
				'fr' => 'Poster des articles d\'actualités.',
				'he' => 'ניהול בלוג',
				'id' => 'Post entri features_demo',
				'it' => 'Pubblica notizie e post per il features_demo.', #update translation
				'lt' => 'Rašykite naujienas bei features_demo\'o įrašus.',
				'nl' => 'Post nieuwsartikelen en features_demos op uw site.',
				'pl' => 'Dodawaj nowe wpisy na features_demou',
				'ru' => 'Управление записями блога.',
				'tw' => '發表新聞訊息、部落格等文章。',
				'cn' => '发表新闻讯息、部落格等文章。',
				'th' => 'โพสต์รายการบล็อก',
				'hu' => 'Features Demo bejegyzések létrehozása.',
				'se' => 'Inlägg i features_demogen.',
			),
			'frontend' => true,
			'backend' => true,
			'skip_xss' => true,
			'menu' => 'content',

			'roles' => array(
				'put_live', 'edit_live', 'delete_live'
			),

			'sections' => array(
				'posts' => array(
					'name' => 'features_demo:posts_title',
					'uri' => 'admin/features_demo',
					'shortcuts' => array(
						array(
							'name' => 'features_demo:create_title',
							'uri' => 'admin/features_demo/create',
							'class' => 'add',
						),
					),
				),
				'categories' => array(
					'name' => 'cat:list_title',
					'uri' => 'admin/features_demo/categories',
					'shortcuts' => array(
						array(
							'name' => 'cat:create_title',
							'uri' => 'admin/features_demo/categories/create',
							'class' => 'add',
						),
					),
				),
			),
		);

		/*if (function_exists('group_has_role'))
		{
			if(group_has_role('features_demo', 'admin_features_demo_fields'))
			{
				$info['sections']['fields'] = array(
							'name' 	=> 'global:custom_fields',
							'uri' 	=> 'admin/features_demo/fields',
								'shortcuts' => array(
									'create' => array(
										'name' 	=> 'streams:add_field',
										'uri' 	=> 'admin/features_demo/fields/create',
										'class' => 'add'
										)
									)
							);
			}
		}*/

		return $info;
	}

	public function install()
	{
		$this->dbforge->drop_table('features_demo_categories');

		$this->load->driver('Streams');
		$this->streams->utilities->remove_namespace('features_demos');

		// Just in case.
		$this->dbforge->drop_table('features_demo');

		if ($this->db->table_exists('data_streams'))
		{
			$this->db->where('stream_namespace', 'features_demos')->delete('data_streams');
		}

		// Create the features_demo categories table.
		$this->install_tables(array(
			'features_demo_categories' => array(
				'id' => array('type' => 'INT', 'constraint' => 11, 'auto_increment' => true, 'primary' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true, 'key' => true),
				'title' => array('type' => 'VARCHAR', 'constraint' => 100, 'null' => false, 'unique' => true),
			),
		));

		$this->streams->streams->add_stream(
			'lang:features_demo:features_demo_title',
			'features_demo',
			'features_demos',
			null,
			null
		);

		// Add the intro field.
		// This can be later removed by an admin.
		/*$intro_field = array(
			'name'		=> 'lang:features_demo:intro_label',
			'slug'		=> 'intro',
			'namespace' => 'features_demos',
			'type'		=> 'wysiwyg',
			'assign'	=> 'features_demo',
			'extra'		=> array('editor_type' => 'simple', 'allow_tags' => 'y'),
			'required'	=> true
		);
		$this->streams->fields->add_field($intro_field);*/

		// Ad the rest of the features_demo fields the normal way.
		$features_demo_fields = array(
				'title' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false, 'unique' => true),
				'slug' => array('type' => 'VARCHAR', 'constraint' => 200, 'null' => false),
				'category_id' => array('type' => 'INT', 'constraint' => 11, 'key' => true),
				'body' => array('type' => 'TEXT'),
				'parsed' => array('type' => 'TEXT'),
				'keywords' => array('type' => 'VARCHAR', 'constraint' => 32, 'default' => ''),
				'author_id' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'created_on' => array('type' => 'INT', 'constraint' => 11),
				'updated_on' => array('type' => 'INT', 'constraint' => 11, 'default' => 0),
				'comments_enabled' => array('type' => 'ENUM', 'constraint' => array('no','1 day','1 week','2 weeks','1 month', '3 months', 'always'), 'default' => '3 months'),
				'status' => array('type' => 'ENUM', 'constraint' => array('draft', 'live'), 'default' => 'draft'),
				'type' => array('type' => 'SET', 'constraint' => array('html', 'markdown', 'wysiwyg-advanced', 'wysiwyg-simple')),
				'preview_hash' => array('type' => 'CHAR', 'constraint' => 32, 'default' => ''),
		);
		return $this->dbforge->add_column('features_demo', $features_demo_fields);
	}

	public function uninstall()
	{
		// Load the Streams Core
		/*$this->load->driver('Streams');

		// Remove streams and destruct everything
		$this->streams->utilities->remove_namespace('features_demos');
		$this->dbforge->drop_table('features_demo');

		return true;*/

		return false;
	}

	public function upgrade($old_version)
	{
		return true;
	}
}
