<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Show a list of features_demo categories.
 *
 * @author        Stephen Cozart
 * @author        PyroCMS Dev Team
 * @package       PyroCMS\Core\Modules\Features_demo\Widgets
 */
class Widget_Features_demo_categories extends Widgets
{
	public $author = 'Stephen Cozart';

	public $website = 'http://github.com/clip/';

	public $version = '1.0.0';

	public $title = array(
		'en' => 'Features_demo Categories',
		'br' => 'Categorias do Features_demo',
		'pt' => 'Categorias do Features_demo',
		'el' => 'Κατηγορίες Ιστολογίου',
		'fr' => 'Catégories du Features_demo',
		'ru' => 'Категории Блога',
		'id' => 'Kateori Features_demo',
            'fa' => 'مجموعه های بلاگ',
	);

	public $description = array(
		'en' => 'Show a list of features_demo categories',
		'br' => 'Mostra uma lista de navegação com as categorias do Features_demo',
		'pt' => 'Mostra uma lista de navegação com as categorias do Features_demo',
		'el' => 'Προβάλει την λίστα των κατηγοριών του ιστολογίου σας',
		'fr' => 'Permet d\'afficher la liste de Catégories du Features_demo',
		'ru' => 'Выводит список категорий блога',
		'id' => 'Menampilkan daftar kategori tulisan',
            'fa' => 'نمایش لیستی از مجموعه های بلاگ',
	);

	public function run()
	{
		$this->load->model('features_demo/features_demo_categories_m');

		$categories = $this->features_demo_categories_m->order_by('title')->get_all();

		return array('categories' => $categories);
	}

}
