<div class="one_full">
	<section class="title">
		<h4><?php echo lang('features_demo:posts_title') ?></h4>
	</section>

	<section class="item">
		<div class="content">
			<?php if ($features_demo) : ?>
				<?php echo $this->load->view('admin/partials/filters') ?>
	
				<?php echo form_open('admin/features_demo/action') ?>
					<div id="filter-stage">
						<?php echo $this->load->view('admin/tables/posts') ?>
					</div>
				<?php echo form_close() ?>
			<?php else : ?>
				<div class="no_data"><?php echo lang('features_demo:currently_no_posts') ?></div>
			<?php endif ?>
		</div>
	</section>
</div>
