<section class="title">
	<h4><?php echo lang('features_demo:features_demos_title') ?></h4>
</section>

<section class="item">
	<div class="content">

	<?php if ($features_demos): ?>
	
		<table border="0" class="table-list" cellspacing="0">
			<thead>
			<tr>
				<th><?php echo lang('features_demo:features_demo_title') ?></th>
				<th><?php echo lang('global:slug') ?></th>
				<th width="120"></th>
			</tr>
			</thead>
			<tbody>
			<?php foreach ($features_demos as $features_demo): ?>
			<tr>
				<td><?php echo lang_label($features_demo->stream_name); ?></td>
				<td><?php echo $features_demo->features_demo_uri; ?></td>
				<td>
					<a href="" class="btn">Edit</a>
					<a href="" class="btn">Delete</a>
					<a href="" class="btn">New Post</a>
				</td>
			</tr>
			<?php endforeach; ?>
			</tbody>
		</table>

		<?php echo form_close() ?>

<?php $this->load->view('admin/partials/pagination') ?>

	<?php else: ?>
		<div class="no_data">No features_demos</div>
	<?php endif ?>
	</div>
</section>