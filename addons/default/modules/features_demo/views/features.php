<?php if ($featured['is_default_demo'] == 1) : ?>
	<div class="product-featured" style="background:url('<?php echo base_url() ?>files/large/<?php echo $featured['banner'] ?>')">
		<div class="container">
			<div class="row">
				<div class="col-md-6" style="margin-top:10%">
					<?php echo $featured['body']; ?>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$('button').addClass('btn');
		$('button').on('click', function(){
			window.location = base_url+'tour';
		});
	</script>
<?php else: ?>
	<div class="container">
		<div class="row">
			<div class="col-md-6" style="margin-top:10%">
				<?php echo $featured['body']; ?>
			</div>
		</div>
	</div>
<?php endif; ?>