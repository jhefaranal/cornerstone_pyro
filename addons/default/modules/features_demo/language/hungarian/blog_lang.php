<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                           = 'Bejegyzés';
$lang['features_demo:posts']                          = 'Bejegyzések';

// labels
$lang['features_demo:posted_label']                   = 'Bejegyezve';
$lang['features_demo:posted_label_alt']               = 'Bejegyezve';
$lang['features_demo:written_by_label']               = 'Írta';
$lang['features_demo:author_unknown']                 = 'Ismeretlen';
$lang['features_demo:keywords_label']                 = 'Kulcsszavak';
$lang['features_demo:tagged_label']                   = 'Tagged';
$lang['features_demo:category_label']                 = 'Kategória';
$lang['features_demo:post_label']                     = 'Bejegyzés';
$lang['features_demo:date_label']                     = 'Dátum';
$lang['features_demo:date_at']                        = '';
$lang['features_demo:time_label']                     = 'Idő';
$lang['features_demo:status_label']                   = 'Állapot';
$lang['features_demo:draft_label']                    = 'Piszkozat';
$lang['features_demo:live_label']                     = 'Élő';
$lang['features_demo:content_label']                  = 'Tartalom';
$lang['features_demo:options_label']                  = 'Beállítások';
$lang['features_demo:intro_label']                    = 'Bevezető';
$lang['features_demo:no_category_select_label']       = '-- Nincs --';
$lang['features_demo:new_category_label']             = 'Kategória hozzáadása';
$lang['features_demo:subscripe_to_rss_label']         = 'Feliratkozás RSS-re';
$lang['features_demo:all_posts_label']                = 'Összes bejegyzés';
$lang['features_demo:posts_of_category_suffix']       = ' bejegyzések';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Szerző neve';
$lang['features_demo:read_more_label']                = 'Tovább&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Létrehozva pár órája';
$lang['features_demo:created_minute']                 = 'Létrehozva pár perce';
$lang['features_demo:comments_enabled_label']         = 'Hozzászólás engedélyezése';

// titles
$lang['features_demo:create_title']                   = 'Bejegyzés hozzáadása';
$lang['features_demo:edit_title']                     = 'A(z) "%s" bejegyzés szerkesztése';
$lang['features_demo:archive_title']                  = 'Archívum';
$lang['features_demo:posts_title']                    = 'Bejegyzések';
$lang['features_demo:rss_posts_title']                = 'Features_demo bejegyzés %s-ért';
$lang['features_demo:features_demo_title']                     = 'Features_demo';
$lang['features_demo:list_title']                     = 'Bejegyzések listája';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']                       = 'Nincs bejegyzés.';
$lang['features_demo:subscripe_to_rss_desc']          = 'A bejegyzést jelentesd meg saját RSS-en. Ezt meg lehet tenni az ismertebb levelezőkkel vagy a <a href="http://reader.google.co.uk/">Google Reader</a>rel.';
$lang['features_demo:currently_no_posts']             = 'Ebben a pillanatban még nincs bejegyzés.';
$lang['features_demo:post_add_success']               = 'A(z) "%s" bejegyzés sikeresen hozzáadva.';
$lang['features_demo:post_add_error']                 = 'A bejegyzés hozzáadása sikertelen.';
$lang['features_demo:edit_success']                   = 'A(z) "%s" bejegyzés sikeresen módosítva.';
$lang['features_demo:edit_error']                     = 'A bejegyzés módosítása sikertelen.';
$lang['features_demo:publish_success']                = 'A(z) "%s" bejegyzés közzétéve.';
$lang['features_demo:mass_publish_success']           = 'A(z) "%s" bejegyzés már közzétéve.';
$lang['features_demo:publish_error']                  = 'Nincs bejegyzés közzétéve.';
$lang['features_demo:delete_success']                 = 'A(z) "%s" bejegyzés törölve.';
$lang['features_demo:mass_delete_success']            = 'A(z) "%s" bejegyzés már törölve.';
$lang['features_demo:delete_error']                   = 'Nincs törölhető bejegyzés.';
$lang['features_demo:already_exist_error']            = 'Egy bejegyzés már létezik ezzel a hivatkozással.';

$lang['features_demo:twitter_posted']                 = 'Bejegyezve "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter Hiba';

// date
$lang['features_demo:archive_date_format']            = "%B %Y";
