<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Bejegyzés élővé tétele';
$lang['features_demo:role_edit_live']            = 'Élő bejegyzés szerkesztése';
$lang['features_demo:role_delete_live']          = 'Élő bejegyzés törlése';