<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']				= 'Escrito';
$lang['features_demo:posted_label_alt']			= 'Escrito em';
$lang['features_demo:written_by_label']			= 'Por';
$lang['features_demo:author_unknown']			= 'Desconhecido';
$lang['features_demo:keywords_label']			= 'Palavras-chave';
$lang['features_demo:tagged_label']				= 'Tagged'; #translate
$lang['features_demo:category_label']			= 'Categoria';
$lang['features_demo:post_label'] 				= 'Artigo';
$lang['features_demo:date_label'] 				= 'Data';
$lang['features_demo:date_at']					= 'às';
$lang['features_demo:time_label'] 				= 'Hora';
$lang['features_demo:status_label'] 				= 'Situação';
$lang['features_demo:draft_label'] 				= 'Rascunho';
$lang['features_demo:live_label'] 				= 'Publico';
$lang['features_demo:content_label']				= 'Conteúdo';
$lang['features_demo:options_label']				= 'Opções';
$lang['features_demo:intro_label'] 				= 'Introdução';
$lang['features_demo:no_category_select_label']	= '-- Nenhuma --';
$lang['features_demo:new_category_label'] 		= 'Adicionar uma categoria';
$lang['features_demo:subscripe_to_rss_label'] 	= 'Assinar o RSS';
$lang['features_demo:all_posts_label'] 			= 'Todos os artigos';
$lang['features_demo:posts_of_category_suffix'] 	= ' artigos';
$lang['features_demo:rss_name_suffix']			= ' Features_demo';
$lang['features_demo:rss_category_suffix']		= ' Features_demo';
$lang['features_demo:author_name_label']			= 'Nome do autor';
$lang['features_demo:read_more_label']			= 'Leia mais &raquo;';
$lang['features_demo:created_hour']				= 'Horário (Hora)';
$lang['features_demo:created_minute']			= 'Horário (Minuto)';
$lang['features_demo:comments_enabled_label']	= 'Habilitar comentários';

// titles
$lang['features_demo:create_title']				= 'Adicionar artigo';
$lang['features_demo:edit_title']				= 'Editar artigo "%s"';
$lang['features_demo:archive_title'] 			= 'Arquivo';
$lang['features_demo:posts_title'] 				= 'Artigos';
$lang['features_demo:rss_posts_title'] 			= 'Artigos novos para %s';
$lang['features_demo:features_demo_title']				= 'Features_demo';
$lang['features_demo:list_title']				= 'Listar artigos';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']					= 'Nenhum artigo.';
$lang['features_demo:subscripe_to_rss_desc']		= 'Fique por dentro das novidades do features_demo assinando o nosso feed RSS. Pode fazer isto pelos mais populares leitores de e-mail ou pode experimentar o <a rel="nofollow" href="http://reader.google.com/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']		= 'Não existem artigos no momento.';
$lang['features_demo:post_add_success']			= 'O artigo "%s" foi adicionado.';
$lang['features_demo:post_add_error']			= 'Ocorreu um erro.';
$lang['features_demo:edit_success']				= 'O artigo "%s" foi actualizado.';
$lang['features_demo:edit_error']				= 'Ocorreu um erro.';
$lang['features_demo:publish_success']			= 'O artigo "%s" foi publicado.';
$lang['features_demo:mass_publish_success']		= 'Os artigos "%s" foram publicados.';
$lang['features_demo:publish_error']				= 'Nenhum artigo foi publicado.';
$lang['features_demo:delete_success']			= 'O artigo "%s" foi removido.';
$lang['features_demo:mass_delete_success']		= 'Os artigos "%s" foram removidos.';
$lang['features_demo:delete_error']				= 'Nenhuma publicação foi removida.';
$lang['features_demo:already_exist_error']		= 'Um artigo com mesmo campo %s já existe.';

$lang['features_demo:twitter_posted']			= 'Escrito "%s" %s';
$lang['features_demo:twitter_error']				= 'Erro do Twitter';

// date
$lang['features_demo:archive_date_format']		= "%B de %Y";