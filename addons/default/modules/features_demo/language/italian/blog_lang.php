<?php
/* Translation made Nicola Tudino */
/* Date 07/11/2010 */

$lang['features_demo:post']                 = 'Articolo';
$lang['features_demo:posts']                   = 'Articoli';

// labels
$lang['features_demo:posted_label'] 			= 'Pubblicato';
$lang['features_demo:posted_label_alt']			= 'Pubblicato alle';
$lang['features_demo:written_by_label']				= 'Scritto da';
$lang['features_demo:author_unknown']				= 'Sconosciuto';
$lang['features_demo:keywords_label']				= 'Parole chiave'; 
$lang['features_demo:tagged_label']					= 'Taggato';
$lang['features_demo:category_label'] 			= 'Categoria';
$lang['features_demo:post_label'] 			= 'Articolo';
$lang['features_demo:date_label'] 			= 'Data';
$lang['features_demo:date_at']				= 'alle';
$lang['features_demo:time_label'] 			= 'Ora';
$lang['features_demo:status_label'] 			= 'Stato';
$lang['features_demo:draft_label'] 			= 'Bozza';
$lang['features_demo:live_label'] 			= 'Pubblicato';
$lang['features_demo:content_label'] 			= 'Contenuto';
$lang['features_demo:options_label'] 			= 'Opzioni';
$lang['features_demo:intro_label'] 			= 'Introduzione';
$lang['features_demo:no_category_select_label'] 		= '-- Nessuna --';
$lang['features_demo:new_category_label'] 		= 'Aggiungi una categoria';
$lang['features_demo:subscripe_to_rss_label'] 		= 'Abbonati all\'RSS';
$lang['features_demo:all_posts_label'] 		= 'Tutti gli articoli';
$lang['features_demo:posts_of_category_suffix'] 	= ' articoli';
$lang['features_demo:rss_name_suffix'] 			= ' Notizie';
$lang['features_demo:rss_category_suffix'] 		= ' Notizie';
$lang['features_demo:author_name_label'] 		= 'Nome autore';
$lang['features_demo:read_more_label'] 			= 'Leggi tutto&nbsp;&raquo;';
$lang['features_demo:created_hour']                  = 'Time (Ora)'; 
$lang['features_demo:created_minute']                = 'Time (Minuto)';
$lang['features_demo:comments_enabled_label']         = 'Commenti abilitati';

// titles
$lang['features_demo:create_title'] 			= 'Aggiungi un articolo';
$lang['features_demo:edit_title'] 			= 'Modifica l\'articolo "%s"';
$lang['features_demo:archive_title'] 			= 'Archivio';
$lang['features_demo:posts_title'] 			= 'Articoli';
$lang['features_demo:rss_posts_title'] 		= 'Notizie per %s';
$lang['features_demo:features_demo_title'] 			= 'Notizie';
$lang['features_demo:list_title'] 			= 'Elenco articoli';

// messages
$lang['features_demo:disabled_after'] 				= 'Non sarà più possibile inserire commenti dopo %s.';
$lang['features_demo:no_posts'] 			= 'Non ci sono articoli.';
$lang['features_demo:subscripe_to_rss_desc'] 		= 'Ricevi gli articoli subito abbonandoti al nostro feed RSS. Lo puoi fare con i comuni programmi di posta elettronica, altrimenti prova <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts'] 		= 'Non ci sono articoli al momento.';
$lang['features_demo:post_add_success'] 		= 'L\'articolo "%s" è stato aggiunto.';
$lang['features_demo:post_add_error'] 		= 'C\'è stato un errore.';
$lang['features_demo:edit_success'] 			= 'L\'articolo "%s" è stato modificato.';
$lang['features_demo:edit_error'] 			= 'C\'è stato un errore.';
$lang['features_demo:publish_success'] 			= 'L\'articolo "%s" è stato pubblicato.';
$lang['features_demo:mass_publish_success'] 		= 'Gli articoli "%s" sono stati pubblicati.';
$lang['features_demo:publish_error'] 			= 'Gli articoli non saranno pubblicati.';
$lang['features_demo:delete_success'] 			= 'L\'articolo "%s" è stato eliminato.';
$lang['features_demo:mass_delete_success'] 		= 'Gli articoli "%s" sono stati eliminati.';
$lang['features_demo:delete_error'] 			= 'Nessun articolo è stato eliminato.';
$lang['features_demo:already_exist_error'] 		= 'Un articolo con questo URL esiste già.';

$lang['features_demo:twitter_posted']			= 'Inviato "%s" %s';
$lang['features_demo:twitter_error'] 			= 'Errore per Twitter';

// date
$lang['features_demo:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation

?>