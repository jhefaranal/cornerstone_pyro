<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Pubblicare nuovi articoli';
$lang['features_demo:role_edit_live']	= 'Modificare articoli pubblicati'; 
$lang['features_demo:role_delete_live'] 	= 'Cancellare articoli pubblicati'; 