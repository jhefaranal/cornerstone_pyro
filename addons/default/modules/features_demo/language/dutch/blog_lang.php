<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label'] 				= 'Geplaatst';
$lang['features_demo:posted_label_alt']			= 'Geplaatst op';
$lang['features_demo:written_by_label']			= 'Geschreven door';
$lang['features_demo:author_unknown']			= 'Onbekend';
$lang['features_demo:keywords_label']			= 'Sleutelwoorden';
$lang['features_demo:tagged_label']				= 'Etiket';
$lang['features_demo:category_label'] 			= 'Categorie';
$lang['features_demo:post_label'] 				= 'Post';
$lang['features_demo:date_label'] 				= 'Datum';
$lang['features_demo:date_at']					= 'op';
$lang['features_demo:time_label'] 				= 'Tijd';
$lang['features_demo:status_label'] 				= 'Status';
$lang['features_demo:draft_label'] 				= 'Concept';
$lang['features_demo:live_label'] 				= 'Live';
$lang['features_demo:content_label'] 			= 'Content';
$lang['features_demo:options_label'] 			= 'Opties';
$lang['features_demo:slug_label'] 				= 'URL';
$lang['features_demo:intro_label'] 				= 'Introductie';
$lang['features_demo:no_category_select_label'] 	= '-- Geen --';
$lang['features_demo:new_category_label'] 		= 'Voeg een categorie toe';
$lang['features_demo:subscripe_to_rss_label'] 	= 'Abonneer op RSS';
$lang['features_demo:all_posts_label'] 			= 'Alle artikelen';
$lang['features_demo:posts_of_category_suffix'] 	= ' artikelen';
$lang['features_demo:rss_name_suffix'] 			= ' Nieuws';
$lang['features_demo:rss_category_suffix'] 		= ' Nieuws';
$lang['features_demo:author_name_label'] 		= 'Auteur naam';
$lang['features_demo:read_more_label'] 			= 'Lees Meer&nbsp;&raquo;';
$lang['features_demo:created_hour']           	= 'Tijd (Uren)';
$lang['features_demo:created_minute']       		= 'Tijd (Minuten)';
$lang['features_demo:comments_enabled_label']	= 'Reacties ingeschakeld';

// titles
$lang['features_demo:disabled_after'] 			= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:create_title'] 				= 'Voeg artikel toe';
$lang['features_demo:edit_title'] 				= 'Wijzig artikel "%s"';
$lang['features_demo:archive_title'] 			= 'Archief';
$lang['features_demo:posts_title'] 				= 'Artikelen';
$lang['features_demo:rss_posts_title'] 			= 'Nieuws artikelen voor %s';
$lang['features_demo:features_demo_title'] 				= 'Nieuws';
$lang['features_demo:list_title'] 				= 'Overzicht artikelen';

// messages
$lang['features_demo:no_posts'] 					= 'Er zijn geen artikelen.';
$lang['features_demo:subscripe_to_rss_desc'] 	= 'Ontvang artikelen meteen door te abonneren op onze RSS feed. U kunt dit doen met de meeste populaire e-mail programma&acute;s, of probeer <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts'] 		= 'Er zijn momenteel geen artikelen.';
$lang['features_demo:post_add_success'] 			= 'Het artikel "%s" is opgeslagen.';
$lang['features_demo:post_add_error'] 			= 'Er is een fout opgetreden.';
$lang['features_demo:edit_success'] 				= 'Het artikel "%s" is opgeslagen.';
$lang['features_demo:edit_error'] 				= 'Er is een fout opgetreden.';
$lang['features_demo:publish_success'] 			= 'Het artikel "%s" is gepubliceerd.';
$lang['features_demo:mass_publish_success'] 		= 'De artikelen "%s" zijn gepubliceerd.';
$lang['features_demo:publish_error'] 			= 'Geen artikelen zijn gepubliceerd.';
$lang['features_demo:delete_success'] 			= 'Het artikel "%s" is verwijderd.';
$lang['features_demo:mass_delete_success'] 		= 'De artikelen "%s" zijn verwijderd.';
$lang['features_demo:delete_error'] 				= 'Geen artikelen zijn verwijderd.';
$lang['features_demo:already_exist_error'] 		= 'Een artikel met deze URL bestaat al.';

$lang['features_demo:twitter_posted']			= 'Geplaatst "%s" %s';
$lang['features_demo:twitter_error'] 			= 'Twitter Fout';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
