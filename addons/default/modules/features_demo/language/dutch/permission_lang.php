<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Zet artikelen online';
$lang['features_demo:role_edit_live']	= 'Bewerk online artikelen';
$lang['features_demo:role_delete_live'] 	= 'Verwijder online artikelen';