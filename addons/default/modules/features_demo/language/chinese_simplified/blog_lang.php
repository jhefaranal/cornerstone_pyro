<?php
/**
 * Chinese Simpplified translation.
 *
 * @author		Kefeng DENG
 * @package		PyroCMS
 * @subpackage 	Features_demo Module
 * @category	Modules
 * @link		http://pyrocms.com
 * @date		2012-06-22
 * @version		1.0
 */
$lang['features_demo:post']                 = '帖子'; #translate
$lang['features_demo:posts']                   = '帖子'; #translate

// labels
$lang['features_demo:posted_label'] 				= '已發佈';
$lang['features_demo:posted_label_alt']			= '發表於';
$lang['features_demo:written_by_label']				= '撰文者';
$lang['features_demo:author_unknown']				= '未知';
$lang['features_demo:keywords_label']				= '關鍵字';
$lang['features_demo:tagged_label']					= '標籤';
$lang['features_demo:category_label'] 			= '分類';
$lang['features_demo:post_label'] 				= '發表';
$lang['features_demo:date_label'] 				= '日期';
$lang['features_demo:date_at']					= '於';
$lang['features_demo:time_label'] 				= '時間';
$lang['features_demo:status_label'] 				= '狀態';
$lang['features_demo:draft_label'] 				= '草稿';
$lang['features_demo:live_label'] 				= '上線';
$lang['features_demo:content_label'] 			= '內容';
$lang['features_demo:options_label'] 			= '選項';
$lang['features_demo:intro_label'] 				= '簡介';
$lang['features_demo:no_category_select_label'] 	= '-- 無 --';
$lang['features_demo:new_category_label'] 		= '新增分類';
$lang['features_demo:subscripe_to_rss_label'] 	= '訂閱 RSS';
$lang['features_demo:all_posts_label'] 		= '所有文章';
$lang['features_demo:posts_of_category_suffix'] 		= ' 文章';
$lang['features_demo:rss_name_suffix'] 					= ' 新聞';
$lang['features_demo:rss_category_suffix'] 				= ' 新聞';
$lang['features_demo:author_name_label'] 				= '作者名稱';
$lang['features_demo:read_more_label'] 					= '閱讀更多&nbsp;&raquo;';
$lang['features_demo:created_hour']                  = '時間 (時)';
$lang['features_demo:created_minute']                = '時間 (分)';
$lang['features_demo:comments_enabled_label']         = '允許回應';

// titles
$lang['features_demo:create_title'] 				= '新增文章';
$lang['features_demo:edit_title'] 				= '編輯文章 "%s"';
$lang['features_demo:archive_title'] 			= '檔案櫃';
$lang['features_demo:posts_title'] 			= '文章';
$lang['features_demo:rss_posts_title'] 		= '%s 的最新文章';
$lang['features_demo:features_demo_title'] 				= '新聞';
$lang['features_demo:list_title'] 				= '文章列表';

// messages
$lang['features_demo:disabled_after'] 				= '在%s之后，回复功能将会被关闭。'; #translate
$lang['features_demo:no_posts'] 				= '沒有文章';
$lang['features_demo:subscripe_to_rss_desc'] 	= '訂閱我們的 RSS 摘要可立即獲得最新的文章，您可以使用慣用的收件軟體，或試試看 <a href="http://reader.google.com.tw">Google 閱讀器</a>。';
$lang['features_demo:currently_no_posts'] 	= '目前沒有文章';
$lang['features_demo:post_add_success'] 		= '文章 "%s" 已經新增';
$lang['features_demo:post_add_error'] 		= '發生了錯誤';
$lang['features_demo:edit_success'] 				= '這文章 "%s" 更新了。';
$lang['features_demo:edit_error'] 				= '發生了錯誤';
$lang['features_demo:publish_success'] 			= '此文章 "%s" 已經被發佈。';
$lang['features_demo:mass_publish_success'] 		= '這些文章 "%s" 已經被發佈。';
$lang['features_demo:publish_error'] 			= '沒有文章被發佈。';
$lang['features_demo:delete_success'] 			= '此文章 "%s" 已經被刪除。';
$lang['features_demo:mass_delete_success'] 		= '這些文章 "%s" 已經被刪除。';
$lang['features_demo:delete_error'] 				= '沒有文章被刪除。';
$lang['features_demo:already_exist_error'] 		= '一則相同網址的文章已經存在。';

$lang['features_demo:twitter_posted']			= '發佈 "%s" %s';
$lang['features_demo:twitter_error'] 			= 'Twitter 錯誤';

// date
$lang['features_demo:archive_date_format']		= "%B' %Y"; #translate format - see php strftime documentation