<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= '將文章上線';
$lang['features_demo:role_edit_live']	= '編輯上線文章';
$lang['features_demo:role_delete_live'] 	= '刪除上線文章';