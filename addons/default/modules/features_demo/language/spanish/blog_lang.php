<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                          = 'Entrada';
$lang['features_demo:posts']                         = 'Entradas';

// labels
$lang['features_demo:posted_label']					= 'Escrito';
$lang['features_demo:keywords_label']				= 'Palabras clave';
$lang['features_demo:tagged_label']					= 'Tagged'; #translate
$lang['features_demo:category_label'] 				= 'Categoría';
$lang['features_demo:written_by_label']				= 'Escrito por';
$lang['features_demo:author_unknown']				= 'Desconocido';
$lang['features_demo:post_label'] 					= 'Entrada';
$lang['features_demo:date_label'] 					= 'Fecha';
$lang['features_demo:time_label'] 					= 'Hora';
$lang['features_demo:status_label'] 					= 'Estado';
$lang['features_demo:content_label'] 				= 'Contenido';
$lang['features_demo:options_label'] 				= 'Opciones';
$lang['features_demo:intro_label'] 					= 'Introducción';
$lang['features_demo:draft_label'] 					= 'Borrador';
$lang['features_demo:live_label'] 					= 'En vivo';
$lang['features_demo:no_category_select_label'] 		= '-- Ninguna --';
$lang['features_demo:new_category_label'] 			= 'Agregar una categoría';
$lang['features_demo:subscripe_to_rss_label'] 		= 'Suscribir al RSS';
$lang['features_demo:all_posts_label'] 				= 'Todos los artículos';
$lang['features_demo:posts_of_category_suffix']		= ' artículos';
$lang['features_demo:rss_name_suffix'] 				= ' Noticias';
$lang['features_demo:rss_category_suffix'] 			= ' Noticias';
$lang['features_demo:posted_label'] 					= 'Escrito en';
$lang['features_demo:author_name_label'] 			= 'Nombre del autor';
$lang['features_demo:read_more_label'] 				= 'Leer más &raquo;';
$lang['features_demo:created_hour']                 	= 'Hora (Hora)';
$lang['features_demo:created_minute']				= 'Hora (Minutos)';
$lang['features_demo:comments_enabled_label']         = 'Comentarios Habilitados';

// titles
$lang['features_demo:create_title'] 					= 'Crear un artículo';
$lang['features_demo:edit_title'] 					= 'Editar artículo "%s"';
$lang['features_demo:archive_title'] 				= 'Archivo';
$lang['features_demo:posts_title'] 					= 'Artículos';
$lang['features_demo:rss_posts_title'] 				= 'Artículo Features_demo de %s';
$lang['features_demo:features_demo_title'] 					= 'Noticias';
$lang['features_demo:list_title'] 					= 'Lista de artículos';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts'] 						= 'No hay artículos.';
$lang['features_demo:subscripe_to_rss_desc'] 		= 'Recibe nuestros artículos inmediatamente suscribiendote a nuestros feeds RSS. Puedes hacer esto utilizando los más clientes de correo electrónico mas populares o utilizando <a href="http://reader.google.com/">Google Reader</a>.';
$lang['features_demo:currently_no_posts'] 			= 'No hay artículos hasta el momento.';
$lang['features_demo:post_add_success'] 				= 'El artículo "%s" fue agregado.';
$lang['features_demo:post_add_error'] 				= 'Ha ocurrido un error.';
$lang['features_demo:edit_success'] 					= 'El artículo "%s" fue actualizado.';
$lang['features_demo:edit_error'] 					= 'Ha ocurrido un error.';
$lang['features_demo:publish_success'] 				= 'El artículo "%s" ha sido publicado.';
$lang['features_demo:mass_publish_success'] 			= 'Los artículos "%s" fueron publicados.';
$lang['features_demo:publish_error'] 				= 'No se han publicado artículos.';
$lang['features_demo:delete_success'] 				= 'El artículo "%s" ha sido eliminado.';
$lang['features_demo:mass_delete_success'] 			= 'Los artículos "%s" han sido eliminados.';
$lang['features_demo:delete_error'] 					= 'No se han eliminado artículos.';
$lang['features_demo:already_exist_error'] 			= 'Ya existe un artículo con esta URL.';

$lang['features_demo:twitter_posted']				= 'Escrito "%s" %s';
$lang['features_demo:twitter_error'] 				= 'Error de Twitter';

// date
$lang['features_demo:archive_date_format']			= "%B %Y";

/* End of file features_demo:lang.php */