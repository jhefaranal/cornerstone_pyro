<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Publikování příspěvků';
$lang['features_demo:role_edit_live']	= 'Úpravy publikovaných příspěvků';
$lang['features_demo:role_delete_live'] 	= 'Mazání publikovaných příspěvků';