<?php

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label'] 			= 'Odesláno';
$lang['features_demo:posted_label_alt']			= 'Odesláno';
$lang['features_demo:written_by_label']				= 'Written by'; #translate
$lang['features_demo:author_unknown']				= 'Unknown'; #translate
$lang['features_demo:keywords_label']				= 'Keywords'; #translate
$lang['features_demo:tagged_label']					= 'Tagged'; #translate
$lang['features_demo:category_label'] 			= 'Kategorie';
$lang['features_demo:post_label'] 			= 'Odeslat';
$lang['features_demo:date_label'] 			= 'Datum';
$lang['features_demo:date_at']				= 'v';
$lang['features_demo:time_label'] 			= 'Čas';
$lang['features_demo:status_label'] 			= 'Stav';
$lang['features_demo:draft_label'] 			= 'Koncept';
$lang['features_demo:live_label'] 			= 'Publikováno';
$lang['features_demo:content_label'] 			= 'Obsah';
$lang['features_demo:options_label'] 			= 'Možnosti';
$lang['features_demo:intro_label'] 			= 'Úvod';
$lang['features_demo:no_category_select_label'] 		= '-- Nic --';
$lang['features_demo:new_category_label'] 		= 'Přidat kategorii';
$lang['features_demo:subscripe_to_rss_label'] 		= 'Přihlásit se k odběru RSS';
$lang['features_demo:all_posts_label'] 		= 'Všechny články';
$lang['features_demo:posts_of_category_suffix'] 	= ' články';
$lang['features_demo:rss_name_suffix'] 			= ' Novinky';
$lang['features_demo:rss_category_suffix'] 		= ' Novinky';
$lang['features_demo:author_name_label'] 		= 'Autor';
$lang['features_demo:read_more_label'] 			= 'Čtěte dále';
$lang['features_demo:created_hour']                      = 'Vytvořeno v hodině';
$lang['features_demo:created_minute']                    = 'Vytvořeno v minutě';
$lang['features_demo:comments_enabled_label']         = 'Komentáře povoleny';

// titles
$lang['features_demo:create_title'] 			= 'Přidat článek';
$lang['features_demo:edit_title'] 			= 'Upravit článek "%s"';
$lang['features_demo:archive_title'] 			= 'Archiv';
$lang['features_demo:posts_title'] 			= 'Články';
$lang['features_demo:rss_posts_title'] 		= 'Novinky pro %s';
$lang['features_demo:features_demo_title'] 			= 'Novinky';
$lang['features_demo:list_title'] 			= 'Seznam článků';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts'] 			= 'Nejsou zde žádné články.';
$lang['features_demo:subscripe_to_rss_desc'] 		= 'Dostávejte články rovnou pomocí RSS. Můžete použít populární e-mailové klienty nebo zkuste <a href="http://reader.google.cz/">Google Reader</a>.';
$lang['features_demo:currently_no_posts'] 		= 'Nejsou zde žádné články.';
$lang['features_demo:post_add_success'] 		= 'Článek "%s" byl přidán.';
$lang['features_demo:post_add_error'] 		= 'Objevila se chyba.';
$lang['features_demo:edit_success'] 			= 'Článek "%s" byl aktualizován.';
$lang['features_demo:edit_error'] 			= 'Objevila se chyba.';
$lang['features_demo:publish_success'] 			= 'Článek "%s" byl publikován.';
$lang['features_demo:mass_publish_success'] 		= 'Články "%s" byly publikovány.';
$lang['features_demo:publish_error'] 			= 'žádné články nebyly publikovány.';
$lang['features_demo:delete_success'] 			= 'Článek "%s" byl smazán.';
$lang['features_demo:mass_delete_success'] 		= 'Články "%s" byly smazány.';
$lang['features_demo:delete_error'] 			= 'Žádné články nebyly smazány.';
$lang['features_demo:already_exist_error'] 		= 'Článek s touto adresou URL již existuje.';

$lang['features_demo:twitter_posted']			= 'Publikováno "%s" %s';
$lang['features_demo:twitter_error'] 			= 'Chyba Twitteru';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";

?>