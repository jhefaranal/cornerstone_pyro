<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Finnish translation.
 *
 * @author Mikael Kundert
 */

$lang['features_demo:post']	= 'Artikkeli';
$lang['features_demo:posts']	= 'Artikkelit';

// labels
$lang['features_demo:posted_label']				= 'Lähetetty';
$lang['features_demo:posted_label_alt']			= 'Lähetettiin';
$lang['features_demo:written_by_label']			= 'Kirjoittanut';
$lang['features_demo:author_unknown']			= 'Tuntematon';
$lang['features_demo:keywords_label']			= 'Avainsanat';
$lang['features_demo:tagged_label']				= 'Merkitty';
$lang['features_demo:category_label']			= 'Kategoria';
$lang['features_demo:post_label']				= 'Artikkeli';
$lang['features_demo:date_label']				= 'Päivä';
$lang['features_demo:date_at']					= 'at'; #translate (in finnish we use adessives by using suffixes!, see http://www.cs.tut.fi/~jkorpela/finnish-cases.html)
$lang['features_demo:time_label']				= 'Aika';
$lang['features_demo:status_label']				= 'Tila';
$lang['features_demo:draft_label']				= 'Luonnos';
$lang['features_demo:live_label']				= 'Julkinen';
$lang['features_demo:content_label']				= 'Sisältö';
$lang['features_demo:options_label']				= 'Valinnat';
$lang['features_demo:intro_label']				= 'Alkuteksti';
$lang['features_demo:no_category_select_label']	= '-- Ei mikään --';
$lang['features_demo:new_category_label']		= 'Luo kategoria';
$lang['features_demo:subscripe_to_rss_label']	= 'Tilaa RSS';
$lang['features_demo:all_posts_label']			= 'Kaikki artikkelit';
$lang['features_demo:posts_of_category_suffix']	= ' artikkelia';
$lang['features_demo:rss_name_suffix']			= ' Uutiset';
$lang['features_demo:rss_category_suffix']		= ' Uutiset';
$lang['features_demo:author_name_label']			= 'Tekijän nimi';
$lang['features_demo:read_more_label']			= 'Lue lisää&nbsp;&raquo;';
$lang['features_demo:created_hour']				= 'Luotiin tunnissa';
$lang['features_demo:created_minute']			= 'Luotiin minuutissa';
$lang['features_demo:comments_enabled_label']	= 'Kommentit päällä';

// titles
$lang['features_demo:disabled_after']	= 'Kommentointi on otettu pois käytöstä %s jälkeen.';
$lang['features_demo:create_title']		= 'Luo artikkeli';
$lang['features_demo:edit_title']		= 'Muokkaa artikkelia "%s"';
$lang['features_demo:archive_title']		= 'Arkisto';
$lang['features_demo:posts_title']		= 'Artikkelit';
$lang['features_demo:rss_posts_title']	= 'Uutisartikkeleita %s';
$lang['features_demo:features_demo_title']		= 'Uutiset';
$lang['features_demo:list_title']		= 'Listaa artikkelit';

// messages
$lang['features_demo:no_posts']				= 'Artikkeleita ei ole.';
$lang['features_demo:subscripe_to_rss_desc']	= 'Lue artikkelit tilaamalla RSS syöte. Suosituimmat sähköposti ohjelmat tukevat tätä. Voit myös vaihtoehtoisesti kokeilla <a href="http://reader.google.co.uk/">Google Readeria</a>.';
$lang['features_demo:currently_no_posts']	= 'Ei artikkeleita tällä hetkellä.';
$lang['features_demo:post_add_success']		= 'Artikkeli "%s" lisättiin.';
$lang['features_demo:post_add_error']		= 'Tapahtui virhe.';
$lang['features_demo:edit_success']			= 'Artikkeli "%s" päivitettiin.';
$lang['features_demo:edit_error']			= 'Tapahtui virhe.';
$lang['features_demo:publish_success']		= 'Artikkeli "%s" julkaistiin.';
$lang['features_demo:mass_publish_success']	= 'Artikkelit "%s" julkaistiin.';
$lang['features_demo:publish_error']			= 'Yhtään artikkelia ei julkaistu.';
$lang['features_demo:delete_success']		= 'Artikkeli "%s" poistettiin.';
$lang['features_demo:mass_delete_success']	= 'Artikkelit "%s" poistettiin.';
$lang['features_demo:delete_error']			= 'Yhtään artikkelia ei poistettu.';
$lang['features_demo:already_exist_error']	= 'Artikkelin URL osoite on jo käytössä.';

$lang['features_demo:twitter_posted']	= 'Lähetetty "%s" %s';
$lang['features_demo:twitter_error']		= 'Twitter Virhe';

// date
$lang['features_demo:archive_date_format'] = "%B, %Y";

?>