<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Artikkelien julkaiseminen';
$lang['features_demo:role_edit_live']	= 'Julkaistujen artikkeleiden muokkaaminen';
$lang['features_demo:role_delete_live'] 	= 'Julkaistujen artikkeleiden poistaminen';
