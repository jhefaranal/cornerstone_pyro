<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'تغییر یک پست به حالت منتشر شده';
$lang['features_demo:role_edit_live']        = 'ویرایش پست های منتظر شده';
$lang['features_demo:role_delete_live'] 	= 'حذف پست های منتشر شده';