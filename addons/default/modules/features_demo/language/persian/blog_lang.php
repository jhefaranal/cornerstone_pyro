<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                          = 'پست';
$lang['features_demo:posts']                         = 'پست ها';

// labels
$lang['features_demo:posted_label']                  = 'ارسال شده';
$lang['features_demo:posted_label_alt']               = 'ارسال شده';
$lang['features_demo:written_by_label']		 = 'نوشته شده توسط';
$lang['features_demo:author_unknown']	  	 = 'نامشخص';
$lang['features_demo:keywords_label']		 = 'کلمات کلیدی';
$lang['features_demo:tagged_label']			 = 'تگ ها';
$lang['features_demo:category_label']                = 'مجموعه';
$lang['features_demo:post_label']                    = 'پست';
$lang['features_demo:date_label']                    = 'تاریخ';
$lang['features_demo:date_at']                       = 'در';
$lang['features_demo:time_label']                    = 'زمان';
$lang['features_demo:status_label']                  = 'وضعیت';
$lang['features_demo:draft_label']                    = 'پیشنویس';
$lang['features_demo:live_label']                     = 'ارسال شده';
$lang['features_demo:content_label']                  = 'محتوا';
$lang['features_demo:options_label']                  = 'تنظیمات';
$lang['features_demo:intro_label']                    = 'چکیده';
$lang['features_demo:no_category_select_label']       = '-- هیچیک --';
$lang['features_demo:new_category_label']             = 'مجموعه جدید';
$lang['features_demo:subscripe_to_rss_label']         = 'عضویت در RSS';
$lang['features_demo:all_posts_label']                = 'همه ی  پست ها';
$lang['features_demo:posts_of_category_suffix']       = ' پست ها';
$lang['features_demo:rss_name_suffix']                = ' بلاگ';
$lang['features_demo:rss_category_suffix']            = ' بلاگ';
$lang['features_demo:author_name_label']              = 'نام نویسنده';
$lang['features_demo:read_more_label']                = 'مشاهده جزئیات';
$lang['features_demo:created_hour']                   = 'ایجاد شده در ساعت';
$lang['features_demo:created_minute']                 = 'ایجاد شده در دقیقه ی ';
$lang['features_demo:comments_enabled_label']         = 'فعال کردن کامنت ها';

// titles
$lang['features_demo:create_title']                   = 'پست جدید';
$lang['features_demo:edit_title']                     = 'ویرایش پست "%s"';
$lang['features_demo:archive_title']                 = 'آرشیو';
$lang['features_demo:posts_title']					= 'پست ها';
$lang['features_demo:rss_posts_title']				= 'ارسال های مربوط به %s';
$lang['features_demo:features_demo_title']					= 'بلاگ';
$lang['features_demo:list_title']					= 'لیست پست ها';

// messages
$lang['features_demo:disabled_after'] 		= 'ارسال نظرات پس از %s غیر فعال شده است.';
$lang['features_demo:no_posts']                      = 'هیچ پستی وجود ندارد';
$lang['features_demo:subscripe_to_rss_desc']          = 'مشترک خبرخوانRSS ما بشوید.';
$lang['features_demo:currently_no_posts']          = 'در حال حاضر هیچ پستی وجود ندارد.';
$lang['features_demo:post_add_success']            = '"%s" اضافه شد.';
$lang['features_demo:post_add_error']              = 'خطایی رخ داده است.';
$lang['features_demo:edit_success']                   = '"%s" آپدیت شد.';
$lang['features_demo:edit_error']                     = 'خطایی رخ داده است.';
$lang['features_demo:publish_success']                = 'پست "%s" منتشر شد.';
$lang['features_demo:mass_publish_success']           = 'پست های "%s" منتشر شدند.';
$lang['features_demo:publish_error']                  = 'هیچ پستی منتشر نشد';
$lang['features_demo:delete_success']                 = 'پست "%s" دلیت شد.';
$lang['features_demo:mass_delete_success']            = 'پست های "%s" دلیت شدند.';
$lang['features_demo:delete_error']                   = 'هیچ پستی دلیت نشد.';
$lang['features_demo:already_exist_error']            = 'همینک  یک پست با این URL موجود است.';

$lang['features_demo:twitter_posted']                 = 'پست شد "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter خطای';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
