<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Publikuj wpisy';
$lang['features_demo:role_edit_live']		= 'Edytuj opublikowane wpisy';
$lang['features_demo:role_delete_live'] 		= 'Usuwaj opublikowane wpisy';