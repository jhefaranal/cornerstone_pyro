<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label'] 					= 'Opublikowano w';
$lang['features_demo:posted_label_alt']					= 'Opublikowano ';
$lang['features_demo:written_by_label']					= 'Napisany przez';
$lang['features_demo:author_unknown']					= 'Nieznany';
$lang['features_demo:keywords_label']					= 'Słowa kluczowe';
$lang['features_demo:tagged_label']					= 'Tagi';
$lang['features_demo:category_label'] 					= 'Kategoria';
$lang['features_demo:post_label'] 					= 'Post';
$lang['features_demo:date_label'] 					= 'Data';
$lang['features_demo:date_at']						= '';
$lang['features_demo:time_label'] 					= 'Czas';
$lang['features_demo:status_label'] 					= 'Status';
$lang['features_demo:draft_label'] 					= 'Robocza';
$lang['features_demo:live_label'] 					= 'Opublikowana';
$lang['features_demo:content_label'] 					= 'Zawartość';
$lang['features_demo:options_label'] 					= 'Opcje';
$lang['features_demo:intro_label'] 					= 'Wprowadzenie';
$lang['features_demo:no_category_select_label'] 				= '-- Brak --';
$lang['features_demo:new_category_label'] 				= 'Dodaj kategorię';
$lang['features_demo:subscripe_to_rss_label'] 				= 'Subskrybuj RSS';
$lang['features_demo:all_posts_label'] 					= 'Wszystkie wpisy';
$lang['features_demo:posts_of_category_suffix'] 				= ' - wpisy';
$lang['features_demo:rss_name_suffix'] 					= '';
$lang['features_demo:rss_category_suffix'] 				= '';
$lang['features_demo:author_name_label'] 				= 'Imię autora';
$lang['features_demo:read_more_label'] 					= 'Czytaj więcej&nbsp;&raquo;';
$lang['features_demo:created_hour']               		    	= 'Czas (Godzina)';
$lang['features_demo:created_minute']              		    	= 'Czas (Minuta)';
$lang['features_demo:comments_enabled_label']         			= 'Komentarze aktywne';

// titles
$lang['features_demo:create_title'] 					= 'Dodaj wpis';
$lang['features_demo:edit_title'] 					= 'Edytuj wpis "%s"';
$lang['features_demo:archive_title'] 					= 'Archiwum';
$lang['features_demo:posts_title'] 					= 'Wpisy';
$lang['features_demo:rss_posts_title'] 					= 'Najnowsze wpisy dla %s';
$lang['features_demo:features_demo_title'] 					= 'Features_demo';
$lang['features_demo:list_title'] 					= 'Lista wpisów';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts'] 						= 'Nie ma żadnych wpisów.';
$lang['features_demo:subscripe_to_rss_desc'] 				= 'Bądź na bieżąco subskrybując nasz kanał RSS. Możesz to zrobić za pomocą popularnych klientów pocztowych, lub skorzystać z <a href="http://reader.google.com/">Google Reader\'a</a>.';
$lang['features_demo:currently_no_posts'] 				= 'W tym momencie nie ma żadnych wpisów.';
$lang['features_demo:post_add_success'] 					= 'Wpis "%s" został dodany.';
$lang['features_demo:post_add_error'] 					= 'Wystąpił błąd.';
$lang['features_demo:edit_success'] 					= 'Wpis "%s" został zaktualizowany.';
$lang['features_demo:edit_error'] 					= 'Wystąpił błąd.';
$lang['features_demo:publish_success'] 					= 'Wpis "%s" został opublikowany.';
$lang['features_demo:mass_publish_success'] 				= 'Wpisy "%s" zostały opublikowane.';
$lang['features_demo:publish_error'] 					= 'Żadne wpisy nie zostały opublikowane.';
$lang['features_demo:delete_success'] 					= 'Wpis "%s" został usunięty.';
$lang['features_demo:mass_delete_success'] 				= 'Wpisy "%s" zostały usunięte.';
$lang['features_demo:delete_error'] 					= 'Żadne wpisy nie zostały usunięte.';
$lang['features_demo:already_exist_error'] 				= 'Wpis z tym adresem URL już istnieje.';

$lang['features_demo:twitter_posted'] 					= 'Opublikowano "%s" %s';
$lang['features_demo:twitter_error'] 					= 'Błąd Twitter\'a';

// date
$lang['features_demo:archive_date_format']				= "%B %Y";