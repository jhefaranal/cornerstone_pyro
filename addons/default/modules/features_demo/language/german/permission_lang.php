<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Artikel live setzen';
$lang['features_demo:role_edit_live']	= 'Live-Artikel bearbeiten';
$lang['features_demo:role_delete_live'] 	= 'Live-Artikel l&ouml;schen';

/* End of file permission_lang.php */