<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Artikel';
$lang['features_demo:posts']                   = 'Artikel';

// labels
$lang['features_demo:posted_label']                   = 'Ver&ouml;ffentlicht';
$lang['features_demo:postet_label_alt']               = 'Ver&ouml;ffentlicht in';
$lang['features_demo:written_by_label']				 = 'Geschrieben von';
$lang['features_demo:author_unknown']				 = 'Unbekannt';
$lang['features_demo:keywords_label']				 = 'Stichw&ouml;rter';
$lang['features_demo:tagged_label']					 = 'Gekennzeichnet';
$lang['features_demo:category_label']                 = 'Kategorie';
$lang['features_demo:post_label']                     = 'Artikel';
$lang['features_demo:date_label']                     = 'Datum';
$lang['features_demo:date_at']                        = 'um';
$lang['features_demo:time_label']                     = 'Uhrzeit';
$lang['features_demo:status_label']                   = 'Status';
$lang['features_demo:draft_label']                    = 'Unver&ouml;ffentlicht';
$lang['features_demo:live_label']                     = 'Ver&ouml;ffentlicht';
$lang['features_demo:content_label']                  = 'Inhalt';
$lang['features_demo:options_label']                  = 'Optionen';
$lang['features_demo:intro_label']                    = 'Einf&uuml;hrung';
$lang['features_demo:no_category_select_label']       = '-- Kein Label --';
$lang['features_demo:new_category_label']             = 'Kategorie hinzuf&uuml;gen';
$lang['features_demo:subscripe_to_rss_label']         = 'RSS abonnieren';
$lang['features_demo:all_posts_label']                = 'Alle Artikel';
$lang['features_demo:posts_of_category_suffix']       = ' Artikel';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Autor';
$lang['features_demo:read_more_label']                = 'Mehr lesen&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Erstellt zur Stunde';
$lang['features_demo:created_minute']                 = 'Erstellt zur Minute';
$lang['features_demo:comments_enabled_label']         = 'Kommentare aktiv';

// titles
$lang['features_demo:create_title']                   = 'Artikel erstellen';
$lang['features_demo:edit_title']                     = 'Artikel "%s" bearbeiten';
$lang['features_demo:archive_title']                  = 'Archiv';
$lang['features_demo:posts_title']                    = 'Artikel';
$lang['features_demo:rss_posts_title']                = 'Features_demo Artikel f&uuml;r %s';
$lang['features_demo:features_demo_title']                     = 'Features_demo';
$lang['features_demo:list_title']                     = 'Artikel auflisten';

// messages
$lang['features_demo:disabled_after'] 				= 'Kommentare nach %s wurden deaktiviert.';
$lang['features_demo:no_posts']                       = 'Es existieren keine Artikel.';
$lang['features_demo:subscripe_to_rss_desc']          = 'Abonnieren Sie unseren RSS Feed und erhalten Sie alle Artikel frei Haus. Sie k&ouml;nnen dies mit den meisten Email-Clients tun, oder z.B. mit <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']             = 'Es existieren zur Zeit keine Artikel.';
$lang['features_demo:post_add_success']               = 'Der Artikel "%s" wurde hinzugef&uuml;gt.';
$lang['features_demo:post_add_error']                 = 'Ein Fehler ist aufgetreten.';
$lang['features_demo:edit_success']                   = 'Der Artikel "%s" wurde aktualisiert.';
$lang['features_demo:edit_error']                     = 'Ein Fehler ist aufgetreten.';
$lang['features_demo:publish_success']                = 'Der Artikel "%s" wurde ver&ouml;ffentlicht.';
$lang['features_demo:mass_publish_success']           = 'Die Artikel "%s" wurden ver&ouml;ffentlicht.';
$lang['features_demo:publish_error']                  = 'Ein Fehler ist aufgetreten. Es wurden keine Artikel ver&ouml;ffentlicht.';
$lang['features_demo:delete_success']                 = 'Der Artikel "%s" wurde gel&ouml;scht.';
$lang['features_demo:mass_delete_success']            = 'Die Artikel "%s" wurden gel&ouml;scht.';
$lang['features_demo:delete_error']                   = 'Ein Fehler ist aufgetreten. Keine Artikel wurden gel&ouml;scht.';
$lang['features_demo:already_exist_error']            = 'Ein Artikel mit dieser URL existiert bereits.';

$lang['features_demo:twitter_posted']                 = 'Gepostet "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter Fehler';

// date
$lang['features_demo:archive_date_format']		     = "%B %Y";
