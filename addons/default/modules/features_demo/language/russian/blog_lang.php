<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * PyroCMS
 * Русский перевод от Dark Preacher - dark[at]darklab.ru
 *
 * @package		PyroCMS
 * @author		Dark Preacher
 * @link			http://pyrocms.com
 */

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// подписи
$lang['features_demo:posted_label']									= 'Дата';
$lang['features_demo:posted_label_alt']							= 'Дата добавления';
$lang['features_demo:written_by_label']							= 'Автор';
$lang['features_demo:author_unknown']								= 'Неизвестно';
$lang['features_demo:keywords_label']				= 'Keywords'; #translate
$lang['features_demo:tagged_label']					= 'Tagged'; #translate
$lang['features_demo:category_label']								= 'Категория';
$lang['features_demo:post_label']										= 'Заголовок';
$lang['features_demo:date_label']										= 'Дата';
$lang['features_demo:date_at']												= 'в';
$lang['features_demo:time_label']										= 'Время';
$lang['features_demo:status_label']									= 'Статус';
$lang['features_demo:draft_label']										= 'Черновик';
$lang['features_demo:live_label']										= 'Опубликовано';
$lang['features_demo:content_label']									= 'Содержание';
$lang['features_demo:options_label']									= 'Опции';
$lang['features_demo:intro_label']										= 'Анонс';
$lang['features_demo:no_category_select_label']			= '-- нет --';
$lang['features_demo:new_category_label']						= 'Создать категорию';
$lang['features_demo:subscripe_to_rss_label']				= 'Подписаться на RSS';
$lang['features_demo:all_posts_label']								= 'Все статьи';
$lang['features_demo:posts_of_category_suffix']			= ' статьи';
$lang['features_demo:rss_name_suffix']								= ' Блог';
$lang['features_demo:rss_category_suffix']						= ' Блог';
$lang['features_demo:author_name_label']							= 'Автор';
$lang['features_demo:read_more_label']								= 'читать целиком&nbsp;&raquo;';
$lang['features_demo:created_hour']									= 'Время (Час)';
$lang['features_demo:created_minute']								= 'Время (Минута)';
$lang['features_demo:comments_enabled_label']				= 'Комментарии разрешены';

// заголовки
$lang['features_demo:create_title']									= 'Создать статью';
$lang['features_demo:edit_title']										= 'Редактирование статьи "%s"';
$lang['features_demo:archive_title']									= 'Архив';
$lang['features_demo:posts_title']										= 'Статьи';
$lang['features_demo:rss_posts_title']								= 'Статьи из %s';
$lang['features_demo:features_demo_title']										= 'Блог';
$lang['features_demo:list_title']										= 'Список статей';

// сообщения
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']											= 'Статьи отсутствуют.';
$lang['features_demo:subscripe_to_rss_desc']					= 'Получайте статьи сразу после их публикации, подпишитесь на нашу ленту новостей. Вы можете сделать это с помощью самых популярных программ для чтения электронных писем, или попробуйте <a href="http://reader.google.ru/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']						= 'В данный момент новости отсутствуют.';
$lang['features_demo:post_add_success']							= 'Статья "%s" добавлена.';
$lang['features_demo:post_add_error']								= 'Во время добавления статьи произошла ошибка.';
$lang['features_demo:edit_success']									= 'Статья "%s" сохранена.';
$lang['features_demo:edit_error']										= 'Во время сохранения статьи произошла ошибка.';
$lang['features_demo:publish_success']								= 'Статья "%s" опубликована.';
$lang['features_demo:mass_publish_success']					= 'Статьи "%s" опубликованы.';
$lang['features_demo:publish_error']									= 'Во время публикации статьи произошла ошибка.';
$lang['features_demo:delete_success']								= 'Статья "%s" удалена.';
$lang['features_demo:mass_delete_success']						= 'Статьи "%s" удалены.';
$lang['features_demo:delete_error']									= 'Во время удаления статьи произошла ошибка.';
$lang['features_demo:already_exist_error']						= 'Статья с данным адресом URL уже существует.';

$lang['features_demo:twitter_posted']								= 'Добавлен "%s" %s';
$lang['features_demo:twitter_error']									= 'Ошибка Twitter\'а';

// дата
$lang['features_demo:archive_date_format']						= "%B %Y"; #see php strftime documentation

/* End of file features_demo_lang.php */