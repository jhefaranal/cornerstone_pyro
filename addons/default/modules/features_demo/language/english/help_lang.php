<?php defined('BASEPATH') OR exit('No direct script access allowed');

// inline help html. Only 'help_body' is used.
$lang['help_body'] = "

<h4>Overview</h4>
<p>
	The Features_demo module is a simple tool for publishing features_demo entries.
</p>

<h4>Categories</h4>
<p>
	You may create as many categories as you like to organize your posts. If you would like your visitors to
	be able to browse by category simply embed the Features_demo Categories widget on the front-end.
</p>

<h4>Posts</h4>
<p>
	Choose a good title for your posts as they will be displayed on the main Features_demo page (along with the introduction)
	and will also be used as the title in search engine results. After creating a post you may either save it as Live to publish it or
	you may save it as a Draft if you want to come back and edit it later. You may also save it as Live but set the date
	in the future and your post will not show until that date is reached.
</p>

";