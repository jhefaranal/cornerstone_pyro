<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Put articles live';
$lang['features_demo:role_edit_live']	= 'Edit live articles';
$lang['features_demo:role_delete_live'] 	= 'Delete live articles';