<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post';
$lang['features_demo:posts']                   = 'Posts';

// labels
$lang['features_demo:posted_label']                   = 'Posted';
$lang['features_demo:posted_label_alt']               = 'Posted at';
$lang['features_demo:written_by_label']				= 'Written by';
$lang['features_demo:author_unknown']				= 'Unknown';
$lang['features_demo:keywords_label']				= 'Keywords';
$lang['features_demo:tagged_label']					= 'Tagged';
$lang['features_demo:category_label']                 = 'Category';
$lang['features_demo:post_label']                     = 'Post';
$lang['features_demo:date_label']                     = 'Date';
$lang['features_demo:date_at']                        = 'at';
$lang['features_demo:time_label']                     = 'Time';
$lang['features_demo:status_label']                   = 'Status';
$lang['features_demo:draft_label']                    = 'Draft';
$lang['features_demo:live_label']                     = 'Live';
$lang['features_demo:content_label']                  = 'Content';
$lang['features_demo:options_label']                  = 'Options';
$lang['features_demo:intro_label']                    = 'Introduction';
$lang['features_demo:no_category_select_label']       = '-- None --';
$lang['features_demo:new_category_label']             = 'Add a category';
$lang['features_demo:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['features_demo:all_posts_label']                = 'All posts';
$lang['features_demo:posts_of_category_suffix']       = ' posts';
$lang['features_demo:rss_name_suffix']                = ' Features Demo';
$lang['features_demo:rss_category_suffix']            = ' Features Demo';
$lang['features_demo:author_name_label']              = 'Author name';
$lang['features_demo:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Created on Hour';
$lang['features_demo:created_minute']                 = 'Created on Minute';
$lang['features_demo:comments_enabled_label']         = 'Comments Enabled';

// titles
$lang['features_demo:create_title']                   = 'Add Features Demo';
$lang['features_demo:edit_title']                     = 'Edit Features Demo "%s"';
$lang['features_demo:archive_title']                 = 'Archive';
$lang['features_demo:posts_title']					= 'Posts';
$lang['features_demo:rss_posts_title']				= 'Features Demo posts for %s';
$lang['features_demo:features_demo_title']					= 'Features Demos';
$lang['features_demo:list_title']					= 'List Posts';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.';
$lang['features_demo:no_posts']                      = 'There are no posts.';
$lang['features_demo:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'There are no posts at the moment.';
$lang['features_demo:post_add_success']            = 'The post "%s" was added.';
$lang['features_demo:post_add_error']              = 'An error occured.';
$lang['features_demo:edit_success']                   = 'The post "%s" was updated.';
$lang['features_demo:edit_error']                     = 'An error occurred.';
$lang['features_demo:publish_success']                = 'The post "%s" has been published.';
$lang['features_demo:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['features_demo:publish_error']                  = 'No posts were published.';
$lang['features_demo:delete_success']                 = 'The post "%s" has been deleted.';
$lang['features_demo:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['features_demo:delete_error']                   = 'No posts were deleted.';
$lang['features_demo:already_exist_error']            = 'A post with this URL already exists.';

$lang['features_demo:twitter_posted']                 = 'Posted "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter Error';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
