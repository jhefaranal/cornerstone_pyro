<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                      = 'Ανάρτηση';
$lang['features_demo:posts']                     = 'Αναρτήσεις';

// labels
$lang['features_demo:posted_label'] 		= 'Δημοσιεύτηκε';
$lang['features_demo:posted_label_alt'] 		= 'Δημοσιεύτηκε στις';
$lang['features_demo:written_by_label'] 		= 'Γράφτηκε από';
$lang['features_demo:author_unknown'] 		= 'Άγνωστο';
$lang['features_demo:keywords_label'] 		= 'Λέξεις κλειδιά';
$lang['features_demo:tagged_label'] 		= 'Ετικέτες';
$lang['features_demo:category_label'] 		= 'Κατηγορία';
$lang['features_demo:post_label'] 		= 'Δημοσίευση';
$lang['features_demo:date_label'] 		= 'Ημερομηνία';
$lang['features_demo:date_at'] 			= 'στις';
$lang['features_demo:time_label'] 		= 'Χρόνος';
$lang['features_demo:status_label'] 		= 'Κατάσταση';
$lang['features_demo:draft_label'] 		= 'Πρόχειρο';
$lang['features_demo:live_label'] 		= 'Δημοσιευμένο';
$lang['features_demo:content_label'] 		= 'Περιεχόμενο';
$lang['features_demo:options_label'] 		= 'Επιλογές';
$lang['features_demo:intro_label'] 		= 'Εισαγωγή';
$lang['features_demo:no_category_select_label'] 	= '-- Καμμία --';
$lang['features_demo:new_category_label'] 	= 'Προσθήκη κατηγορίας';
$lang['features_demo:subscripe_to_rss_label'] 	= 'Εγγραφή στο RSS';
$lang['features_demo:all_posts_label'] 		= 'Όλες οι δημοσιεύσεις';
$lang['features_demo:posts_of_category_suffix'] 	= ' δημοσιεύσεις';
$lang['features_demo:rss_name_suffix'] 		= ' Ιστολόγιο';
$lang['features_demo:rss_category_suffix'] 	= ' Ιστολόγιο';
$lang['features_demo:author_name_label'] 	= 'Όνομα συγγραφέα';
$lang['features_demo:read_more_label'] 		= 'Διαβάστε περισσότερα &raquo;';
$lang['features_demo:created_hour'] 		= 'Ώρα δημιουργίας';
$lang['features_demo:created_minute'] 		= 'Λεπτό δημιουργίας';
$lang['features_demo:comments_enabled_label'] 	= 'Σχόλια Ενεργά';

// titles
$lang['features_demo:create_title'] 		= 'Προσθήκη δημοσίευσης';
$lang['features_demo:edit_title'] 		= 'Επεξεργασία δημοσίευσης "%s"';
$lang['features_demo:archive_title'] 		= 'Αρχειοθήκη';
$lang['features_demo:posts_title'] 		= 'Δημοσιεύσεις';
$lang['features_demo:rss_posts_title'] 		= 'Δημοσιεύσεις ιστολογίου για %s';
$lang['features_demo:features_demo_title'] 		= 'Ιστολόγιο';
$lang['features_demo:list_title'] 		= 'Λίστα δημοσιεύσεων';

// messages
$lang['features_demo:disabled_after'] 				= 'Η ανάρτηση σχολίων μετά τις %s έχει απενεργοποιηθεί.';
$lang['features_demo:no_posts'] 			= 'Δεν υπάρχουν δημοσιεύσεις.';
$lang['features_demo:subscripe_to_rss_desc'] 	= 'Μπορείτε να λαμβάνετε νεότερες δημοσιεύσεις με το να εγγραφείτε στην τροφοδοσία RSS μας. Μπορείτε να το κάνετε χρησιμοποιώντας τα δημοφιλή προγράμματα e-mail, ή με το <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts'] 	= 'Προς το παρόν δεν υπάρχουν δημοσιεύσεις.';
$lang['features_demo:post_add_success'] 		= 'Η δημοσίευση "%s" προστέθηκε.';
$lang['features_demo:post_add_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['features_demo:edit_success'] 		= 'Η δημοσίευση "%s" ενημερώθηκε.';
$lang['features_demo:edit_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['features_demo:publish_success'] 		= 'Η δημοσίευση "%s" αναρτήθηκε.';
$lang['features_demo:mass_publish_success'] 	= 'Οι δημοσιεύσεις "%s" αναρτήθηκαν.';
$lang['features_demo:publish_error'] 		= 'Δεν αναρτήθηκε καμία δημοσίευση.';
$lang['features_demo:delete_success'] 		= 'Η δημοσίευση "%s" διαγράφηκε.';
$lang['features_demo:mass_delete_success'] 	= 'Οι δημοσιεύσεις "%s" διαγράφηκαν.';
$lang['features_demo:delete_error'] 		= 'Δεν διαγράφηκε καμία δημοσίευση.';
$lang['features_demo:already_exist_error'] 	= 'Υπάρχει ήδη μια δημοσίευση με αυτό το URL.';

$lang['features_demo:twitter_posted'] 		= 'Δημοσιεύτηκε "%s" %s';
$lang['features_demo:twitter_error'] 		= 'Σφάλμα Twitter';

// date
$lang['features_demo:archive_date_format'] 	= "%B %Y";
