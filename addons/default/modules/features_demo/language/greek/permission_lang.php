<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live'] = 'Να δημοσιεύει άρθρα';
$lang['features_demo:role_edit_live'] = 'Επεξεργασία δημοσιευμένων άρθρων';
$lang['features_demo:role_delete_live'] 	= 'Διαγραφή δημοσιευμένων άρθρων';