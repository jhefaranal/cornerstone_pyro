<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live'] = 'הכנס מאמרים חיים';
$lang['features_demo:role_edit_live'] = 'ערוך מאמרים חיים';
$lang['features_demo:role_delete_live'] 	= 'Delete live articles'; #translate