<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']                   = 'Posted';
$lang['features_demo:posted_label_alt']               = 'Posted at';
$lang['features_demo:written_by_label']				= 'Written by'; #translate
$lang['features_demo:author_unknown']				= 'Unknown'; #translate
$lang['features_demo:keywords_label']				= 'Keywords'; #translate
$lang['features_demo:tagged_label']					= 'Tagged'; #translate
$lang['features_demo:category_label']                 = 'Category';
$lang['features_demo:post_label']                     = 'Post';
$lang['features_demo:date_label']                     = 'Date';
$lang['features_demo:date_at']                        = 'at';
$lang['features_demo:time_label']                     = 'Time';
$lang['features_demo:status_label']                   = 'Status';
$lang['features_demo:draft_label']                    = 'Draft';
$lang['features_demo:live_label']                     = 'Live';
$lang['features_demo:content_label']                  = 'Content';
$lang['features_demo:options_label']                  = 'Options';
$lang['features_demo:intro_label']                    = 'Introduction';
$lang['features_demo:no_category_select_label']       = '-- None --';
$lang['features_demo:new_category_label']             = 'Add a category';
$lang['features_demo:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['features_demo:all_posts_label']             = 'All posts';
$lang['features_demo:posts_of_category_suffix']    = ' posts';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Author name';
$lang['features_demo:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Created on Hour';
$lang['features_demo:created_minute']                 = 'Created on Minute';

// titles
$lang['features_demo:create_title']                   = 'הוספ פוסט';
$lang['features_demo:edit_title']                     = 'ערוך פוסט "%s"';
$lang['features_demo:archive_title']                  = 'ארכיון';
$lang['features_demo:posts_title']                 = 'פוסטים';
$lang['features_demo:rss_posts_title']             = 'Features_demo posts for %s';
$lang['features_demo:features_demo_title']                     = 'בלוג';
$lang['features_demo:list_title']                     = 'רשימת הפוסטים';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']                    = 'אין פוסטים.';
$lang['features_demo:subscripe_to_rss_desc']          = 'קבל פוסטי ישירות ע"י הרשמה לRSS שלנו. ניתןלעשות זאת בעזרת <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'כרגע אין פוסטים.';
$lang['features_demo:post_add_success']            = 'הפוסט "%s" הוסף בהצלחה.';
$lang['features_demo:post_add_error']              = 'התרחשה שגיעה.';
$lang['features_demo:edit_success']                   = 'The post "%s" was updated.';
$lang['features_demo:edit_error']                     = 'התרחשה שגיעה.';
$lang['features_demo:publish_success']                = 'The post "%s" has been published.';
$lang['features_demo:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['features_demo:publish_error']                  = 'No posts were published.';
$lang['features_demo:delete_success']                 = 'The post "%s" has been deleted.';
$lang['features_demo:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['features_demo:delete_error']                   = 'No posts were deleted.';
$lang['features_demo:already_exist_error']            = 'An post with this URL already exists.';

$lang['features_demo:twitter_posted']                 = 'Posted "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter Error';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
