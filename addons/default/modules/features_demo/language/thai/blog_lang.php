<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Thai translation.
*
* @author	Nateetorn Lertkhonsan <nateetorn.l@gmail.com>
* @package	PyroCMS  
* @link		http://pyrocms.com
* @date		2012-04-19
* @version	1.0.0
**/

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']                   = 'โพสต์แล้ว';
$lang['features_demo:posted_label_alt']               = 'โพสต์แล้วที่';
$lang['features_demo:written_by_label']				= 'เขียนโดย';
$lang['features_demo:author_unknown']				= 'ไม่ทราบ';
$lang['features_demo:keywords_label']				= 'คำหลัก';
$lang['features_demo:tagged_label']					= 'แท็ก';
$lang['features_demo:category_label']                 = 'หมวดหมู่';
$lang['features_demo:post_label']                     = 'โพสต์';
$lang['features_demo:date_label']                     = 'วันที่';
$lang['features_demo:date_at']                        = 'ที่';
$lang['features_demo:time_label']                     = 'เวลา';
$lang['features_demo:status_label']                   = 'สถานะ';
$lang['features_demo:draft_label']                    = 'ร่างบล็อก';
$lang['features_demo:live_label']                     = 'ไลฟ์บล็อก';
$lang['features_demo:content_label']                  = 'เนื้อหา';
$lang['features_demo:options_label']                  = 'ตัวเลือก';
$lang['features_demo:intro_label']                    = 'บทนำ';
$lang['features_demo:no_category_select_label']       = '-- ไม่มี --';
$lang['features_demo:new_category_label']             = 'เพิ่มหมวดหมู่';
$lang['features_demo:subscripe_to_rss_label']         = 'สมัครสมาชิก RSS';
$lang['features_demo:all_posts_label']             = 'โพสต์ทั้งหมด';
$lang['features_demo:posts_of_category_suffix']    = ' โพสต์';
$lang['features_demo:rss_name_suffix']                = ' บล็อก';
$lang['features_demo:rss_category_suffix']            = ' บล็อก';
$lang['features_demo:author_name_label']              = 'ชื่อผู้เขียน';
$lang['features_demo:read_more_label']                = 'อ่านเพิ่มเติม&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'สร้างเมื่อชั่วโมงที่แล้ว';
$lang['features_demo:created_minute']                 = 'สร้างเมื่อนาทีที่แล้ว';
$lang['features_demo:comments_enabled_label']         = 'ความคิดเห็นถูกเปิดใช้งาน';

// titles
$lang['features_demo:create_title']                   = 'เพิ่มโพสต์';
$lang['features_demo:edit_title']                     = 'แก้ไขโพสต์ "%s"';
$lang['features_demo:archive_title']                 = 'คลัง';
$lang['features_demo:posts_title']					= 'โพสต์';
$lang['features_demo:rss_posts_title']				= 'โพสต์บล็อกสำหรับ %s';
$lang['features_demo:features_demo_title']					= 'บล็อก';
$lang['features_demo:list_title']					= 'รายการโพสต์';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']                    = 'ไม่มีโพสต์';
$lang['features_demo:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'ไม่มีโพสต์ในขณะนี้';
$lang['features_demo:post_add_success']            = 'โพสต์ "%s" ถูกเพิ่มแล้ว';
$lang['features_demo:post_add_error']              = 'เกิดข้อผิดพลาด';
$lang['features_demo:edit_success']                   = 'ปรับปรุงโพสต์ "%s" แล้ว';
$lang['features_demo:edit_error']                     = 'เกิดข้อผิดพลาด';
$lang['features_demo:publish_success']                = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['features_demo:mass_publish_success']           = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['features_demo:publish_error']                  = 'ไม่มีโพสต์ถูกเผยแพร่.';
$lang['features_demo:delete_success']                 = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['features_demo:mass_delete_success']            = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['features_demo:delete_error']                   = 'ไม่มีโพสต์ถูกลบ';
$lang['features_demo:already_exist_error']            = 'โพสต์ลิงค์นี้มีอยู่แล้ว';

$lang['features_demo:twitter_posted']                 = 'โพสต์ "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter มีข้อผิดพลาด';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
