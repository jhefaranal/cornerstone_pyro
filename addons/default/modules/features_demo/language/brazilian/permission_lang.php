<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Publicar artigos';
$lang['features_demo:role_edit_live']	= 'Editar artigos publicados';
$lang['features_demo:role_delete_live'] 	= 'Remover artigos publicados';