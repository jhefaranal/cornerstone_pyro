<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Author: Thanh Nguyen
* 		  nguyenhuuthanh@gmail.com
*
* Location: http://techmix.net
*
* Created:  10.26.2011
*
* Description:  Vietnamese language file
*
*/

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']                   = 'Đã gửi';
$lang['features_demo:posted_label_alt']               = 'Gửi lúc at';
$lang['features_demo:written_by_label']				= 'Viết bởi';
$lang['features_demo:author_unknown']				= 'Chưa rõ';
$lang['features_demo:keywords_label']				= 'Keywords'; #translate
$lang['features_demo:tagged_label']					= 'Tagged'; #translate
$lang['features_demo:category_label']                 = 'Danh mục';
$lang['features_demo:post_label']                     = 'Bài viết';
$lang['features_demo:date_label']                     = 'Ngày';
$lang['features_demo:date_at']                        = 'lúc';
$lang['features_demo:time_label']                     = 'thời gian';
$lang['features_demo:status_label']                   = 'Trạng thái';
$lang['features_demo:draft_label']                    = 'Nháp';
$lang['features_demo:live_label']                     = 'Xuất bản';
$lang['features_demo:content_label']                  = 'Nội dung';
$lang['features_demo:options_label']                  = 'Tùy biến';
$lang['features_demo:intro_label']                    = 'Giới thiệu';
$lang['features_demo:no_category_select_label']       = '-- Không --';
$lang['features_demo:new_category_label']             = 'Thêm chuyên mục';
$lang['features_demo:subscripe_to_rss_label']         = 'Nhận RSS';
$lang['features_demo:all_posts_label']             = 'Tất cả bài viết';
$lang['features_demo:posts_of_category_suffix']    = ' bài viết';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Tên tác giả';
$lang['features_demo:read_more_label']                = 'Đọc thêm&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Giờ tạo';
$lang['features_demo:created_minute']                 = 'Phút tạo';
$lang['features_demo:comments_enabled_label']         = 'Cho phép phản hồi';

// titles
$lang['features_demo:create_title']                   = 'Thêm bài viết';
$lang['features_demo:edit_title']                     = 'Sửa bài viết "%s"';
$lang['features_demo:archive_title']                  = 'Lưu trữ';
$lang['features_demo:posts_title']                 = 'Bài viết';
$lang['features_demo:rss_posts_title']             = 'Các bài viết cho %s';
$lang['features_demo:features_demo_title']                     = 'Features_demo';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']                    = 'Không có bài viết nào.';
$lang['features_demo:subscripe_to_rss_desc']          = 'Hãy sử dụng RSS Feed để nhận những bài viết mới nhất. Bạn có thể sử dụng nhiều chương trình khác nhau, hoặc sử dụng <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'Không có bài viết nào.';
$lang['features_demo:post_add_success']            = 'Đã thêm bài viết "%s".';
$lang['features_demo:post_add_error']              = 'Có lỗi xảy ra.';
$lang['features_demo:edit_success']                   = 'Bài viết "%s" đã được cập nhật.';
$lang['features_demo:edit_error']                     = 'Có lỗi xảy ra.';
$lang['features_demo:publish_success']                = 'Bài viết "%s" đã được xuất bản.';
$lang['features_demo:mass_publish_success']           = 'Bài viết "%s" đã được xuất bản.';
$lang['features_demo:publish_error']                  = 'Không có bài viết nào được xuât bản.';
$lang['features_demo:delete_success']                 = 'Đã xóa bài viết "%s".';
$lang['features_demo:mass_delete_success']            = 'Đã xóa bài viết "%s".';
$lang['features_demo:delete_error']                   = 'Không có bài viết nào được xóa.';
$lang['features_demo:already_exist_error']            = 'URL của bài viết đã tồn tại.';

$lang['features_demo:twitter_posted']                 = 'Đã gửi "%s" %s';
$lang['features_demo:twitter_error']                  = 'Lỗi Twitter';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
