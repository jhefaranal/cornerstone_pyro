<?php defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * Swedish translation.
 *
 * @author		marcus@incore.se
 * @package		PyroCMS  
 * @link		http://pyrocms.com
 * @date		2012-10-23
 * @version		1.1.0
 */

$lang['features_demo:post'] = 'Inlägg';
$lang['features_demo:posts'] = 'Inlägg';
$lang['features_demo:posted_label'] = 'Skriven';
$lang['features_demo:posted_label_alt'] = 'Skriven den';
$lang['features_demo:written_by_label'] = 'Skriven av';
$lang['features_demo:author_unknown'] = 'Okänd';
$lang['features_demo:keywords_label'] = 'Nyckelord';
$lang['features_demo:tagged_label'] = 'Taggad';
$lang['features_demo:category_label'] = 'Kategori';
$lang['features_demo:post_label'] = 'Inlägg';
$lang['features_demo:date_label'] = 'Datum';
$lang['features_demo:date_at'] = 'den';
$lang['features_demo:time_label'] = 'Tid';
$lang['features_demo:status_label'] = 'Status';
$lang['features_demo:draft_label'] = 'Utkast';
$lang['features_demo:live_label'] = 'Publik';
$lang['features_demo:content_label'] = 'Innehåll';
$lang['features_demo:options_label'] = 'Val';
$lang['features_demo:intro_label'] = 'Introduktion';
$lang['features_demo:no_category_select_label'] = '-- Ingen --';
$lang['features_demo:new_category_label'] = 'Lägg till en kategori';
$lang['features_demo:subscripe_to_rss_label'] = 'Prenumerera på RSS';
$lang['features_demo:all_posts_label'] = 'Alla inlägg';
$lang['features_demo:posts_of_category_suffix'] = 'inlägg';
$lang['features_demo:rss_name_suffix'] = 'Features_demog';
$lang['features_demo:rss_category_suffix'] = 'Features_demog';
$lang['features_demo:author_name_label'] = 'Skribent';
$lang['features_demo:read_more_label'] = 'Läs mer »';
$lang['features_demo:created_hour'] = 'Skapad, timme';
$lang['features_demo:created_minute'] = 'Skapad, minut';
$lang['features_demo:comments_enabled_label'] = 'Kommentarer aktiverad';
$lang['features_demo:create_title'] = 'Lägg till post';
$lang['features_demo:edit_title'] = 'Redigera post "%s"';
$lang['features_demo:archive_title'] = 'Arkiv';
$lang['features_demo:posts_title'] = 'Inlägg';
$lang['features_demo:rss_posts_title'] = 'Features_demoginlägg för %s';
$lang['features_demo:features_demo_title'] = 'Features_demog';
$lang['features_demo:list_title'] = 'Lista inlägg';
$lang['features_demo:disabled_after'] = 'Skickade kommentarer efter %s har blivit inaktiverade.';
$lang['features_demo:no_posts'] = 'Det finns inga poster';
$lang['features_demo:subscripe_to_rss_desc'] = 'Få inlägg direkt genom att prenumerera på vårt RSS-flöde. Du flesta populära e-postklienter har stöd för detta, eller prova <a href="http://reader.google.co.uk/">Google Reader</ a>.';
$lang['features_demo:currently_no_posts'] = 'Det finns inga poster just nu';
$lang['features_demo:post_add_success'] = 'Inlägget "%s" har lagts till.';
$lang['features_demo:post_add_error'] = 'Ett fel inträffade.';
$lang['features_demo:edit_success'] = 'Inlägget "%s" uppdaterades.';
$lang['features_demo:edit_error'] = 'Ett fel inträffade.';
$lang['features_demo:publish_success'] = 'Inlägget "%s" publicerades.';
$lang['features_demo:mass_publish_success'] = 'Inläggen "%s" publicerades.';
$lang['features_demo:publish_error'] = 'Inga inlägg publicerades';
$lang['features_demo:delete_success'] = 'Inlägget "%s" har raderats.';
$lang['features_demo:mass_delete_success'] = 'Inläggen "%s" har raderats.';
$lang['features_demo:delete_error'] = 'Inga inlägg raderades';
$lang['features_demo:already_exist_error'] = 'Ett inlägg med denna URL finns redan';
$lang['features_demo:twitter_posted'] = 'Sparad "%s" %s';
$lang['features_demo:twitter_error'] = 'Twitterfel';
$lang['features_demo:archive_date_format'] = '%B\' %Y';


/* End of file features_demo_lang.php */  
/* Location: system/cms/modules/features_demo/language/swedish/features_demo_lang.php */  
