<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                          = 'Įrašas';
$lang['features_demo:posts']                         = 'Įrašai';

// labels
$lang['features_demo:posted_label']                  = 'Paskelbta';
$lang['features_demo:posted_label_alt']              = 'Paskelbta...';
$lang['features_demo:written_by_label']              = 'Autorius';
$lang['features_demo:author_unknown']				= 'Nežinomas';
$lang['features_demo:keywords_label']				= 'Raktažodžiai';
$lang['features_demo:tagged_label']					= 'Žymėtas';
$lang['features_demo:category_label']                = 'Kategorija';
$lang['features_demo:post_label']                    = 'Įrašas';
$lang['features_demo:date_label']                    = 'Data';
$lang['features_demo:date_at']                       = '';
$lang['features_demo:time_label']                    = 'Laikas';
$lang['features_demo:status_label']                  = 'Statusas';
$lang['features_demo:draft_label']                   = 'Projektas';
$lang['features_demo:live_label']                    = 'Gyvai';
$lang['features_demo:content_label']                 = 'Turinys';
$lang['features_demo:options_label']                 = 'Funkcijos';
$lang['features_demo:title_label']                   = 'Pavadinimas';
$lang['features_demo:slug_label']                    = 'URL';
$lang['features_demo:intro_label']                   = 'Įžanga';
$lang['features_demo:no_category_select_label']      = '-- Nėra --';
$lang['features_demo:new_category_label']            = 'Pridėti kategoriją';
$lang['features_demo:subscripe_to_rss_label']        = 'Prenumeruoti RSS';
$lang['features_demo:all_posts_label']               = 'Visi įrašai';
$lang['features_demo:posts_of_category_suffix']      = ' įrašai';
$lang['features_demo:rss_name_suffix']               = ' Naujienos';
$lang['features_demo:rss_category_suffix']           = ' Naujienos';
$lang['features_demo:author_name_label']             = 'Autoriaus vardas';
$lang['features_demo:read_more_label']               = 'Plačiau&nbsp;&raquo;';
$lang['features_demo:created_hour']                  = 'Sukurta valandą';
$lang['features_demo:created_minute']                = 'Sukurta minutę';
$lang['features_demo:comments_enabled_label']        = 'Įjungti komentarus?';

// titles
$lang['features_demo:create_title']                  = 'Pridėti įrašą';
$lang['features_demo:edit_title']                    = 'Redaguoti įrašą "%s"';
$lang['features_demo:archive_title']                 = 'Archyvas';
$lang['features_demo:posts_title']                   = 'Įrašai';
$lang['features_demo:rss_posts_title']               = 'Naujienų įrašai %s';
$lang['features_demo:features_demo_title']                    = 'Naujiena';
$lang['features_demo:list_title']                    = 'Įrašų sąrašas';

// messages
$lang['features_demo:disabled_after'] 				= 'Paskelbti komentarai po %s buvo atjungti.';
$lang['features_demo:no_posts']                      = 'Nėra įrašų.';
$lang['features_demo:subscripe_to_rss_desc']         = 'Gaukite žinutes iš karto prenumeruodami į RSS kanalą. Jūs galite tai padaryti per populiariausias elektroninio pašto paskyras, arba bandykite <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']            = 'Šiuo metu nėra įrašų.';
$lang['features_demo:post_add_success']              = 'Įrašas "%s" pridėtas.';
$lang['features_demo:post_add_error']                = 'Įvyko klaida.';
$lang['features_demo:edit_success']                  = 'Įrašas "%s" atnaujintas.';
$lang['features_demo:edit_error']                    = 'Įvyko klaida.';
$lang['features_demo:publish_success']               = 'Įrašas "%s" paskelbtas.';
$lang['features_demo:mass_publish_success']          = 'Įrašai "%s" pasklbti.';
$lang['features_demo:publish_error']                 = 'Nėra paskelbtų įrašų.';
$lang['features_demo:delete_success']                = 'Įrašas "%s" ištrintas.';
$lang['features_demo:mass_delete_success']           = 'Įrašai "%s" ištrinti.';
$lang['features_demo:delete_error']                  = 'Nėra ištrintų įrašų.';
$lang['features_demo:already_exist_error']           = 'Įrašas su šiuo URL jau egzistuoja.';

$lang['features_demo:twitter_posted']                = 'Įrašyta "%s" %s';
$lang['features_demo:twitter_error']                 = 'Twitter nepasiekiamas';

// date
$lang['features_demo:archive_date_format']           = "%B %Y";
