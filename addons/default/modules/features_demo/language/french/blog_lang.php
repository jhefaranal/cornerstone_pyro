<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']  = 'Actualité';
$lang['features_demo:posts'] = 'Actualités';

// labels
$lang['features_demo:posted_label']             = 'Publié le';
$lang['features_demo:posted_label_alt']         = 'Envoyé à';
$lang['features_demo:written_by_label']         = 'Écrit par';
$lang['features_demo:author_unknown']           = 'Inconnu';
$lang['features_demo:keywords_label']           = 'Mots-clés';
$lang['features_demo:tagged_label']             = 'Taggé';
$lang['features_demo:category_label']           = 'Catégorie';
$lang['features_demo:post_label']               = 'Article';
$lang['features_demo:date_label']               = 'Date';
$lang['features_demo:date_at']                  = 'le';
$lang['features_demo:time_label']               = 'Heure';
$lang['features_demo:status_label']             = 'Statut';
$lang['features_demo:draft_label']              = 'Brouillon';
$lang['features_demo:live_label']               = 'Live';
$lang['features_demo:content_label']            = 'Contenu';
$lang['features_demo:options_label']            = 'Options';
$lang['features_demo:intro_label']              = 'Introduction';
$lang['features_demo:no_category_select_label'] = '-- Aucune --';
$lang['features_demo:new_category_label']       = 'Ajouter une catégorie';
$lang['features_demo:subscripe_to_rss_label']   = 'Souscrire au RSS';
$lang['features_demo:all_posts_label']          = 'Tous les Articles';
$lang['features_demo:posts_of_category_suffix'] = ' articles';
$lang['features_demo:rss_name_suffix']          = ' Articles';
$lang['features_demo:rss_category_suffix']      = ' Articles';
$lang['features_demo:author_name_label']        = 'Auteur';
$lang['features_demo:read_more_label']          = 'Lire la suite&nbsp;&raquo;';
$lang['features_demo:created_hour']             = 'Heure de création';
$lang['features_demo:created_minute']           = 'Minute de création';
$lang['features_demo:comments_enabled_label']   = 'Commentaires activés';

// titles
$lang['features_demo:create_title']    			= 'Créer un article';
$lang['features_demo:edit_title']      			= 'Modifier l\'article "%s"';
$lang['features_demo:archive_title']   			= 'Archives';
$lang['features_demo:posts_title']     			= 'Articles';
$lang['features_demo:rss_posts_title'] 			= ' pour %s';
$lang['features_demo:features_demo_title']      			= 'Articles';
$lang['features_demo:list_title']      			= 'Lister les posts';

// messages
$lang['features_demo:disabled_after'] 			= 'Poster des commentaires après  %s a été désactivé.';
$lang['features_demo:no_posts']              = 'Il n\'y a pas d\'articles.';
$lang['features_demo:subscripe_to_rss_desc'] = 'Restez informé des derniers articles en temps réel en vous abonnant au flux RSS. Vous pouvez faire cela avec la plupart des logiciels de messagerie, ou essayez <a href="http://www.google.fr/reader">Google Reader</a>.';
$lang['features_demo:currently_no_posts']    = 'Il n\'y a aucun article actuellement.';
$lang['features_demo:post_add_success']      = 'L\'article "%s" a été ajouté.';
$lang['features_demo:post_add_error']        = 'Une erreur est survenue.';
$lang['features_demo:edit_success']          = 'Le post "%s" a été mis à jour.';
$lang['features_demo:edit_error']            = 'Une erreur est survenue.';
$lang['features_demo:publish_success']       = 'Les articles "%s" ont été publiés.';
$lang['features_demo:mass_publish_success']  = 'Les articles "%s" ont été publiés.';
$lang['features_demo:publish_error']         = 'Aucun article n\'a été publié.';
$lang['features_demo:delete_success']        = 'Les articles "%s" ont été supprimés.';
$lang['features_demo:mass_delete_success']   = 'Les articles "%s" ont été supprimés.';
$lang['features_demo:delete_error']          = 'Aucun article n\'a été supprimé.';
$lang['features_demo:already_exist_error']   = 'Un article avec cet URL existe déjà.';

$lang['features_demo:twitter_posted'] = 'Envoyé "%s" %s';
$lang['features_demo:twitter_error']  = 'Erreur Twitter';

// date
$lang['features_demo:archive_date_format'] = "%B %Y";
