<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Mettre l\'article en direct';
$lang['features_demo:role_edit_live']	= 'Modifier les articles en direct';
$lang['features_demo:role_delete_live'] 	= 'Supprimer les articles en direct';