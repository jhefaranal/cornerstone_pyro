<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']                   = 'Dipostkan';
$lang['features_demo:posted_label_alt']               = 'Dipostkan pada';
$lang['features_demo:written_by_label']				= 'Ditulis oleh';
$lang['features_demo:author_unknown']				= 'Tidak dikenal';
$lang['features_demo:keywords_label']				= 'Kata Kunci';
$lang['features_demo:tagged_label']					= 'Label';
$lang['features_demo:category_label']                 = 'Kategori';
$lang['features_demo:post_label']                     = 'Post';
$lang['features_demo:date_label']                     = 'Tanggal';
$lang['features_demo:date_at']                        = 'pada';
$lang['features_demo:time_label']                     = 'Waktu';
$lang['features_demo:status_label']                   = 'Status';
$lang['features_demo:draft_label']                    = 'Draft';
$lang['features_demo:live_label']                     = 'Publikasikan';
$lang['features_demo:content_label']                  = 'Konten';
$lang['features_demo:options_label']                  = 'Opsi';
$lang['features_demo:intro_label']                    = 'Pendahuluan';
$lang['features_demo:no_category_select_label']       = '-- tidak ada --';
$lang['features_demo:new_category_label']             = 'Tambah kategori';
$lang['features_demo:subscripe_to_rss_label']         = 'Berlangganan RSS';
$lang['features_demo:all_posts_label']             = 'Semua post';
$lang['features_demo:posts_of_category_suffix']    = ' post';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Nama penulis';
$lang['features_demo:read_more_label']                = 'Selengkapnya&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Dibuat pada Jam';
$lang['features_demo:created_minute']                 = 'Dibuat pada Menit';
$lang['features_demo:comments_enabled_label']         = 'Perbolehkan Komentar';

// titles
$lang['features_demo:create_title']                   = 'Tambah Post';
$lang['features_demo:edit_title']                     = 'Edit post "%s"';
$lang['features_demo:archive_title']                 = 'Arsip';
$lang['features_demo:posts_title']					= 'Post';
$lang['features_demo:rss_posts_title']				= 'Features_demo post untuk %s';
$lang['features_demo:features_demo_title']					= 'Features_demo';
$lang['features_demo:list_title']					= 'Daftar Post';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']                    = 'Tidak ada post.';
$lang['features_demo:subscripe_to_rss_desc']          = 'Ambil post langsung dengan berlangganan melalui RSS feed kami. Anda dapat melakukannya menggunakan hampir semua e-mail client, atau coba <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'Untuk sementara tidak ada post.';
$lang['features_demo:post_add_success']            = 'Post "%s" telah ditambahkan.';
$lang['features_demo:post_add_error']              = 'Terjadi kesalahan.';
$lang['features_demo:edit_success']                   = 'Post "%s" telah diperbaharui.';
$lang['features_demo:edit_error']                     = 'Terjadi kesalahan.';
$lang['features_demo:publish_success']                = 'Post "%s" telah dipublikasikan.';
$lang['features_demo:mass_publish_success']           = 'Post "%s" telah dipublikasikan.';
$lang['features_demo:publish_error']                  = 'Tidak ada post yang terpublikasi.';
$lang['features_demo:delete_success']                 = 'Post "%s" telah dihapus.';
$lang['features_demo:mass_delete_success']            = 'Post "%s" telah dihapus.';
$lang['features_demo:delete_error']                   = 'Tidak ada post yan terhapus.';
$lang['features_demo:already_exist_error']            = 'Post dengan URL ini sudah ada.';

$lang['features_demo:twitter_posted']                 = 'Dipostkan "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter Error';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
