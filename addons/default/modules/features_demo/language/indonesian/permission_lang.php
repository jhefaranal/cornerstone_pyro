<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Publikasikan artikel';
$lang['features_demo:role_edit_live']	= 'Edit artikel terpublikasi';
$lang['features_demo:role_delete_live'] 	= 'Hapus artikel terpublikasi';
