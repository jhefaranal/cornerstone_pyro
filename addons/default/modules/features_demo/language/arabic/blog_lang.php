<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'تدوينة';
$lang['features_demo:posts']                   = 'تدوينات';

// labels
$lang['features_demo:posted_label'] 			= 'تاريخ النشر';
$lang['features_demo:posted_label_alt']			= 'نشر في';
$lang['features_demo:written_by_label']				= 'كتبها';
$lang['features_demo:author_unknown']				= 'مجهول';
$lang['features_demo:keywords_label']				= 'كلمات البحث';
$lang['features_demo:tagged_label']					= 'موسومة';
$lang['features_demo:category_label'] 			= 'التصنيف';
$lang['features_demo:post_label'] 			= 'إرسال';
$lang['features_demo:date_label'] 			= 'التاريخ';
$lang['features_demo:date_at']				= 'عند';
$lang['features_demo:time_label'] 			= 'الوقت';
$lang['features_demo:status_label'] 			= 'الحالة';
$lang['features_demo:draft_label'] 			= 'مسودّة';
$lang['features_demo:live_label'] 			= 'منشور';
$lang['features_demo:content_label'] 			= 'المُحتوى';
$lang['features_demo:options_label'] 			= 'خيارات';
$lang['features_demo:intro_label'] 			= 'المٌقدّمة';
$lang['features_demo:no_category_select_label'] 		= '-- لاشيء --';
$lang['features_demo:new_category_label'] 		= 'إضافة تصنيف';
$lang['features_demo:subscripe_to_rss_label'] 		= 'اشترك في خدمة RSS';
$lang['features_demo:all_posts_label'] 		= 'جميع التدوينات';
$lang['features_demo:posts_of_category_suffix'] 	= ' &raquo; التدوينات';
$lang['features_demo:rss_name_suffix'] 			= ' &raquo; المُدوّنة';
$lang['features_demo:rss_category_suffix'] 		= ' &raquo; المُدوّنة';
$lang['features_demo:author_name_label'] 		= 'إسم الكاتب';
$lang['features_demo:read_more_label'] 			= 'إقرأ المزيد&nbsp;&raquo;';
$lang['features_demo:created_hour']                  = 'الوقت (الساعة)';
$lang['features_demo:created_minute']                = 'الوقت (الدقيقة)';
$lang['features_demo:comments_enabled_label']         = 'إتاحة التعليقات';

// titles
$lang['features_demo:create_title'] 			= 'إضافة مقال';
$lang['features_demo:edit_title'] 			= 'تعديل التدوينة "%s"';
$lang['features_demo:archive_title'] 			= 'الأرشيف';
$lang['features_demo:posts_title'] 			= 'التدوينات';
$lang['features_demo:rss_posts_title'] 		= 'تدوينات %s';
$lang['features_demo:features_demo_title'] 			= 'المُدوّنة';
$lang['features_demo:list_title'] 			= 'سرد التدوينات';

// messages
$lang['features_demo:disabled_after'] 				= 'تم تعطيل التعليقات بعد %s.';
$lang['features_demo:no_posts'] 			= 'لا يوجد تدوينات.';
$lang['features_demo:subscripe_to_rss_desc'] 		= 'اطلع على آخر التدوينات مباشرة بالاشتراك بخدمة RSS. يمكنك القيام بذلك من خلال معظم برامج البريد الإلكتروني الشائعة، أو تجربة <a href="http://reader.google.com/">قارئ جوجل</a>.';
$lang['features_demo:currently_no_posts'] 		= 'لا يوجد تدوينات حالياً.';
$lang['features_demo:post_add_success'] 		= 'تمت إضافة التدوينة "%s".';
$lang['features_demo:post_add_error'] 		= 'حدث خطأ.';
$lang['features_demo:edit_success'] 			= 'تم تحديث التدوينة "%s".';
$lang['features_demo:edit_error'] 			= 'حدث خطأ.';
$lang['features_demo:publish_success'] 			= 'تم نشر التدوينة "%s".';
$lang['features_demo:mass_publish_success'] 		= 'تم نشر التدوينات "%s".';
$lang['features_demo:publish_error'] 			= 'لم يتم نشر أي تدوينات.';
$lang['features_demo:delete_success'] 			= 'تم حذف التدوينة "%s".';
$lang['features_demo:mass_delete_success'] 		= 'تم حذف التدوينات "%s".';
$lang['features_demo:delete_error'] 			= 'لم يتم حذف أي تدوينات.';
$lang['features_demo:already_exist_error'] 		= 'يوجد مقال له عنوان URL مطابق.';

$lang['features_demo:twitter_posted']			= 'منشور في "%s" %s';
$lang['features_demo:twitter_error'] 			= 'خطأ في تويتر';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";

?>
