<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live'] = 'نشر التدوينات';
$lang['features_demo:role_edit_live'] = 'تعديل التدوينات المنشورة';
$lang['features_demo:role_delete_live'] 	= 'حذف التدوينات المنشورة';
