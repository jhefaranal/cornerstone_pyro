<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']                   = 'Objavljeno';
$lang['features_demo:posted_label_alt']               = 'Objavljeno ob';
$lang['features_demo:written_by_label']				= 'Objavil';
$lang['features_demo:author_unknown']				= 'Neznan';
$lang['features_demo:keywords_label']				= 'Klj. besede';
$lang['features_demo:tagged_label']					= 'Označen';
$lang['features_demo:category_label']                 = 'Kategorija';
$lang['features_demo:post_label']                     = 'Objava';
$lang['features_demo:date_label']                     = 'Datum';
$lang['features_demo:date_at']                        = 'ob';
$lang['features_demo:time_label']                     = 'Čas';
$lang['features_demo:status_label']                   = 'Stanje';
$lang['features_demo:draft_label']                    = 'Osnutek';
$lang['features_demo:live_label']                     = 'Vidno';
$lang['features_demo:content_label']                  = 'Vsebina';
$lang['features_demo:options_label']                  = 'Možnosti';
$lang['features_demo:intro_label']                    = 'Navodila';
$lang['features_demo:no_category_select_label']       = '-- Brez --';
$lang['features_demo:new_category_label']             = 'Dodaj kategorijo';
$lang['features_demo:subscripe_to_rss_label']         = 'Prijava na RSS';
$lang['features_demo:all_posts_label']             = 'Vsi prispevki';
$lang['features_demo:posts_of_category_suffix']    = ' prispevki';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Ime avtorja';
$lang['features_demo:read_more_label']                = 'Preberi vse&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Ustvarjeno ura';
$lang['features_demo:created_minute']                 = 'Ustvarjeno minuta';
$lang['features_demo:comments_enabled_label']         = 'Komentaji omogočeni';

// titles
$lang['features_demo:create_title']                   = 'Dodaj prispevek';
$lang['features_demo:edit_title']                     = 'Uredi prispevek "%s"';
$lang['features_demo:archive_title']                  = 'Arhiv';
$lang['features_demo:posts_title']                 = 'Članki';
$lang['features_demo:rss_posts_title']             = 'Features_demo prispevki za %s';
$lang['features_demo:features_demo_title']                     = 'Features_demo';
$lang['features_demo:list_title']                     = 'Seznam prispevkov';

// messages
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:no_posts']                    = 'Trenutno še ni prispevkov.';
$lang['features_demo:subscripe_to_rss_desc']          = 'Pridite do prispevkov v najkrajšem možnem času tako da se prijavite na RSS podajalca. To lahko storite preko popularnega email programa ali poizkusite  <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'Ta trenutek ni prispevkov';
$lang['features_demo:post_add_success']            = 'Vap prispevek je bil dodan "%s" ';
$lang['features_demo:post_add_error']              = 'Prišlo je do napake';
$lang['features_demo:edit_success']                   = 'Prispevek "%s" je bil oddan.';
$lang['features_demo:edit_error']                     = 'Prišlo je do napake.';
$lang['features_demo:publish_success']                = 'Prispevek "%s" je bil objavljen.';
$lang['features_demo:mass_publish_success']           = 'Prispeveki "%s" so bili objavljeni.';
$lang['features_demo:publish_error']                  = 'Noben prispevk ni bil objavljen.';
$lang['features_demo:delete_success']                 = 'Prispevki "%s" so bili izbrisani.';
$lang['features_demo:mass_delete_success']            = 'Prispevki "%s" so bili izbrisani.';
$lang['features_demo:delete_error']                   = 'Noben od prispevkov ni bil izbrisan..';
$lang['features_demo:already_exist_error']            = 'Prispevek s tem URL-jem že obstaja.';

$lang['features_demo:twitter_posted']                 = 'Objavljeno "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter napaka';

// date
$lang['features_demo:archive_date_format']		= "%d' %m' %Y";

/* End of file features_demo_lang.php */