<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Objavi članek';
$lang['features_demo:role_edit_live']	= 'Uredi objavljen članek';
$lang['features_demo:role_delete_live'] 	= 'Izbriši objavljen članek';

// slovenian premission_lang.php