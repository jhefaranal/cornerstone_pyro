<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['features_demo:post']                 = 'Post'; #translate
$lang['features_demo:posts']                   = 'Posts'; #translate

// labels
$lang['features_demo:posted_label']                   = 'Opslået';
$lang['features_demo:posted_label_alt']               = 'Opslået på';
$lang['features_demo:written_by_label']				= 'Skrevet af';
$lang['features_demo:author_unknown']				= 'Ukendt';
$lang['features_demo:keywords_label']				= 'Keywords'; #translate
$lang['features_demo:tagged_label']					= 'Tagged'; #translate
$lang['features_demo:category_label']                 = 'Kategori';
$lang['features_demo:post_label']                     = 'Opslå';
$lang['features_demo:date_label']                     = 'Dato';
$lang['features_demo:date_at']                        = 'på';
$lang['features_demo:time_label']                     = 'Tid';
$lang['features_demo:status_label']                   = 'Status';
$lang['features_demo:draft_label']                    = 'Udkast';
$lang['features_demo:live_label']                     = 'Live';
$lang['features_demo:content_label']                  = 'Indhold';
$lang['features_demo:options_label']                  = 'Indstillinger';
$lang['features_demo:intro_label']                    = 'Introduktion';
$lang['features_demo:no_category_select_label']       = '-- Ingen --';
$lang['features_demo:new_category_label']             = 'Tilføj en kategori';
$lang['features_demo:subscripe_to_rss_label']         = 'Abonnér på RSS';
$lang['features_demo:all_posts_label']             = 'Alle opslag';
$lang['features_demo:posts_of_category_suffix']    = ' opslag';
$lang['features_demo:rss_name_suffix']                = ' Features_demo';
$lang['features_demo:rss_category_suffix']            = ' Features_demo';
$lang['features_demo:author_name_label']              = 'Forfatter';
$lang['features_demo:read_more_label']                = 'Læs mere&nbsp;&raquo;';
$lang['features_demo:created_hour']                   = 'Oprettet on time';
$lang['features_demo:created_minute']                 = 'Oprettet on minut';
$lang['features_demo:comments_enabled_label']         = 'Kommentarer aktiveret';

// titles
$lang['features_demo:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['features_demo:create_title']                   = 'Tilføj opslag';
$lang['features_demo:edit_title']                     = 'Redigér opslag "%s"';
$lang['features_demo:archive_title']                  = 'Arkivér';
$lang['features_demo:posts_title']                 = 'Opslag';
$lang['features_demo:rss_posts_title']             = 'Features_demo-opslag for %s';
$lang['features_demo:features_demo_title']                     = 'Features_demo';
$lang['features_demo:list_title']                     = 'Liste opslag';

// messages
$lang['features_demo:no_posts']                    = 'Der er ingen opslag.';
$lang['features_demo:subscripe_to_rss_desc']          = 'Få opslag med det same ved at abonnere på vores RSS feed. Dette kan du gøre dette via de mest populære e-mail klienter, eller du kan prøve <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['features_demo:currently_no_posts']          = 'Der er i øjeblikket ingen opslag';
$lang['features_demo:post_add_success']            = 'Opslaget "%s" er tilføjet.';
$lang['features_demo:post_add_error']              = 'Der opstod en fejl.';
$lang['features_demo:edit_success']                   = 'Opslaget "%s" er opdateret.';
$lang['features_demo:edit_error']                     = 'Der opstod en fejl.';
$lang['features_demo:publish_success']                = 'Opslaget "%s" er blevet publiceret.';
$lang['features_demo:mass_publish_success']           = 'Opslagene "%s" er blevet publiceret.';
$lang['features_demo:publish_error']                  = 'Ingen opslag er blevet publiceret.';
$lang['features_demo:delete_success']                 = 'Opslaget "%s" er slettet.';
$lang['features_demo:mass_delete_success']            = 'Opslagene "%s" er slettet.';
$lang['features_demo:delete_error']                   = 'Ingen opslag er blevet slettet.';
$lang['features_demo:already_exist_error']            = 'Et opslag med denne URL findes allerede.';

$lang['features_demo:twitter_posted']                 = 'Publiceret "%s" %s';
$lang['features_demo:twitter_error']                  = 'Twitter fejl';

// date
$lang['features_demo:archive_date_format']		= "%B %Y";
