<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Features_demo Permissions
$lang['features_demo:role_put_live']		= 'Læg artikler online';
$lang['features_demo:role_edit_live']	= 'Redigér online artikler';
$lang['features_demo:role_delete_live'] 	= 'Slet online artikler';