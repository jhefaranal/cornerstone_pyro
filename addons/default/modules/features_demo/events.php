<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Sample Events Class
*
* @package 		PyroCMS
* @subpackage 	Social Module
* @category 	features_demos
* @author 		PyroCMS Dev Team
*/
class Events_Features_demo
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();

        // Load the search index model
        $this->ci->load->model('search/search_index_m');

		// Post a blog to twitter and whatnot
        Events::register('features_demo_published', array($this, 'index_features_demo'));
        Events::register('features_demo_updated', array($this, 'index_features_demo'));
        Events::register('features_demo_deleted', array($this, 'drop_features_demo'));

    }
    
    public function index_features_demo($id)
    {
    	$this->ci->load->model('features_demo/features_demo_m');

    	$post = $this->ci->features_demo_m->get($id);

    	// Only index live articles
    	if ($post->status === 'live')
    	{
    		$this->ci->search_index_m->index(
    			'features_demo', 
    			'features_demo:post', 
    			'features_demo:posts', 
    			$id,
    			//'features_demo/'.date('Y/m/', $features_demo->created_on).$post->slug,
    			'features_demos',
    			$post->title,
    			$post->body, 
    			array(
    				'cp_edit_uri' 	=> 'admin/features_demo/edit/'.$id,
    				'cp_delete_uri' => 'admin/features_demo/delete/'.$id,
    				'keywords' 		=> $post->keywords,
    			)
    		);
    	}
    	// Remove draft articles
    	else
    	{
    		$this->ci->search_index_m->drop_index('features_demo', 'features_demo:post', $id);
    	}
	}

    public function drop_features_demo($ids)
    {
    	foreach ($ids as $id)
    	{
			$this->ci->search_index_m->drop_index('features_demo', 'features_demo:post', $id);
		}
	}
  
}

/* End of file features_demos.php */