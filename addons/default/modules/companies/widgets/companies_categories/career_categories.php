<?php defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Show a list of companies categories.
 *
 * @author        Stephen Cozart
 * @author        PyroCMS Dev Team
 * @package       PyroCMS\Core\Modules\Companies\Widgets
 */
class Widget_Companies_categories extends Widgets
{
	public $author = 'Stephen Cozart';

	public $website = 'http://github.com/clip/';

	public $version = '1.0.0';

	public $title = array(
		'en' => 'Companies Categories',
		'br' => 'Categorias do Companies',
		'pt' => 'Categorias do Companies',
		'el' => 'Κατηγορίες Ιστολογίου',
		'fr' => 'Catégories du Companies',
		'ru' => 'Категории Блога',
		'id' => 'Kateori Companies',
            'fa' => 'مجموعه های بلاگ',
	);

	public $description = array(
		'en' => 'Show a list of companies categories',
		'br' => 'Mostra uma lista de navegação com as categorias do Companies',
		'pt' => 'Mostra uma lista de navegação com as categorias do Companies',
		'el' => 'Προβάλει την λίστα των κατηγοριών του ιστολογίου σας',
		'fr' => 'Permet d\'afficher la liste de Catégories du Companies',
		'ru' => 'Выводит список категорий блога',
		'id' => 'Menampilkan daftar kategori tulisan',
            'fa' => 'نمایش لیستی از مجموعه های بلاگ',
	);

	public function run()
	{
		$this->load->model('companies/companies_categories_m');

		$categories = $this->companies_categories_m->order_by('title')->get_all();

		return array('categories' => $categories);
	}

}
