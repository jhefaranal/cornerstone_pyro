	<table cellspacing="0">
		<thead>
			<tr>
				<?php if (!$hide_sort): ?>
				<th></th>
				<?php endif; ?>
				<th width="20"><?php echo form_checkbox(array('name' => 'action_to_all', 'class' => 'check-all')) ?></th>
				<th><?php echo lang('companies:post_label') ?></th>
				<th class="collapse"><?php echo lang('companies:date_label') ?></th>
				<th class="collapse"><?php echo lang('companies:written_by_label') ?></th>
				<th><?php echo lang('companies:status_label') ?></th>
				<th width="180"><?php echo lang('global:actions') ?></th>
			</tr>
		</thead>
		<tbody id="<?php if (!$hide_sort) echo 'sortable' ?>">
			<?php foreach ($companies as $post) : ?>
				<tr>
					<?php if (!$hide_sort): ?>
					<td class="handle"><span class="icon-th"></span></td>
					<?php endif; ?>
					<td><?php echo form_checkbox('action_to[]', $post->id) ?></td>
					<td><?php echo $post->title ?></td>
					<td class="collapse"><?php echo format_date($post->created_on) ?></td>
					<td class="collapse">
					<?php if (isset($post->display_name)): ?>
						<?php echo anchor('user/'.$post->username, $post->display_name, 'target="_blank"') ?>
					<?php else: ?>
						<?php echo lang('companies:author_unknown') ?>
					<?php endif ?>
					</td>
					<td><?php echo lang('companies:'.$post->status.'_label') ?></td>
					<td style="padding-top:10px;">
                        <a href="<?php echo site_url('admin/companies/edit/' . $post->id) ?>" title="<?php echo lang('global:edit')?>" class="button"><?php echo lang('global:edit')?></a>
						<a href="<?php echo site_url('admin/companies/delete/' . $post->id) ?>" title="<?php echo lang('global:delete')?>" class="button confirm"><?php echo lang('global:delete')?></a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>

	<?php $this->load->view('admin/partials/pagination') ?>

	<br>

	<div class="table_action_buttons">
		<?php $this->load->view('admin/partials/buttons', array('buttons' => array('delete', 'publish'))) ?>
	</div>

	<style>
		.icon-th {
			opacity:.6;
			cursor:move;
		}
	</style>

	<script>
		(function($) {
			$(function(){
				$('#sortable').sortable({
					handle:'.handle',
					start: function(event, ui) {
						$('tr').removeClass('alt');
					},
					update: function() {
						order = new Array();
						$('tr', this).each(function(){
							order.push( $(this).find('input[name="action_to[]"]').val() );
						});
						order = order.join(',');
			
						$.ajax({
							dataType: 'text',
							type: 'POST',
							data: 'order='+order+'&offset='+fields_offset+'&csrf_hash_name='+$.cookie(pyro.csrf_cookie_name),
							url:  SITE_URL+'admin/companies/update_sort_order',
							success: function() {
								$('tr').removeClass('alt');
								$('tr:even').addClass('alt');
							}
						});

					},
					stop: function(event, ui) {
						$("tbody tr:nth-child(even)").livequery(function () {
							$(this).addClass("alt");
						});
					}				
				}).disableSelection();
			});
		})(jQuery);
	</script>