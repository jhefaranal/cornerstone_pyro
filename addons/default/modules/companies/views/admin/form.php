<section class="title">
<?php if ($this->method == 'create'): ?>
	<h4><?php echo lang('companies:create_title') ?></h4>
<?php else: ?>
	<h4><?php echo sprintf(lang('companies:edit_title'), $post->title) ?></h4>
<?php endif ?>
</section>

<section class="item">
<div class="content">

<?php echo form_open_multipart() ?>

<div class="tabs">

	<ul class="tab-menu">
		<li><a href="#companies-content-tab"><span><?php echo lang('companies:content_label') ?></span></a></li>
		<!-- <li><a href="#companies-options-tab"><span><?php //echo lang('companies:options_label') ?></span></a></li> -->
	</ul>

	<!-- Content tab -->
	<div class="form_inputs" id="companies-content-tab">
		<fieldset>
			<ul>
				<li>
					<label for="title"><?php echo lang('global:title') ?> <span>*</span></label>
					<div class="input"><?php echo form_input('title', htmlspecialchars_decode($post->title), 'maxlength="100" id="title"') ?></div>
				</li>
	
				<li>
					<label for="slug"><?php echo lang('global:slug') ?> <span>*</span></label>
					<div class="input"><?php echo form_input('slug', $post->slug, 'maxlength="100" class="width-20"') ?></div>
				</li>
	
				<li>
					<label for="status"><?php echo lang('companies:status_label') ?></label>
					<div class="input"><?php echo form_dropdown('status', array('draft' => lang('companies:draft_label'), 'live' => lang('companies:live_label')), $post->status) ?></div>
				</li>

				<?php if ($stream_fields): ?>
					<?php foreach ($stream_fields as $field) echo $this->load->view('admin/partials/streams/form_single_display', array('field' => $field), true) ?>
				<?php endif;?>	
			</ul>
		</fieldset>
	</div>

	<!-- Options tab -->
	<div class="form_inputs" id="companies-options-tab">
		<fieldset>
			<ul>
				<div style="display:none">
					<li class="date-meta">
						<label><?php echo lang('companies:date_label') ?></label>
		
						<div class="input datetime_input">
							<?php echo form_input('created_on', date('Y-m-d', $post->created_on), 'maxlength="10" id="datepicker" class="text width-20"') ?> &nbsp;
							<?php echo form_dropdown('created_on_hour', $hours, date('H', $post->created_on)) ?> :
							<?php echo form_dropdown('created_on_minute', $minutes, date('i', ltrim($post->created_on, '0'))) ?>
						</div>
					</li>
				</div>
			</ul>
		</fieldset>
	</div>

</div>

<input type="hidden" name="row_edit_id" value="<?php if ($this->method != 'create'): echo $post->id; endif; ?>" />

<div class="buttons">
	<?php $this->load->view('admin/partials/buttons', array('buttons' => array('save', 'save_exit', 'cancel'))) ?>
</div>

<?php echo form_close() ?>

</div>
</section>