<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Mettre l\'article en direct';
$lang['companies:role_edit_live']	= 'Modifier les articles en direct';
$lang['companies:role_delete_live'] 	= 'Supprimer les articles en direct';