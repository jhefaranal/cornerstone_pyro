<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']  = 'Actualité';
$lang['companies:posts'] = 'Actualités';

// labels
$lang['companies:posted_label']             = 'Publié le';
$lang['companies:posted_label_alt']         = 'Envoyé à';
$lang['companies:written_by_label']         = 'Écrit par';
$lang['companies:author_unknown']           = 'Inconnu';
$lang['companies:keywords_label']           = 'Mots-clés';
$lang['companies:tagged_label']             = 'Taggé';
$lang['companies:category_label']           = 'Catégorie';
$lang['companies:post_label']               = 'Article';
$lang['companies:date_label']               = 'Date';
$lang['companies:date_at']                  = 'le';
$lang['companies:time_label']               = 'Heure';
$lang['companies:status_label']             = 'Statut';
$lang['companies:draft_label']              = 'Brouillon';
$lang['companies:live_label']               = 'Live';
$lang['companies:content_label']            = 'Contenu';
$lang['companies:options_label']            = 'Options';
$lang['companies:intro_label']              = 'Introduction';
$lang['companies:no_category_select_label'] = '-- Aucune --';
$lang['companies:new_category_label']       = 'Ajouter une catégorie';
$lang['companies:subscripe_to_rss_label']   = 'Souscrire au RSS';
$lang['companies:all_posts_label']          = 'Tous les Articles';
$lang['companies:posts_of_category_suffix'] = ' articles';
$lang['companies:rss_name_suffix']          = ' Articles';
$lang['companies:rss_category_suffix']      = ' Articles';
$lang['companies:author_name_label']        = 'Auteur';
$lang['companies:read_more_label']          = 'Lire la suite&nbsp;&raquo;';
$lang['companies:created_hour']             = 'Heure de création';
$lang['companies:created_minute']           = 'Minute de création';
$lang['companies:comments_enabled_label']   = 'Commentaires activés';

// titles
$lang['companies:create_title']    			= 'Créer un article';
$lang['companies:edit_title']      			= 'Modifier l\'article "%s"';
$lang['companies:archive_title']   			= 'Archives';
$lang['companies:posts_title']     			= 'Articles';
$lang['companies:rss_posts_title'] 			= ' pour %s';
$lang['companies:companies_title']      			= 'Articles';
$lang['companies:list_title']      			= 'Lister les posts';

// messages
$lang['companies:disabled_after'] 			= 'Poster des commentaires après  %s a été désactivé.';
$lang['companies:no_posts']              = 'Il n\'y a pas d\'articles.';
$lang['companies:subscripe_to_rss_desc'] = 'Restez informé des derniers articles en temps réel en vous abonnant au flux RSS. Vous pouvez faire cela avec la plupart des logiciels de messagerie, ou essayez <a href="http://www.google.fr/reader">Google Reader</a>.';
$lang['companies:currently_no_posts']    = 'Il n\'y a aucun article actuellement.';
$lang['companies:post_add_success']      = 'L\'article "%s" a été ajouté.';
$lang['companies:post_add_error']        = 'Une erreur est survenue.';
$lang['companies:edit_success']          = 'Le post "%s" a été mis à jour.';
$lang['companies:edit_error']            = 'Une erreur est survenue.';
$lang['companies:publish_success']       = 'Les articles "%s" ont été publiés.';
$lang['companies:mass_publish_success']  = 'Les articles "%s" ont été publiés.';
$lang['companies:publish_error']         = 'Aucun article n\'a été publié.';
$lang['companies:delete_success']        = 'Les articles "%s" ont été supprimés.';
$lang['companies:mass_delete_success']   = 'Les articles "%s" ont été supprimés.';
$lang['companies:delete_error']          = 'Aucun article n\'a été supprimé.';
$lang['companies:already_exist_error']   = 'Un article avec cet URL existe déjà.';

$lang['companies:twitter_posted'] = 'Envoyé "%s" %s';
$lang['companies:twitter_error']  = 'Erreur Twitter';

// date
$lang['companies:archive_date_format'] = "%B %Y";
