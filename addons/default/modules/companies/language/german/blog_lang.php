<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Artikel';
$lang['companies:posts']                   = 'Artikel';

// labels
$lang['companies:posted_label']                   = 'Ver&ouml;ffentlicht';
$lang['companies:postet_label_alt']               = 'Ver&ouml;ffentlicht in';
$lang['companies:written_by_label']				 = 'Geschrieben von';
$lang['companies:author_unknown']				 = 'Unbekannt';
$lang['companies:keywords_label']				 = 'Stichw&ouml;rter';
$lang['companies:tagged_label']					 = 'Gekennzeichnet';
$lang['companies:category_label']                 = 'Kategorie';
$lang['companies:post_label']                     = 'Artikel';
$lang['companies:date_label']                     = 'Datum';
$lang['companies:date_at']                        = 'um';
$lang['companies:time_label']                     = 'Uhrzeit';
$lang['companies:status_label']                   = 'Status';
$lang['companies:draft_label']                    = 'Unver&ouml;ffentlicht';
$lang['companies:live_label']                     = 'Ver&ouml;ffentlicht';
$lang['companies:content_label']                  = 'Inhalt';
$lang['companies:options_label']                  = 'Optionen';
$lang['companies:intro_label']                    = 'Einf&uuml;hrung';
$lang['companies:no_category_select_label']       = '-- Kein Label --';
$lang['companies:new_category_label']             = 'Kategorie hinzuf&uuml;gen';
$lang['companies:subscripe_to_rss_label']         = 'RSS abonnieren';
$lang['companies:all_posts_label']                = 'Alle Artikel';
$lang['companies:posts_of_category_suffix']       = ' Artikel';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Autor';
$lang['companies:read_more_label']                = 'Mehr lesen&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Erstellt zur Stunde';
$lang['companies:created_minute']                 = 'Erstellt zur Minute';
$lang['companies:comments_enabled_label']         = 'Kommentare aktiv';

// titles
$lang['companies:create_title']                   = 'Artikel erstellen';
$lang['companies:edit_title']                     = 'Artikel "%s" bearbeiten';
$lang['companies:archive_title']                  = 'Archiv';
$lang['companies:posts_title']                    = 'Artikel';
$lang['companies:rss_posts_title']                = 'Companies Artikel f&uuml;r %s';
$lang['companies:companies_title']                     = 'Companies';
$lang['companies:list_title']                     = 'Artikel auflisten';

// messages
$lang['companies:disabled_after'] 				= 'Kommentare nach %s wurden deaktiviert.';
$lang['companies:no_posts']                       = 'Es existieren keine Artikel.';
$lang['companies:subscripe_to_rss_desc']          = 'Abonnieren Sie unseren RSS Feed und erhalten Sie alle Artikel frei Haus. Sie k&ouml;nnen dies mit den meisten Email-Clients tun, oder z.B. mit <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']             = 'Es existieren zur Zeit keine Artikel.';
$lang['companies:post_add_success']               = 'Der Artikel "%s" wurde hinzugef&uuml;gt.';
$lang['companies:post_add_error']                 = 'Ein Fehler ist aufgetreten.';
$lang['companies:edit_success']                   = 'Der Artikel "%s" wurde aktualisiert.';
$lang['companies:edit_error']                     = 'Ein Fehler ist aufgetreten.';
$lang['companies:publish_success']                = 'Der Artikel "%s" wurde ver&ouml;ffentlicht.';
$lang['companies:mass_publish_success']           = 'Die Artikel "%s" wurden ver&ouml;ffentlicht.';
$lang['companies:publish_error']                  = 'Ein Fehler ist aufgetreten. Es wurden keine Artikel ver&ouml;ffentlicht.';
$lang['companies:delete_success']                 = 'Der Artikel "%s" wurde gel&ouml;scht.';
$lang['companies:mass_delete_success']            = 'Die Artikel "%s" wurden gel&ouml;scht.';
$lang['companies:delete_error']                   = 'Ein Fehler ist aufgetreten. Keine Artikel wurden gel&ouml;scht.';
$lang['companies:already_exist_error']            = 'Ein Artikel mit dieser URL existiert bereits.';

$lang['companies:twitter_posted']                 = 'Gepostet "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter Fehler';

// date
$lang['companies:archive_date_format']		     = "%B %Y";
