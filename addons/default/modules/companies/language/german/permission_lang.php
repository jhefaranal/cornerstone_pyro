<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Artikel live setzen';
$lang['companies:role_edit_live']	= 'Live-Artikel bearbeiten';
$lang['companies:role_delete_live'] 	= 'Live-Artikel l&ouml;schen';

/* End of file permission_lang.php */