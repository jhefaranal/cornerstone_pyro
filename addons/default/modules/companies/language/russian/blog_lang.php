<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * PyroCMS
 * Русский перевод от Dark Preacher - dark[at]darklab.ru
 *
 * @package		PyroCMS
 * @author		Dark Preacher
 * @link			http://pyrocms.com
 */

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// подписи
$lang['companies:posted_label']									= 'Дата';
$lang['companies:posted_label_alt']							= 'Дата добавления';
$lang['companies:written_by_label']							= 'Автор';
$lang['companies:author_unknown']								= 'Неизвестно';
$lang['companies:keywords_label']				= 'Keywords'; #translate
$lang['companies:tagged_label']					= 'Tagged'; #translate
$lang['companies:category_label']								= 'Категория';
$lang['companies:post_label']										= 'Заголовок';
$lang['companies:date_label']										= 'Дата';
$lang['companies:date_at']												= 'в';
$lang['companies:time_label']										= 'Время';
$lang['companies:status_label']									= 'Статус';
$lang['companies:draft_label']										= 'Черновик';
$lang['companies:live_label']										= 'Опубликовано';
$lang['companies:content_label']									= 'Содержание';
$lang['companies:options_label']									= 'Опции';
$lang['companies:intro_label']										= 'Анонс';
$lang['companies:no_category_select_label']			= '-- нет --';
$lang['companies:new_category_label']						= 'Создать категорию';
$lang['companies:subscripe_to_rss_label']				= 'Подписаться на RSS';
$lang['companies:all_posts_label']								= 'Все статьи';
$lang['companies:posts_of_category_suffix']			= ' статьи';
$lang['companies:rss_name_suffix']								= ' Блог';
$lang['companies:rss_category_suffix']						= ' Блог';
$lang['companies:author_name_label']							= 'Автор';
$lang['companies:read_more_label']								= 'читать целиком&nbsp;&raquo;';
$lang['companies:created_hour']									= 'Время (Час)';
$lang['companies:created_minute']								= 'Время (Минута)';
$lang['companies:comments_enabled_label']				= 'Комментарии разрешены';

// заголовки
$lang['companies:create_title']									= 'Создать статью';
$lang['companies:edit_title']										= 'Редактирование статьи "%s"';
$lang['companies:archive_title']									= 'Архив';
$lang['companies:posts_title']										= 'Статьи';
$lang['companies:rss_posts_title']								= 'Статьи из %s';
$lang['companies:companies_title']										= 'Блог';
$lang['companies:list_title']										= 'Список статей';

// сообщения
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']											= 'Статьи отсутствуют.';
$lang['companies:subscripe_to_rss_desc']					= 'Получайте статьи сразу после их публикации, подпишитесь на нашу ленту новостей. Вы можете сделать это с помощью самых популярных программ для чтения электронных писем, или попробуйте <a href="http://reader.google.ru/">Google Reader</a>.';
$lang['companies:currently_no_posts']						= 'В данный момент новости отсутствуют.';
$lang['companies:post_add_success']							= 'Статья "%s" добавлена.';
$lang['companies:post_add_error']								= 'Во время добавления статьи произошла ошибка.';
$lang['companies:edit_success']									= 'Статья "%s" сохранена.';
$lang['companies:edit_error']										= 'Во время сохранения статьи произошла ошибка.';
$lang['companies:publish_success']								= 'Статья "%s" опубликована.';
$lang['companies:mass_publish_success']					= 'Статьи "%s" опубликованы.';
$lang['companies:publish_error']									= 'Во время публикации статьи произошла ошибка.';
$lang['companies:delete_success']								= 'Статья "%s" удалена.';
$lang['companies:mass_delete_success']						= 'Статьи "%s" удалены.';
$lang['companies:delete_error']									= 'Во время удаления статьи произошла ошибка.';
$lang['companies:already_exist_error']						= 'Статья с данным адресом URL уже существует.';

$lang['companies:twitter_posted']								= 'Добавлен "%s" %s';
$lang['companies:twitter_error']									= 'Ошибка Twitter\'а';

// дата
$lang['companies:archive_date_format']						= "%B %Y"; #see php strftime documentation

/* End of file companies_lang.php */