<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label'] 				= 'Geplaatst';
$lang['companies:posted_label_alt']			= 'Geplaatst op';
$lang['companies:written_by_label']			= 'Geschreven door';
$lang['companies:author_unknown']			= 'Onbekend';
$lang['companies:keywords_label']			= 'Sleutelwoorden';
$lang['companies:tagged_label']				= 'Etiket';
$lang['companies:category_label'] 			= 'Categorie';
$lang['companies:post_label'] 				= 'Post';
$lang['companies:date_label'] 				= 'Datum';
$lang['companies:date_at']					= 'op';
$lang['companies:time_label'] 				= 'Tijd';
$lang['companies:status_label'] 				= 'Status';
$lang['companies:draft_label'] 				= 'Concept';
$lang['companies:live_label'] 				= 'Live';
$lang['companies:content_label'] 			= 'Content';
$lang['companies:options_label'] 			= 'Opties';
$lang['companies:slug_label'] 				= 'URL';
$lang['companies:intro_label'] 				= 'Introductie';
$lang['companies:no_category_select_label'] 	= '-- Geen --';
$lang['companies:new_category_label'] 		= 'Voeg een categorie toe';
$lang['companies:subscripe_to_rss_label'] 	= 'Abonneer op RSS';
$lang['companies:all_posts_label'] 			= 'Alle artikelen';
$lang['companies:posts_of_category_suffix'] 	= ' artikelen';
$lang['companies:rss_name_suffix'] 			= ' Nieuws';
$lang['companies:rss_category_suffix'] 		= ' Nieuws';
$lang['companies:author_name_label'] 		= 'Auteur naam';
$lang['companies:read_more_label'] 			= 'Lees Meer&nbsp;&raquo;';
$lang['companies:created_hour']           	= 'Tijd (Uren)';
$lang['companies:created_minute']       		= 'Tijd (Minuten)';
$lang['companies:comments_enabled_label']	= 'Reacties ingeschakeld';

// titles
$lang['companies:disabled_after'] 			= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:create_title'] 				= 'Voeg artikel toe';
$lang['companies:edit_title'] 				= 'Wijzig artikel "%s"';
$lang['companies:archive_title'] 			= 'Archief';
$lang['companies:posts_title'] 				= 'Artikelen';
$lang['companies:rss_posts_title'] 			= 'Nieuws artikelen voor %s';
$lang['companies:companies_title'] 				= 'Nieuws';
$lang['companies:list_title'] 				= 'Overzicht artikelen';

// messages
$lang['companies:no_posts'] 					= 'Er zijn geen artikelen.';
$lang['companies:subscripe_to_rss_desc'] 	= 'Ontvang artikelen meteen door te abonneren op onze RSS feed. U kunt dit doen met de meeste populaire e-mail programma&acute;s, of probeer <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts'] 		= 'Er zijn momenteel geen artikelen.';
$lang['companies:post_add_success'] 			= 'Het artikel "%s" is opgeslagen.';
$lang['companies:post_add_error'] 			= 'Er is een fout opgetreden.';
$lang['companies:edit_success'] 				= 'Het artikel "%s" is opgeslagen.';
$lang['companies:edit_error'] 				= 'Er is een fout opgetreden.';
$lang['companies:publish_success'] 			= 'Het artikel "%s" is gepubliceerd.';
$lang['companies:mass_publish_success'] 		= 'De artikelen "%s" zijn gepubliceerd.';
$lang['companies:publish_error'] 			= 'Geen artikelen zijn gepubliceerd.';
$lang['companies:delete_success'] 			= 'Het artikel "%s" is verwijderd.';
$lang['companies:mass_delete_success'] 		= 'De artikelen "%s" zijn verwijderd.';
$lang['companies:delete_error'] 				= 'Geen artikelen zijn verwijderd.';
$lang['companies:already_exist_error'] 		= 'Een artikel met deze URL bestaat al.';

$lang['companies:twitter_posted']			= 'Geplaatst "%s" %s';
$lang['companies:twitter_error'] 			= 'Twitter Fout';

// date
$lang['companies:archive_date_format']		= "%B %Y";
