<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Zet artikelen online';
$lang['companies:role_edit_live']	= 'Bewerk online artikelen';
$lang['companies:role_delete_live'] 	= 'Verwijder online artikelen';