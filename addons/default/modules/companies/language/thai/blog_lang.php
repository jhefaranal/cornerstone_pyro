<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
* Thai translation.
*
* @author	Nateetorn Lertkhonsan <nateetorn.l@gmail.com>
* @package	PyroCMS  
* @link		http://pyrocms.com
* @date		2012-04-19
* @version	1.0.0
**/

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']                   = 'โพสต์แล้ว';
$lang['companies:posted_label_alt']               = 'โพสต์แล้วที่';
$lang['companies:written_by_label']				= 'เขียนโดย';
$lang['companies:author_unknown']				= 'ไม่ทราบ';
$lang['companies:keywords_label']				= 'คำหลัก';
$lang['companies:tagged_label']					= 'แท็ก';
$lang['companies:category_label']                 = 'หมวดหมู่';
$lang['companies:post_label']                     = 'โพสต์';
$lang['companies:date_label']                     = 'วันที่';
$lang['companies:date_at']                        = 'ที่';
$lang['companies:time_label']                     = 'เวลา';
$lang['companies:status_label']                   = 'สถานะ';
$lang['companies:draft_label']                    = 'ร่างบล็อก';
$lang['companies:live_label']                     = 'ไลฟ์บล็อก';
$lang['companies:content_label']                  = 'เนื้อหา';
$lang['companies:options_label']                  = 'ตัวเลือก';
$lang['companies:intro_label']                    = 'บทนำ';
$lang['companies:no_category_select_label']       = '-- ไม่มี --';
$lang['companies:new_category_label']             = 'เพิ่มหมวดหมู่';
$lang['companies:subscripe_to_rss_label']         = 'สมัครสมาชิก RSS';
$lang['companies:all_posts_label']             = 'โพสต์ทั้งหมด';
$lang['companies:posts_of_category_suffix']    = ' โพสต์';
$lang['companies:rss_name_suffix']                = ' บล็อก';
$lang['companies:rss_category_suffix']            = ' บล็อก';
$lang['companies:author_name_label']              = 'ชื่อผู้เขียน';
$lang['companies:read_more_label']                = 'อ่านเพิ่มเติม&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'สร้างเมื่อชั่วโมงที่แล้ว';
$lang['companies:created_minute']                 = 'สร้างเมื่อนาทีที่แล้ว';
$lang['companies:comments_enabled_label']         = 'ความคิดเห็นถูกเปิดใช้งาน';

// titles
$lang['companies:create_title']                   = 'เพิ่มโพสต์';
$lang['companies:edit_title']                     = 'แก้ไขโพสต์ "%s"';
$lang['companies:archive_title']                 = 'คลัง';
$lang['companies:posts_title']					= 'โพสต์';
$lang['companies:rss_posts_title']				= 'โพสต์บล็อกสำหรับ %s';
$lang['companies:companies_title']					= 'บล็อก';
$lang['companies:list_title']					= 'รายการโพสต์';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']                    = 'ไม่มีโพสต์';
$lang['companies:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'ไม่มีโพสต์ในขณะนี้';
$lang['companies:post_add_success']            = 'โพสต์ "%s" ถูกเพิ่มแล้ว';
$lang['companies:post_add_error']              = 'เกิดข้อผิดพลาด';
$lang['companies:edit_success']                   = 'ปรับปรุงโพสต์ "%s" แล้ว';
$lang['companies:edit_error']                     = 'เกิดข้อผิดพลาด';
$lang['companies:publish_success']                = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['companies:mass_publish_success']           = 'โพสต์ "%s" ได้รับการเผยแพร่';
$lang['companies:publish_error']                  = 'ไม่มีโพสต์ถูกเผยแพร่.';
$lang['companies:delete_success']                 = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['companies:mass_delete_success']            = 'โพสต์ "%s" ถูกลบแล้ว';
$lang['companies:delete_error']                   = 'ไม่มีโพสต์ถูกลบ';
$lang['companies:already_exist_error']            = 'โพสต์ลิงค์นี้มีอยู่แล้ว';

$lang['companies:twitter_posted']                 = 'โพสต์ "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter มีข้อผิดพลาด';

// date
$lang['companies:archive_date_format']		= "%B %Y";
