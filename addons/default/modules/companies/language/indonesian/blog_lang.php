<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']                   = 'Dipostkan';
$lang['companies:posted_label_alt']               = 'Dipostkan pada';
$lang['companies:written_by_label']				= 'Ditulis oleh';
$lang['companies:author_unknown']				= 'Tidak dikenal';
$lang['companies:keywords_label']				= 'Kata Kunci';
$lang['companies:tagged_label']					= 'Label';
$lang['companies:category_label']                 = 'Kategori';
$lang['companies:post_label']                     = 'Post';
$lang['companies:date_label']                     = 'Tanggal';
$lang['companies:date_at']                        = 'pada';
$lang['companies:time_label']                     = 'Waktu';
$lang['companies:status_label']                   = 'Status';
$lang['companies:draft_label']                    = 'Draft';
$lang['companies:live_label']                     = 'Publikasikan';
$lang['companies:content_label']                  = 'Konten';
$lang['companies:options_label']                  = 'Opsi';
$lang['companies:intro_label']                    = 'Pendahuluan';
$lang['companies:no_category_select_label']       = '-- tidak ada --';
$lang['companies:new_category_label']             = 'Tambah kategori';
$lang['companies:subscripe_to_rss_label']         = 'Berlangganan RSS';
$lang['companies:all_posts_label']             = 'Semua post';
$lang['companies:posts_of_category_suffix']    = ' post';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Nama penulis';
$lang['companies:read_more_label']                = 'Selengkapnya&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Dibuat pada Jam';
$lang['companies:created_minute']                 = 'Dibuat pada Menit';
$lang['companies:comments_enabled_label']         = 'Perbolehkan Komentar';

// titles
$lang['companies:create_title']                   = 'Tambah Post';
$lang['companies:edit_title']                     = 'Edit post "%s"';
$lang['companies:archive_title']                 = 'Arsip';
$lang['companies:posts_title']					= 'Post';
$lang['companies:rss_posts_title']				= 'Companies post untuk %s';
$lang['companies:companies_title']					= 'Companies';
$lang['companies:list_title']					= 'Daftar Post';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']                    = 'Tidak ada post.';
$lang['companies:subscripe_to_rss_desc']          = 'Ambil post langsung dengan berlangganan melalui RSS feed kami. Anda dapat melakukannya menggunakan hampir semua e-mail client, atau coba <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'Untuk sementara tidak ada post.';
$lang['companies:post_add_success']            = 'Post "%s" telah ditambahkan.';
$lang['companies:post_add_error']              = 'Terjadi kesalahan.';
$lang['companies:edit_success']                   = 'Post "%s" telah diperbaharui.';
$lang['companies:edit_error']                     = 'Terjadi kesalahan.';
$lang['companies:publish_success']                = 'Post "%s" telah dipublikasikan.';
$lang['companies:mass_publish_success']           = 'Post "%s" telah dipublikasikan.';
$lang['companies:publish_error']                  = 'Tidak ada post yang terpublikasi.';
$lang['companies:delete_success']                 = 'Post "%s" telah dihapus.';
$lang['companies:mass_delete_success']            = 'Post "%s" telah dihapus.';
$lang['companies:delete_error']                   = 'Tidak ada post yan terhapus.';
$lang['companies:already_exist_error']            = 'Post dengan URL ini sudah ada.';

$lang['companies:twitter_posted']                 = 'Dipostkan "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter Error';

// date
$lang['companies:archive_date_format']		= "%B %Y";
