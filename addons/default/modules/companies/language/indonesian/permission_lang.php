<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Publikasikan artikel';
$lang['companies:role_edit_live']	= 'Edit artikel terpublikasi';
$lang['companies:role_delete_live'] 	= 'Hapus artikel terpublikasi';
