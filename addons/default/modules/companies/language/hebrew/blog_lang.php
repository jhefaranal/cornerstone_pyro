<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']                   = 'Posted';
$lang['companies:posted_label_alt']               = 'Posted at';
$lang['companies:written_by_label']				= 'Written by'; #translate
$lang['companies:author_unknown']				= 'Unknown'; #translate
$lang['companies:keywords_label']				= 'Keywords'; #translate
$lang['companies:tagged_label']					= 'Tagged'; #translate
$lang['companies:category_label']                 = 'Category';
$lang['companies:post_label']                     = 'Post';
$lang['companies:date_label']                     = 'Date';
$lang['companies:date_at']                        = 'at';
$lang['companies:time_label']                     = 'Time';
$lang['companies:status_label']                   = 'Status';
$lang['companies:draft_label']                    = 'Draft';
$lang['companies:live_label']                     = 'Live';
$lang['companies:content_label']                  = 'Content';
$lang['companies:options_label']                  = 'Options';
$lang['companies:intro_label']                    = 'Introduction';
$lang['companies:no_category_select_label']       = '-- None --';
$lang['companies:new_category_label']             = 'Add a category';
$lang['companies:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['companies:all_posts_label']             = 'All posts';
$lang['companies:posts_of_category_suffix']    = ' posts';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Author name';
$lang['companies:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Created on Hour';
$lang['companies:created_minute']                 = 'Created on Minute';

// titles
$lang['companies:create_title']                   = 'הוספ פוסט';
$lang['companies:edit_title']                     = 'ערוך פוסט "%s"';
$lang['companies:archive_title']                  = 'ארכיון';
$lang['companies:posts_title']                 = 'פוסטים';
$lang['companies:rss_posts_title']             = 'Companies posts for %s';
$lang['companies:companies_title']                     = 'בלוג';
$lang['companies:list_title']                     = 'רשימת הפוסטים';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']                    = 'אין פוסטים.';
$lang['companies:subscripe_to_rss_desc']          = 'קבל פוסטי ישירות ע"י הרשמה לRSS שלנו. ניתןלעשות זאת בעזרת <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'כרגע אין פוסטים.';
$lang['companies:post_add_success']            = 'הפוסט "%s" הוסף בהצלחה.';
$lang['companies:post_add_error']              = 'התרחשה שגיעה.';
$lang['companies:edit_success']                   = 'The post "%s" was updated.';
$lang['companies:edit_error']                     = 'התרחשה שגיעה.';
$lang['companies:publish_success']                = 'The post "%s" has been published.';
$lang['companies:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['companies:publish_error']                  = 'No posts were published.';
$lang['companies:delete_success']                 = 'The post "%s" has been deleted.';
$lang['companies:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['companies:delete_error']                   = 'No posts were deleted.';
$lang['companies:already_exist_error']            = 'An post with this URL already exists.';

$lang['companies:twitter_posted']                 = 'Posted "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter Error';

// date
$lang['companies:archive_date_format']		= "%B %Y";
