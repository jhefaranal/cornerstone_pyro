<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label'] 					= 'Opublikowano w';
$lang['companies:posted_label_alt']					= 'Opublikowano ';
$lang['companies:written_by_label']					= 'Napisany przez';
$lang['companies:author_unknown']					= 'Nieznany';
$lang['companies:keywords_label']					= 'Słowa kluczowe';
$lang['companies:tagged_label']					= 'Tagi';
$lang['companies:category_label'] 					= 'Kategoria';
$lang['companies:post_label'] 					= 'Post';
$lang['companies:date_label'] 					= 'Data';
$lang['companies:date_at']						= '';
$lang['companies:time_label'] 					= 'Czas';
$lang['companies:status_label'] 					= 'Status';
$lang['companies:draft_label'] 					= 'Robocza';
$lang['companies:live_label'] 					= 'Opublikowana';
$lang['companies:content_label'] 					= 'Zawartość';
$lang['companies:options_label'] 					= 'Opcje';
$lang['companies:intro_label'] 					= 'Wprowadzenie';
$lang['companies:no_category_select_label'] 				= '-- Brak --';
$lang['companies:new_category_label'] 				= 'Dodaj kategorię';
$lang['companies:subscripe_to_rss_label'] 				= 'Subskrybuj RSS';
$lang['companies:all_posts_label'] 					= 'Wszystkie wpisy';
$lang['companies:posts_of_category_suffix'] 				= ' - wpisy';
$lang['companies:rss_name_suffix'] 					= '';
$lang['companies:rss_category_suffix'] 				= '';
$lang['companies:author_name_label'] 				= 'Imię autora';
$lang['companies:read_more_label'] 					= 'Czytaj więcej&nbsp;&raquo;';
$lang['companies:created_hour']               		    	= 'Czas (Godzina)';
$lang['companies:created_minute']              		    	= 'Czas (Minuta)';
$lang['companies:comments_enabled_label']         			= 'Komentarze aktywne';

// titles
$lang['companies:create_title'] 					= 'Dodaj wpis';
$lang['companies:edit_title'] 					= 'Edytuj wpis "%s"';
$lang['companies:archive_title'] 					= 'Archiwum';
$lang['companies:posts_title'] 					= 'Wpisy';
$lang['companies:rss_posts_title'] 					= 'Najnowsze wpisy dla %s';
$lang['companies:companies_title'] 					= 'Companies';
$lang['companies:list_title'] 					= 'Lista wpisów';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts'] 						= 'Nie ma żadnych wpisów.';
$lang['companies:subscripe_to_rss_desc'] 				= 'Bądź na bieżąco subskrybując nasz kanał RSS. Możesz to zrobić za pomocą popularnych klientów pocztowych, lub skorzystać z <a href="http://reader.google.com/">Google Reader\'a</a>.';
$lang['companies:currently_no_posts'] 				= 'W tym momencie nie ma żadnych wpisów.';
$lang['companies:post_add_success'] 					= 'Wpis "%s" został dodany.';
$lang['companies:post_add_error'] 					= 'Wystąpił błąd.';
$lang['companies:edit_success'] 					= 'Wpis "%s" został zaktualizowany.';
$lang['companies:edit_error'] 					= 'Wystąpił błąd.';
$lang['companies:publish_success'] 					= 'Wpis "%s" został opublikowany.';
$lang['companies:mass_publish_success'] 				= 'Wpisy "%s" zostały opublikowane.';
$lang['companies:publish_error'] 					= 'Żadne wpisy nie zostały opublikowane.';
$lang['companies:delete_success'] 					= 'Wpis "%s" został usunięty.';
$lang['companies:mass_delete_success'] 				= 'Wpisy "%s" zostały usunięte.';
$lang['companies:delete_error'] 					= 'Żadne wpisy nie zostały usunięte.';
$lang['companies:already_exist_error'] 				= 'Wpis z tym adresem URL już istnieje.';

$lang['companies:twitter_posted'] 					= 'Opublikowano "%s" %s';
$lang['companies:twitter_error'] 					= 'Błąd Twitter\'a';

// date
$lang['companies:archive_date_format']				= "%B %Y";