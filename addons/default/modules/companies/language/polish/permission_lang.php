<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Publikuj wpisy';
$lang['companies:role_edit_live']		= 'Edytuj opublikowane wpisy';
$lang['companies:role_delete_live'] 		= 'Usuwaj opublikowane wpisy';