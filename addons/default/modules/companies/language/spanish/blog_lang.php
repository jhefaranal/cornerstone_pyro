<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                          = 'Entrada';
$lang['companies:posts']                         = 'Entradas';

// labels
$lang['companies:posted_label']					= 'Escrito';
$lang['companies:keywords_label']				= 'Palabras clave';
$lang['companies:tagged_label']					= 'Tagged'; #translate
$lang['companies:category_label'] 				= 'Categoría';
$lang['companies:written_by_label']				= 'Escrito por';
$lang['companies:author_unknown']				= 'Desconocido';
$lang['companies:post_label'] 					= 'Entrada';
$lang['companies:date_label'] 					= 'Fecha';
$lang['companies:time_label'] 					= 'Hora';
$lang['companies:status_label'] 					= 'Estado';
$lang['companies:content_label'] 				= 'Contenido';
$lang['companies:options_label'] 				= 'Opciones';
$lang['companies:intro_label'] 					= 'Introducción';
$lang['companies:draft_label'] 					= 'Borrador';
$lang['companies:live_label'] 					= 'En vivo';
$lang['companies:no_category_select_label'] 		= '-- Ninguna --';
$lang['companies:new_category_label'] 			= 'Agregar una categoría';
$lang['companies:subscripe_to_rss_label'] 		= 'Suscribir al RSS';
$lang['companies:all_posts_label'] 				= 'Todos los artículos';
$lang['companies:posts_of_category_suffix']		= ' artículos';
$lang['companies:rss_name_suffix'] 				= ' Noticias';
$lang['companies:rss_category_suffix'] 			= ' Noticias';
$lang['companies:posted_label'] 					= 'Escrito en';
$lang['companies:author_name_label'] 			= 'Nombre del autor';
$lang['companies:read_more_label'] 				= 'Leer más &raquo;';
$lang['companies:created_hour']                 	= 'Hora (Hora)';
$lang['companies:created_minute']				= 'Hora (Minutos)';
$lang['companies:comments_enabled_label']         = 'Comentarios Habilitados';

// titles
$lang['companies:create_title'] 					= 'Crear un artículo';
$lang['companies:edit_title'] 					= 'Editar artículo "%s"';
$lang['companies:archive_title'] 				= 'Archivo';
$lang['companies:posts_title'] 					= 'Artículos';
$lang['companies:rss_posts_title'] 				= 'Artículo Companies de %s';
$lang['companies:companies_title'] 					= 'Noticias';
$lang['companies:list_title'] 					= 'Lista de artículos';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts'] 						= 'No hay artículos.';
$lang['companies:subscripe_to_rss_desc'] 		= 'Recibe nuestros artículos inmediatamente suscribiendote a nuestros feeds RSS. Puedes hacer esto utilizando los más clientes de correo electrónico mas populares o utilizando <a href="http://reader.google.com/">Google Reader</a>.';
$lang['companies:currently_no_posts'] 			= 'No hay artículos hasta el momento.';
$lang['companies:post_add_success'] 				= 'El artículo "%s" fue agregado.';
$lang['companies:post_add_error'] 				= 'Ha ocurrido un error.';
$lang['companies:edit_success'] 					= 'El artículo "%s" fue actualizado.';
$lang['companies:edit_error'] 					= 'Ha ocurrido un error.';
$lang['companies:publish_success'] 				= 'El artículo "%s" ha sido publicado.';
$lang['companies:mass_publish_success'] 			= 'Los artículos "%s" fueron publicados.';
$lang['companies:publish_error'] 				= 'No se han publicado artículos.';
$lang['companies:delete_success'] 				= 'El artículo "%s" ha sido eliminado.';
$lang['companies:mass_delete_success'] 			= 'Los artículos "%s" han sido eliminados.';
$lang['companies:delete_error'] 					= 'No se han eliminado artículos.';
$lang['companies:already_exist_error'] 			= 'Ya existe un artículo con esta URL.';

$lang['companies:twitter_posted']				= 'Escrito "%s" %s';
$lang['companies:twitter_error'] 				= 'Error de Twitter';

// date
$lang['companies:archive_date_format']			= "%B %Y";

/* End of file companies:lang.php */