<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Colocar artículos disponibles';
$lang['companies:role_edit_live']	= 'Editar artículos disponibles';
$lang['companies:role_delete_live'] 	= 'Eliminar artículos disponibles';