<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']                   = 'Opslået';
$lang['companies:posted_label_alt']               = 'Opslået på';
$lang['companies:written_by_label']				= 'Skrevet af';
$lang['companies:author_unknown']				= 'Ukendt';
$lang['companies:keywords_label']				= 'Keywords'; #translate
$lang['companies:tagged_label']					= 'Tagged'; #translate
$lang['companies:category_label']                 = 'Kategori';
$lang['companies:post_label']                     = 'Opslå';
$lang['companies:date_label']                     = 'Dato';
$lang['companies:date_at']                        = 'på';
$lang['companies:time_label']                     = 'Tid';
$lang['companies:status_label']                   = 'Status';
$lang['companies:draft_label']                    = 'Udkast';
$lang['companies:live_label']                     = 'Live';
$lang['companies:content_label']                  = 'Indhold';
$lang['companies:options_label']                  = 'Indstillinger';
$lang['companies:intro_label']                    = 'Introduktion';
$lang['companies:no_category_select_label']       = '-- Ingen --';
$lang['companies:new_category_label']             = 'Tilføj en kategori';
$lang['companies:subscripe_to_rss_label']         = 'Abonnér på RSS';
$lang['companies:all_posts_label']             = 'Alle opslag';
$lang['companies:posts_of_category_suffix']    = ' opslag';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Forfatter';
$lang['companies:read_more_label']                = 'Læs mere&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Oprettet on time';
$lang['companies:created_minute']                 = 'Oprettet on minut';
$lang['companies:comments_enabled_label']         = 'Kommentarer aktiveret';

// titles
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:create_title']                   = 'Tilføj opslag';
$lang['companies:edit_title']                     = 'Redigér opslag "%s"';
$lang['companies:archive_title']                  = 'Arkivér';
$lang['companies:posts_title']                 = 'Opslag';
$lang['companies:rss_posts_title']             = 'Companies-opslag for %s';
$lang['companies:companies_title']                     = 'Companies';
$lang['companies:list_title']                     = 'Liste opslag';

// messages
$lang['companies:no_posts']                    = 'Der er ingen opslag.';
$lang['companies:subscripe_to_rss_desc']          = 'Få opslag med det same ved at abonnere på vores RSS feed. Dette kan du gøre dette via de mest populære e-mail klienter, eller du kan prøve <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'Der er i øjeblikket ingen opslag';
$lang['companies:post_add_success']            = 'Opslaget "%s" er tilføjet.';
$lang['companies:post_add_error']              = 'Der opstod en fejl.';
$lang['companies:edit_success']                   = 'Opslaget "%s" er opdateret.';
$lang['companies:edit_error']                     = 'Der opstod en fejl.';
$lang['companies:publish_success']                = 'Opslaget "%s" er blevet publiceret.';
$lang['companies:mass_publish_success']           = 'Opslagene "%s" er blevet publiceret.';
$lang['companies:publish_error']                  = 'Ingen opslag er blevet publiceret.';
$lang['companies:delete_success']                 = 'Opslaget "%s" er slettet.';
$lang['companies:mass_delete_success']            = 'Opslagene "%s" er slettet.';
$lang['companies:delete_error']                   = 'Ingen opslag er blevet slettet.';
$lang['companies:already_exist_error']            = 'Et opslag med denne URL findes allerede.';

$lang['companies:twitter_posted']                 = 'Publiceret "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter fejl';

// date
$lang['companies:archive_date_format']		= "%B %Y";
