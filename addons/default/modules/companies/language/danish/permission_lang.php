<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Læg artikler online';
$lang['companies:role_edit_live']	= 'Redigér online artikler';
$lang['companies:role_delete_live'] 	= 'Slet online artikler';