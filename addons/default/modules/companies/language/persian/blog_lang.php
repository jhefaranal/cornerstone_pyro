<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                          = 'پست';
$lang['companies:posts']                         = 'پست ها';

// labels
$lang['companies:posted_label']                  = 'ارسال شده';
$lang['companies:posted_label_alt']               = 'ارسال شده';
$lang['companies:written_by_label']		 = 'نوشته شده توسط';
$lang['companies:author_unknown']	  	 = 'نامشخص';
$lang['companies:keywords_label']		 = 'کلمات کلیدی';
$lang['companies:tagged_label']			 = 'تگ ها';
$lang['companies:category_label']                = 'مجموعه';
$lang['companies:post_label']                    = 'پست';
$lang['companies:date_label']                    = 'تاریخ';
$lang['companies:date_at']                       = 'در';
$lang['companies:time_label']                    = 'زمان';
$lang['companies:status_label']                  = 'وضعیت';
$lang['companies:draft_label']                    = 'پیشنویس';
$lang['companies:live_label']                     = 'ارسال شده';
$lang['companies:content_label']                  = 'محتوا';
$lang['companies:options_label']                  = 'تنظیمات';
$lang['companies:intro_label']                    = 'چکیده';
$lang['companies:no_category_select_label']       = '-- هیچیک --';
$lang['companies:new_category_label']             = 'مجموعه جدید';
$lang['companies:subscripe_to_rss_label']         = 'عضویت در RSS';
$lang['companies:all_posts_label']                = 'همه ی  پست ها';
$lang['companies:posts_of_category_suffix']       = ' پست ها';
$lang['companies:rss_name_suffix']                = ' بلاگ';
$lang['companies:rss_category_suffix']            = ' بلاگ';
$lang['companies:author_name_label']              = 'نام نویسنده';
$lang['companies:read_more_label']                = 'مشاهده جزئیات';
$lang['companies:created_hour']                   = 'ایجاد شده در ساعت';
$lang['companies:created_minute']                 = 'ایجاد شده در دقیقه ی ';
$lang['companies:comments_enabled_label']         = 'فعال کردن کامنت ها';

// titles
$lang['companies:create_title']                   = 'پست جدید';
$lang['companies:edit_title']                     = 'ویرایش پست "%s"';
$lang['companies:archive_title']                 = 'آرشیو';
$lang['companies:posts_title']					= 'پست ها';
$lang['companies:rss_posts_title']				= 'ارسال های مربوط به %s';
$lang['companies:companies_title']					= 'بلاگ';
$lang['companies:list_title']					= 'لیست پست ها';

// messages
$lang['companies:disabled_after'] 		= 'ارسال نظرات پس از %s غیر فعال شده است.';
$lang['companies:no_posts']                      = 'هیچ پستی وجود ندارد';
$lang['companies:subscripe_to_rss_desc']          = 'مشترک خبرخوانRSS ما بشوید.';
$lang['companies:currently_no_posts']          = 'در حال حاضر هیچ پستی وجود ندارد.';
$lang['companies:post_add_success']            = '"%s" اضافه شد.';
$lang['companies:post_add_error']              = 'خطایی رخ داده است.';
$lang['companies:edit_success']                   = '"%s" آپدیت شد.';
$lang['companies:edit_error']                     = 'خطایی رخ داده است.';
$lang['companies:publish_success']                = 'پست "%s" منتشر شد.';
$lang['companies:mass_publish_success']           = 'پست های "%s" منتشر شدند.';
$lang['companies:publish_error']                  = 'هیچ پستی منتشر نشد';
$lang['companies:delete_success']                 = 'پست "%s" دلیت شد.';
$lang['companies:mass_delete_success']            = 'پست های "%s" دلیت شدند.';
$lang['companies:delete_error']                   = 'هیچ پستی دلیت نشد.';
$lang['companies:already_exist_error']            = 'همینک  یک پست با این URL موجود است.';

$lang['companies:twitter_posted']                 = 'پست شد "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter خطای';

// date
$lang['companies:archive_date_format']		= "%B %Y";
