<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'تغییر یک پست به حالت منتشر شده';
$lang['companies:role_edit_live']        = 'ویرایش پست های منتظر شده';
$lang['companies:role_delete_live'] 	= 'حذف پست های منتشر شده';