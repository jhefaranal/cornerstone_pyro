<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live'] = 'Να δημοσιεύει άρθρα';
$lang['companies:role_edit_live'] = 'Επεξεργασία δημοσιευμένων άρθρων';
$lang['companies:role_delete_live'] 	= 'Διαγραφή δημοσιευμένων άρθρων';