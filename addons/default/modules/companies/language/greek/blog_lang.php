<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                      = 'Ανάρτηση';
$lang['companies:posts']                     = 'Αναρτήσεις';

// labels
$lang['companies:posted_label'] 		= 'Δημοσιεύτηκε';
$lang['companies:posted_label_alt'] 		= 'Δημοσιεύτηκε στις';
$lang['companies:written_by_label'] 		= 'Γράφτηκε από';
$lang['companies:author_unknown'] 		= 'Άγνωστο';
$lang['companies:keywords_label'] 		= 'Λέξεις κλειδιά';
$lang['companies:tagged_label'] 		= 'Ετικέτες';
$lang['companies:category_label'] 		= 'Κατηγορία';
$lang['companies:post_label'] 		= 'Δημοσίευση';
$lang['companies:date_label'] 		= 'Ημερομηνία';
$lang['companies:date_at'] 			= 'στις';
$lang['companies:time_label'] 		= 'Χρόνος';
$lang['companies:status_label'] 		= 'Κατάσταση';
$lang['companies:draft_label'] 		= 'Πρόχειρο';
$lang['companies:live_label'] 		= 'Δημοσιευμένο';
$lang['companies:content_label'] 		= 'Περιεχόμενο';
$lang['companies:options_label'] 		= 'Επιλογές';
$lang['companies:intro_label'] 		= 'Εισαγωγή';
$lang['companies:no_category_select_label'] 	= '-- Καμμία --';
$lang['companies:new_category_label'] 	= 'Προσθήκη κατηγορίας';
$lang['companies:subscripe_to_rss_label'] 	= 'Εγγραφή στο RSS';
$lang['companies:all_posts_label'] 		= 'Όλες οι δημοσιεύσεις';
$lang['companies:posts_of_category_suffix'] 	= ' δημοσιεύσεις';
$lang['companies:rss_name_suffix'] 		= ' Ιστολόγιο';
$lang['companies:rss_category_suffix'] 	= ' Ιστολόγιο';
$lang['companies:author_name_label'] 	= 'Όνομα συγγραφέα';
$lang['companies:read_more_label'] 		= 'Διαβάστε περισσότερα &raquo;';
$lang['companies:created_hour'] 		= 'Ώρα δημιουργίας';
$lang['companies:created_minute'] 		= 'Λεπτό δημιουργίας';
$lang['companies:comments_enabled_label'] 	= 'Σχόλια Ενεργά';

// titles
$lang['companies:create_title'] 		= 'Προσθήκη δημοσίευσης';
$lang['companies:edit_title'] 		= 'Επεξεργασία δημοσίευσης "%s"';
$lang['companies:archive_title'] 		= 'Αρχειοθήκη';
$lang['companies:posts_title'] 		= 'Δημοσιεύσεις';
$lang['companies:rss_posts_title'] 		= 'Δημοσιεύσεις ιστολογίου για %s';
$lang['companies:companies_title'] 		= 'Ιστολόγιο';
$lang['companies:list_title'] 		= 'Λίστα δημοσιεύσεων';

// messages
$lang['companies:disabled_after'] 				= 'Η ανάρτηση σχολίων μετά τις %s έχει απενεργοποιηθεί.';
$lang['companies:no_posts'] 			= 'Δεν υπάρχουν δημοσιεύσεις.';
$lang['companies:subscripe_to_rss_desc'] 	= 'Μπορείτε να λαμβάνετε νεότερες δημοσιεύσεις με το να εγγραφείτε στην τροφοδοσία RSS μας. Μπορείτε να το κάνετε χρησιμοποιώντας τα δημοφιλή προγράμματα e-mail, ή με το <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts'] 	= 'Προς το παρόν δεν υπάρχουν δημοσιεύσεις.';
$lang['companies:post_add_success'] 		= 'Η δημοσίευση "%s" προστέθηκε.';
$lang['companies:post_add_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['companies:edit_success'] 		= 'Η δημοσίευση "%s" ενημερώθηκε.';
$lang['companies:edit_error'] 		= 'Συνέβη κάποιο σφάλμα.';
$lang['companies:publish_success'] 		= 'Η δημοσίευση "%s" αναρτήθηκε.';
$lang['companies:mass_publish_success'] 	= 'Οι δημοσιεύσεις "%s" αναρτήθηκαν.';
$lang['companies:publish_error'] 		= 'Δεν αναρτήθηκε καμία δημοσίευση.';
$lang['companies:delete_success'] 		= 'Η δημοσίευση "%s" διαγράφηκε.';
$lang['companies:mass_delete_success'] 	= 'Οι δημοσιεύσεις "%s" διαγράφηκαν.';
$lang['companies:delete_error'] 		= 'Δεν διαγράφηκε καμία δημοσίευση.';
$lang['companies:already_exist_error'] 	= 'Υπάρχει ήδη μια δημοσίευση με αυτό το URL.';

$lang['companies:twitter_posted'] 		= 'Δημοσιεύτηκε "%s" %s';
$lang['companies:twitter_error'] 		= 'Σφάλμα Twitter';

// date
$lang['companies:archive_date_format'] 	= "%B %Y";
