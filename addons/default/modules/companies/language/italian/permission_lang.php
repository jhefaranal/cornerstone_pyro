<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Pubblicare nuovi articoli';
$lang['companies:role_edit_live']	= 'Modificare articoli pubblicati'; 
$lang['companies:role_delete_live'] 	= 'Cancellare articoli pubblicati'; 