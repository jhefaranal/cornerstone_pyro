<?php
/* Translation made Nicola Tudino */
/* Date 07/11/2010 */

$lang['companies:post']                 = 'Articolo';
$lang['companies:posts']                   = 'Articoli';

// labels
$lang['companies:posted_label'] 			= 'Pubblicato';
$lang['companies:posted_label_alt']			= 'Pubblicato alle';
$lang['companies:written_by_label']				= 'Scritto da';
$lang['companies:author_unknown']				= 'Sconosciuto';
$lang['companies:keywords_label']				= 'Parole chiave'; 
$lang['companies:tagged_label']					= 'Taggato';
$lang['companies:category_label'] 			= 'Categoria';
$lang['companies:post_label'] 			= 'Articolo';
$lang['companies:date_label'] 			= 'Data';
$lang['companies:date_at']				= 'alle';
$lang['companies:time_label'] 			= 'Ora';
$lang['companies:status_label'] 			= 'Stato';
$lang['companies:draft_label'] 			= 'Bozza';
$lang['companies:live_label'] 			= 'Pubblicato';
$lang['companies:content_label'] 			= 'Contenuto';
$lang['companies:options_label'] 			= 'Opzioni';
$lang['companies:intro_label'] 			= 'Introduzione';
$lang['companies:no_category_select_label'] 		= '-- Nessuna --';
$lang['companies:new_category_label'] 		= 'Aggiungi una categoria';
$lang['companies:subscripe_to_rss_label'] 		= 'Abbonati all\'RSS';
$lang['companies:all_posts_label'] 		= 'Tutti gli articoli';
$lang['companies:posts_of_category_suffix'] 	= ' articoli';
$lang['companies:rss_name_suffix'] 			= ' Notizie';
$lang['companies:rss_category_suffix'] 		= ' Notizie';
$lang['companies:author_name_label'] 		= 'Nome autore';
$lang['companies:read_more_label'] 			= 'Leggi tutto&nbsp;&raquo;';
$lang['companies:created_hour']                  = 'Time (Ora)'; 
$lang['companies:created_minute']                = 'Time (Minuto)';
$lang['companies:comments_enabled_label']         = 'Commenti abilitati';

// titles
$lang['companies:create_title'] 			= 'Aggiungi un articolo';
$lang['companies:edit_title'] 			= 'Modifica l\'articolo "%s"';
$lang['companies:archive_title'] 			= 'Archivio';
$lang['companies:posts_title'] 			= 'Articoli';
$lang['companies:rss_posts_title'] 		= 'Notizie per %s';
$lang['companies:companies_title'] 			= 'Notizie';
$lang['companies:list_title'] 			= 'Elenco articoli';

// messages
$lang['companies:disabled_after'] 				= 'Non sarà più possibile inserire commenti dopo %s.';
$lang['companies:no_posts'] 			= 'Non ci sono articoli.';
$lang['companies:subscripe_to_rss_desc'] 		= 'Ricevi gli articoli subito abbonandoti al nostro feed RSS. Lo puoi fare con i comuni programmi di posta elettronica, altrimenti prova <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts'] 		= 'Non ci sono articoli al momento.';
$lang['companies:post_add_success'] 		= 'L\'articolo "%s" è stato aggiunto.';
$lang['companies:post_add_error'] 		= 'C\'è stato un errore.';
$lang['companies:edit_success'] 			= 'L\'articolo "%s" è stato modificato.';
$lang['companies:edit_error'] 			= 'C\'è stato un errore.';
$lang['companies:publish_success'] 			= 'L\'articolo "%s" è stato pubblicato.';
$lang['companies:mass_publish_success'] 		= 'Gli articoli "%s" sono stati pubblicati.';
$lang['companies:publish_error'] 			= 'Gli articoli non saranno pubblicati.';
$lang['companies:delete_success'] 			= 'L\'articolo "%s" è stato eliminato.';
$lang['companies:mass_delete_success'] 		= 'Gli articoli "%s" sono stati eliminati.';
$lang['companies:delete_error'] 			= 'Nessun articolo è stato eliminato.';
$lang['companies:already_exist_error'] 		= 'Un articolo con questo URL esiste già.';

$lang['companies:twitter_posted']			= 'Inviato "%s" %s';
$lang['companies:twitter_error'] 			= 'Errore per Twitter';

// date
$lang['companies:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation

?>