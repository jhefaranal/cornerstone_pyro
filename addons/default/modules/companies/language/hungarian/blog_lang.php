<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                           = 'Bejegyzés';
$lang['companies:posts']                          = 'Bejegyzések';

// labels
$lang['companies:posted_label']                   = 'Bejegyezve';
$lang['companies:posted_label_alt']               = 'Bejegyezve';
$lang['companies:written_by_label']               = 'Írta';
$lang['companies:author_unknown']                 = 'Ismeretlen';
$lang['companies:keywords_label']                 = 'Kulcsszavak';
$lang['companies:tagged_label']                   = 'Tagged';
$lang['companies:category_label']                 = 'Kategória';
$lang['companies:post_label']                     = 'Bejegyzés';
$lang['companies:date_label']                     = 'Dátum';
$lang['companies:date_at']                        = '';
$lang['companies:time_label']                     = 'Idő';
$lang['companies:status_label']                   = 'Állapot';
$lang['companies:draft_label']                    = 'Piszkozat';
$lang['companies:live_label']                     = 'Élő';
$lang['companies:content_label']                  = 'Tartalom';
$lang['companies:options_label']                  = 'Beállítások';
$lang['companies:intro_label']                    = 'Bevezető';
$lang['companies:no_category_select_label']       = '-- Nincs --';
$lang['companies:new_category_label']             = 'Kategória hozzáadása';
$lang['companies:subscripe_to_rss_label']         = 'Feliratkozás RSS-re';
$lang['companies:all_posts_label']                = 'Összes bejegyzés';
$lang['companies:posts_of_category_suffix']       = ' bejegyzések';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Szerző neve';
$lang['companies:read_more_label']                = 'Tovább&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Létrehozva pár órája';
$lang['companies:created_minute']                 = 'Létrehozva pár perce';
$lang['companies:comments_enabled_label']         = 'Hozzászólás engedélyezése';

// titles
$lang['companies:create_title']                   = 'Bejegyzés hozzáadása';
$lang['companies:edit_title']                     = 'A(z) "%s" bejegyzés szerkesztése';
$lang['companies:archive_title']                  = 'Archívum';
$lang['companies:posts_title']                    = 'Bejegyzések';
$lang['companies:rss_posts_title']                = 'Companies bejegyzés %s-ért';
$lang['companies:companies_title']                     = 'Companies';
$lang['companies:list_title']                     = 'Bejegyzések listája';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']                       = 'Nincs bejegyzés.';
$lang['companies:subscripe_to_rss_desc']          = 'A bejegyzést jelentesd meg saját RSS-en. Ezt meg lehet tenni az ismertebb levelezőkkel vagy a <a href="http://reader.google.co.uk/">Google Reader</a>rel.';
$lang['companies:currently_no_posts']             = 'Ebben a pillanatban még nincs bejegyzés.';
$lang['companies:post_add_success']               = 'A(z) "%s" bejegyzés sikeresen hozzáadva.';
$lang['companies:post_add_error']                 = 'A bejegyzés hozzáadása sikertelen.';
$lang['companies:edit_success']                   = 'A(z) "%s" bejegyzés sikeresen módosítva.';
$lang['companies:edit_error']                     = 'A bejegyzés módosítása sikertelen.';
$lang['companies:publish_success']                = 'A(z) "%s" bejegyzés közzétéve.';
$lang['companies:mass_publish_success']           = 'A(z) "%s" bejegyzés már közzétéve.';
$lang['companies:publish_error']                  = 'Nincs bejegyzés közzétéve.';
$lang['companies:delete_success']                 = 'A(z) "%s" bejegyzés törölve.';
$lang['companies:mass_delete_success']            = 'A(z) "%s" bejegyzés már törölve.';
$lang['companies:delete_error']                   = 'Nincs törölhető bejegyzés.';
$lang['companies:already_exist_error']            = 'Egy bejegyzés már létezik ezzel a hivatkozással.';

$lang['companies:twitter_posted']                 = 'Bejegyezve "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter Hiba';

// date
$lang['companies:archive_date_format']            = "%B %Y";
