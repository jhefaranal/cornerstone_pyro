<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Bejegyzés élővé tétele';
$lang['companies:role_edit_live']            = 'Élő bejegyzés szerkesztése';
$lang['companies:role_delete_live']          = 'Élő bejegyzés törlése';