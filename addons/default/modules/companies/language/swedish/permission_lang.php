<?php defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * Swedish translation.
 *
 * @author		marcus@incore.se
 * @package		PyroCMS  
 * @link		http://pyrocms.com
 * @date		2012-10-22
 * @version		1.1.0
 */

$lang['companies:role_put_live'] = 'Publicera artiklar';
$lang['companies:role_edit_live'] = 'Redigera publika artiklar';
$lang['companies:role_delete_live'] = 'Radera publika artiklar';


/* End of file permission_lang.php */  
/* Location: system/cms/modules/companies/language/swedish/permission_lang.php */  
