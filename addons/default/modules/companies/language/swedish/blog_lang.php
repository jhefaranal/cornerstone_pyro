<?php defined('BASEPATH') or exit('No direct script access allowed');

 /**
 * Swedish translation.
 *
 * @author		marcus@incore.se
 * @package		PyroCMS  
 * @link		http://pyrocms.com
 * @date		2012-10-23
 * @version		1.1.0
 */

$lang['companies:post'] = 'Inlägg';
$lang['companies:posts'] = 'Inlägg';
$lang['companies:posted_label'] = 'Skriven';
$lang['companies:posted_label_alt'] = 'Skriven den';
$lang['companies:written_by_label'] = 'Skriven av';
$lang['companies:author_unknown'] = 'Okänd';
$lang['companies:keywords_label'] = 'Nyckelord';
$lang['companies:tagged_label'] = 'Taggad';
$lang['companies:category_label'] = 'Kategori';
$lang['companies:post_label'] = 'Inlägg';
$lang['companies:date_label'] = 'Datum';
$lang['companies:date_at'] = 'den';
$lang['companies:time_label'] = 'Tid';
$lang['companies:status_label'] = 'Status';
$lang['companies:draft_label'] = 'Utkast';
$lang['companies:live_label'] = 'Publik';
$lang['companies:content_label'] = 'Innehåll';
$lang['companies:options_label'] = 'Val';
$lang['companies:intro_label'] = 'Introduktion';
$lang['companies:no_category_select_label'] = '-- Ingen --';
$lang['companies:new_category_label'] = 'Lägg till en kategori';
$lang['companies:subscripe_to_rss_label'] = 'Prenumerera på RSS';
$lang['companies:all_posts_label'] = 'Alla inlägg';
$lang['companies:posts_of_category_suffix'] = 'inlägg';
$lang['companies:rss_name_suffix'] = 'Companiesg';
$lang['companies:rss_category_suffix'] = 'Companiesg';
$lang['companies:author_name_label'] = 'Skribent';
$lang['companies:read_more_label'] = 'Läs mer »';
$lang['companies:created_hour'] = 'Skapad, timme';
$lang['companies:created_minute'] = 'Skapad, minut';
$lang['companies:comments_enabled_label'] = 'Kommentarer aktiverad';
$lang['companies:create_title'] = 'Lägg till post';
$lang['companies:edit_title'] = 'Redigera post "%s"';
$lang['companies:archive_title'] = 'Arkiv';
$lang['companies:posts_title'] = 'Inlägg';
$lang['companies:rss_posts_title'] = 'Companiesginlägg för %s';
$lang['companies:companies_title'] = 'Companiesg';
$lang['companies:list_title'] = 'Lista inlägg';
$lang['companies:disabled_after'] = 'Skickade kommentarer efter %s har blivit inaktiverade.';
$lang['companies:no_posts'] = 'Det finns inga poster';
$lang['companies:subscripe_to_rss_desc'] = 'Få inlägg direkt genom att prenumerera på vårt RSS-flöde. Du flesta populära e-postklienter har stöd för detta, eller prova <a href="http://reader.google.co.uk/">Google Reader</ a>.';
$lang['companies:currently_no_posts'] = 'Det finns inga poster just nu';
$lang['companies:post_add_success'] = 'Inlägget "%s" har lagts till.';
$lang['companies:post_add_error'] = 'Ett fel inträffade.';
$lang['companies:edit_success'] = 'Inlägget "%s" uppdaterades.';
$lang['companies:edit_error'] = 'Ett fel inträffade.';
$lang['companies:publish_success'] = 'Inlägget "%s" publicerades.';
$lang['companies:mass_publish_success'] = 'Inläggen "%s" publicerades.';
$lang['companies:publish_error'] = 'Inga inlägg publicerades';
$lang['companies:delete_success'] = 'Inlägget "%s" har raderats.';
$lang['companies:mass_delete_success'] = 'Inläggen "%s" har raderats.';
$lang['companies:delete_error'] = 'Inga inlägg raderades';
$lang['companies:already_exist_error'] = 'Ett inlägg med denna URL finns redan';
$lang['companies:twitter_posted'] = 'Sparad "%s" %s';
$lang['companies:twitter_error'] = 'Twitterfel';
$lang['companies:archive_date_format'] = '%B\' %Y';


/* End of file companies_lang.php */  
/* Location: system/cms/modules/companies/language/swedish/companies_lang.php */  
