<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                          = 'Įrašas';
$lang['companies:posts']                         = 'Įrašai';

// labels
$lang['companies:posted_label']                  = 'Paskelbta';
$lang['companies:posted_label_alt']              = 'Paskelbta...';
$lang['companies:written_by_label']              = 'Autorius';
$lang['companies:author_unknown']				= 'Nežinomas';
$lang['companies:keywords_label']				= 'Raktažodžiai';
$lang['companies:tagged_label']					= 'Žymėtas';
$lang['companies:category_label']                = 'Kategorija';
$lang['companies:post_label']                    = 'Įrašas';
$lang['companies:date_label']                    = 'Data';
$lang['companies:date_at']                       = '';
$lang['companies:time_label']                    = 'Laikas';
$lang['companies:status_label']                  = 'Statusas';
$lang['companies:draft_label']                   = 'Projektas';
$lang['companies:live_label']                    = 'Gyvai';
$lang['companies:content_label']                 = 'Turinys';
$lang['companies:options_label']                 = 'Funkcijos';
$lang['companies:title_label']                   = 'Pavadinimas';
$lang['companies:slug_label']                    = 'URL';
$lang['companies:intro_label']                   = 'Įžanga';
$lang['companies:no_category_select_label']      = '-- Nėra --';
$lang['companies:new_category_label']            = 'Pridėti kategoriją';
$lang['companies:subscripe_to_rss_label']        = 'Prenumeruoti RSS';
$lang['companies:all_posts_label']               = 'Visi įrašai';
$lang['companies:posts_of_category_suffix']      = ' įrašai';
$lang['companies:rss_name_suffix']               = ' Naujienos';
$lang['companies:rss_category_suffix']           = ' Naujienos';
$lang['companies:author_name_label']             = 'Autoriaus vardas';
$lang['companies:read_more_label']               = 'Plačiau&nbsp;&raquo;';
$lang['companies:created_hour']                  = 'Sukurta valandą';
$lang['companies:created_minute']                = 'Sukurta minutę';
$lang['companies:comments_enabled_label']        = 'Įjungti komentarus?';

// titles
$lang['companies:create_title']                  = 'Pridėti įrašą';
$lang['companies:edit_title']                    = 'Redaguoti įrašą "%s"';
$lang['companies:archive_title']                 = 'Archyvas';
$lang['companies:posts_title']                   = 'Įrašai';
$lang['companies:rss_posts_title']               = 'Naujienų įrašai %s';
$lang['companies:companies_title']                    = 'Naujiena';
$lang['companies:list_title']                    = 'Įrašų sąrašas';

// messages
$lang['companies:disabled_after'] 				= 'Paskelbti komentarai po %s buvo atjungti.';
$lang['companies:no_posts']                      = 'Nėra įrašų.';
$lang['companies:subscripe_to_rss_desc']         = 'Gaukite žinutes iš karto prenumeruodami į RSS kanalą. Jūs galite tai padaryti per populiariausias elektroninio pašto paskyras, arba bandykite <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']            = 'Šiuo metu nėra įrašų.';
$lang['companies:post_add_success']              = 'Įrašas "%s" pridėtas.';
$lang['companies:post_add_error']                = 'Įvyko klaida.';
$lang['companies:edit_success']                  = 'Įrašas "%s" atnaujintas.';
$lang['companies:edit_error']                    = 'Įvyko klaida.';
$lang['companies:publish_success']               = 'Įrašas "%s" paskelbtas.';
$lang['companies:mass_publish_success']          = 'Įrašai "%s" pasklbti.';
$lang['companies:publish_error']                 = 'Nėra paskelbtų įrašų.';
$lang['companies:delete_success']                = 'Įrašas "%s" ištrintas.';
$lang['companies:mass_delete_success']           = 'Įrašai "%s" ištrinti.';
$lang['companies:delete_error']                  = 'Nėra ištrintų įrašų.';
$lang['companies:already_exist_error']           = 'Įrašas su šiuo URL jau egzistuoja.';

$lang['companies:twitter_posted']                = 'Įrašyta "%s" %s';
$lang['companies:twitter_error']                 = 'Twitter nepasiekiamas';

// date
$lang['companies:archive_date_format']           = "%B %Y";
