<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']				= 'Escrito';
$lang['companies:posted_label_alt']			= 'Escrito em';
$lang['companies:written_by_label']			= 'Por';
$lang['companies:author_unknown']			= 'Desconhecido';
$lang['companies:keywords_label']			= 'Palavras-chave';
$lang['companies:tagged_label']				= 'Tagged'; #translate
$lang['companies:category_label']			= 'Categoria';
$lang['companies:post_label'] 				= 'Artigo';
$lang['companies:date_label'] 				= 'Data';
$lang['companies:date_at']					= 'às';
$lang['companies:time_label'] 				= 'Hora';
$lang['companies:status_label'] 				= 'Situação';
$lang['companies:draft_label'] 				= 'Rascunho';
$lang['companies:live_label'] 				= 'Publico';
$lang['companies:content_label']				= 'Conteúdo';
$lang['companies:options_label']				= 'Opções';
$lang['companies:intro_label'] 				= 'Introdução';
$lang['companies:no_category_select_label']	= '-- Nenhuma --';
$lang['companies:new_category_label'] 		= 'Adicionar uma categoria';
$lang['companies:subscripe_to_rss_label'] 	= 'Assinar o RSS';
$lang['companies:all_posts_label'] 			= 'Todos os artigos';
$lang['companies:posts_of_category_suffix'] 	= ' artigos';
$lang['companies:rss_name_suffix']			= ' Companies';
$lang['companies:rss_category_suffix']		= ' Companies';
$lang['companies:author_name_label']			= 'Nome do autor';
$lang['companies:read_more_label']			= 'Leia mais &raquo;';
$lang['companies:created_hour']				= 'Horário (Hora)';
$lang['companies:created_minute']			= 'Horário (Minuto)';
$lang['companies:comments_enabled_label']	= 'Habilitar comentários';

// titles
$lang['companies:create_title']				= 'Adicionar artigo';
$lang['companies:edit_title']				= 'Editar artigo "%s"';
$lang['companies:archive_title'] 			= 'Arquivo';
$lang['companies:posts_title'] 				= 'Artigos';
$lang['companies:rss_posts_title'] 			= 'Artigos novos para %s';
$lang['companies:companies_title']				= 'Companies';
$lang['companies:list_title']				= 'Listar artigos';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']					= 'Nenhum artigo.';
$lang['companies:subscripe_to_rss_desc']		= 'Fique por dentro das novidades do companies assinando o nosso feed RSS. Pode fazer isto pelos mais populares leitores de e-mail ou pode experimentar o <a rel="nofollow" href="http://reader.google.com/">Google Reader</a>.';
$lang['companies:currently_no_posts']		= 'Não existem artigos no momento.';
$lang['companies:post_add_success']			= 'O artigo "%s" foi adicionado.';
$lang['companies:post_add_error']			= 'Ocorreu um erro.';
$lang['companies:edit_success']				= 'O artigo "%s" foi actualizado.';
$lang['companies:edit_error']				= 'Ocorreu um erro.';
$lang['companies:publish_success']			= 'O artigo "%s" foi publicado.';
$lang['companies:mass_publish_success']		= 'Os artigos "%s" foram publicados.';
$lang['companies:publish_error']				= 'Nenhum artigo foi publicado.';
$lang['companies:delete_success']			= 'O artigo "%s" foi removido.';
$lang['companies:mass_delete_success']		= 'Os artigos "%s" foram removidos.';
$lang['companies:delete_error']				= 'Nenhuma publicação foi removida.';
$lang['companies:already_exist_error']		= 'Um artigo com mesmo campo %s já existe.';

$lang['companies:twitter_posted']			= 'Escrito "%s" %s';
$lang['companies:twitter_error']				= 'Erro do Twitter';

// date
$lang['companies:archive_date_format']		= "%B de %Y";