<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Publicar artigos';
$lang['companies:role_edit_live']	= 'Editar artigos publicados';
$lang['companies:role_delete_live'] 	= 'Remover artigos publicados';