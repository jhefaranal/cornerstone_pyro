<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live'] = 'نشر التدوينات';
$lang['companies:role_edit_live'] = 'تعديل التدوينات المنشورة';
$lang['companies:role_delete_live'] 	= 'حذف التدوينات المنشورة';
