<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'تدوينة';
$lang['companies:posts']                   = 'تدوينات';

// labels
$lang['companies:posted_label'] 			= 'تاريخ النشر';
$lang['companies:posted_label_alt']			= 'نشر في';
$lang['companies:written_by_label']				= 'كتبها';
$lang['companies:author_unknown']				= 'مجهول';
$lang['companies:keywords_label']				= 'كلمات البحث';
$lang['companies:tagged_label']					= 'موسومة';
$lang['companies:category_label'] 			= 'التصنيف';
$lang['companies:post_label'] 			= 'إرسال';
$lang['companies:date_label'] 			= 'التاريخ';
$lang['companies:date_at']				= 'عند';
$lang['companies:time_label'] 			= 'الوقت';
$lang['companies:status_label'] 			= 'الحالة';
$lang['companies:draft_label'] 			= 'مسودّة';
$lang['companies:live_label'] 			= 'منشور';
$lang['companies:content_label'] 			= 'المُحتوى';
$lang['companies:options_label'] 			= 'خيارات';
$lang['companies:intro_label'] 			= 'المٌقدّمة';
$lang['companies:no_category_select_label'] 		= '-- لاشيء --';
$lang['companies:new_category_label'] 		= 'إضافة تصنيف';
$lang['companies:subscripe_to_rss_label'] 		= 'اشترك في خدمة RSS';
$lang['companies:all_posts_label'] 		= 'جميع التدوينات';
$lang['companies:posts_of_category_suffix'] 	= ' &raquo; التدوينات';
$lang['companies:rss_name_suffix'] 			= ' &raquo; المُدوّنة';
$lang['companies:rss_category_suffix'] 		= ' &raquo; المُدوّنة';
$lang['companies:author_name_label'] 		= 'إسم الكاتب';
$lang['companies:read_more_label'] 			= 'إقرأ المزيد&nbsp;&raquo;';
$lang['companies:created_hour']                  = 'الوقت (الساعة)';
$lang['companies:created_minute']                = 'الوقت (الدقيقة)';
$lang['companies:comments_enabled_label']         = 'إتاحة التعليقات';

// titles
$lang['companies:create_title'] 			= 'إضافة مقال';
$lang['companies:edit_title'] 			= 'تعديل التدوينة "%s"';
$lang['companies:archive_title'] 			= 'الأرشيف';
$lang['companies:posts_title'] 			= 'التدوينات';
$lang['companies:rss_posts_title'] 		= 'تدوينات %s';
$lang['companies:companies_title'] 			= 'المُدوّنة';
$lang['companies:list_title'] 			= 'سرد التدوينات';

// messages
$lang['companies:disabled_after'] 				= 'تم تعطيل التعليقات بعد %s.';
$lang['companies:no_posts'] 			= 'لا يوجد تدوينات.';
$lang['companies:subscripe_to_rss_desc'] 		= 'اطلع على آخر التدوينات مباشرة بالاشتراك بخدمة RSS. يمكنك القيام بذلك من خلال معظم برامج البريد الإلكتروني الشائعة، أو تجربة <a href="http://reader.google.com/">قارئ جوجل</a>.';
$lang['companies:currently_no_posts'] 		= 'لا يوجد تدوينات حالياً.';
$lang['companies:post_add_success'] 		= 'تمت إضافة التدوينة "%s".';
$lang['companies:post_add_error'] 		= 'حدث خطأ.';
$lang['companies:edit_success'] 			= 'تم تحديث التدوينة "%s".';
$lang['companies:edit_error'] 			= 'حدث خطأ.';
$lang['companies:publish_success'] 			= 'تم نشر التدوينة "%s".';
$lang['companies:mass_publish_success'] 		= 'تم نشر التدوينات "%s".';
$lang['companies:publish_error'] 			= 'لم يتم نشر أي تدوينات.';
$lang['companies:delete_success'] 			= 'تم حذف التدوينة "%s".';
$lang['companies:mass_delete_success'] 		= 'تم حذف التدوينات "%s".';
$lang['companies:delete_error'] 			= 'لم يتم حذف أي تدوينات.';
$lang['companies:already_exist_error'] 		= 'يوجد مقال له عنوان URL مطابق.';

$lang['companies:twitter_posted']			= 'منشور في "%s" %s';
$lang['companies:twitter_error'] 			= 'خطأ في تويتر';

// date
$lang['companies:archive_date_format']		= "%B %Y";

?>
