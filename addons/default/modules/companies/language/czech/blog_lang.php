<?php

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label'] 			= 'Odesláno';
$lang['companies:posted_label_alt']			= 'Odesláno';
$lang['companies:written_by_label']				= 'Written by'; #translate
$lang['companies:author_unknown']				= 'Unknown'; #translate
$lang['companies:keywords_label']				= 'Keywords'; #translate
$lang['companies:tagged_label']					= 'Tagged'; #translate
$lang['companies:category_label'] 			= 'Kategorie';
$lang['companies:post_label'] 			= 'Odeslat';
$lang['companies:date_label'] 			= 'Datum';
$lang['companies:date_at']				= 'v';
$lang['companies:time_label'] 			= 'Čas';
$lang['companies:status_label'] 			= 'Stav';
$lang['companies:draft_label'] 			= 'Koncept';
$lang['companies:live_label'] 			= 'Publikováno';
$lang['companies:content_label'] 			= 'Obsah';
$lang['companies:options_label'] 			= 'Možnosti';
$lang['companies:intro_label'] 			= 'Úvod';
$lang['companies:no_category_select_label'] 		= '-- Nic --';
$lang['companies:new_category_label'] 		= 'Přidat kategorii';
$lang['companies:subscripe_to_rss_label'] 		= 'Přihlásit se k odběru RSS';
$lang['companies:all_posts_label'] 		= 'Všechny články';
$lang['companies:posts_of_category_suffix'] 	= ' články';
$lang['companies:rss_name_suffix'] 			= ' Novinky';
$lang['companies:rss_category_suffix'] 		= ' Novinky';
$lang['companies:author_name_label'] 		= 'Autor';
$lang['companies:read_more_label'] 			= 'Čtěte dále';
$lang['companies:created_hour']                      = 'Vytvořeno v hodině';
$lang['companies:created_minute']                    = 'Vytvořeno v minutě';
$lang['companies:comments_enabled_label']         = 'Komentáře povoleny';

// titles
$lang['companies:create_title'] 			= 'Přidat článek';
$lang['companies:edit_title'] 			= 'Upravit článek "%s"';
$lang['companies:archive_title'] 			= 'Archiv';
$lang['companies:posts_title'] 			= 'Články';
$lang['companies:rss_posts_title'] 		= 'Novinky pro %s';
$lang['companies:companies_title'] 			= 'Novinky';
$lang['companies:list_title'] 			= 'Seznam článků';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts'] 			= 'Nejsou zde žádné články.';
$lang['companies:subscripe_to_rss_desc'] 		= 'Dostávejte články rovnou pomocí RSS. Můžete použít populární e-mailové klienty nebo zkuste <a href="http://reader.google.cz/">Google Reader</a>.';
$lang['companies:currently_no_posts'] 		= 'Nejsou zde žádné články.';
$lang['companies:post_add_success'] 		= 'Článek "%s" byl přidán.';
$lang['companies:post_add_error'] 		= 'Objevila se chyba.';
$lang['companies:edit_success'] 			= 'Článek "%s" byl aktualizován.';
$lang['companies:edit_error'] 			= 'Objevila se chyba.';
$lang['companies:publish_success'] 			= 'Článek "%s" byl publikován.';
$lang['companies:mass_publish_success'] 		= 'Články "%s" byly publikovány.';
$lang['companies:publish_error'] 			= 'žádné články nebyly publikovány.';
$lang['companies:delete_success'] 			= 'Článek "%s" byl smazán.';
$lang['companies:mass_delete_success'] 		= 'Články "%s" byly smazány.';
$lang['companies:delete_error'] 			= 'Žádné články nebyly smazány.';
$lang['companies:already_exist_error'] 		= 'Článek s touto adresou URL již existuje.';

$lang['companies:twitter_posted']			= 'Publikováno "%s" %s';
$lang['companies:twitter_error'] 			= 'Chyba Twitteru';

// date
$lang['companies:archive_date_format']		= "%B %Y";

?>