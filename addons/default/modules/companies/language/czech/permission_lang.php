<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Publikování příspěvků';
$lang['companies:role_edit_live']	= 'Úpravy publikovaných příspěvků';
$lang['companies:role_delete_live'] 	= 'Mazání publikovaných příspěvků';