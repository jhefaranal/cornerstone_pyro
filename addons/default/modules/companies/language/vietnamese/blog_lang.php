<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Author: Thanh Nguyen
* 		  nguyenhuuthanh@gmail.com
*
* Location: http://techmix.net
*
* Created:  10.26.2011
*
* Description:  Vietnamese language file
*
*/

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']                   = 'Đã gửi';
$lang['companies:posted_label_alt']               = 'Gửi lúc at';
$lang['companies:written_by_label']				= 'Viết bởi';
$lang['companies:author_unknown']				= 'Chưa rõ';
$lang['companies:keywords_label']				= 'Keywords'; #translate
$lang['companies:tagged_label']					= 'Tagged'; #translate
$lang['companies:category_label']                 = 'Danh mục';
$lang['companies:post_label']                     = 'Bài viết';
$lang['companies:date_label']                     = 'Ngày';
$lang['companies:date_at']                        = 'lúc';
$lang['companies:time_label']                     = 'thời gian';
$lang['companies:status_label']                   = 'Trạng thái';
$lang['companies:draft_label']                    = 'Nháp';
$lang['companies:live_label']                     = 'Xuất bản';
$lang['companies:content_label']                  = 'Nội dung';
$lang['companies:options_label']                  = 'Tùy biến';
$lang['companies:intro_label']                    = 'Giới thiệu';
$lang['companies:no_category_select_label']       = '-- Không --';
$lang['companies:new_category_label']             = 'Thêm chuyên mục';
$lang['companies:subscripe_to_rss_label']         = 'Nhận RSS';
$lang['companies:all_posts_label']             = 'Tất cả bài viết';
$lang['companies:posts_of_category_suffix']    = ' bài viết';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Tên tác giả';
$lang['companies:read_more_label']                = 'Đọc thêm&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Giờ tạo';
$lang['companies:created_minute']                 = 'Phút tạo';
$lang['companies:comments_enabled_label']         = 'Cho phép phản hồi';

// titles
$lang['companies:create_title']                   = 'Thêm bài viết';
$lang['companies:edit_title']                     = 'Sửa bài viết "%s"';
$lang['companies:archive_title']                  = 'Lưu trữ';
$lang['companies:posts_title']                 = 'Bài viết';
$lang['companies:rss_posts_title']             = 'Các bài viết cho %s';
$lang['companies:companies_title']                     = 'Companies';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']                    = 'Không có bài viết nào.';
$lang['companies:subscripe_to_rss_desc']          = 'Hãy sử dụng RSS Feed để nhận những bài viết mới nhất. Bạn có thể sử dụng nhiều chương trình khác nhau, hoặc sử dụng <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'Không có bài viết nào.';
$lang['companies:post_add_success']            = 'Đã thêm bài viết "%s".';
$lang['companies:post_add_error']              = 'Có lỗi xảy ra.';
$lang['companies:edit_success']                   = 'Bài viết "%s" đã được cập nhật.';
$lang['companies:edit_error']                     = 'Có lỗi xảy ra.';
$lang['companies:publish_success']                = 'Bài viết "%s" đã được xuất bản.';
$lang['companies:mass_publish_success']           = 'Bài viết "%s" đã được xuất bản.';
$lang['companies:publish_error']                  = 'Không có bài viết nào được xuât bản.';
$lang['companies:delete_success']                 = 'Đã xóa bài viết "%s".';
$lang['companies:mass_delete_success']            = 'Đã xóa bài viết "%s".';
$lang['companies:delete_error']                   = 'Không có bài viết nào được xóa.';
$lang['companies:already_exist_error']            = 'URL của bài viết đã tồn tại.';

$lang['companies:twitter_posted']                 = 'Đã gửi "%s" %s';
$lang['companies:twitter_error']                  = 'Lỗi Twitter';

// date
$lang['companies:archive_date_format']		= "%B %Y";
