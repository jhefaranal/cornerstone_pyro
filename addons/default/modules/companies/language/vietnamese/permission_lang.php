<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Author: Thanh Nguyen
* 		  nguyenhuuthanh@gmail.com
*
* Location: http://techmix.net
*
* Created:  10.26.2011
*
* Description:  Vietnamese language file
*
*/
// Companies Permissions
$lang['companies:role_put_live']		= 'Đưa bài viết lên site';
$lang['companies:role_edit_live']	= 'Sửa bài viết';
$lang['companies:role_delete_live'] 	= 'Xóa bài viết';