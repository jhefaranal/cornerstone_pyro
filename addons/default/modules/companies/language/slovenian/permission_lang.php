<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Objavi članek';
$lang['companies:role_edit_live']	= 'Uredi objavljen članek';
$lang['companies:role_delete_live'] 	= 'Izbriši objavljen članek';

// slovenian premission_lang.php