<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post'; #translate
$lang['companies:posts']                   = 'Posts'; #translate

// labels
$lang['companies:posted_label']                   = 'Objavljeno';
$lang['companies:posted_label_alt']               = 'Objavljeno ob';
$lang['companies:written_by_label']				= 'Objavil';
$lang['companies:author_unknown']				= 'Neznan';
$lang['companies:keywords_label']				= 'Klj. besede';
$lang['companies:tagged_label']					= 'Označen';
$lang['companies:category_label']                 = 'Kategorija';
$lang['companies:post_label']                     = 'Objava';
$lang['companies:date_label']                     = 'Datum';
$lang['companies:date_at']                        = 'ob';
$lang['companies:time_label']                     = 'Čas';
$lang['companies:status_label']                   = 'Stanje';
$lang['companies:draft_label']                    = 'Osnutek';
$lang['companies:live_label']                     = 'Vidno';
$lang['companies:content_label']                  = 'Vsebina';
$lang['companies:options_label']                  = 'Možnosti';
$lang['companies:intro_label']                    = 'Navodila';
$lang['companies:no_category_select_label']       = '-- Brez --';
$lang['companies:new_category_label']             = 'Dodaj kategorijo';
$lang['companies:subscripe_to_rss_label']         = 'Prijava na RSS';
$lang['companies:all_posts_label']             = 'Vsi prispevki';
$lang['companies:posts_of_category_suffix']    = ' prispevki';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Ime avtorja';
$lang['companies:read_more_label']                = 'Preberi vse&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Ustvarjeno ura';
$lang['companies:created_minute']                 = 'Ustvarjeno minuta';
$lang['companies:comments_enabled_label']         = 'Komentaji omogočeni';

// titles
$lang['companies:create_title']                   = 'Dodaj prispevek';
$lang['companies:edit_title']                     = 'Uredi prispevek "%s"';
$lang['companies:archive_title']                  = 'Arhiv';
$lang['companies:posts_title']                 = 'Članki';
$lang['companies:rss_posts_title']             = 'Companies prispevki za %s';
$lang['companies:companies_title']                     = 'Companies';
$lang['companies:list_title']                     = 'Seznam prispevkov';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.'; #translate
$lang['companies:no_posts']                    = 'Trenutno še ni prispevkov.';
$lang['companies:subscripe_to_rss_desc']          = 'Pridite do prispevkov v najkrajšem možnem času tako da se prijavite na RSS podajalca. To lahko storite preko popularnega email programa ali poizkusite  <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'Ta trenutek ni prispevkov';
$lang['companies:post_add_success']            = 'Vap prispevek je bil dodan "%s" ';
$lang['companies:post_add_error']              = 'Prišlo je do napake';
$lang['companies:edit_success']                   = 'Prispevek "%s" je bil oddan.';
$lang['companies:edit_error']                     = 'Prišlo je do napake.';
$lang['companies:publish_success']                = 'Prispevek "%s" je bil objavljen.';
$lang['companies:mass_publish_success']           = 'Prispeveki "%s" so bili objavljeni.';
$lang['companies:publish_error']                  = 'Noben prispevk ni bil objavljen.';
$lang['companies:delete_success']                 = 'Prispevki "%s" so bili izbrisani.';
$lang['companies:mass_delete_success']            = 'Prispevki "%s" so bili izbrisani.';
$lang['companies:delete_error']                   = 'Noben od prispevkov ni bil izbrisan..';
$lang['companies:already_exist_error']            = 'Prispevek s tem URL-jem že obstaja.';

$lang['companies:twitter_posted']                 = 'Objavljeno "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter napaka';

// date
$lang['companies:archive_date_format']		= "%d' %m' %Y";

/* End of file companies_lang.php */