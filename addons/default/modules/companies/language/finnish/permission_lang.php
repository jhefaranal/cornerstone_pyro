<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Artikkelien julkaiseminen';
$lang['companies:role_edit_live']	= 'Julkaistujen artikkeleiden muokkaaminen';
$lang['companies:role_delete_live'] 	= 'Julkaistujen artikkeleiden poistaminen';
