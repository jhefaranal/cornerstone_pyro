<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
 * Finnish translation.
 *
 * @author Mikael Kundert
 */

$lang['companies:post']	= 'Artikkeli';
$lang['companies:posts']	= 'Artikkelit';

// labels
$lang['companies:posted_label']				= 'Lähetetty';
$lang['companies:posted_label_alt']			= 'Lähetettiin';
$lang['companies:written_by_label']			= 'Kirjoittanut';
$lang['companies:author_unknown']			= 'Tuntematon';
$lang['companies:keywords_label']			= 'Avainsanat';
$lang['companies:tagged_label']				= 'Merkitty';
$lang['companies:category_label']			= 'Kategoria';
$lang['companies:post_label']				= 'Artikkeli';
$lang['companies:date_label']				= 'Päivä';
$lang['companies:date_at']					= 'at'; #translate (in finnish we use adessives by using suffixes!, see http://www.cs.tut.fi/~jkorpela/finnish-cases.html)
$lang['companies:time_label']				= 'Aika';
$lang['companies:status_label']				= 'Tila';
$lang['companies:draft_label']				= 'Luonnos';
$lang['companies:live_label']				= 'Julkinen';
$lang['companies:content_label']				= 'Sisältö';
$lang['companies:options_label']				= 'Valinnat';
$lang['companies:intro_label']				= 'Alkuteksti';
$lang['companies:no_category_select_label']	= '-- Ei mikään --';
$lang['companies:new_category_label']		= 'Luo kategoria';
$lang['companies:subscripe_to_rss_label']	= 'Tilaa RSS';
$lang['companies:all_posts_label']			= 'Kaikki artikkelit';
$lang['companies:posts_of_category_suffix']	= ' artikkelia';
$lang['companies:rss_name_suffix']			= ' Uutiset';
$lang['companies:rss_category_suffix']		= ' Uutiset';
$lang['companies:author_name_label']			= 'Tekijän nimi';
$lang['companies:read_more_label']			= 'Lue lisää&nbsp;&raquo;';
$lang['companies:created_hour']				= 'Luotiin tunnissa';
$lang['companies:created_minute']			= 'Luotiin minuutissa';
$lang['companies:comments_enabled_label']	= 'Kommentit päällä';

// titles
$lang['companies:disabled_after']	= 'Kommentointi on otettu pois käytöstä %s jälkeen.';
$lang['companies:create_title']		= 'Luo artikkeli';
$lang['companies:edit_title']		= 'Muokkaa artikkelia "%s"';
$lang['companies:archive_title']		= 'Arkisto';
$lang['companies:posts_title']		= 'Artikkelit';
$lang['companies:rss_posts_title']	= 'Uutisartikkeleita %s';
$lang['companies:companies_title']		= 'Uutiset';
$lang['companies:list_title']		= 'Listaa artikkelit';

// messages
$lang['companies:no_posts']				= 'Artikkeleita ei ole.';
$lang['companies:subscripe_to_rss_desc']	= 'Lue artikkelit tilaamalla RSS syöte. Suosituimmat sähköposti ohjelmat tukevat tätä. Voit myös vaihtoehtoisesti kokeilla <a href="http://reader.google.co.uk/">Google Readeria</a>.';
$lang['companies:currently_no_posts']	= 'Ei artikkeleita tällä hetkellä.';
$lang['companies:post_add_success']		= 'Artikkeli "%s" lisättiin.';
$lang['companies:post_add_error']		= 'Tapahtui virhe.';
$lang['companies:edit_success']			= 'Artikkeli "%s" päivitettiin.';
$lang['companies:edit_error']			= 'Tapahtui virhe.';
$lang['companies:publish_success']		= 'Artikkeli "%s" julkaistiin.';
$lang['companies:mass_publish_success']	= 'Artikkelit "%s" julkaistiin.';
$lang['companies:publish_error']			= 'Yhtään artikkelia ei julkaistu.';
$lang['companies:delete_success']		= 'Artikkeli "%s" poistettiin.';
$lang['companies:mass_delete_success']	= 'Artikkelit "%s" poistettiin.';
$lang['companies:delete_error']			= 'Yhtään artikkelia ei poistettu.';
$lang['companies:already_exist_error']	= 'Artikkelin URL osoite on jo käytössä.';

$lang['companies:twitter_posted']	= 'Lähetetty "%s" %s';
$lang['companies:twitter_error']		= 'Twitter Virhe';

// date
$lang['companies:archive_date_format'] = "%B, %Y";

?>