<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= 'Put articles live';
$lang['companies:role_edit_live']	= 'Edit live articles';
$lang['companies:role_delete_live'] 	= 'Delete live articles';