<?php defined('BASEPATH') OR exit('No direct script access allowed');

$lang['companies:post']                 = 'Post';
$lang['companies:posts']                   = 'Posts';

// labels
$lang['companies:posted_label']                   = 'Posted';
$lang['companies:posted_label_alt']               = 'Posted at';
$lang['companies:written_by_label']				= 'Written by';
$lang['companies:author_unknown']				= 'Unknown';
$lang['companies:keywords_label']				= 'Keywords';
$lang['companies:tagged_label']					= 'Tagged';
$lang['companies:category_label']                 = 'Category';
$lang['companies:post_label']                     = 'Post';
$lang['companies:date_label']                     = 'Date';
$lang['companies:date_at']                        = 'at';
$lang['companies:time_label']                     = 'Time';
$lang['companies:status_label']                   = 'Status';
$lang['companies:draft_label']                    = 'Draft';
$lang['companies:live_label']                     = 'Live';
$lang['companies:content_label']                  = 'Content';
$lang['companies:options_label']                  = 'Options';
$lang['companies:intro_label']                    = 'Introduction';
$lang['companies:no_category_select_label']       = '-- None --';
$lang['companies:new_category_label']             = 'Add a category';
$lang['companies:subscripe_to_rss_label']         = 'Subscribe to RSS';
$lang['companies:all_posts_label']                = 'All posts';
$lang['companies:posts_of_category_suffix']       = ' posts';
$lang['companies:rss_name_suffix']                = ' Companies';
$lang['companies:rss_category_suffix']            = ' Companies';
$lang['companies:author_name_label']              = 'Author name';
$lang['companies:read_more_label']                = 'Read More&nbsp;&raquo;';
$lang['companies:created_hour']                   = 'Created on Hour';
$lang['companies:created_minute']                 = 'Created on Minute';
$lang['companies:comments_enabled_label']         = 'Comments Enabled';

// titles
$lang['companies:create_title']                   = 'Add Company';
$lang['companies:edit_title']                     = 'Edit Company "%s"';
$lang['companies:archive_title']                 = 'Archive';
$lang['companies:posts_title']					= 'Posts';
$lang['companies:rss_posts_title']				= 'Company posts for %s';
$lang['companies:companies_title']					= 'Companies';
$lang['companies:list_title']					= 'List Posts';

// messages
$lang['companies:disabled_after'] 				= 'Posting comments after %s has been disabled.';
$lang['companies:no_posts']                      = 'There are no posts.';
$lang['companies:subscripe_to_rss_desc']          = 'Get posts straight away by subscribing to our RSS feed. You can do this via most popular e-mail clients, or try <a href="http://reader.google.co.uk/">Google Reader</a>.';
$lang['companies:currently_no_posts']          = 'There are no posts at the moment.';
$lang['companies:post_add_success']            = 'The post "%s" was added.';
$lang['companies:post_add_error']              = 'An error occured.';
$lang['companies:edit_success']                   = 'The post "%s" was updated.';
$lang['companies:edit_error']                     = 'An error occurred.';
$lang['companies:publish_success']                = 'The post "%s" has been published.';
$lang['companies:mass_publish_success']           = 'The posts "%s" have been published.';
$lang['companies:publish_error']                  = 'No posts were published.';
$lang['companies:delete_success']                 = 'The post "%s" has been deleted.';
$lang['companies:mass_delete_success']            = 'The posts "%s" have been deleted.';
$lang['companies:delete_error']                   = 'No posts were deleted.';
$lang['companies:already_exist_error']            = 'A post with this URL already exists.';

$lang['companies:twitter_posted']                 = 'Posted "%s" %s';
$lang['companies:twitter_error']                  = 'Twitter Error';

// date
$lang['companies:archive_date_format']		= "%B %Y";
