<?php

$lang['companies:post']                 		= '文章';
$lang['companies:posts']                   	= '文章';

// labels
$lang['companies:posted_label'] 				= '已發佈';
$lang['companies:posted_label_alt']			= '發表於';
$lang['companies:written_by_label']			= '撰文者';
$lang['companies:author_unknown']			= '未知';
$lang['companies:keywords_label']			= '關鍵字';
$lang['companies:tagged_label']				= '標籤';
$lang['companies:category_label'] 			= '分類';
$lang['companies:post_label'] 				= '發表';
$lang['companies:date_label'] 				= '日期';
$lang['companies:date_at']					= '於';
$lang['companies:time_label'] 				= '時間';
$lang['companies:status_label'] 				= '狀態';
$lang['companies:draft_label'] 				= '草稿';
$lang['companies:live_label'] 				= '上線';
$lang['companies:content_label'] 			= '內容';
$lang['companies:options_label'] 			= '選項';
$lang['companies:intro_label'] 				= '簡介';
$lang['companies:no_category_select_label'] 	= '-- 無 --';
$lang['companies:new_category_label'] 		= '新增分類';
$lang['companies:subscripe_to_rss_label'] 	= '訂閱 RSS';
$lang['companies:all_posts_label'] 			= '所有文章';
$lang['companies:posts_of_category_suffix'] 	= ' 文章';
$lang['companies:rss_name_suffix'] 			= ' 新聞';
$lang['companies:rss_category_suffix'] 		= ' 新聞';
$lang['companies:author_name_label'] 		= '作者名稱';
$lang['companies:read_more_label'] 			= '閱讀更多&nbsp;&raquo;';
$lang['companies:created_hour']              = '時間 (時)';
$lang['companies:created_minute']            = '時間 (分)';
$lang['companies:comments_enabled_label']    = '允許回應';

// titles
$lang['companies:create_title'] 				= '新增文章';
$lang['companies:edit_title'] 				= '編輯文章 "%s"';
$lang['companies:archive_title'] 			= '檔案櫃';
$lang['companies:posts_title'] 				= '文章';
$lang['companies:rss_posts_title'] 			= '%s 的最新文章';
$lang['companies:companies_title'] 				= '新聞';
$lang['companies:list_title'] 				= '文章列表';

// messages
$lang['companies:disabled_after'] 			= '在 %s 後面 發表評論的功能 已經被關閉。';
$lang['companies:no_posts'] 					= '沒有文章';
$lang['companies:subscripe_to_rss_desc'] 	= '訂閱我們的 RSS 摘要可立即獲得最新的文章，您可以使用慣用的收件軟體，或試試看 <a href="http://reader.google.com.tw">Google 閱讀器</a>。';
$lang['companies:currently_no_posts'] 		= '目前沒有文章';
$lang['companies:post_add_success'] 			= '文章 "%s" 已經新增';
$lang['companies:post_add_error'] 			= '發生了錯誤';
$lang['companies:edit_success'] 				= '這文章 "%s" 更新了。';
$lang['companies:edit_error'] 				= '發生了錯誤';
$lang['companies:publish_success'] 			= '此文章 "%s" 已經被發佈。';
$lang['companies:mass_publish_success'] 		= '這些文章 "%s" 已經被發佈。';
$lang['companies:publish_error'] 			= '沒有文章被發佈。';
$lang['companies:delete_success'] 			= '此文章 "%s" 已經被刪除。';
$lang['companies:mass_delete_success'] 		= '這些文章 "%s" 已經被刪除。';
$lang['companies:delete_error'] 				= '沒有文章被刪除。';
$lang['companies:already_exist_error'] 		= '一則相同網址的文章已經存在。';

$lang['companies:twitter_posted']			= '發佈 "%s" %s';
$lang['companies:twitter_error'] 			= 'Twitter 錯誤';

// date
$lang['companies:archive_date_format']		= "%B %Y"; #translate format - see php strftime documentation
