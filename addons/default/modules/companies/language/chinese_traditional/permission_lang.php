<?php defined('BASEPATH') OR exit('No direct script access allowed');

// Companies Permissions
$lang['companies:role_put_live']		= '將文章上線';
$lang['companies:role_edit_live']	= '編輯上線文章';
$lang['companies:role_delete_live'] 	= '刪除上線文章';