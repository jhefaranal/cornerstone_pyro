<?php defined('BASEPATH') OR exit('No direct script access allowed');
/**
* Sample Events Class
*
* @package 		PyroCMS
* @subpackage 	Social Module
* @category 	companies
* @author 		PyroCMS Dev Team
*/
class Events_Companies
{
    protected $ci;
    
    public function __construct()
    {
        $this->ci =& get_instance();

        // Load the search index model
        $this->ci->load->model('search/search_index_m');

		// Post a blog to twitter and whatnot
        Events::register('companies_published', array($this, 'index_companies'));
        Events::register('companies_updated', array($this, 'index_companies'));
        Events::register('companies_deleted', array($this, 'drop_companies'));

    }
    
    public function index_companies($id)
    {
    	$this->ci->load->model('companies/companies_m');

    	$post = $this->ci->companies_m->get($id);

    	// Only index live articles
    	if ($post->status === 'live')
    	{
    		$this->ci->search_index_m->index(
    			'companies', 
    			'companies:post', 
    			'companies:posts', 
    			$id,
    			//'companies/'.date('Y/m/', $companies->created_on).$post->slug,
    			'companies',
    			$post->title,
    			$post->body, 
    			array(
    				'cp_edit_uri' 	=> 'admin/companies/edit/'.$id,
    				'cp_delete_uri' => 'admin/companies/delete/'.$id,
    				'keywords' 		=> $post->keywords,
    			)
    		);
    	}
    	// Remove draft articles
    	else
    	{
    		$this->ci->search_index_m->drop_index('companies', 'companies:post', $id);
    	}
	}

    public function drop_companies($ids)
    {
    	foreach ($ids as $id)
    	{
			$this->ci->search_index_m->drop_index('companies', 'companies:post', $id);
		}
	}
  
}

/* End of file companies.php */