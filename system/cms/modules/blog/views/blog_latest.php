<?php if (!empty($blogs)): ?>
	<?php foreach ($blogs as $key => $blog): ?>
		<div class="row text-left">
			<div class="col-md-5">
				<img src="{{ url:site }}files/large/<?php echo $blog['thumbnail'] ?>" onerror="this.src = '{{ url:site }}addons/default/themes/cornerstone_pyro_2/img/placeholder.png';" style="width:100%">
			</div>
			<div class="col-md-7">
				<a href="{{ url:site }}blog/<?php echo date('Y/m',strtotime($blog['created'])).'/'.$blog['slug']; ?>" class="text-danger"><b><?php echo $blog['title'] ?></b></a>
				<br/><small><?php echo substr($blog['intro'], 0, 120).'...' ?></small>
			</div>
		</div>
	<?php endforeach ?>
<?php else: ?>
	<div class="row text-left">
		<div class="col-md-12">
			<div class="alert alert-warning" style="margin-bottom:0px !important;">
				<h6 class="text-center"> There are no blogs right now. </h6>
			</div>
		</div>
	</div>
<?php endif ?>