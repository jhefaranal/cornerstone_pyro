<style type="text/css">
	#quick-links hr { width:100%; background:#c22127; height:1.5px; }
	#quick-links h5 { color:#c22127; }
	.post .title { margin-top:0px !important; }
	.post .author { margin-top:10px; }
</style>

<script type="text/javascript" src="//platform-api.sharethis.com/js/sharethis.js#property=59a5113039509a0011b10d87&product=custom-share-buttons"></script>

{{ post }}

<div class="post">
	<div id="quick-links">
		<div class="row">
			<div class="col-md-4" style="height:80px;">
				{{ if category }}
					<h5><a href="{{ url:site }}blog/category/{{ category:slug }}">{{ category:title }}</a></h5>
				{{ endif }}
				<hr>
			</div>
			<div class="col-md-offset-4 col-md-4">
				<h5 class="pull-right">
					<a href="{{ url:site }}blog">
						<i class="fa fa-arrow-right"></i>
						See All Tips
					</a>	
				</h5>
			</div>
		</div>
	</div>

	<h1 class="title">{{ title }}</h1>
	
	<div class="meta">
		<div class="author">
			<object data="{{ url:site }}files/large/44d581b0dc3a13da04f926798adc231c.png" type="image/png">
				<img class="author_img" src="<?php echo site_url('files/thumb/{{ author_id }}/50/50/fit'); ?>">
			</object>
			<div>
				<span>by <strong>{{ created_by:display_name }}</strong></span>
				<span class="date">{{ helper:date timestamp=created_on }}</span>
			</div>
		</div>

		{{ if category }}
		<div class="category">
			<div class="hide">
				<span class="post_category"><a href="{{ url:site }}blog/category/{{ category:slug }}">{{ category:title }}</a></span>
				<span><a href="{{ url:site }}blog">See all Blog</a></span>
			</div>

			<span class="count"></span>
			<div data-network="twitter" class="st-custom-button"><i class="fa fa-twitter"></i></div>
			<div data-network="facebook" class="st-custom-button"><i class="fa fa-facebook" style="padding-left:3px;"></i></div> 
			<div data-network="linkedin" class="st-custom-button"><i class="fa fa-linkedin"></i></div> 
		</div>
		{{ endif }}

		{{ if keywords }}
		<div class="keywords">
			{{ keywords }}
				<span><a href="{{ url:site }}blog/tagged/{{ keyword }}">{{ keyword }}</a></span>
			{{ /keywords }}
		</div>
		{{ endif }}
	</div>

	<img src="{{ thumbnail:image }}" width="100%">

	<div class="body">
		{{ body }}
	</div>
</div>

{{ /post }}

<?php if (Settings::get('enable_comments')): ?>

<div id="comments">

	<div id="existing-comments">
		<h4><?php echo lang('comments:title') ?></h4>
		<?php echo $this->comments->display() ?>
	</div>

	<?php if ($form_display): ?>
		<?php echo $this->comments->form() ?>
	<?php else: ?>
	<?php echo sprintf(lang('blog:disabled_after'), strtolower(lang('global:duration:'.str_replace(' ', '-', $post[0]['comments_enabled'])))) ?>
	<?php endif ?>
</div>

<?php endif ?>

<style type="text/css">
	.post .title {
		font-weight: 500 !important;
	}
	.author {
		margin-bottom: 10px;
		display: inline-block;
		float: left;
	}
	.author .author_img {
		width: 50px;
		height: 50px;
		border-radius: 50%;
		border: 3px solid #ddd;
		float: left;
		margin-right: 25px;
	}
	.author object {
		width: 50px;
		height: 50px;
		border-radius: 50%;
		border: 3px solid #c22127;
		float: left;
		margin-right: 25px;
	}
	.author div {
		display: inline-block;
	}
	.author span {
		display: block;
	}
	.author span.date {
		font-style: normal;
	}
	.category {
		float: right;
		display: inline-block;
	}
	.category span {
		display: block;
		text-align: right;
		padding: 0 20px;
		color: #1978b4;
	}
	.category a {
		color: #515d6f;
	}
	.category a:hover {
		color: #c22127;
	}
	.category .post_category a {
		font-weight: 500;
	}
	.post .body {
		font-size: 18px;
		margin-top: 30px;
		margin-bottom: 30px;
	}

	.st-custom-button[data-network] {
	   	display: inline-block;
	    cursor: pointer;
	    color: #3f3f3f;
	    margin-right: 10px;
	    border: 1.5px solid #3f3f3f;
	    width: 50px;
	    height: 50px;
	    border-radius: 50%;
	    padding:12px 15px;
        font-size: 14pt;
	}

	.st-custom-button[data-network]:hover, .st-custom-button[data-network]:focus {
      	background-color: #3f3f3f;
      	color:#fff;
      	transition: background-color 0.5s ease;
	}
</style>
