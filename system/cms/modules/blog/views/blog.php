<style type="text/css">
	#blog-posts hr { width:100%; background:#c22127; height:1.5px; }
	#blog-posts h5 { color:#c22127; }
	#blog-posts img { width:100%; height:220px; border-radius:5px; }
</style>

<div class="row" id="blog-posts">
	<?php if (!empty($blogs)): ?>
		<?php foreach ($blogs as $key => $blog): ?>
			<div class="col-md-3 wow fadeInUp" data-wow-duration="1.5s">
				<img src="{{ url:site }}files/large/<?php echo $blog['thumbnail'] ?>" onerror="this.src = '{{ url:site }}addons/default/themes/cornerstone_pyro_2/img/placeholder.png';" />

				<div class="text-left">
					<a href="{{ url:site }}blog/category/<?php echo $blog['category_slug'] ?>"><h5><?php echo $blog['category_title'] ?></h5></a>
					<hr/>
					<a href="{{ url:site }}blog/<?php echo date('Y/m',strtotime($blog['created'])).'/'.$blog['slug']; ?>"><h4><?php echo $blog['title'] ?></h4></a>
					
					<br/><small><?php echo date('F j, Y',strtotime($blog['updated'])) ?></small>
					<p class="text-justify"><?php echo strlen($blog['intro']) > 250? (substr($blog['intro'], 0, 250).'...') : $blog['intro'] ?></p>
				</div>
			</div>
		<?php endforeach ?>
	<?php else: ?>
		<div class="col-md-12">
			<div class="alert alert-warning" role="alert">
				<h6 class="text-center"> There are no blogs right now. </h6>
			</div>
		</div>
	<?php endif ?>
</div>