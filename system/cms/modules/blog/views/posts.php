<h2 class="text-center">Keep Learning. Be Updated</h2>
<p style="text-align:center">We provide valuable resources on a regular basis to help you succeed in your real estate business.</p>

{{ if posts }}
	
	{{ posts }}

		<div class="row blog-posts">
			{{ if (count % 2) == 0 }}
				<div class="col-md-4" align="center">
					<img src="{{ thumbnail:image }}" onerror="this.src = '{{ url:site }}addons/default/themes/cornerstone_pyro_2/img/placeholder.png';" style="max-width:200px; max-height:200px">
				</div>
				
				<div class="col-md-8">
					{{ if category }}
						<h5><a href="{{ url:site }}blog/category/{{ category:slug }}">{{ category:title }}</a></h5>
					{{ endif }}
					<hr/>

					<h4 class="title"><a href="{{ url }}">{{ title }}</a></h4>
					<div class="meta">
						<div class="date">
							{{ helper:date timestamp=created_on }}
						</div>

						{{ if keywords }}
						<div class="keywords">
							{{ keywords }}
								<span><a href="{{ url:site }}blog/tagged/{{ keyword }}">{{ keyword }}</a></span>
							{{ /keywords }}
						</div>
						{{ endif }}
					</div>

					<div class="preview">
						{{ preview }}
					</div>

					<p><a href="{{ url }}">{{ helper:lang line="blog:read_more_label" }}</a></p>
				</div>
			{{ else }}
				<div class="col-md-8">
					{{ if category }}
						<h5><a href="{{ url:site }}blog/category/{{ category:slug }}">{{ category:title }}</a></h5>
					{{ endif }}
					<hr/>

					<h4 class="title"><a href="{{ url }}">{{ title }}</a></h4>
					<div class="meta">
						<div class="date">
							{{ helper:date timestamp=created_on }}
						</div>

						{{ if keywords }}
						<div class="keywords">
							{{ keywords }}
								<span><a href="{{ url:site }}blog/tagged/{{ keyword }}">{{ keyword }}</a></span>
							{{ /keywords }}
						</div>
						{{ endif }}
					</div>

					<div class="preview">
						{{ preview }}
					</div>

					<p><a href="{{ url }}">{{ helper:lang line="blog:read_more_label" }}</a></p>
				</div>

				<div class="col-md-4" align="center">
					<img src="{{ thumbnail:image }}" onerror="this.src = '{{ url:site }}addons/default/themes/cornerstone_pyro_2/img/placeholder.png';" style="max-width:200px; max-height:200px">
				</div>
			{{ endif }}
		</div>

	{{ /posts }}

	{{ pagination }}

{{ else }}
	
	{{ helper:lang line="blog:currently_no_posts" }}

{{ endif }}

<script type="text/javascript">
	$(function() {
		// $('.post .title').matchHeight();
		// $('.post').matchHeight();
	});
</script>

<style type="text/css">
	.title a {
		color: #515d6f;
	}
	.title a:hover {
		color: #c22127;
	}
	.date {
		font-style: italic;
		display: inline-block;
	}
	.category {
		float: right;
		display: inline-block;
		font-weight: 500;
	}
	.category span {
		display: block;
		text-align: right;
		padding: 0 20px;
		border-right: 2px solid #c22127;
		color: #1978b4;
	}
	.category a {
		color: #515d6f;
	}
	.category a:hover {
		color: #c22127;
	}	
	.post .preview {
		font-size: 14px;
		margin-top: 15px;
	}
</style>